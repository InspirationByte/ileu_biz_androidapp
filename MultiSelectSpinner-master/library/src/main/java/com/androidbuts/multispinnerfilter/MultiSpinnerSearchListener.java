package com.androidbuts.multispinnerfilter;

import java.util.List;

/**
 * Created by macair on 22.09.17.
 */

public interface MultiSpinnerSearchListener {
    void onItemsSelected(List<KeyPairBoolData> items);
}
