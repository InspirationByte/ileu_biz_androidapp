package biz.ileu.ileuapp.ui.process.processComments.processCommentsUsersAdd;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import biz.ileu.ileuapi.models.process.ParticipantViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.process.UsersViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.process.processComments.processCommentsUsers.ProcessCommentsUsersActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import biz.ileu.ileuapp.utils.ui.ParticipantsCheckClick;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by macair on 09.08.17.
 */

public class ProcessCommentsUsersAddActivity extends BaseActivity implements ProcessCommentsUsersAddContract.ProcessCommentsUsersAddView, ParticipantsCheckClick.OnParticipantPositionCheckedUncheckedListener {

    @BindView(R.id.rlProcessUsers)
    RecyclerView rlProcessUsers;
    private int countSelected = 0;

    private Process currentProcess;
    private CompanyWorkInfoListViewModel currentCompany = null;

    private List<UsersViewModel> usersList;
    private Set<ParticipantViewModel> participantViewModelSet;

    private ProcessCommentsUserAdapter adapter;
    private ProcessCommentsUsersAddPresenter presenter;

    ProcessCommentsUsersActivity curr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_comments_users);
        ButterKnife.bind(this);
        bindViews();
        setData();
    }

    @Override
    public void bindViews() {
        initBaseToolbar(true);
        getSupportActionBar().setTitle(R.string.process_detail_participants);
        usersList = new ArrayList<>();

        adapter = new ProcessCommentsUserAdapter(usersList, this);
        presenter = new ProcessCommentsUsersAddPresenter(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rlProcessUsers.setLayoutManager(layoutManager);
        rlProcessUsers.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setData() {
        currentCompany = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_COMPANY));
        currentProcess = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_PROCESS));

        presenter.getUsers(currentCompany.getId());


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.participants_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.actionAddParticipants) {
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnPositionChecked(View v, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Сообщение");

        // Set up the input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT );
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String m_Text = input.getText().toString();
                presenter.addUser(usersList.get(position).getId(), currentProcess.getId(), currentCompany.getId(), m_Text);
            }
        });
        builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    @Override
    public void OnPositionUnchecked(View v, int position) {

    }


    @Override
    public void usersLoaded(List<UsersViewModel> users) {
        List<Integer> participantIds = new ArrayList<Integer>();
        participantIds.add(this.currentProcess.getCreator().getId());
        participantIds.add(this.currentProcess.getExecutor().getId());
        participantIds.add(this.currentProcess.getResponsible().getId());
        for (ParticipantViewModel participant: currentProcess.getParticipants()) {
            participantIds.add(participant.getId());
        }
        for (UsersViewModel user : users) {
            if (!participantIds.contains(user.getId())) {
                usersList.add(user);
            }
        }
        adapter.notifyDataSetChanged();

    }
    @Override
    public void finish() {
        Intent data = new Intent();
        setResult(RESULT_OK, data);

        super.finish();
    }
    @Override
    public void usersFail(String e) {
        Toast.makeText(this, e, Toast.LENGTH_SHORT).show();

    }
    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs
    }
    @Override
    public void participantAdded(ParticipantViewModel users) {
        int index = 0;
        for (UsersViewModel u : usersList) {
            if (u.getId() == users.getId()) {
                usersList.remove(index);
                break;
            }
            index++;
        }
        adapter.notifyDataSetChanged();
    }
}
