package biz.ileu.ileuapp.ui.company.create;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Calendar;

import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class CompanyCreateActivity extends BaseActivity implements CompanyCreateContract.CompanyCreateView {

    @BindView(R.id.etCompanyName)
    EditText etCompanyName;
    @BindView(R.id.etCompanyBIN)
    EditText etCompanyBIN;
    @BindView(R.id.etCompanyEmail)
    EditText etCompanyEmail;
    @BindView(R.id.etCompanyWebsite)
    EditText etCompanyWebsite;
    @BindView(R.id.etCompanyDesc)
    EditText etCompanyDesc;
    @BindView(R.id.etCompanyDateFound)
    EditText etCompanyDateFound;
    @BindView(R.id.etCompanyType)
    Spinner spCompanyType;
    @BindView(R.id.cbAddBasicProccesses)
    CheckBox cbAddBasicProccesses;
    @BindView(R.id.cbAddDatabaseProccesses)
    CheckBox cbAddDatabaseProccesses;
    @BindView(R.id.btnCompanyCreate)
    Button btnProfileSettingsChangePassword;
    private String current = "";
    private String ddmmyyyy = "DDMMYYYY";
    private Calendar cal = Calendar.getInstance();


    private CompanyCreatePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_add);
        ButterKnife.bind(this);
        bindViews();
        setData();
    }

    @Override
    public void bindViews() {
        initBaseToolbar(true);
        getSupportActionBar().setTitle(R.string.company_create);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void setData() {
        presenter = new CompanyCreatePresenter(this);

        ArrayAdapter<String> adapter;
        String[] list = getResources().getStringArray(R.array.company_type);

        adapter = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.compony_type_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCompanyType.setAdapter(adapter);
    }
    @OnTextChanged(R.id.etCompanyDateFound)
    public void dateFoundChange(CharSequence s, int start, int before, int count) {
        if (!s.toString().equals(current)) {
            String clean = s.toString().replaceAll("[^\\d.]", "");
            String cleanC = current.replaceAll("[^\\d.]", "");

            int cl = clean.length();
            int sel = cl;
            for (int i = 2; i <= cl && i < 6; i += 2) {
                sel++;
            }
            //Fix for pressing delete next to a forward slash
            if (clean.equals(cleanC)) sel--;

            if (clean.length() < 8){
                clean = clean + ddmmyyyy.substring(clean.length());
            }else{
                //This part makes sure that when we finish entering numbers
                //the date is correct, fixing it otherwise
                int day  = Integer.parseInt(clean.substring(0,2));
                int mon  = Integer.parseInt(clean.substring(2,4));
                int year = Integer.parseInt(clean.substring(4,8));

                if(mon > 12) mon = 12;
                cal.set(Calendar.MONTH, mon-1);
                year = (year<1900)?1900:(year>2100)?2100:year;
                cal.set(Calendar.YEAR, year);
                // ^ first set year for the line below to work correctly
                //with leap years - otherwise, date e.g. 29/02/2012
                //would be automatically corrected to 28/02/2012

                day = (day > cal.getActualMaximum(Calendar.DATE))? cal.getActualMaximum(Calendar.DATE):day;
                clean = String.format("%02d%02d%02d",day, mon, year);
            }

            clean = String.format("%s/%s/%s", clean.substring(0, 2),
                    clean.substring(2, 4),
                    clean.substring(4, 8));

            sel = sel < 0 ? 0 : sel;
            current = clean;
            etCompanyDateFound.setText(current);
            etCompanyDateFound.setSelection(sel < current.length() ? sel : current.length());
        }
    }
    @OnClick(R.id.btnCompanyCreate)
    public void onCompanyCreate() {

        String companyName = etCompanyName.getText().toString();
        String companyBIN = etCompanyBIN.getText().toString();
        String companyWebsite = etCompanyWebsite.getText().toString();
        String companyEmail = etCompanyEmail.getText().toString();
        String companyDesc = etCompanyDesc.getText().toString();
        String dateFound = etCompanyDateFound.getText().toString();
        long companyType = spCompanyType.getSelectedItemId();
        boolean isAddBasicProccess = cbAddBasicProccesses.isChecked();
        boolean isAddDataProccess = cbAddDatabaseProccesses.isChecked();

        if (companyName.isEmpty()) {
            Toast.makeText(this, R.string.message_company_name_is_empty, Toast.LENGTH_SHORT).show();
            etCompanyName.requestFocus();
            return;
        }

        if (spCompanyType.getSelectedItemPosition() == 0) {
            Toast.makeText(this, R.string.message_company_type_is_empty, Toast.LENGTH_SHORT).show();
            spCompanyType.requestFocus();
            return;
        }




        presenter.createCompany(companyName, companyBIN, companyWebsite, dateFound, companyEmail, companyDesc, companyType, isAddBasicProccess, isAddDataProccess);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
           finish();
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void createdCompany() {
        Toast.makeText(this, R.string.success_created, Toast.LENGTH_SHORT).show();
        this.finish();
    }

    @Override
    public void failCreatingCompany(String e) {
        Toast.makeText(this, e, Toast.LENGTH_SHORT).show();
    }
}
