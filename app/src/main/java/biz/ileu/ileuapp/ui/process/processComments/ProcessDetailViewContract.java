package biz.ileu.ileuapp.ui.process.processComments;

import okhttp3.ResponseBody;

/**
 * Created by macair on 09.08.17.
 */

public interface ProcessDetailViewContract {
    public interface ProcessDetailView {
        public void failedResult(String e);
        public void successChanged(ResponseBody responseBody);
    }
}
