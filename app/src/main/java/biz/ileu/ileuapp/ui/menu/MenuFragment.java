package biz.ileu.ileuapp.ui.menu;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.IleuApp;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.chatsList.ChatsListFragment;
import biz.ileu.ileuapp.ui.newsList.NewsListFragment;
import biz.ileu.ileuapp.ui.notificationsList.NotificationsListFragment;
import biz.ileu.ileuapp.ui.process.ProcessContainerFragment;
import biz.ileu.ileuapp.ui.profile.ProfileFragment;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.cicerone.Screen;
import biz.ileu.ileuapp.utils.events.CompanyCountersChangedEvent;
import biz.ileu.ileuapp.utils.ui.BaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.commands.Back;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends BaseFragment implements AHBottomNavigation.OnTabSelectedListener {


    @BindView(R.id.menuContainer)
    FrameLayout menuContainer;
    @BindView(R.id.bottomNavigation)
    AHBottomNavigation bottomNavigation;
    Unbinder unbinder;

    private CompanyWorkInfoListViewModel currentCompany = null;

    private Fragment processContainerFragment, notificationsListFragment, chatsListFragment,
                     newsListFragment, profileFragment;

    private Navigator navigator = new Navigator() {
        @Override
        public void applyCommand(Command command) {
            if (command instanceof Back) {
                getActivity().finish();
            }
            else if (command instanceof Replace) {
                switch (((Replace) command).getScreenKey()) {
                    case Screen.SCREEN_PROCESSES_CONTAINER: {
                        getChildFragmentManager()
                                .beginTransaction()
                                .detach(profileFragment)
                                .detach(newsListFragment)
                                .detach(chatsListFragment)
                                .detach(notificationsListFragment)
                                .attach(processContainerFragment)
                                .commit();
                        break;
                    }
                    case Screen.SCREEN_NOTIFICATIONS_LIST: {
                        getChildFragmentManager()
                                .beginTransaction()
                                .detach(profileFragment)
                                .detach(newsListFragment)
                                .detach(chatsListFragment)
                                .detach(processContainerFragment)
                                .attach(notificationsListFragment)
                                .commit();
                        break;
                    }
                    case Screen.SCREEN_CHATS_LIST: {
                        getChildFragmentManager()
                                .beginTransaction()
                                .detach(profileFragment)
                                .detach(newsListFragment)
                                .detach(processContainerFragment)
                                .detach(notificationsListFragment)
                                .attach(chatsListFragment)
                                .commit();
                        break;
                    }
                    case Screen.SCREEN_NEWS_LIST: {
                        getChildFragmentManager()
                                .beginTransaction()
                                .detach(profileFragment)
                                .detach(processContainerFragment)
                                .detach(chatsListFragment)
                                .detach(notificationsListFragment)
                                .attach(newsListFragment)
                                .commit();
                        break;
                    }
                    case Screen.SCREEN_PROFILE: {
                        getChildFragmentManager()
                                .beginTransaction()
                                .detach(processContainerFragment)
                                .detach(newsListFragment)
                                .detach(chatsListFragment)
                                .detach(notificationsListFragment)
                                .attach(profileFragment)
                                .commit();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        }
    };

    public MenuFragment() {
        // Required empty public constructor
    }

    public static MenuFragment newInstance() {
        return new MenuFragment();
    }

    public static MenuFragment newInstance(Bundle bundle) {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
        args.putAll(bundle);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        unbinder = ButterKnife.bind(this, view);

        Bundle args = getArguments();

        if (args != null && args.containsKey(Constants.ENTITY_COMPANY))
            currentCompany = Parcels.unwrap(args.getParcelable(Constants.ENTITY_COMPANY));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews();
        initFragments();

        if (currentCompany != null) {
            CompanyCountersChangedEvent event = new CompanyCountersChangedEvent();
            event.companyId = currentCompany.getId();
            event.unreadChatMessages = currentCompany.getNumUnreadChatMessages();
            event.unreadProcessComments = currentCompany.getNumUnreadProcessComments();
            setBadgesForBottomBarByCompany(event);
        } else {
            setBadgesForBottomBarByCompany(null);
        }
        if (savedInstanceState == null) {
            IleuApp.INSTANCE.getRouter().replaceScreen(Screen.SCREEN_PROCESSES_CONTAINER);
        }
    }

    @Override
    public void bindViews() {
        bottomNavigation.addItem(new AHBottomNavigationItem("Tab 1", R.drawable.ic_home_black_24px));
        bottomNavigation.addItem(new AHBottomNavigationItem("Tab 2", R.drawable.ic_notifications_black_24px));
        bottomNavigation.addItem(new AHBottomNavigationItem("Tab 3", R.drawable.ic_chat_black_24px));
        bottomNavigation.addItem(new AHBottomNavigationItem("Tab 4", R.drawable.ic_news_black_24px));
        bottomNavigation.addItem(new AHBottomNavigationItem("Tab 5", R.drawable.ic_profile_black_24px));

        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);
        bottomNavigation.setCurrentItem(0);
        bottomNavigation.setOnTabSelectedListener(this);

        bottomNavigation.setAccentColor(Color.parseColor("#45aba4"));
        bottomNavigation.setInactiveColor(Color.parseColor("#82000000"));
    }

    @Override
    public void setData() {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProcessCountersChangedEvent(CompanyCountersChangedEvent event) {
        Log.d("EVENT", "FRAGMENT");
        if (event.companyId == currentCompany.getId()) {
            //currentCompany.setNumUnreadProcesses(currentCompany.getNumUnreadProcesses() - 1 == 0 ? 0 : currentCompany.getNumUnreadProcesses() - 1 );
            setBadgesForBottomBarByCompany(event);
        }
    }


    public void setBadgesForBottomBarByCompany(CompanyCountersChangedEvent event) {
        if (currentCompany != null && event != null) {

            currentCompany.setNumUnreadChatMessages(event.unreadChatMessages);
            currentCompany.setNumUnreadProcesses(event.unreadProcessComments);

            bottomNavigation.setNotification(currentCompany.getNumUnreadProcesses() > 0 ?
                    currentCompany.getNumUnreadProcesses() + "" : "", 1);

            bottomNavigation.setNotification(currentCompany.getNumUnreadChatMessages() > 0 ?
                    currentCompany.getNumUnreadChatMessages() + "" : "", 2);
        }
    }

    @Override
    public String getScreenName() {
        return null;
    }

    private void initFragments() {
        Bundle args = new Bundle();

        if (currentCompany != null)
            args.putParcelable(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));

        processContainerFragment =
                (ProcessContainerFragment)getChildFragmentManager()
                        .findFragmentByTag(Screen.SCREEN_PROCESSES_CONTAINER);
        if (processContainerFragment == null) {
            processContainerFragment = (currentCompany != null) ? ProcessContainerFragment.newInstance(args) : ProcessContainerFragment.newInstance();
            detachFragment(processContainerFragment, R.id.menuContainer, Screen.SCREEN_PROCESSES_CONTAINER);
        }

        notificationsListFragment =
                (NotificationsListFragment)getChildFragmentManager()
                        .findFragmentByTag(Screen.SCREEN_NOTIFICATIONS_LIST);
        if (notificationsListFragment == null) {
            notificationsListFragment = (currentCompany != null) ? NotificationsListFragment.newInstance(args) : NotificationsListFragment.newInstance();
            detachFragment(notificationsListFragment, R.id.menuContainer, Screen.SCREEN_NOTIFICATIONS_LIST);
        }

        chatsListFragment =
                (ChatsListFragment)getChildFragmentManager()
                        .findFragmentByTag(Screen.SCREEN_CHATS_LIST);
        if (chatsListFragment == null) {
            chatsListFragment = ChatsListFragment.newInstance(args);
            detachFragment(chatsListFragment, R.id.menuContainer, Screen.SCREEN_CHATS_LIST);
        }

        newsListFragment =
                (NewsListFragment)getChildFragmentManager()
                        .findFragmentByTag(Screen.SCREEN_NEWS_LIST);
        if (newsListFragment == null) {
            newsListFragment = (currentCompany != null) ? NewsListFragment.newInstance(args) : NewsListFragment.newInstance();
            detachFragment(newsListFragment, R.id.menuContainer, Screen.SCREEN_NEWS_LIST);
        }

        profileFragment =
                (ProfileFragment)getChildFragmentManager()
                        .findFragmentByTag(Screen.SCREEN_PROFILE);
        if (profileFragment == null) {
            profileFragment = ProfileFragment.newInstance();
            ((ProfileFragment)profileFragment).setCurrentCompany(this.currentCompany);
            detachFragment(profileFragment, R.id.menuContainer, Screen.SCREEN_PROFILE);
        }
    }

    private void detachFragment(Fragment fragment, int layoutId, String tag) {
        getChildFragmentManager()
                .beginTransaction()
                .add(layoutId, fragment, tag)
                .detach(fragment)
                .commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        IleuApp.INSTANCE.getNavigatorHolder().setNavigator(navigator);
    }

    @Override
    public void onPause() {
        super.onPause();
        IleuApp.INSTANCE.getNavigatorHolder().removeNavigator();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        switch (position) {
            case 0: {
                IleuApp.INSTANCE.getRouter().replaceScreen(Screen.SCREEN_PROCESSES_CONTAINER);
                return true;
            }
            case 1: {
                IleuApp.INSTANCE.getRouter().replaceScreen(Screen.SCREEN_NOTIFICATIONS_LIST);
                return true;
            }
            case 2: {
                IleuApp.INSTANCE.getRouter().replaceScreen(Screen.SCREEN_CHATS_LIST);
                return true;
            }
            case 3: {
                IleuApp.INSTANCE.getRouter().replaceScreen(Screen.SCREEN_NEWS_LIST);
                return true;
            }
            case 4: {
                IleuApp.INSTANCE.getRouter().replaceScreen(Screen.SCREEN_PROFILE);
                return true;
            }
            default: {
                return false;
            }

        }
    }
}
