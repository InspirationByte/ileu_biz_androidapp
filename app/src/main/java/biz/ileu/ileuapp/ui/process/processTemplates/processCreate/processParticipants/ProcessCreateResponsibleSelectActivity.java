package biz.ileu.ileuapp.ui.process.processTemplates.processCreate.processParticipants;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.process.EmployeeList;
import biz.ileu.ileuapi.models.process.EmployeeViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import biz.ileu.ileuapp.utils.ui.EndlessRecyclerOnScrollListener;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ProcessCreateResponsibleSelectActivity extends BaseActivity implements RecyclerViewClick.OnItemClickListener {

    @BindView(R.id.rlResponsible)
    RecyclerView rlResponsible;
    @BindView(R.id.srlProcessCreateResponsibleSelect)
    SwipeRefreshLayout srlProcessCreateResponsibleSelect;
    private CompanyWorkInfoListViewModel currentCompany;

    private List<EmployeeViewModel> list;
    private ProcessCreateResponsibleSelectAdapter adapter;
    private EndlessRecyclerOnScrollListener listener;

    private CompositeSubscription compositeSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_create_responsible_select);
        ButterKnife.bind(this);
        bindViews();
        setData();
    }

    @Override
    public void bindViews() {
        initBaseToolbar(true);

        int code = getIntent().getIntExtra("requestCode", 0);

        if (code == Constants.PROCESS_CREATE_SET_RESPONSIBLE_CODE)
            getSupportActionBar().setTitle(R.string.process_create_set_responsible);
        else
            getSupportActionBar().setTitle(R.string.process_create_set_executor);


        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void setData() {
        compositeSubscription = new CompositeSubscription();

        currentCompany = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_COMPANY));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        list = new ArrayList<>();
        adapter = new ProcessCreateResponsibleSelectAdapter(list, this);
        rlResponsible.setLayoutManager(layoutManager);
        rlResponsible.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        listener = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                request(current_page, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        };


        srlProcessCreateResponsibleSelect.post(new Runnable() {
            @Override
            public void run() {
                srlProcessCreateResponsibleSelect.setRefreshing(true);
                request(1, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        });

        srlProcessCreateResponsibleSelect.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                list.clear();
                listener.reset();
                adapter.notifyDataSetChanged();
                request(1, Constants.DEFAULT_PAGE_MAX_SIZE);

                srlProcessCreateResponsibleSelect.setRefreshing(true);
            }
        });
    }



    private void request(int page, int per) {
        Subscription subscription = RestClient.request()
                .getCompanyEmployees(PrefUtils.getToken(), currentCompany.getId(), page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<EmployeeList>() {
                    @Override
                    public void onCompleted() {
                        srlProcessCreateResponsibleSelect.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        srlProcessCreateResponsibleSelect.setRefreshing(false);
                        e.printStackTrace();
                        Toast.makeText(ProcessCreateResponsibleSelectActivity.this, R.string.server_error, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onNext(EmployeeList employeeList) {
                        list.addAll(employeeList.getEmployeeViewModelList());
                        adapter.notifyDataSetChanged();
                        srlProcessCreateResponsibleSelect.setRefreshing(false);
                    }
                });

        compositeSubscription.add(subscription);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();
    }


    @Override
    public void onItemClick(View view, int position) {
        if (list.size() > 0) {
            Intent intent = new Intent();
            intent.putExtra(Constants.ENTITY_EMPLOYEE, Parcels.wrap(list.get(position)));
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
