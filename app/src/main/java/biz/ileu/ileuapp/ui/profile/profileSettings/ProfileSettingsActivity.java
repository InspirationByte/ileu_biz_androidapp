package biz.ileu.ileuapp.ui.profile.profileSettings;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.parceler.Parcels;

import java.io.File;
import java.net.URI;

import biz.ileu.ileuapi.models.profile.ProfileAvatarUpdateViewModel;
import biz.ileu.ileuapi.models.profile.ProfileInfo;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.login.LoginActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

public class ProfileSettingsActivity extends BaseActivity implements ProfileSettingsContract.ProfileSettingsView {

    @BindView(R.id.etProfileSettingsFirstName)
    EditText etProfileSettingsFirstName;
    @BindView(R.id.etProfileSettingsSurname)
    EditText etProfileSettingsSurname;
    @BindView(R.id.etProfileSettingsPhone)
    EditText etProfileSettingsPhone;
    @BindView(R.id.btnProfileSettingsLogout)
    Button btnProfileSettingsLogout;
    @BindView(R.id.etProfileSettingsOldPassword)
    EditText etProfileSettingsOldPassword;
    @BindView(R.id.etProfileSettingsNewPassword)
    EditText etProfileSettingsNewPassword;
    @BindView(R.id.etProfileSettingsConfirmPassword)
    EditText etProfileSettingsConfirmPassword;
    @BindView(R.id.btnProfileSettingsChangePassword)
    Button btnProfileSettingsChangePassword;

    @BindView(R.id.civProfileSettingsAvatar)
    CircleImageView civProfileSettingsAvatar;


    private ProfileInfo profileInfo;
    private ProgressDialog progressDialog;

    private ProfileSettingsPresenter presenter;

    private Uri mCropImageUri;
    private File compressedImagePath = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_settings);
        ButterKnife.bind(this);
        bindViews();
        setData();
    }

    @Override
    public void bindViews() {
        initBaseToolbar(true);
        getSupportActionBar().setTitle(R.string.profile_settings);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void setData() {
        profileInfo = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_PROFILE_INFO));
        presenter = new ProfileSettingsPresenter(this, profileInfo);

        if (profileInfo != null) {
            etProfileSettingsFirstName.setText(profileInfo.getFirstname());
            etProfileSettingsPhone.setText(profileInfo.getPhoneNumber());
            etProfileSettingsSurname.setText(profileInfo.getSurname());
            Picasso.with(this)
                    .load(Constants.BASE_URL + profileInfo.getProfileImage())
                    .into(civProfileSettingsAvatar);
        }


        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.profile_settings_saving);
    }

    @OnClick(R.id.btnProfileSettingsLogout)
    public void onLogoutClicked() {
        PrefUtils.clearAllPrefsExceptFirebaseToken();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();
        else if (item.getItemId() == R.id.actionProfileSave) {

            if (etProfileSettingsPhone.getText().toString().length() > 10) {
                Toast.makeText(this, R.string.message_profile_phone_incorrect, Toast.LENGTH_SHORT).show();
                etProfileSettingsPhone.requestFocus();
                return false;
            }

            presenter.saveProfile(etProfileSettingsPhone.getText().toString(),
                    etProfileSettingsFirstName.getText().toString(),
                    etProfileSettingsSurname.getText().toString());
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void savingProcess() {
        progressDialog.setTitle(R.string.profile_settings_saving);
        progressDialog.show();
    }

    @Override
    public void profileSaveError(String error) {
        progressDialog.dismiss();

        Toast.makeText(this, R.string.message_profile_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void profileSaved() {
        progressDialog.dismiss();
        Toast.makeText(this, R.string.message_profile_saved, Toast.LENGTH_SHORT).show();

        profileInfo.setFirstname(etProfileSettingsFirstName.getText().toString());
        profileInfo.setSurname(etProfileSettingsSurname.getText().toString());
        profileInfo.setPhoneNumber(etProfileSettingsPhone.getText().toString());
    }

    @Override
    public void changingPassword() {
        progressDialog.setTitle(R.string.profile_password_changing);
        progressDialog.show();
    }

    @Override
    public void changePasswordError(String error) {
        progressDialog.dismiss();
        Toast.makeText(this, R.string.message_profile_password_change_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void passwordChanged() {
        progressDialog.dismiss();
        Toast.makeText(this, R.string.message_profile_password_changed, Toast.LENGTH_SHORT).show();

        etProfileSettingsOldPassword.setText("");
        etProfileSettingsNewPassword.setText("");
        etProfileSettingsConfirmPassword.setText("");
    }


    @OnClick(R.id.btnProfileSettingsChangePassword)
    public void onPasswordChangeClicked() {

        String oldPassword = etProfileSettingsOldPassword.getText().toString();
        String newPassword = etProfileSettingsNewPassword.getText().toString();
        String repeatPassword = etProfileSettingsConfirmPassword.getText().toString();

        if (oldPassword.isEmpty()) {
            Toast.makeText(this, R.string.message_password_empty, Toast.LENGTH_SHORT).show();
            etProfileSettingsOldPassword.requestFocus();
            return;
        }

        if (newPassword.isEmpty()) {
            Toast.makeText(this, R.string.message_new_password_is_empty, Toast.LENGTH_SHORT).show();
            etProfileSettingsNewPassword.requestFocus();
            return;
        }

        if (newPassword.length() < 6) {
            Toast.makeText(this, R.string.message_new_password_is_empty, Toast.LENGTH_SHORT).show();
            etProfileSettingsNewPassword.requestFocus();
            return;
        }

        if (repeatPassword.isEmpty()) {
            Toast.makeText(this, R.string.message_confirm_password_empty, Toast.LENGTH_SHORT).show();
            etProfileSettingsConfirmPassword.requestFocus();
            return;
        }

        if (!newPassword.equals(repeatPassword)) {
            Toast.makeText(this, R.string.message_password_not_equal, Toast.LENGTH_SHORT).show();
            etProfileSettingsNewPassword.requestFocus();
            return;
        }

        presenter.changePassword(oldPassword, newPassword, repeatPassword);
    }

    @OnClick(R.id.civProfileSettingsAvatar)
    public void onAvatarClicked() {
        if (CropImage.isExplicitCameraPermissionRequired(this)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            CropImage.startPickImageActivity(this);
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setRequestedSize(320, 280)
                .setMultiTouchEnabled(true)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE ) {
            Log.d("PERMISSION", "PICK_IMAGE");
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(mCropImageUri);
            } else {
                Toast.makeText(this, R.string.message_profile_avatar_permissions_cancelled, Toast.LENGTH_LONG).show();
            }
        }

        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                CropImage.startPickImageActivity(this);
            } else {
                Toast.makeText(this, R.string.message_profile_avatar_permissions_cancelled, Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("Cropper_request", requestCode + " ---- " + resultCode);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            Log.d("Cropper_request_path", imageUri.getPath());

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
                }
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                Uri resultUri = result.getUri();
                compressedImagePath = new File(resultUri.getPath());
                //civProfileSettingsAvatar.setImageURI(resultUri);
                presenter.saveAvatar(compressedImagePath);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    public void changingAvatar() {
        progressDialog.setTitle(R.string.profile_avatar_saving);
        progressDialog.show();
    }

    @Override
    public void changeAvatarError(String error) {
        progressDialog.dismiss();
        Toast.makeText(this, R.string.message_profile_avatar_save_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void avatarChanged(ProfileAvatarUpdateViewModel profileAvatarUpdateViewModel) {
        progressDialog.dismiss();
        Toast.makeText(this, R.string.message_profile_avatar_saved, Toast.LENGTH_SHORT).show();
        Picasso.with(this)
                .load(Constants.BASE_URL +  profileAvatarUpdateViewModel.getProfileImage())
                .into(civProfileSettingsAvatar);
    }

}
