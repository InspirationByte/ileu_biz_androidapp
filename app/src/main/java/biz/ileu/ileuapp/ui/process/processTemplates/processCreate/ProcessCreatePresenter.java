package biz.ileu.ileuapp.ui.process.processTemplates.processCreate;

import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.news.FileInfoViewModel;
import biz.ileu.ileuapi.models.process.CreateProcessViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.process.ProcessCreateStatus;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.ui.attachedFile.AttachedFile;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.Subscriptions;

/**
 * Created by Daniq on 16.04.2017.
 */

public class ProcessCreatePresenter implements ProcessCreateContract.ProcessCreatePresenter {

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private CompanyWorkInfoListViewModel currentCompany;

    private ProcessCreateContract.ProcessCreateView processCreateView;

    public ProcessCreatePresenter(ProcessCreateContract.ProcessCreateView processCreateView, CompanyWorkInfoListViewModel currentCompany) {
        this.currentCompany = currentCompany;
        this.processCreateView = processCreateView;
    }

    @Override
    public void createProcess(CreateProcessViewModel createProcessViewModel, List<AttachedFile> attachedFiles) {
        if (attachedFiles.size() == 0)
            createProcessWithoutAttachments(createProcessViewModel);
        else
            createProcessWithAttachments(createProcessViewModel, attachedFiles);
    }

    private void createProcessWithAttachments(final CreateProcessViewModel createProcessViewModel, List<AttachedFile> attachedFiles) {
        List<MultipartBody.Part> filesParts = new ArrayList<>();
        for (AttachedFile attachedFile: attachedFiles) {
            File file = attachedFile.getFile();

            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(attachedFile.getExtention());

            RequestBody body = MultipartBody.create(MediaType.parse(mimeType), file);

            MultipartBody.Part filePart = MultipartBody.Part.createFormData(file.getName(), file.getName(), body);
            filesParts.add(filePart);

            try {
                Log.d("Files", filePart.body().contentLength() + "");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Observable<List<FileInfoViewModel>> observable = RestClient.request().doUpload(PrefUtils.getToken(), currentCompany.getId(), filesParts);

        Subscription subscription = observable.flatMap(new Func1<List<FileInfoViewModel>, Observable<ProcessCreateStatus>>() {
            @Override
            public Observable<ProcessCreateStatus> call(List<FileInfoViewModel> fileInfoViewModels) {

                int[] fileIds = new int[fileInfoViewModels.size()];
                for (int i = 0; i < fileInfoViewModels.size(); i++)
                    fileIds[i] = fileInfoViewModels.get(i).getId();

                createProcessViewModel.setFileUploads(fileIds);

                return RestClient.request().createProcess(PrefUtils.getToken(), currentCompany.getId(), createProcessViewModel);
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Subscriber<ProcessCreateStatus>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                processCreateView.processCreateError(e.getMessage());
            }

            @Override
            public void onNext(ProcessCreateStatus processCreateStatus) {
                processCreateView.processCreated(processCreateStatus);
            }
        });

        processCreateView.processCreating();
        compositeSubscription.add(subscription);
    }

    private void createProcessWithoutAttachments(CreateProcessViewModel createProcessViewModel) {
        Subscription subscription = RestClient.request()
                .createProcess(PrefUtils.getToken(), currentCompany.getId(), createProcessViewModel)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessCreateStatus>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCreateView.processCreateError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProcessCreateStatus processCreateStatus) {
                        processCreateView.processCreated(processCreateStatus);
                    }
                });
        processCreateView.processCreating();
        compositeSubscription.add(subscription);
    }

    @Override
    public void getProcess(int id) {
        Subscription subscription = RestClient.request()
                .getProcessData(PrefUtils.getToken(), currentCompany.getId(), id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Process>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCreateView.processCreateError(e.getMessage());
                    }

                    @Override
                    public void onNext(Process process) {
                        processCreateView.processLoaded(process);
                    }
                });

        processCreateView.processLoading();
        compositeSubscription.add(subscription);
    }

    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();
    }

}
