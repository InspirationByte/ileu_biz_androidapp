package biz.ileu.ileuapp.ui.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import biz.ileu.ileuapi.models.profile.ProfileInfo;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.menu.MenuActivity;
import biz.ileu.ileuapp.ui.register.RegisterActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.services.SignalRService;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class LoginActivity extends BaseActivity implements LoginContract.LoginView {

    @BindView(R.id.buttonLogin)
    Button buttonLogin;
    @BindView(R.id.etLoginEmail)
    EditText etLoginEmail;
    @BindView(R.id.etLoginPassword)
    EditText etLoginPassword;
    @BindView(R.id.btnLoginRegister)
    Button btnLoginRegister;
    private LoginPresenter loginPresenter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        if (loginPresenter == null) {
            loginPresenter = new LoginPresenter(this);
        }
        bindViews();
        setData();
        Log.d("FIREBASE_TOKEN", "FIREBASE: " + PrefUtils.getFirebaseToken());
    }

    @Override
    public void bindViews() {

    }

    @Override
    public void setData() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.login_message);
    }

    @Override
    public void loginLoading() {
        progressDialog.show();
    }

    @Override
    public void loginError(String error) {
        progressDialog.dismiss();

        Toast.makeText(this, R.string.login_wrong_login_or_password, Toast.LENGTH_SHORT).show();
        Timber.tag("Login_init").d(error);
    }

    @Override
    public void loginData(String token) {
        progressDialog.dismiss();

        Timber.tag("Login_init_timber").d(token);
        Log.d("Login_init_log", token);
        PrefUtils.setData(PrefUtils.TOKEN, token);

        loginPresenter.getProfileInfo();
        Intent serviceIntent = new Intent(this, SignalRService.class);
        startService(serviceIntent);
    }

    @Override
    public void profileLoading() {
        progressDialog.show();
    }

    @Override
    public void profileError(String error) {
        progressDialog.dismiss();
        PrefUtils.clearAllPrefsExceptFirebaseToken();
        Toast.makeText(this, R.string.login_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void profileLoaded(ProfileInfo profileInfo) {
        progressDialog.dismiss();
        PrefUtils.setData(PrefUtils.CURRENT_USER_ID, profileInfo.getId());

        Log.d("Login_firebase", "FIREBASE: " + PrefUtils.getFirebaseToken());
        loginPresenter.sendFirebaseToken(PrefUtils.getFirebaseToken());
    }


    @Override
    public void firebaseTokenLoading() {
        progressDialog.show();
    }

    @Override
    public void firebaseTokenIsSent() {
        progressDialog.dismiss();
        proceed();
    }

    private void proceed() {
        Log.d("TOKEN", PrefUtils.getToken());
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }


    @OnClick(R.id.buttonLogin)
    public void login() {
        String email = etLoginEmail.getText().toString();
        String password = etLoginPassword.getText().toString();

        if (email.isEmpty()) {
            Toast.makeText(this, R.string.message_email_empty, Toast.LENGTH_SHORT).show();
            etLoginEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            Toast.makeText(this, R.string.message_password_empty, Toast.LENGTH_SHORT).show();
            etLoginPassword.requestFocus();
            return;
        }

        loginPresenter.login(Constants.GRANT_TYPE, email, password);
    }

    @OnClick(R.id.btnLoginRegister)
    public void register() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}
