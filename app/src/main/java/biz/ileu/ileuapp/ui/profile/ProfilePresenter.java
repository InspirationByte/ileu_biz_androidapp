package biz.ileu.ileuapp.ui.profile;

import android.util.Log;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.TokenData;
import biz.ileu.ileuapi.models.invitation.CompanyInvitationDTO;
import biz.ileu.ileuapi.models.invitation.CompanyInvitationResultDTO;
import biz.ileu.ileuapi.models.invitation.InvitationEntryDTO;
import biz.ileu.ileuapi.models.profile.ProfileInfo;
import biz.ileu.ileuapp.utils.PrefUtils;
import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniq on 06.04.2017.
 */

public class ProfilePresenter implements ProfileContract.ProfilePresenter {

    private ProfileContract.ProfileView profileView;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public ProfilePresenter(ProfileContract.ProfileView profileView) {
        this.profileView = profileView;
    }

    public void getProfile() {
        profileView.profileLoading();

        Subscription subscription = RestClient.request()
                .getProfileInfo(PrefUtils.getToken())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProfileInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        profileView.profileError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileInfo profileInfo) {
                        profileView.profileData(profileInfo);
                    }
                });

        compositeSubscription.add(subscription);
    }
    public void invite(String[] texts, int companyId) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        CompanyInvitationDTO companyInvitationDTO = new CompanyInvitationDTO();
        for (String text: texts) {
            InvitationEntryDTO invitationEntryDTO = new InvitationEntryDTO();
            if (text.matches(emailPattern)) {
                invitationEntryDTO.setEmail(text);
                invitationEntryDTO.setPhone("");
            } else {
                invitationEntryDTO.setEmail("");
                invitationEntryDTO.setPhone(text);
            }
            companyInvitationDTO.getDestinations().add(invitationEntryDTO);
        }

        Log.d("RESULT", companyInvitationDTO.toString());
        profileView.profileLoading();
        Subscription subscription = RestClient.request()
                .sendInvitationMessages(PrefUtils.getToken(), companyId, companyInvitationDTO)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CompanyInvitationResultDTO>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        profileView.profileError(e.getMessage());
                    }

                    @Override
                    public void onNext(CompanyInvitationResultDTO profileInfo) {
                        profileView.successInvited(profileInfo);
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void logout() {
        Subscription subscription = RestClient.request()
                .logout(PrefUtils.getToken())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        profileView.logoutError(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        profileView.logoutSuccess();
                    }
                });
        compositeSubscription.add(subscription);
    }

    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();

        profileView = null;
    }
}
