package biz.ileu.ileuapp.ui.notificationsList;

import android.widget.Toast;

import java.util.List;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.process.CommentReadStateUpdatedViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.process.ProcessNotificationList;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.utils.PrefUtils;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniq on 05.05.2017.
 */

public class NotificationsListPresenter implements NotificationsListContract.NotificationsListPresenter {

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private CompanyWorkInfoListViewModel currentCompany;

    private NotificationsListContract.NotificationsListView notificationsListView;

    public NotificationsListPresenter(NotificationsListContract.NotificationsListView notificationsListView, CompanyWorkInfoListViewModel currentCompany) {
        this.currentCompany = currentCompany;
        this.notificationsListView = notificationsListView;
    }

    public void getNotifications(int page, int per) {
        Subscription subscription = RestClient.request()
                .getProcessNotifications(PrefUtils.getToken(), currentCompany.getId(), page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessNotificationList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        notificationsListView.loadNotificationsError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProcessNotificationList processNotificationList) {
                        notificationsListView.notificationsLoaded(processNotificationList.getNotificationList());
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void markNotification(int processId, final int notificationPosition) {
        Subscription subscription = RestClient.request()
                .markMessagesRead(PrefUtils.getToken(), currentCompany.getId(), processId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<CommentReadStateUpdatedViewModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        notificationsListView.markNotificationError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<CommentReadStateUpdatedViewModel> commentReadStateUpdatedViewModels) {
                        notificationsListView.notificationIsMarked(notificationPosition);
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void loadProcess(int processId) {

        notificationsListView.loadingProcess();

        Subscription subscription = RestClient.request()
                .getProcessData(PrefUtils.getToken(), currentCompany.getId(), processId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Process>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        notificationsListView.loadingProcessError(e.getMessage());
                    }

                    @Override
                    public void onNext(Process process) {
                        notificationsListView.processLoaded(process);
                    }
                });

        compositeSubscription.add(subscription);
    }

    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();

        notificationsListView = null;
    }
}
