package biz.ileu.ileuapp.ui.process.processComments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.MimeTypeMap;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import biz.ileu.ileuapi.models.process.DocumentAgreedByViewModel;
import biz.ileu.ileuapi.models.process.DocumentViewModel;
import biz.ileu.ileuapi.models.process.ParticipantChangeViewModel;
import biz.ileu.ileuapi.models.process.ParticipantViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.process.ProcessCommentViewModel;
import biz.ileu.ileuapi.models.process.ProcessFileViewModel;
import biz.ileu.ileuapi.models.process.ProcessNameColumnViewModel;
import biz.ileu.ileuapi.models.processtypes.ProcessTemplateViewModel;
import biz.ileu.ileuapi.models.tables.DSResultWithTypeOfProcessRowDTO;
import biz.ileu.ileuapi.models.tables.ProcessColumnValueDTO;
import biz.ileu.ileuapi.models.tables.ProcessRowDTO;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapi.models.usercontext.ContextInfoDTO;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.menu.MenuActivity;
import biz.ileu.ileuapp.ui.process.processComments.processCommentsUsers.ProcessCommentsUsersActivity;
import biz.ileu.ileuapp.ui.process.processComments.processCommentsUsersAdd.ProcessCommentsUsersAddActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.events.FileRemovedEvent;
import biz.ileu.ileuapp.utils.events.ParticipantAddedEvent;
import biz.ileu.ileuapp.utils.events.ParticipantRemovedEvent;
import biz.ileu.ileuapp.utils.events.ProcessClosedEvent;
import biz.ileu.ileuapp.utils.events.ProcessOpenedEvent;
import biz.ileu.ileuapp.utils.events.ProcessStatusChangedEvent;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import biz.ileu.ileuapp.utils.ui.EndlessRecyclerOnScrollListener;
import biz.ileu.ileuapp.utils.ui.attachedFile.AttachedFile;
import biz.ileu.ileuapp.utils.ui.attachedFile.AttachedFileViewClickListener;
import biz.ileu.ileuapp.utils.ui.attachedFile.AttachedFileViewHolder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import rx.subscriptions.CompositeSubscription;

@RuntimePermissions
public class ProcessCommentsActivity extends BaseActivity implements
        ProcessCommentsContract.ProcessCommentsView,
        ProcessCommentClick.OnItemClickListener,
        AttachedFileViewClickListener, View.OnTouchListener, TabHost.OnTabChangeListener {

    private static final int SELECT_ADD_USERS = 2;
    private static final int PROCESS_COMMENTS = 12;
    @BindView(R.id.rlProcessComments)
    RecyclerView rlProcessComments;
//    @BindView(R.id.rlProcessCommentsFiles)
//    RecyclerView rlProcessCommentsFiles;
//    @BindView(R.id.rlProcessTable)
//    RecyclerView rlProcessTable;
//    @BindView(R.id.srlProcessTable)
//    SwipeRefreshLayout srlProcessTable;

    @BindView(R.id.llFirst)
    LinearLayout llFirst;
    @BindView(R.id.llSecond)
    LinearLayout llSecond;
    @BindView(R.id.llThird)
    LinearLayout llThird;
    @BindView(R.id.llFour)
    LinearLayout llFour;
    @BindView(R.id.llRowProcessTableActions)
    LinearLayout llRowProcessTableActions;
    @BindView(R.id.llRowProcessTableParticipants)
    LinearLayout llRowProcessTableParticipants;
    @BindView(R.id.llTableLabels)
    LinearLayout llTableLabels;

    @BindView(R.id.llTableHolder)
    LinearLayout llTableHolder;
    private TabHost tabHost;

    @BindView(R.id.ibProcessCommentAttach)
    ImageButton ivProcessCommentAttach;
    @BindView(R.id.etProcessComment)
    EditText etProcessComment;
    @BindView(R.id.ibProcessCommentVoice)
    ImageButton ibProcessCommentVoice;
    @BindView(R.id.ibProcessCommentPost)
    ImageButton ibProcessCommentPost;
    @BindView(R.id.srlProcessComments)
    SwipeRefreshLayout srlProcessComments;
//    @BindView(R.id.srlProcessCommentsFiles)
//    SwipeRefreshLayout srlProcessCommentsFiles;
    @BindView(R.id.llProcessCommentsAttachments)
    LinearLayout llProcessCommentsAttachments;

    @BindView(R.id.etFromName)
    EditText etFromName;
    @BindView(R.id.etToName)
    EditText etToName;
    @BindView(R.id.etDocumentText)
    EditText etDocumentText;


    @BindView(R.id.llDocument)
    LinearLayout llDocument;
    @BindView(R.id.llAddDocument)
    LinearLayout llAddDocument;
    @BindView(R.id.toolbar_process)
    Toolbar toolbar;
    @BindView(R.id.tvToolbarProcessTitle)
    TextView tvToolbarTitle;
    @BindView(R.id.tvToolbarProcessSubtitle)
    TextView tvToolbarSubtitle;
    @BindView(R.id.llToolbarProcess)
    LinearLayout llToolbarProcess;

    @BindView(R.id.ivProcessCommentVoiceRecording)
    ImageView ivProcessCommentVoiceRecording;
    @BindView(R.id.chronoProcessCommentVoiceRecordingTime)
    Chronometer chronoProcessCommentVoiceRecordingTime;
    @BindView(R.id.tvProcessCommentVoiceRecordingCancel)
    TextView tvProcessCommentVoiceRecordingCancel;
    @BindView(R.id.llProcessCommentPost)
    LinearLayout llProcessCommentPost;

    @BindView(R.id.tvProcessCommentReply)
    TextView tvProcessCommentReply;
    @BindView(R.id.ibProcessCommentReply)
    ImageButton ibProcessCommentReply;
    @BindView(R.id.llProcessCommentReply)
    LinearLayout llProcessCommentReply;
    @BindView(R.id.llRowProcessDocumentActions)
    LinearLayout llRowProcessDocumentActions;
    @BindView(R.id.tvProcessCommentNotify)
    TextView tvProcessCommentNotify;
    @BindView(R.id.ibProcessCommentNotify)
    ImageButton ibProcessCommentNotify;
    @BindView(R.id.llProcessCommentNotify)
    LinearLayout llProcessCommentNotify;

    @BindView(R.id.civRowProcessDocumentAvatar)
    CircleImageView civRowProcessDocumentAvatar;
    @BindView(R.id.llRowProcessDocumentParticipants)
    LinearLayout llRowProcessDocumentParticipants;
    @BindView(R.id.tvRowProcessDocumentProfileName)
    TextView tvRowProcessDocumentProfileName;

    @BindView(R.id.tvRowProcessCommentDate)
    TextView tvRowProcessCommentDate;

    @BindView(R.id.tvRowProcessCommentFrom)
    TextView tvRowProcessCommentFrom;

    @BindView(R.id.tvRowProcessCommentTo)
    TextView tvRowProcessCommentTo;

    @BindView(R.id.tvRowProcessDocumentText)
    TextView tvRowProcessDocumentText;


    @BindView(R.id.glProcessAllFiles)
    GridLayout glProcessFiles;

    private Process currentProcess;
    private CompanyWorkInfoListViewModel currentCompany = null;
    private ProcessTemplateViewModel currentProcessTemplate;
    private ArrayList<String> selectedFilesPaths;
    private ArrayList<AttachedFile> attachedFiles;
    private int columnsAmount = 1;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private List<ProcessFileViewModel> fileViewModels;
    private List<ProcessCommentViewModel> list;
    private List<ProcessCommentViewModel> list3;
    private List<ProcessRowDTO> list4;
    private ProcessCommentsListAdapter adapter;
    private ProcessCommentsListAdapter adapter3;
    private ProcessCommentsTableListAdapter adapter4;
    private EndlessRecyclerOnScrollListener listener;
    private EndlessRecyclerOnScrollListener listener4;
    private DocumentViewModel currentDocument;
    private ProgressDialog progressDialog;
    private PopupWindow popupWindow;
    private DSResultWithTypeOfProcessRowDTO currentTable;
    private ProcessCommentsPresenter presenter;
    private int currentPosition;
    private ProcessDetailViewHolder processDetailViewHolder;

    private boolean isNew = false;

    private int replyCommentId = 0;
    private int[] userIdsToNotify = null;

    private final int SELECT_USERS_TO_NOTIFY = 1;
    private ContextInfoDTO contextInfoDTO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_process_comments);
        ButterKnife.bind(this);
        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup();
        tabHost.setOnTabChangedListener(this);
        currentCompany = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_COMPANY));
        currentProcess = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_PROCESS));
        presenter = new ProcessCommentsPresenter(this, currentCompany, currentProcess);
        bindViews();
        setData();
        int counter = 1;
        // add views to tab host
        tabHost.addTab(tabHost.newTabSpec(Constants.CHAT).setIndicator(Constants.CHAT).setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String arg0) {
                return llFirst;
            }
        }));

        if (currentProcess.getDocumentState() > 0 || currentProcess.isHasTable()) {
            tabHost.addTab(tabHost.newTabSpec(Constants.DOCUMENTS).setIndicator(Constants.DOCUMENTS).setContent(new TabHost.TabContentFactory() {
                public View createTabContent(String arg0) {
                    return llSecond;
                }
            }));
            llSecond.setVisibility(View.VISIBLE);
            counter++;
        } else {
            llSecond.setVisibility(View.GONE);
        }
        tabHost.addTab(tabHost.newTabSpec(Constants.FILES).setIndicator(Constants.FILES).setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String arg0) {
                return llThird;
            }
        }));
        llThird.setVisibility(View.VISIBLE);
        if (currentProcess.isHasTable()) {
                tabHost.addTab(tabHost.newTabSpec(Constants.TABLES).setIndicator(Constants.TABLES).setContent(new TabHost.TabContentFactory() {
                    public View createTabContent(String arg0) {
                        return llFour;
                    }
                }));
                counter++;

                llFour.setVisibility(View.VISIBLE);

        }
         else {
            llFour.setVisibility(View.GONE);
        }
        //HACK to get the list view to show up first,
        // otherwise the mapview would be bleeding through and visible
        if (counter == 3) {

            tabHost.setCurrentTab(3);
            tabHost.setCurrentTab(2);
        }
        if (counter == 2) {
            tabHost.setCurrentTab(2);
        }
        tabHost.setCurrentTab(1);
        tabHost.setCurrentTab(0);
    }



    @Override
    public void bindViews() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvToolbarTitle.setText(currentProcess.getProcessName().length() < 24 ? currentProcess.getProcessName() + " #" + currentProcess.getId() :
                currentProcess.getProcessName().substring(0, 23) + "..." + " #" + currentProcess.getId());
        tvToolbarSubtitle.setText(currentProcess.getAllParticipantsNumber() + " участника");

        View popupLayout = LayoutInflater.from(this).inflate(R.layout.process_detail_view_layout, null, false);

        popupWindow = new PopupWindow(popupLayout,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                true);
        popupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
        processDetailViewHolder = new ProcessDetailViewHolder(this, popupLayout, currentProcess, popupWindow, currentCompany);


        llToolbarProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
            }
        });

        ibProcessCommentVoice.setOnTouchListener(this);

        llProcessCommentReply.setVisibility(View.GONE);
        ibProcessCommentReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replyCommentId = 0;
                llProcessCommentReply.setVisibility(View.GONE);
            }
        });

        llProcessCommentNotify.setVisibility(View.GONE);
        ibProcessCommentNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userIdsToNotify = null;
                llProcessCommentNotify.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void setData() {
        isNew = getIntent().getBooleanExtra(Constants.PROCESS_IS_NEW, false);
        selectedFilesPaths = new ArrayList<>();
        attachedFiles = new ArrayList<>();

        fileViewModels = new ArrayList<>();
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        final RecyclerView.LayoutManager layoutManager3 = new LinearLayoutManager(this);
//        final RecyclerView.LayoutManager layoutManager4 = new LinearLayoutManager(this);
        rlProcessComments.setLayoutManager(layoutManager);
//        rlProcessCommentsFiles.setLayoutManager(layoutManager3);
//        rlProcessTable.setLayoutManager(layoutManager4);

        list = new ArrayList<>();
        list3 = new ArrayList<>();
        list4 = new ArrayList<>();
        adapter = new ProcessCommentsListAdapter(list, this, Constants.CHAT);
        adapter3 = new ProcessCommentsListAdapter(list3, this, Constants.FILES);
//        adapter4 = new ProcessCommentsTableListAdapter(list4, this, presenter);
        rlProcessComments.setAdapter(adapter);
//        rlProcessCommentsFiles.setAdapter(adapter3);
//        rlProcessTable.setAdapter(adapter4);
        adapter.notifyDataSetChanged();
        adapter3.notifyDataSetChanged();
//        adapter3.notifyDataSetChanged();
//        adapter4.notifyDataSetChanged();

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getResources().getString(R.string.process_comment_sending));
        srlProcessComments.post(new Runnable() {
            @Override
            public void run() {
                list.clear();
                srlProcessComments.setRefreshing(true);
                presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE, Constants.CHAT);
            }
        });

//        srlProcessCommentsFiles.post(new Runnable() {
//            @Override
//            public void run() {
//                srlProcessCommentsFiles.setRefreshing(true);
//                presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE, Constants.FILES);
//            }
//        });

//        srlProcessTable.post(new Runnable() {
//            @Override
//            public void run() {
//                srlProcessTable.setRefreshing(true);
//                presenter.getRows(1, Constants.DEFAULT_PAGE_MAX_SIZE);
//            }
//        });
//        listener = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
//            @Override
//            public void onLoadMore(int current_page) {
//                presenter.getComments(current_page, Constants.DEFAULT_PAGE_MAX_SIZE, Constants.CHAT);
//            }
//        };
//        listener4 = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
//            @Override
//            public void onLoadMore(int current_page) {
//                presenter.getRows(current_page, Constants.DEFAULT_PAGE_MAX_SIZE);
//            }
//        };
//

        srlProcessComments.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                list.clear();
                adapter.notifyDataSetChanged();
                srlProcessComments.setRefreshing(true);
//                listener.reset();
                presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE, Constants.CHAT);
            }
        });

//        srlProcessCommentsFiles.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                list3.clear();
//                adapter3.notifyDataSetChanged();
//                srlProcessCommentsFiles.setRefreshing(true);
////                listener.reset();
//                presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE, Constants.FILES);
//            }
//        });

//
//        srlProcessTable.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                list4.clear();
//                adapter4.notifyDataSetChanged();
//                srlProcessTable.setRefreshing(true);
//                listener4.reset();
//                presenter.getRows(1, Constants.DEFAULT_PAGE_MAX_SIZE);
//            }
//        });


//        rlProcessComments.addOnScrollListener(listener);
//        rlProcessCommentsFiles.addOnScrollListener(listener);
        presenter.getCompanyContext();
        presenter.getProcessFiles();
        presenter.getProcessNameTemplate(this.currentProcess.getProcessNameId());
        if (isNew) {
            ProcessOpenedEvent event = new ProcessOpenedEvent();
            event.processId = currentProcess.getId();
            EventBus.getDefault().post(event);
        } else {
            // THERE MUST BE SOME KIND OF LOGIC
        }

        presenter.markCommentsAsRead();
    }



    @OnClick(R.id.llRowProcessDocumentEdit)
    public void onDocumentEditClicked() {
        llDocument.setVisibility(View.GONE);
        llAddDocument.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.ibProcessCommentPost)
    public void onPostCommentClicked() {
        if (etProcessComment.getText().toString().length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.process_comment_text_empty), Toast.LENGTH_SHORT).show();
            etProcessComment.requestFocus();

            return;
        }

        if (userIdsToNotify == null && replyCommentId == 0) {
            Toast.makeText(this, R.string.message_process_comments_select_users_to_notify, Toast.LENGTH_SHORT).show();
            openUsersSelect();
            return;
        }

        if (attachedFiles.size() > 0)
            presenter.sendCommentWithAttachments(etProcessComment.getText().toString(), attachedFiles, replyCommentId, userIdsToNotify);
        else
            presenter.sendComment(etProcessComment.getText().toString(), replyCommentId, userIdsToNotify);

    }

    @OnClick(R.id.ibProcessCommentAttach)
    public void onAttachFileViewClicked() {
        ProcessCommentsActivityPermissionsDispatcher.attachFilesWithCheck(this);
    }

    @OnClick(R.id.llRowProcessDocumentAgree)
    public void onAgreeClicked() {
        presenter.agreeDocument();
    }

//
//    @OnClick(R.id.llRowProcessTableAgree)
//    public void onTableAgreeClicked() {
//        presenter.agreeTable();
//    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    public void attachFiles() {
        FilePickerBuilder.getInstance()
                .setMaxCount(3)
                .setSelectedFiles(selectedFilesPaths)
                .setActivityTheme(R.style.CustomTheme)
                .addVideoPicker()
                .enableDocSupport(true)
                .pickFile(this);
    }


    @Override
    public ContentResolver getResolver() {
        return getContentResolver();
    }

    @Override
    public void loadCommentsError(String error) {
        Toast.makeText(this, R.string.message_process_comments_load_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadDocumetsError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void documentsLoaded(DocumentViewModel document) {

        this.currentDocument = document;

        llDocument.setVisibility(View.VISIBLE);
        llAddDocument.setVisibility(View.GONE);

        ParticipantViewModel author = null;
        if (currentProcess.getResponsible().getId() == this.currentDocument.getWhoseId())
            author = currentProcess.getResponsible();
        if (currentProcess.getExecutor().getId() == this.currentDocument.getWhoseId())
            author = currentProcess.getExecutor();
        if (currentProcess.getCreator().getId() == this.currentDocument.getWhoseId())
            author = currentProcess.getCreator();
        for (ParticipantViewModel temp : currentProcess.getParticipants()) {
            if (temp.getId() == this.currentDocument.getWhoseId()) {
                author = temp;
                break;
            }
        }
//        if ((document.getSpecsToAgree().contains(contextInfoDTO.getSpecId()) || document.getSpecsToAgree().isEmpty()) && currentProcess.getStatus() != 4 && currentProcess.getStatus() != 5) {
            llRowProcessDocumentActions.setVisibility(View.VISIBLE);
//        } else {
//            llRowProcessDocumentActions.setVisibility(View.GONE);
//        }

        llRowProcessTableParticipants.setVisibility(View.VISIBLE);
//        for (DocumentAgreedByViewModel agreedBy : this.currentDocument.getAgreedBy()) {
//            if (agreedBy.getProfileId() == PrefUtils.getCurrentUserId()) {
//                llRowProcessDocumentActions.setVisibility(View.GONE);
//                break;
//            }
//        }
        if (author != null) {
            Picasso.with(this)
                    .load(Constants.BASE_URL + author.getProfilePicture())
                    .into(civRowProcessDocumentAvatar);
            tvRowProcessDocumentProfileName.setText(document.getWhoseName());
        }
        tvRowProcessCommentFrom.setText("От: " + document.getFromName());
        tvRowProcessCommentTo.setText("Кому: " + document.getToName());
        etFromName.setText(document.getFromName());
        etToName.setText(document.getToName());
        tvRowProcessCommentDate.setText(Constants.getFormattedDateTime(document.getWhen()));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            etDocumentText.setText(Html.fromHtml(document.getDocumentText(), Html.FROM_HTML_MODE_COMPACT));
            tvRowProcessDocumentText.setText(Html.fromHtml(document.getDocumentText(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            etDocumentText.setText(Html.fromHtml(document.getDocumentText()));
            tvRowProcessDocumentText.setText(Html.fromHtml(document.getDocumentText()));
        }
//        tvRowProcessDocumentText.setText(document.getDocumentText());
        llRowProcessDocumentParticipants.removeAllViews();
//        llRowProcessTableParticipants.removeAllViews();

        // Adding creator
        if (currentProcess.getCreator().getId() == currentProcess.getResponsible().getId())
            addParticipantAvatar(currentProcess.getCreator(), true);
        else
            addParticipantAvatar(currentProcess.getCreator(), false);


        // Add executor if it is not creator
        if (currentProcess.getCreator().getId() != currentProcess.getExecutor().getId())
            addParticipantAvatar(currentProcess.getExecutor(), false);

        // Add responsible if it is not creator and not executor
        if (currentProcess.getResponsible().getId() != currentProcess.getExecutor().getId() &&
                currentProcess.getResponsible().getId() != currentProcess.getCreator().getId())
            addParticipantAvatar(currentProcess.getResponsible(), true);
        for (ParticipantViewModel participant: currentProcess.getParticipants()) {
            if (participant.getId() != currentProcess.getCreator().getId() &&
                    participant.getId() != currentProcess.getExecutor().getId() &&
                    participant.getId() != currentProcess.getResponsible().getId())
                addParticipantAvatar(participant, false);

        }
//        currentProcess.getP
    }
    private void addParticipantAvatar(ParticipantViewModel participant,  boolean hasBorder) {
        int wantedHeight = 36;
        int wantedWidth = 36;

        int heightDpi = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                wantedHeight,
                this.getResources().getDisplayMetrics());

        int widthDpi = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                wantedWidth,
                this.getResources().getDisplayMetrics());

        CircleImageView civCreator = new CircleImageView(this);
        RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(heightDpi, widthDpi);
        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(heightDpi, widthDpi);
        RelativeLayout rlParticipant = new RelativeLayout(this);

        int wantedMarginLeft = 8;
        int marginLeft = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                wantedMarginLeft, this.getResources().getDisplayMetrics());
        params2.setMargins(marginLeft, 0, 0, 0);
        rlParticipant.setTag(participant.getId());
        if (params2 != null)
        rlParticipant.setLayoutParams(params2);
        if (params != null)
        civCreator.setLayoutParams(params);
        if (hasBorder) {
            civCreator.setBorderColor(Color.parseColor("#FBC02D"));
            civCreator.setBorderWidth(5);
        } else if (participant.isIsExecuting()) {
            civCreator.setBorderColor(Color.parseColor("#0092ff"));
            civCreator.setBorderWidth(5);
        } else {
            civCreator.setBorderWidth(0);
        }

        rlParticipant.addView(civCreator);

        for (DocumentAgreedByViewModel agreedBy : this.currentDocument.getAgreedBy()) {
            if (agreedBy.getProfileId() == participant.getId()) {
                if (agreedBy.getProfileId() == Constants.currentUserId)
                    llRowProcessDocumentActions.setVisibility(View.GONE);
                int checkWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16,
                        this.getResources().getDisplayMetrics());
                int checkHeight = checkWidth;
                RelativeLayout.LayoutParams paramsCheck = new RelativeLayout.LayoutParams(checkHeight, checkWidth);
                paramsCheck.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                paramsCheck.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                ImageView img = new ImageView(this);
                img.setImageResource(R.drawable.ic_checked_agree);
                if (paramsCheck != null)
                img.setLayoutParams(paramsCheck);
                rlParticipant.addView(img);
                break;
            }
        }
        this.llRowProcessDocumentParticipants.addView(rlParticipant);
//        this.llRowProcessTableParticipants.addView(rlParticipant);
        Picasso.with(this)
                .load(Constants.BASE_URL + participant.getProfilePicture())
                .into(civCreator);
    }

    @Override
    public void commentsLoaded(List<ProcessCommentViewModel> comments, String tabId) {
        switch (tabId) {
            case Constants.CHAT:
                list.addAll(comments);
                adapter.notifyDataSetChanged();
                rlProcessComments.scrollToPosition(list.size() - 1);
                srlProcessComments.setRefreshing(false);
                break;
//            case Constants.FILES:
//                for (ProcessCommentViewModel comment : comments) {
//                    Document commentTextDoc = Jsoup.parse(comment.getText());
//                    Elements links = commentTextDoc.getElementsByTag("a");
//                    for (Element link: links) {
//                        if (!link.attr("href").contains("GetFile"))
//                            continue;
//                        list3.add(comment);
//                        break;
//                    }
//                }
//
//                adapter3.notifyDataSetChanged();
//                rlProcessCommentsFiles.scrollToPosition(list3.size() - 1);
//                srlProcessCommentsFiles.setRefreshing(false);
//                break;
            default:
                break;

        }
    }

    @Override
    public void sendingComment() {
        progressDialog.setTitle(R.string.process_comment_sending);
        progressDialog.show();
    }

    @Override
    public void sendingCommentError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
    }

    @Override
    public void commentSent(ProcessCommentViewModel comment) {
//        list.clear();
//        srlProcessComments.setRefreshing(true);
//        adapter.notifyDataSetChanged();

//        presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE);

        Toast.makeText(ProcessCommentsActivity.this, getResources().getString(R.string.process_comment_added),
                Toast.LENGTH_SHORT).show();

        progressDialog.dismiss();
        etProcessComment.setText("");

        if (attachedFiles.size() > 0)
            presenter.getProcessFiles();
        attachedFiles.clear();
        llProcessCommentsAttachments.removeAllViews();

        rlProcessComments.scrollToPosition(adapter.getItemCount() - 1);

        replyCommentId = 0;
        llProcessCommentReply.setVisibility(View.GONE);
        etProcessComment.clearFocus();

        llProcessCommentNotify.setVisibility(View.GONE);
        userIdsToNotify = null;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCommentPosted(ProcessCommentViewModel data) {
        if (data.getProcessId() != currentProcess.getId())
            return;

        list.add(data);
        adapter.notifyDataSetChanged();

        rlProcessComments.scrollToPosition(adapter.getItemCount() - 1);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFileAdded(ProcessFileViewModel data) {
        this.addFile(data);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFileRemoved(FileRemovedEvent event) {
        this.removeFile(event.fileId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onStatusChanged(ProcessStatusChangedEvent event) {
        processDetailViewHolder.setStatus(event.status);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onParticipantAdded(ParticipantAddedEvent event) {
        currentProcess.getParticipants().add(event.participantViewModel);
        processDetailViewHolder.setParticipants();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onParticipantRemoved(ParticipantRemovedEvent event) {
        for (Iterator<ParticipantViewModel> iterator = currentProcess.getParticipants().iterator(); iterator.hasNext();) {
            ParticipantViewModel participantViewModel = iterator.next();
            if (participantViewModel.getId() == event.participantId) {
                iterator.remove();
                break;
            }
        }
        processDetailViewHolder.setParticipants();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEmployeesChanged(ParticipantChangeViewModel data) {
        if (data.getExecutor() != null) {
            if (data.getExecutor().getId() != currentProcess.getExecutor().getId()) {
                for (Iterator<ParticipantViewModel> iterator = currentProcess.getParticipants().iterator(); iterator.hasNext();) {
                    ParticipantViewModel participantViewModel = iterator.next();
                    if (participantViewModel.getId() == data.getExecutor().getId()) {
                        iterator.remove();
                        break;
                    }
                }
                currentProcess.getParticipants().add(currentProcess.getExecutor());
            }

            currentProcess.setExecutor(data.getExecutor().getId() == 0 ? null : data.getExecutor());
        }

        if (data.getResponsible() != null) {
            if (data.getResponsible().getId() != currentProcess.getResponsible().getId()) {
                for (Iterator<ParticipantViewModel> iterator = currentProcess.getParticipants().iterator(); iterator.hasNext();) {
                    ParticipantViewModel participantViewModel = iterator.next();
                    if (participantViewModel.getId() == data.getResponsible().getId()) {
                        iterator.remove();
                        break;
                    }
                }
                currentProcess.getParticipants().add(currentProcess.getResponsible());
            }

            currentProcess.setResponsible(data.getResponsible());
        }

        processDetailViewHolder.setParticipants();
    }

    @Override
    public void loadFilesError(String error) {
        Toast.makeText(this, R.string.message_process_files_load_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void filesLoaded(List<ProcessFileViewModel> processFileViewModels) {
        this.setFiles(processFileViewModels);
    }
    public void addFile(ProcessFileViewModel fileViewModel) {
        this.fileViewModels.add(fileViewModel);

            View view = LayoutInflater.from(this).inflate(R.layout.item_process_file, glProcessFiles, false);
            ProcessFileViewHolder holder = new ProcessFileViewHolder(view, fileViewModel);

            glProcessFiles.addView(view);

    }

    public void removeFile(int fileId) {
        for (Iterator<ProcessFileViewModel> iterator = fileViewModels.iterator(); iterator.hasNext();) {
            ProcessFileViewModel processFileViewModel = iterator.next();
            if (processFileViewModel.getId() == fileId) {
                iterator.remove();
                break;
            }
        }
        List<ProcessFileViewModel> processFileViewModels = new ArrayList<>();
        processFileViewModels.addAll(fileViewModels);
        setFiles(processFileViewModels);
    }
    public void setFiles(List<ProcessFileViewModel> fileViewModels) {
        this.fileViewModels.clear();
        this.fileViewModels.addAll(fileViewModels);

        glProcessFiles.removeAllViews();

        for (ProcessFileViewModel model: this.fileViewModels) {
            View view = LayoutInflater.from(this).inflate(R.layout.item_process_file, glProcessFiles, false);
            ProcessFileViewHolder holder = new ProcessFileViewHolder(view, model);

            glProcessFiles.addView(view);
        }
    }
    @Override
    public void downloadingFile() {
        progressDialog.setTitle(R.string.process_file_downloading);
        progressDialog.show();
    }

    @Override
    public void downloadingCancelled() {
        Toast.makeText(this, R.string.message_process_file_download_canceled, Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
    }

    @Override
    public void downloadError(String string) {
        Toast.makeText(this, R.string.message_process_file_download_error, Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
    }

    @Override
    public void downloadCompleted(File file) {
        progressDialog.dismiss();
        Intent myIntent = new Intent(Intent.ACTION_VIEW);
        String type = "*/*";
        String extension = MimeTypeMap.getFileExtensionFromUrl(file.getPath());
        if (extension != null)
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        myIntent.setDataAndType(Uri.fromFile(file), type);

        Intent intent = Intent.createChooser(myIntent, getResources().getString(R.string.process_file_choose_type));
        startActivity(intent);
    }

    @Override
    public void audioRecordError(String error) {
        Toast.makeText(this, R.string.message_process_voice_record_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void audioPlayError(String error) {
        Toast.makeText(this, R.string.message_process_audio_play_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void processLoaded(Process process) {
        this.currentProcess = process;
        tvToolbarSubtitle.setText(currentProcess.getAllParticipantsNumber() + " участника");
        processDetailViewHolder.setCurrentProcess(process);
        processDetailViewHolder.setParticipants();
        processDetailViewHolder.updateButtons(process);
    }

    public void updateProccess(Process process) {
        this.currentProcess = process;
        tvToolbarSubtitle.setText(currentProcess.getAllParticipantsNumber() + " участника");
        processDetailViewHolder.setParticipants();
        processDetailViewHolder.updateButtons(process);

    }

    @Override
    public void agreedError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void successAgreed(DocumentAgreedByViewModel agreed) {
        Toast.makeText(this, R.string.success_agreed, Toast.LENGTH_SHORT).show();
        llRowProcessDocumentActions.setVisibility(View.GONE);
        RelativeLayout temp;
        for (int i = 0; i < llRowProcessDocumentParticipants.getChildCount(); i++) {
            temp = (RelativeLayout)llRowProcessDocumentParticipants.getChildAt(i);
            if ((int)temp.getTag() == agreed.getProfileId()) {
                int checkWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16,
                        this.getResources().getDisplayMetrics());
                int checkHeight = checkWidth;
                RelativeLayout.LayoutParams paramsCheck = new RelativeLayout.LayoutParams(checkHeight, checkWidth);
                paramsCheck.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                paramsCheck.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                ImageView img = new ImageView(this);
                img.setImageResource(R.drawable.ic_checked_agree);
                if (paramsCheck != null)
                img.setLayoutParams(paramsCheck);
                temp.addView(img);
            }   
        }
        updateProcess();

    }

    @Override
    public void documentStored(DocumentViewModel document) {
        if (document.getWhoseId() == 0) {
            llDocument.setVisibility(View.GONE);
            llAddDocument.setVisibility(View.VISIBLE);
            Toast.makeText(this, R.string.document_save_denied, Toast.LENGTH_SHORT).show();
            return;
        } else {
            this.documentsLoaded(document);
            updateProcess();
        }

    }

    @Override
    public void documentStoreError(String message) {
        Toast.makeText(this, R.string.message_process_comments_load_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadRowsError(String message) {

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void rowsLoaded(DSResultWithTypeOfProcessRowDTO dsResult) {
        currentTable = dsResult;
        list4.addAll(dsResult.getData());
//        list4.add(new ProcessRowDTO());
//        adapter4.notifyDataSetChanged();
//        rlProcessTable.scrollToPosition(list4.size() - 1);
//        srlProcessTable.setRefreshing(false);
        if(((LinearLayout) llTableHolder).getChildCount() > 0)
            ((LinearLayout) llTableHolder).removeAllViews();
        int width = 120;
        int i = 0;
        for (ProcessRowDTO row : list4) {
            List<ProcessColumnValueDTO> columnValues = row.getColumnValues();
            Collections.sort(columnValues, new Comparator<ProcessColumnValueDTO>() {
                @Override
                public int compare(ProcessColumnValueDTO o1, ProcessColumnValueDTO o2) {
                    int o1x = currentProcessTemplate.getOrderPosById(o1.getColumnId());
                    int o2x = currentProcessTemplate.getOrderPosById(o2.getColumnId());
                    return Integer.compare(o1x, o2x);
                }
            });
            LinearLayout llRow = new LinearLayout(this);
            LinearLayout.LayoutParams lparams = null;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int widthS = displayMetrics.widthPixels;
            if (currentProcess.getResponsible().getId() == PrefUtils.getCurrentUserId()) {
                widthS -= 50;
            } else
            if (columnValues.size() < 4 && columnValues.size() != 0) {

                lparams = new LinearLayout.LayoutParams(widthS/columnValues.size() - 10, LinearLayout.LayoutParams.WRAP_CONTENT);
            } else {
                lparams = new LinearLayout.LayoutParams(320, LinearLayout.LayoutParams.WRAP_CONTENT);
            }

            for (ProcessColumnValueDTO columnValue : columnValues) {
                EditText et = new EditText(this);
                if (lparams != null)
                et.setLayoutParams(lparams);
                et.setText(columnValue.getValue()) ;
                et.setLines(1);
                et.setSingleLine();
                et.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                llRow.addView(et);
                et.setTextSize(TypedValue.COMPLEX_UNIT_DIP,12);
            }
//            ic_close_grey_24px
            if (currentProcess.getResponsible().getId() == PrefUtils.getCurrentUserId()) {
                ImageView btn = new ImageView(this);
                btn.setBackgroundResource(R.drawable.ic_close_grey_24px);
                btn.setTag(i);

                btn.setLayoutParams(new LinearLayoutCompat.LayoutParams(50, 50));
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int id = (int) v.getTag();
                        presenter.removeRow(list4.get(id).getId());
                        list4.remove(id);
                        llTableHolder.getChildAt(id).setVisibility(View.GONE);
                    }
                });
                llRow.addView(btn);
            }
            if (columnValues.size() < 4) {
                llRow.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            } else
                llRow.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            llTableHolder.addView(llRow);
            i++;
        }

    }

    @Override
    public void processNameTemplatedLoaded(ProcessTemplateViewModel processTemplate) {
        this.currentProcessTemplate = processTemplate;
//        tvFirstColumn
        List<ProcessNameColumnViewModel> columns = new ArrayList<>();
        columns.addAll(this.currentProcessTemplate.getProcessColumns());
        if (columns.size() > 0) {
            llFour.setVisibility(View.GONE);
        }
        int i =0;
        Collections.sort(columns, new Comparator<ProcessNameColumnViewModel>() {
            public int compare(ProcessNameColumnViewModel left, ProcessNameColumnViewModel right)  {
                return Integer.compare(left.getOrderPos(), right.getOrderPos()); // The order depends on the direction of sorting.
            }
        });
        int width = 120;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int widthS = displayMetrics.widthPixels;
        columnsAmount = columns.size();
        for (ProcessNameColumnViewModel column : columns) {
            LinearLayout.LayoutParams lparams = null;
            if (columns.size() < 4 && columns.size() > 0)
                lparams = new LinearLayout.LayoutParams(widthS/columns.size(), TableLayout.LayoutParams.WRAP_CONTENT, 0f); // Width , height
            else lparams = new LinearLayout.LayoutParams(320,  TableLayout.LayoutParams.WRAP_CONTENT);

            TextView tv = new TextView(this);
            if (lparams != null)
            tv.setLayoutParams(lparams);
            tv.setText(column.getName());
            llTableLabels.addView(tv);
//            switch (i) {
//                case 0:
//                    tvFirstColumn.setText(column.getName());
//                    break;
//                case 1:
//                    tvSecondColumn.setText(column.getName());
//                    break;
//                case 2:
//                    tvThirdColumn.setText(column.getName());
//                    break;
//                default:
//                    break;
//            }
//            i++;
        }
        presenter.getRows(1, Constants.DEFAULT_PAGE_MAX_SIZE);
    }

    @Override
    public void processRowRemoved() {
        list4.clear();
        presenter.getRows(1, Constants.DEFAULT_PAGE_MAX_SIZE);
        Toast.makeText(this, R.string.success_removed, Toast.LENGTH_SHORT).show();
        updateProcess();
    }

    @Override
    public void updatedRow(ProcessRowDTO dsResult) {
        for (int i = 0; i < list4.size(); i++) {
            if (list4.get(i).getId() == dsResult.getId()) {
                list4.set(i, dsResult);
            }
        }

        Toast.makeText(this, R.string.process_comment_table_row_success_updated, Toast.LENGTH_SHORT).show();
        updateProcess();
    }

    @Override
    public void createdRow(ProcessRowDTO dsResult) {

        Toast.makeText(this, R.string.process_comment_table_row_success_added, Toast.LENGTH_SHORT).show();
        list4.add(dsResult);
        updateProcess();
    }

    @Override
    public void companyContextLoaded(ContextInfoDTO contextInfoDTO) {
        this.contextInfoDTO = contextInfoDTO;
        presenter.loadDocument();

    }

    @OnClick(R.id.addRow)
    public void addRow() {
        LinearLayout llRow = new LinearLayout(this);
        LinearLayout.LayoutParams lparams = null;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int widthS = displayMetrics.widthPixels;
        if (columnsAmount < 4 && columnsAmount > 0) {
            lparams = new LinearLayout.LayoutParams((int)widthS/columnsAmount, LinearLayout.LayoutParams.WRAP_CONTENT, 0f);
        } else {
            lparams = new LinearLayout.LayoutParams(320,LinearLayout.LayoutParams.WRAP_CONTENT);
        }
//        if (lparams != null)
//            lparams.(RelativeLayout.ALIGN_PARENT_RIGHT);
        for (int i = 0; i < columnsAmount; i++) {
            EditText et = new EditText(this);
            if (lparams != null)
            et.setLayoutParams(lparams);
            et.setLines(1);
            et.setSingleLine();
            et.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            et.setTextSize(TypedValue.COMPLEX_UNIT_DIP,12);
            llRow.addView(et);
        }
        if (columnsAmount < 4) {
            llRow.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        } else
            llRow.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        llTableHolder.addView(llRow);
    }
    @OnClick(R.id.saveRows)
    public void saveRows() {
        int i = 0;
        ProcessCommentsTableListAdapter.ProcessCommentTableViewHolder holder;
        int children = llTableHolder.getChildCount();
        LinearLayout temp;
        boolean isUpdate = false;
        for (int c = 0; c < children; c++ ) {
            temp = (LinearLayout) llTableHolder.getChildAt(c);
            List<ProcessColumnValueDTO> valueList = new ArrayList<>();

            for (int cc = 0; cc < columnsAmount; cc++) {
                EditText tempEt = (EditText)temp.getChildAt(cc);
                if (tempEt == null)
                    continue;
                ProcessColumnValueDTO tempColumn = new ProcessColumnValueDTO();
                tempColumn.setValue(tempEt.getText().toString());
                tempColumn.setColumnId(currentProcessTemplate.getIdByOrderPos(cc));
                tempColumn.setAddedById(PrefUtils.getCurrentUserId());
                valueList.add(tempColumn);
            }
            ProcessRowDTO row = new ProcessRowDTO();
            row.setColumnValues(valueList);
            row.setAddedById(PrefUtils.getCurrentUserId());
            if (c >= list4.size()) {
                presenter.createRow(row);

            } else {
                List<ProcessColumnValueDTO> columnValues = list4.get(c).getColumnValues();
                Collections.sort(columnValues, new Comparator<ProcessColumnValueDTO>() {
                    @Override
                    public int compare(ProcessColumnValueDTO o1, ProcessColumnValueDTO o2) {
                        int o1x = currentProcessTemplate.getOrderPosById(o1.getColumnId());
                        int o2x = currentProcessTemplate.getOrderPosById(o2.getColumnId());
                        return Integer.compare(o1x, o2x);
                        }
                });
                isUpdate = false;
                /*  ВЫЛЕТАЕТ */
                for (int cl = 0; cl < columnValues.size(); cl++) {
                    if (!row.getColumnValues().get(cl).getValue().equals(columnValues.get(cl).getValue() == null ? "" : columnValues.get(cl).getValue() )) {
                        isUpdate = true;
                        break;
                    }
                }
                if (isUpdate) {
                    row.setId(list4.get(c).getId());
                    presenter.updateRow(row);
                }
            }

        }

    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
//        srlProcessComments.post(new Runnable() {
//            @Override
//            public void run() {
//                list.clear();
//                srlProcessComments.setRefreshing(true);
//                presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE, Constants.CHAT);
//            }
//        });
//        srlProcessCommentsFiles.post(new Runnable() {
//            @Override
//            public void run() {
//                list3.clear();
//                srlProcessCommentsFiles.setRefreshing(true);
//                presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE, Constants.FILES);
//            }
//        });
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ProcessClosedEvent event = new ProcessClosedEvent();
        event.processId = currentProcess.getId();
        EventBus.getDefault().post(event);
        presenter.destroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.process_comments, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            if (isNew) {
                Intent intent = new Intent(this, MenuActivity.class);
                startActivity(intent);
                intent = new Intent();
                intent.putExtra(Constants.ENTITY_PROCESS, Parcels.wrap(currentProcess));
                setResult(PROCESS_COMMENTS, intent);
                finish();
            } else {
                Intent intent = new Intent();
                intent.putExtra(Constants.ENTITY_PROCESS, Parcels.wrap(currentProcess));
                setResult(PROCESS_COMMENTS, intent);
                finish();
            }
        } else if (item.getItemId() == R.id.actionNotifyUsers) {
            openUsersSelect();
        } else if (item.getItemId() == R.id.actionAddUser) {
            openAddUsersSelect();
        }

        return super.onOptionsItemSelected(item);
    }

    public void openUsersSelect() {
        Intent intent = new Intent(this, ProcessCommentsUsersActivity.class);
        intent.putExtra(Constants.ENTITY_PROCESS, Parcels.wrap(currentProcess));
        intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));

        startActivityForResult(intent, SELECT_USERS_TO_NOTIFY);
    }
    public void openAddUsersSelect() {
        Intent intent = new Intent(this, ProcessCommentsUsersAddActivity.class);
        intent.putExtra(Constants.ENTITY_PROCESS, Parcels.wrap(currentProcess));
        intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));

        startActivityForResult(intent, SELECT_ADD_USERS);
    }
    @Override
    public void onBackPressed() {
        if (isNew) {
            Intent intent = new Intent(this, MenuActivity.class);
            startActivity(intent);
            intent = new Intent();
            intent.putExtra(Constants.ENTITY_PROCESS, Parcels.wrap(currentProcess));
            setResult(PROCESS_COMMENTS, intent);
            finish();
        } else {
            Intent intent = new Intent();
            intent.putExtra(Constants.ENTITY_PROCESS, Parcels.wrap(currentProcess));
            setResult(PROCESS_COMMENTS, intent);
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ProcessCommentsActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FilePickerConst.REQUEST_CODE_DOC) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                List<String> filePaths = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS);


                llProcessCommentsAttachments.removeAllViews();
                attachedFiles.clear();
                for (String path : filePaths) {
                    AttachedFile attachedFile = new AttachedFile(path);
                    attachedFiles.add(attachedFile);
                    View attachedFileView = LayoutInflater.from(this).inflate(R.layout.item_attached_file, null, false);
                    AttachedFileViewHolder holder = new AttachedFileViewHolder(attachedFileView, attachedFile, this);

                    llProcessCommentsAttachments.addView(attachedFileView);
                }

            }
        } else if (requestCode == SELECT_USERS_TO_NOTIFY) {
            if (resultCode == RESULT_OK && data != null) {
                userIdsToNotify = data.getIntArrayExtra(Constants.ENTITY_PROCESS_SELECTED_USERS);
                Log.d("USERS", userIdsToNotify.length + "");

                if (userIdsToNotify.length == 1) {
                    llProcessCommentNotify.setVisibility(View.VISIBLE);
                    tvProcessCommentNotify.setText("Выбран один пользователь");
                } else if (userIdsToNotify.length > 1) {
                    llProcessCommentNotify.setVisibility(View.VISIBLE);
                    tvProcessCommentNotify.setText("Выбрано пользователей " + userIdsToNotify.length);
                }
            }
            if (resultCode == RESULT_FIRST_USER && data != null) {
                srlProcessComments.post(new Runnable() {
                    @Override
                    public void run() {
                        list.clear();
                        srlProcessComments.setRefreshing(true);
                        presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE, Constants.CHAT);
                    }
                });
                presenter.updateProcess(currentCompany.getId(), currentProcess.getId());
            }

        } else if (requestCode == SELECT_ADD_USERS) {
            if (resultCode == RESULT_OK && data != null) {
                srlProcessComments.post(new Runnable() {
                    @Override
                    public void run() {
                        list.clear();
                        srlProcessComments.setRefreshing(true);
                        presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE, Constants.CHAT);
                    }
                });
                presenter.updateProcess(currentCompany.getId(), currentProcess.getId());
            }
        }
    }


    public void updateProcess() {
        srlProcessComments.post(new Runnable() {
            @Override
            public void run() {
                list.clear();
                srlProcessComments.setRefreshing(true);
                presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE, Constants.CHAT);
            }
        });
        presenter.updateProcess(currentCompany.getId(), currentProcess.getId());
    }
    @OnPermissionDenied(Manifest.permission.READ_EXTERNAL_STORAGE)
    void showDeniedForStorage() {
        Toast.makeText(this, R.string.message_permission_denied, Toast.LENGTH_SHORT).show();
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForWrite() {
        Toast.makeText(this, R.string.message_permission_denied, Toast.LENGTH_SHORT).show();
    }

    @OnPermissionDenied(Manifest.permission.RECORD_AUDIO)
    void showDeniedForAudio() {
        Toast.makeText(this, R.string.message_permission_denied, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onAttachedFileClick(AttachedFile attachedFile, View v) {
        attachedFiles.remove(attachedFile);
        llProcessCommentsAttachments.removeView(v);
    }

    @Override
    public void onCommentClick(int position) {
        ProcessCommentViewModel commentViewModel = list.get(position);
        if (commentViewModel.getEmployeeId() != PrefUtils.getCurrentUserId()) {
            replyCommentId = commentViewModel.getId();
            llProcessCommentReply.setVisibility(View.VISIBLE);
            tvProcessCommentReply.setText("Ответ для " + commentViewModel.getDisplayName());
            etProcessComment.requestFocus();
        }
    }


    @Override
    public void onDownloadFileClick(String fileName, String processId, String fileId) {
        ProcessCommentsActivityPermissionsDispatcher.downloadFileWithCheck(this, fileName, processId, fileId);
    }

    @Override
    public void onDownloadAudioClick(ProcessCommentsListAdapter.ProcessCommentAudioViewHolder audioViewHolder) {
        ProcessCommentsActivityPermissionsDispatcher.downloadAudioWithCheck(this, audioViewHolder);
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void downloadAudio(ProcessCommentsListAdapter.ProcessCommentAudioViewHolder audioViewHolder) {
        presenter.downloadAudio(audioViewHolder);
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void downloadFile(String fileName, String processId, String fileId) {
        presenter.downloadFile(fileName, processId, fileId);
    }


    public Context getContext() {
        return this;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v.getId() == R.id.ibProcessCommentVoice)
            ProcessCommentsActivityPermissionsDispatcher.audioRecordTouchedWithCheck(this, event);
        return false;
    }

    @NeedsPermission({Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void audioRecordTouched(MotionEvent event) {
        Rect llRect = new Rect();
        llProcessCommentPost.getGlobalVisibleRect(llRect);


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            ibProcessCommentPost.setVisibility(View.GONE);
            etProcessComment.setVisibility(View.GONE);
            ivProcessCommentAttach.setVisibility(View.GONE);

            ivProcessCommentVoiceRecording.setVisibility(View.VISIBLE);
            chronoProcessCommentVoiceRecordingTime.setVisibility(View.VISIBLE);
            tvProcessCommentVoiceRecordingCancel.setVisibility(View.VISIBLE);
            ibProcessCommentVoice.setImageResource(R.drawable.ic_keyboard_voice_green_24px);

            chronoProcessCommentVoiceRecordingTime.setBase(SystemClock.elapsedRealtime());
            chronoProcessCommentVoiceRecordingTime.start();

            presenter.startRecording();


        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {

            if (llRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                tvProcessCommentVoiceRecordingCancel.setText(R.string.process_voice_cancel);
                tvProcessCommentVoiceRecordingCancel.setTextColor(getResources().getColor(R.color.defaultTextColor));
            } else {
                tvProcessCommentVoiceRecordingCancel.setText(R.string.process_voice_cancel_2);
                tvProcessCommentVoiceRecordingCancel.setTextColor(getResources().getColor(R.color.colorOrange));
            }

        } else if (event.getAction() == MotionEvent.ACTION_UP) {

            ibProcessCommentPost.setVisibility(View.VISIBLE);
            etProcessComment.setVisibility(View.VISIBLE);
            ivProcessCommentAttach.setVisibility(View.VISIBLE);

            ivProcessCommentVoiceRecording.setVisibility(View.GONE);
            chronoProcessCommentVoiceRecordingTime.setVisibility(View.GONE);
            tvProcessCommentVoiceRecordingCancel.setVisibility(View.GONE);
            ibProcessCommentVoice.setImageResource(R.drawable.ic_keyboard_voice_grey_24px);

            chronoProcessCommentVoiceRecordingTime.stop();

            if (llRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                int seconds = (int) (SystemClock.elapsedRealtime() - chronoProcessCommentVoiceRecordingTime.getBase()) / 1000;
                if (seconds < 1) {
                    Toast.makeText(this, R.string.message_process_voice_send_too_short, Toast.LENGTH_SHORT).show();
                    presenter.cancelRecording();
                } else
                    presenter.stopRecording(seconds, replyCommentId, userIdsToNotify);
            } else {
                Toast.makeText(this, R.string.message_process_voice_send_cancelled, Toast.LENGTH_SHORT).show();
                presenter.cancelRecording();
            }


        }
    }
    @OnClick(R.id.btnSaveDocument)
    public void onSaveDocumentClicked() {
//        if (etFromName.getText().toString().equals("")) {
//            Toast.makeText(this, R.string.message_from_name, Toast.LENGTH_SHORT).show();
//            return;
//        }
//        if (etToName.getText().toString().equals("")) {
//            Toast.makeText(this, R.string.message_to_name, Toast.LENGTH_SHORT).show();
//            return;
//        }
        if (etDocumentText.getText().toString().equals("")) {
            Toast.makeText(this, R.string.message_document_text, Toast.LENGTH_SHORT).show();
            return;
        }
        DocumentViewModel doc = new DocumentViewModel();
        doc.setFromName(etFromName.getText().toString());
        doc.setToName(etToName.getText().toString());
        doc.setDocumentText(etDocumentText.getText().toString());
        doc.setCanAgree(true);
        doc.setFullyAgreed(false);
        presenter.saveDocument(doc);
    }
    public void onRowRemoveClick() {

    }
    @Override
    public void onTabChanged(String tabId) {

//                srlProcessCommentsDocuments.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        srlProcessCommentsDocuments.setRefreshing(true);
//                        presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE);
//                    }
//                });
//                srlProcessCommentsTables.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        srlProcessCommentsTables.setRefreshing(true);
//                        presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE);
//                    }
//                });
//        }
    }

    public class ProcessFileViewHolder implements View.OnClickListener {
        @BindView(R.id.ivProcessFile)
        public ImageView ivProcessFile;

        @BindView(R.id.tvProcessFile)
        public TextView tvProcessFile;

        @BindView(R.id.llProcessFile)
        public LinearLayout llProcessFile;

        public ProcessFileViewModel processFileViewModel;

        ProcessFileViewHolder(View view, ProcessFileViewModel processFileViewModel) {
            ButterKnife.bind(this, view);
            this.processFileViewModel = processFileViewModel;

            tvProcessFile.setText(processFileViewModel.getFileName().length() < 10 ?
                    processFileViewModel.getFileName() : processFileViewModel.getFileName().substring(0, 9) + "...");
            if (processFileViewModel.getFileMime().equals("application/pdf"))
                ivProcessFile.setImageResource(R.mipmap.ic_pdf_dark_grey);
            else if (processFileViewModel.getFileMime().equals("video/mp4") ||
                    processFileViewModel.getFileMime().equals("video/3gpp") ||
                    processFileViewModel.getFileMime().equals("video/mpeg") ||
                    processFileViewModel.getFileMime().equals("video/x-ms-wmv") ||
                    processFileViewModel.getFileMime().equals("video/x-flv") ||
                    processFileViewModel.getFileMime().equals("application/x-troff-msvideo") ||
                    processFileViewModel.getFileMime().equals("video/avi"))
                ivProcessFile.setImageResource(R.mipmap.ic_video_dark_grey);
            else if (processFileViewModel.getFileMime().startsWith("audio/"))
                ivProcessFile.setImageResource(R.mipmap.ic_audio_dark_grey);
            else
                ivProcessFile.setImageResource(R.mipmap.ic_file_dark_grey);

            llProcessFile.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            ProcessCommentsActivityPermissionsDispatcher.downloadFileWithCheck((ProcessCommentsActivity) getContext(),
                    processFileViewModel.getFileName(),
                    currentProcess.getId() + "", processFileViewModel.getId() + "");
        }
    }
}
