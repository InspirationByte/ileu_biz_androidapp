package biz.ileu.ileuapp.ui.chatsList.chatMessages;

import android.content.Context;

import java.util.List;

import biz.ileu.ileuapi.models.chat.ChatMessageViewModel;
import biz.ileu.ileuapi.models.chat.ChatsViewModel;
import biz.ileu.ileuapi.models.chat.MessageFromDto;
import biz.ileu.ileuapi.models.chat.PostMessageViewModel;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by Daniq on 19.04.2017.
 */

public interface ChatMessagesContract {

    interface ChatMessagesView extends BaseView {
        void loadMessagesError(String error);

        Context getContext();

        void messagesLoaded(List<ChatMessageViewModel> messagesList);

        void messageIsSending(ChatMessageViewModel message);

        void messageSendError(ChatMessageViewModel message, String error);

        void messageIsSent(ChatMessageViewModel message);

        void chatCreating();

        void chatCreateError(String error);

        void chatCreated(ChatsViewModel chatsViewModel);
    }

    interface ChatMessagesPresenter extends BasePresenter {

    }

}
