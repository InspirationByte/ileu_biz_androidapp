package biz.ileu.ileuapp.ui.process.processTemplates.processCreate.processParticipants;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import biz.ileu.ileuapi.models.process.EmployeeViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Daniq on 16.04.2017.
 */

public class ProcessCreateResponsibleSelectAdapter extends RecyclerView.Adapter<ProcessCreateResponsibleSelectAdapter.ResponsibleViewHolder> {

    private List<EmployeeViewModel> employeeList;

    private RecyclerViewClick.OnItemClickListener listener;

    public ProcessCreateResponsibleSelectAdapter(List<EmployeeViewModel> employeeList, RecyclerViewClick.OnItemClickListener listener) {
        this.employeeList = employeeList;
        this.listener = listener;
    }

    @Override
    public ResponsibleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_process_create_participant_responsible_select, parent, false);
        return new ResponsibleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ResponsibleViewHolder holder, int position) {
        EmployeeViewModel employee = employeeList.get(position);

        holder.tvRowProcessCreateParticipantResponsibleEmail.setText(employee.getEmail());
        holder.tvRowProcessCreateParticipantResponsibleName.setText(employee.getNameSurname());

        Picasso.with(holder.context)
                .load(Constants.IMAGE_BASE_URL + employee.getProfilePicture())
                .into(holder.civRowProcessCreateParticipantResponsible);
    }

    @Override
    public int getItemCount() {
        return employeeList.size();
    }

    public class ResponsibleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Context context;

        @BindView(R.id.civRowProcessCreateParticipantResponsible)
        CircleImageView civRowProcessCreateParticipantResponsible;

        @BindView(R.id.tvRowProcessCreateParticipantResponsibleName)
        TextView tvRowProcessCreateParticipantResponsibleName;

        @BindView(R.id.tvRowProcessCreateParticipantResponsibleEmail)
        TextView tvRowProcessCreateParticipantResponsibleEmail;


        public ResponsibleViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.onItemClick(v, getAdapterPosition());
        }
    }

}
