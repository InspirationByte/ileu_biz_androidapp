package biz.ileu.ileuapp.ui.process.processComments.processCommentsUsers;

import android.widget.Toast;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.company.CompanyList;
import biz.ileu.ileuapi.models.process.ProcessFileList;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.company.CompaniesContract;
import biz.ileu.ileuapp.utils.PrefUtils;
import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by macair on 09.08.17.
 */

public class ProcessCommentsUserPresenter {
    private ProcessCommentsUserContract.ProcessCommentsUserView userView;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public ProcessCommentsUserPresenter(ProcessCommentsUserContract.ProcessCommentsUserView userView) {
        this.userView = userView;
    }

    public void removeParticipants(int id, int processId, int companyId) {
        Subscription subscription = RestClient.request().removeParticipant(PrefUtils.getToken(),
                id, processId, companyId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        userView.errorRemove(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        userView.successRemoved();
                    }
                });

        compositeSubscription.add(subscription);
    }
}
