package biz.ileu.ileuapp.ui.process.processTemplates;

import biz.ileu.ileuapi.models.processtypes.ProcessTemplateViewModel;

/**
 * Created by Daniq on 16.04.2017.
 */

public interface TemplateListClick {
    interface OnTemplateNameClickListener {
        void onTemplateClick(ProcessTemplateViewModel template);
    }
}
