package biz.ileu.ileuapp.ui.newsList.news;

import android.content.Context;

import java.io.File;
import java.util.List;

import biz.ileu.ileuapi.models.news.NewsCommentViewModel;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by Daniq on 16.04.2017.
 */

public interface NewsContract {

    interface NewsView extends BaseView {

        Context getContext();

        void loadCommentsError(String error);

        void commentsLoaded(List<NewsCommentViewModel> comments);

        void sendingComment();

        void sendingCommentError(String error);

        void commentSent(NewsCommentViewModel comment);

        void downloadingFile();
        void downloadError(String string);
        void downloadCompleted(File file);


    }

    interface NewsPresenter extends BasePresenter {

    }

}
