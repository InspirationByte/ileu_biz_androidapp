package biz.ileu.ileuapp.ui.profile.profileSettings;

import android.util.Log;
import android.webkit.MimeTypeMap;

import com.google.gson.JsonObject;

import java.io.File;
import java.util.HashMap;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.profile.ProfileAvatarUpdateViewModel;
import biz.ileu.ileuapi.models.profile.ProfileInfo;
import biz.ileu.ileuapp.utils.PrefUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniq on 25.04.2017.
 */

public class ProfileSettingsPresenter implements ProfileSettingsContract.ProfileSettingsPresenter {

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private ProfileSettingsContract.ProfileSettingsView profileSettingsView;

    private ProfileInfo profileInfo;

    public ProfileSettingsPresenter(ProfileSettingsContract.ProfileSettingsView profileSettingsView, ProfileInfo profileInfo) {
        this.profileSettingsView = profileSettingsView;
        this.profileInfo = profileInfo;
    }

    public void saveProfile(String phone, String name, String surname) {
        Subscription subscription = RestClient.request()
                .saveProfileInfo(PrefUtils.getToken(), name, surname, phone)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProfileInfo>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        profileSettingsView.profileSaveError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileInfo profileInfo) {

                        profileSettingsView.profileSaved();
                    }
                });
        profileSettingsView.savingProcess();
        compositeSubscription.add(subscription);
    }

    public void changePassword(String oldPassword, String newPassword, String repeatPassword) {
        Subscription subscription = RestClient.request()
                .changePassword(PrefUtils.getToken(), oldPassword, newPassword, repeatPassword)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonObject>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        profileSettingsView.changePasswordError(e.getMessage());
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        if (jsonObject.has("error")) {
                            profileSettingsView.changePasswordError(jsonObject.get("error").getAsString());
                        } else
                            profileSettingsView.passwordChanged();
                    }
                });

        profileSettingsView.changingPassword();
        compositeSubscription.add(subscription);
    }


    public void saveAvatar(File filePath) {

        String extention = filePath.getPath().substring(filePath.getPath().lastIndexOf(".") + 1, filePath.getPath().length()).toLowerCase();

        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extention);

        RequestBody body = MultipartBody.create(MediaType.parse(mimeType), filePath);

        MultipartBody.Part filePart = MultipartBody.Part.createFormData(filePath.getName(), filePath.getName(), body);

        Subscription subscription = RestClient.request().saveAvatar(PrefUtils.getToken(), filePart)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProfileAvatarUpdateViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        profileSettingsView.changeAvatarError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileAvatarUpdateViewModel profileAvatarUpdateViewModel) {
                        profileSettingsView.avatarChanged(profileAvatarUpdateViewModel);
                    }
                });

        profileSettingsView.changingAvatar();
        compositeSubscription.add(subscription);
    }


    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();
    }
}
