package biz.ileu.ileuapp.ui.process.processesList;

import java.util.List;

import biz.ileu.ileuapi.models.bookmarks.BookmarkViewModel;
import biz.ileu.ileuapi.models.bookmarks.BookmarksCategoriesList;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by Daniq on 26.04.2017.
 */

public interface ProcessesListContract {

    interface ProcessesListView extends BaseView {

        void processesLoading();

        void loadProcessesError(String error);

        void processesLoaded(List<Process> processList);

        void addBookmarkError(String message);

        void addedBookmark(BookmarkViewModel bookmark);

        void bookmarkCategoriesLoaded(BookmarksCategoriesList bookmarksCategoriesList);
    }

    interface ProcessesListPresenter extends BasePresenter {

    }

}
