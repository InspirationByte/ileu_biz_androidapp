package biz.ileu.ileuapp.ui.bookmarks;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import biz.ileu.ileuapi.models.bookmarks.BookmarkCategoryViewModel;
import biz.ileu.ileuapi.models.bookmarks.BookmarksCategoriesList;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.process.processComments.processCommentsUsers.ProcessCommentsUsersActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import biz.ileu.ileuapp.utils.ui.BookmarksCategoryCheckClick;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by macair on 09.08.17.
 */

public class BookmarkCategoriesActivity extends BaseActivity implements BookmarkCategoriesContract.BookmarkCategoriesView, BookmarksCategoryCheckClick.OnBookmarkCategoryPositionCheckedUncheckedListener  {

    @BindView(R.id.rlProcessUsers)
    RecyclerView rlProcessUsers;


    private Process currentProcess;
    private CompanyWorkInfoListViewModel currentCompany = null;
    private int countSelected = 0;
    private List<BookmarkCategoryViewModel> usersList;
    private List<Integer> categoriesIds;
    private Set<BookmarkCategoryViewModel> participantViewModelSet;

    private BookmarkCategoriesAdapter adapter;
    private BookmakrCategoriesPresenter presenter;
    private boolean isFullyRemoved = true;
    private List<BookmarkCategoryViewModel> selectedParticipants;
    ProcessCommentsUsersActivity curr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_comments_users);
        ButterKnife.bind(this);
        bindViews();
        setData();
    }

    @Override
    public void bindViews() {
        initBaseToolbar(true);
        getSupportActionBar().setTitle(R.string.process_bookmarks);
        usersList = new ArrayList<>();
        adapter = new BookmarkCategoriesAdapter(usersList, this, null);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rlProcessUsers.setLayoutManager(layoutManager);
        rlProcessUsers.setAdapter(adapter);
        setData();
    }

    @Override
    public void setData() {
        currentCompany = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_COMPANY));
        currentProcess = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_PROCESS));
        presenter = new BookmakrCategoriesPresenter(this, currentCompany);

        selectedParticipants = new ArrayList<>();
        participantViewModelSet = new LinkedHashSet<>();

        presenter.loadBookmarkCategories(1, Constants.DEFAULT_PAGE_MAX_SIZE);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.actionProfileSave) {
            if (categoriesIds.size() > 0) {
                boolean ch = false;
                for(Integer i : categoriesIds) {
                    for (BookmarkCategoryViewModel b : selectedParticipants) {
                        if (b.getId() == i)  {
                            ch = true;
                            break;
                        }
                    }
                    if (!ch) {
                        presenter.removeBookmark(currentProcess.getId(), i);
                    }
                }
            }
            if (selectedParticipants.size() > 0) {
                int[] ids = new int[selectedParticipants.size()];
                for (int i = 0; i < selectedParticipants.size(); i++) {
                    ids[i] = selectedParticipants.get(i).getId();
                    if (!categoriesIds.contains(ids[i])) {
                        isFullyRemoved = false;
                        presenter.addBookmark(currentProcess.getId(), ids[i]);
                    }
                }
            }
            if (selectedParticipants.size() == 0 && categoriesIds.size() == 0) {
                Toast.makeText(this, R.string.message_bookmark_select_category, Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnPositionChecked(View v, int position) {

        selectedParticipants.add(usersList.get(position));
        Log.d("SELECTED", selectedParticipants.size() + "");
    }

    @Override
    public void OnPositionUnchecked(View v, int position) {
        selectedParticipants.remove(usersList.get(position));
        Log.d("SELECTED", selectedParticipants.size() + "");
    }


    @Override
    public void loadingError(String e) {
        Toast.makeText(this, e, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void successAdded() {
        if (selectedParticipants.size() == ++countSelected) {
            Intent intent = new Intent();
            intent.putExtra("HASBOOKMARK", true);
            setResult(RESULT_OK, intent);
            currentProcess.setHasBookmark(true);
            finish();
        }
    }

    @Override
    public void addingError(String message) {
        if (selectedParticipants.size() == ++countSelected) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent();
//            intent.putExtra("HASBOOKMARK", false);
//            setResult(RESULT_OK, intent);
//            finish();
        }
    }

    @Override
    public void categoriesIdsLoaded(List<Integer> categoriesIds) {


// add elements to al, including duplicates
        Set<Integer> hs = new HashSet<>();
        hs.addAll(categoriesIds);
        categoriesIds.clear();
        categoriesIds.addAll(hs);
        this.categoriesIds = categoriesIds;
//        selectedParticipants.clear();
//        for (BookmarkCategoryViewModel t : this.usersList) {
//            for (Integer i : categoriesIds) {
//                if (i == t.getId()) {
//                    selectedParticipants.add(t);
//                }
//            }
//        }
        adapter.setSelectedIds(categoriesIds);
        adapter.notifyDataSetChanged();
    }
    @Override
    public void successRemoved() {
        if (selectedParticipants.size() == countSelected++) {
            Intent intent = new Intent();
            if (isFullyRemoved) {
                intent.putExtra("HASBOOKMARK", false);
                currentProcess.setHasBookmark(false);
            } else {
                intent.putExtra("HASBOOKMARK", true);
            }
            setResult(RESULT_OK, intent);
            finish();
        }
    }
    @Override
    public void categoriesLoaded(BookmarksCategoriesList categoriesList) {
        this.usersList.clear();
        this.usersList.addAll(categoriesList.getBookmarkCategoryViewModelList());
        presenter.getBookmarkCategoriesFor(this.currentProcess.getId());
        adapter.notifyDataSetChanged();
    }

}
