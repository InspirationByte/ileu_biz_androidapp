package biz.ileu.ileuapp.ui.menu;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.ExecutionException;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.chat.CreateChatViewModel;
import biz.ileu.ileuapi.models.chat.MessageFromDto;
import biz.ileu.ileuapi.models.chat.UserChatStatusDto;
import biz.ileu.ileuapi.models.process.ParticipantChangeViewModel;
import biz.ileu.ileuapi.models.process.ParticipantViewModel;
import biz.ileu.ileuapi.models.process.ProcessCommentViewModel;
import biz.ileu.ileuapi.models.process.ProcessFileViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModelList;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.events.FileRemovedEvent;
import biz.ileu.ileuapp.utils.events.ParticipantAddedEvent;
import biz.ileu.ileuapp.utils.events.ParticipantRemovedEvent;
import biz.ileu.ileuapp.utils.events.ProcessStatusChangedEvent;
import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.Credentials;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.LogLevel;
import microsoft.aspnet.signalr.client.Logger;
import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.http.Request;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniq on 04.05.2017.
 */

public class MenuActivityPresenter implements MenuActivityContract.MenuActivityPresenter {

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private MenuActivityContract.MenuActivityView menuActivityView;



    public MenuActivityPresenter(MenuActivityContract.MenuActivityView menuActivityView) {
        this.menuActivityView = menuActivityView;


    }



    public void getCompanies(int page, int per) {
        Subscription subscription = RestClient.request()
                .getJoinedCompanyListWithInfo(PrefUtils.getToken(), page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CompanyWorkInfoListViewModelList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        menuActivityView.loadCompaniesError(e.getMessage());
                    }

                    @Override
                    public void onNext(CompanyWorkInfoListViewModelList companyWorkInfoListViewModelList) {
                        menuActivityView.companiesLoaded(companyWorkInfoListViewModelList.getCompanies());
                    }
                });

        compositeSubscription.add(subscription);
    }


    public void getCompany(int id) {
        Subscription subscription = RestClient.request()
                .getJoinedCompanyListWithInfoWithFilter(PrefUtils.getToken(), "Id~eq~" + id, 1, 1)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CompanyWorkInfoListViewModelList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(CompanyWorkInfoListViewModelList companyWorkInfoListViewModelList) {
                        if (companyWorkInfoListViewModelList.getTotal() > 0) {
                            menuActivityView.companyLoaded(companyWorkInfoListViewModelList.getCompanies().get(0));
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }


    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();

        menuActivityView = null;
    }
}
