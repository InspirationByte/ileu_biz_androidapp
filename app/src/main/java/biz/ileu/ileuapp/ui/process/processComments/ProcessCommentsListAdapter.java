package biz.ileu.ileuapp.ui.process.processComments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.List;
import java.util.Map;

import biz.ileu.ileuapi.models.process.ProcessCommentViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Daniq on 06.04.2017.
 */

public class ProcessCommentsListAdapter extends RecyclerView.Adapter<ProcessCommentsListAdapter.ProcessCommentViewHolder> {

    private List<ProcessCommentViewModel> commentList;

    private ProcessCommentClick.OnItemClickListener listener;

    private String tabId;

    public ProcessCommentsListAdapter(List<ProcessCommentViewModel> commentList,
                                      ProcessCommentClick.OnItemClickListener listener, String tabId) {
        this.commentList = commentList;
        this.listener = listener;
        this.tabId = tabId;
    }

    @Override
    public int getItemCount() {
        return this.commentList.size();
    }

    @Override
    public ProcessCommentsListAdapter.ProcessCommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_process_comment, parent, false);
        return new ProcessCommentViewHolder(row);
    }

    @Override
    public void onBindViewHolder(ProcessCommentsListAdapter.ProcessCommentViewHolder holder, int position) {
        ProcessCommentViewModel comment = commentList.get(position);

        Picasso.with(holder.context)
                .load(Constants.BASE_URL + comment.getProfilePicture())
                .into(holder.civRowProcessCommentAvatar);

        holder.tvRowProcessCommentProfileName.setText(comment.getDisplayName());
        holder.tvRowProcessCommentDate.setText(Constants.getFormattedDateTime(comment.getMsgTime()));

        if (comment.getReplyTo() != null) {
            holder.tvRowProcessCommentReplyTo.setText("");
            int replyId = comment.getReplyTo();
            for (ProcessCommentViewModel commentViewModel: commentList) {
                if (commentViewModel.getId() == replyId) {
                    holder.tvRowProcessCommentReplyTo.setText("Ответ на сообщение " + commentViewModel.getDisplayName());
                    break;
                }
            }
            if (holder.tvRowProcessCommentReplyTo.getText().length() == 0)
                holder.llRowProcessCommentReplyTo.setVisibility(View.GONE);
            else
                holder.llRowProcessCommentReplyTo.setVisibility(View.VISIBLE);

        } else {
            holder.llRowProcessCommentReplyTo.setVisibility(View.GONE);
        }

        Document commentTextDoc = Jsoup.parse(comment.getText());
        Elements links = commentTextDoc.getElementsByTag("a");
        holder.tvRowProcessCommentText.setText(commentTextDoc.text());

        holder.llRowProcessCommentFiles.removeAllViews();
        for (Element link: links) {
            if (!link.attr("href").contains("GetFile"))
                continue;
            View view = null;
                view = LayoutInflater.from(holder.context).inflate(R.layout.item_process_comment_file,
                        holder.llRowProcessCommentFiles, false);
                ProcessCommentFileViewHolder fileHolder = new ProcessCommentFileViewHolder(view,
                        link.text(), link.attr("href"));

            holder.llRowProcessCommentFiles.addView(view);
        }

        if (comment.isHasAudioMessage()) {
            View view = LayoutInflater.from(holder.context).inflate(R.layout.audio_file,
                    holder.llRowProcessCommentFiles, false);
            ProcessCommentAudioViewHolder audioHolder = new ProcessCommentAudioViewHolder(view, comment);
            holder.llRowProcessCommentFiles.addView(view);
            holder.tvRowProcessCommentText.setVisibility(View.GONE);
        } else {
            holder.tvRowProcessCommentText.setVisibility(View.VISIBLE);
        }

        holder.tvRowProcessCommentText.setClickable(true);
        holder.tvRowProcessCommentText.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public class ProcessCommentFileViewHolder {
        @BindView(R.id.tvProcessCommentFile)
        public TextView tvProcessCommentFile;

        @BindView(R.id.ivProcessCommentFile)
        public ImageView ivProcessCommentFile;

        @BindView(R.id.llProcessCommentFile)
        public LinearLayout llProcessCommentFile;

        public ProcessCommentFileViewHolder(final View view, final String name, final String link) {
            ButterKnife.bind(this, view);

            tvProcessCommentFile.setText(name.length() < 12 ? name : name.substring(0, 11) + "...");
            if (name.endsWith(".pdf"))
                ivProcessCommentFile.setImageResource(R.mipmap.ic_pdf);
            else if (name.endsWith(".avi") ||
                    name.endsWith(".3gp") ||
                    name.endsWith(".mp4") ||
                    name.endsWith(".wmv") || name.endsWith(".mpeg"))
                ivProcessCommentFile.setImageResource(R.mipmap.ic_video);
            else if (name.endsWith(".mp3") ||
                    name.endsWith(".amr"))
                ivProcessCommentFile.setImageResource(R.mipmap.ic_audio);

            Map<String, List<String>> queries = Constants.getQueryParams(link);
            String processId = "";
            String fileId = "";

            processId = queries.get("processId").get(0);
            fileId = queries.get("fileId").get(0);

            final String finalProcessId = processId;
            final String finalFileId = fileId;
            llProcessCommentFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!finalProcessId.isEmpty() && !finalFileId.isEmpty() && listener != null)
                        listener.onDownloadFileClick(name, finalProcessId, finalFileId);
                }
            });

        }


    }

    public class ProcessCommentAudioViewHolder {
        @BindView(R.id.llAudioFile)
        LinearLayout llAudioFile;

        @BindView(R.id.ivAudioFile)
        ImageView ivAudioFile;

        @BindView(R.id.aviAudioFile)
        AVLoadingIndicatorView avAudioFile;


        public View view;
        public ProcessCommentViewModel commentViewModel;

        public ProcessCommentAudioViewHolder(View view, ProcessCommentViewModel commentViewModel) {
            this.commentViewModel = commentViewModel;
            this.view = view;
            ButterKnife.bind(this, view);


            ivAudioFile.setImageResource(R.mipmap.ic_audio);
            llAudioFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    avAudioFile.setVisibility(View.VISIBLE);
                    avAudioFile.show();
                    ivAudioFile.setVisibility(View.GONE);
                    if (listener != null)
                        listener.onDownloadAudioClick(ProcessCommentAudioViewHolder.this);
                }
            });
        }

        public void showDefault() {
            avAudioFile.setVisibility(View.GONE);
            avAudioFile.hide();
            ivAudioFile.setVisibility(View.VISIBLE);
            ivAudioFile.setImageResource(R.mipmap.ic_audio);
        }


        public void showPlaying() {
            avAudioFile.setVisibility(View.GONE);
            avAudioFile.hide();
            ivAudioFile.setVisibility(View.VISIBLE);
            ivAudioFile.setImageResource(R.mipmap.ic_audio_stop);
        }

        public void showStopped() {
            avAudioFile.setVisibility(View.GONE);
            avAudioFile.hide();
            ivAudioFile.setVisibility(View.VISIBLE);
            ivAudioFile.setImageResource(R.mipmap.ic_audio_play);
        }
    }


    public class ProcessCommentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        Context context;

        @BindView(R.id.civRowProcessCommentAvatar)
        CircleImageView civRowProcessCommentAvatar;

        @BindView(R.id.tvRowProcessCommentProfileName)
        TextView tvRowProcessCommentProfileName;

        @BindView(R.id.tvRowProcessCommentDate)
        TextView tvRowProcessCommentDate;

        @BindView(R.id.tvRowProcessCommentText)
        TextView tvRowProcessCommentText;

        @BindView(R.id.llRowProcessCommentFiles)
        LinearLayout llRowProcessCommentFiles;

        @BindView(R.id.llRowProcessCommentReplyTo)
        LinearLayout llRowProcessCommentReplyTo;

        @BindView(R.id.tvRowProcessCommentReplyTo)
        TextView tvRowProcessCommentReplyTo;

        public ProcessCommentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.onCommentClick(getAdapterPosition());
        }
    }
}
