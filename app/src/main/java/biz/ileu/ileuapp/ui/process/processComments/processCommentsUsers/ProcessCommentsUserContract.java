package biz.ileu.ileuapp.ui.process.processComments.processCommentsUsers;

/**
 * Created by macair on 09.08.17.
 */

public interface ProcessCommentsUserContract {
    public interface ProcessCommentsUserView {
        public void successRemoved();
        public void errorRemove(String e);
    }
}
