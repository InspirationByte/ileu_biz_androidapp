package biz.ileu.ileuapp.ui.company;

import java.util.List;

import biz.ileu.ileuapi.models.company.CompanyList;
import biz.ileu.ileuapi.models.company.CompanyViewModel;
import biz.ileu.ileuapi.models.news.NewsCommentViewModel;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by macair on 06.08.17.
 */

public interface CompaniesContract {

    interface CompaniesView extends BaseView {
        void loadCompaniesErrror(String error);

        void companiesLoaded(List<CompanyViewModel> companies);
        void successJoined();
        void joinError(String error);

    }
    interface CompaniesPresenter extends BasePresenter {

    }
}
