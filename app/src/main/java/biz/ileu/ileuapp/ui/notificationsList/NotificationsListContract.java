package biz.ileu.ileuapp.ui.notificationsList;

import java.util.List;

import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.process.ProcessNotificationViewModel;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by Daniq on 05.05.2017.
 */

public interface NotificationsListContract {
    interface NotificationsListView extends BaseView{
        void loadNotificationsError(String error);

        void notificationsLoaded(List<ProcessNotificationViewModel> notificationList);

        void markNotificationError(String error);

        void notificationIsMarked(int notificationPosition);

        void loadingProcess();
        void loadingProcessError(String error);
        void processLoaded(Process process);
    }

    interface NotificationsListPresenter extends BasePresenter{

    }
}
