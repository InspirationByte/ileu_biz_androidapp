package biz.ileu.ileuapp.ui.process.processComments;

import java.util.HashSet;
import java.util.Set;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.process.EventEndDateLogViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.utils.PrefUtils;
import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by macair on 09.08.17.
 */

public class ProcessDetailViewPresenter {

    private ProcessDetailViewContract.ProcessDetailView processDetailView;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private Process currentProcess;
    private CompanyWorkInfoListViewModel currentCompany;

    Set<Integer> usersSet;


    public ProcessDetailViewPresenter(ProcessDetailViewContract.ProcessDetailView processDetailView, Process currentProcess, CompanyWorkInfoListViewModel currentCompany) {
        this.processDetailView = processDetailView;
        this.currentProcess = currentProcess;
        this.currentCompany = currentCompany;

        usersSet = new HashSet<>();

    }


    public void processCompleted() {
        Subscription subscription = RestClient.request()
                .finishProcess(PrefUtils.getToken(), currentProcess.getId(), currentCompany.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processDetailView.failedResult(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        processDetailView.successChanged(responseBody);
                    }
                });

        compositeSubscription.add(subscription);
    }


    public void processFinishNegative(String reason) {
        Subscription subscription = RestClient.request()
                .completeNegative(PrefUtils.getToken(), currentProcess.getId(), reason, currentCompany.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processDetailView.failedResult(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        processDetailView.successChanged(responseBody);
                    }
                });

        compositeSubscription.add(subscription);
    }
    public void processFinishPositive() {
        Subscription subscription = RestClient.request()
                .completePositive(PrefUtils.getToken(), currentProcess.getId(), currentCompany.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processDetailView.failedResult(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        processDetailView.successChanged(responseBody);
                    }
                });

        compositeSubscription.add(subscription);
    }
    public void processAddToCart() {
        Subscription subscription = RestClient.request()
                .recycleProcess(PrefUtils.getToken(), currentProcess.getId(), currentCompany.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processDetailView.failedResult(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        processDetailView.successChanged(responseBody);
                    }
                });

        compositeSubscription.add(subscription);
    }
    public void processChangeDate(String date) {
        Subscription subscription = RestClient.request()
                .saveEndDateTime(PrefUtils.getToken(),date, currentProcess.getId(), currentCompany.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<EventEndDateLogViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processDetailView.failedResult(e.getMessage());
                    }

                    @Override
                    public void onNext(EventEndDateLogViewModel eventEndDateLogViewModel) {
                        processDetailView.successChanged(null);
                    }
                });

        compositeSubscription.add(subscription);
    }


}
