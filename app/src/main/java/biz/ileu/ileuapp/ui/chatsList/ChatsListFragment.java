package biz.ileu.ileuapp.ui.chatsList;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import biz.ileu.ileuapi.models.chat.ChatMessageViewModel;
import biz.ileu.ileuapi.models.chat.ChatUsersViewModel;
import biz.ileu.ileuapi.models.chat.ChatsViewModel;
import biz.ileu.ileuapi.models.chat.MessageFromDto;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.chatsList.chatCreate.ChatCreateParticipantsSelectActivity;
import biz.ileu.ileuapp.ui.chatsList.chatMessages.ChatMessagesActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.cicerone.Screen;
import biz.ileu.ileuapp.utils.events.CompanyCountersChangedEvent;
import biz.ileu.ileuapp.utils.ui.BaseFragment;
import biz.ileu.ileuapp.utils.ui.EndlessRecyclerOnScrollListener;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatsListFragment extends BaseFragment implements ChatsListContract.ChatsListView,
        RecyclerViewClick.OnItemClickListener, SearchView.OnQueryTextListener {


    @BindView(R.id.rlChats)
    RecyclerView rlChats;
    @BindView(R.id.srlChats)
    SwipeRefreshLayout srlChats;
    Unbinder unbinder;
    @BindView(R.id.fabChatCreate)
    FloatingActionButton fabChatCreate;
    private CompanyWorkInfoListViewModel currentCompany = null;

    private ChatsListPresenter presenter;

    private EndlessRecyclerOnScrollListener listener;
    private List<ChatsViewModel> list;
    private List<ChatsViewModel> adapterList;
    private List<List<ChatMessageViewModel>> allMessagesList;
    private ChatsListAdapter adapter;

    public ChatsListFragment() {
        // Required empty public constructor
    }

    public static ChatsListFragment newInstance() {
        return new ChatsListFragment();
    }

    public static ChatsListFragment newInstance(Bundle bundle) {
        ChatsListFragment fragment = new ChatsListFragment();
        Bundle args = new Bundle();
        args.putAll(bundle);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chats_list, container, false);

        if (getArguments() != null && getArguments().containsKey(Constants.ENTITY_COMPANY))
            currentCompany = Parcels.unwrap(getArguments().getParcelable(Constants.ENTITY_COMPANY));

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews();
        setData();
    }

    @Override
    public void bindViews() {
        getActionBar().setTitle(getResources().getString(R.string.chats));
    }

    @Override
    public void setData() {
        if (currentCompany == null)
            return;

        presenter = new ChatsListPresenter(this, currentCompany);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rlChats.setLayoutManager(layoutManager);

        list = new ArrayList<>();
        adapterList = new ArrayList<>();
        allMessagesList = new ArrayList<>();

        adapter = new ChatsListAdapter(list, this);
        rlChats.setAdapter(adapter);
        adapter.notifyDataSetChanged();
//
//        listener = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
//            @Override
//            public void onLoadMore(int current_page) {
//
//                presenter.getChats("", current_page, Constants.DEFAULT_PAGE_SIZE);
//            }
//        };
//
//        rlChats.addOnScrollListener(listener);

        srlChats.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                list.clear();
                adapter.notifyDataSetChanged();
                srlChats.setRefreshing(true);
                presenter.getChats("", 1, Constants.DEFAULT_PAGE_MAX_SIZE);
//                listener.reset();
            }
        });
    }


    @Override
    public void chatsLoading() {
//        list.clear();
//        adapter.notifyDataSetChanged();
        if (!srlChats.isRefreshing())
            srlChats.setRefreshing(true);
    }

    @Override
    public void loadChatsError(String error) {
        Toast.makeText(getActivity(), R.string.server_error, Toast.LENGTH_LONG).show();
        srlChats.setRefreshing(false);
    }

    @Override
    public void chatsLoaded(List<ChatsViewModel> chatsList) {
        Log.d("CHATS_LIST", chatsList.size() + "");
        srlChats.setRefreshing(false);
        list.clear();
        list.addAll(chatsList);
        adapterList.clear();
        adapterList.addAll(chatsList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void chatLoaded(ChatsViewModel chat) {
        if (list == null)
            return;

        for (int i = 0; i < list.size(); i++) {
            if (chat.getId() == list.get(i).getId()) {
                list.set(i, chat);
                adapter.notifyItemChanged(i);
            }
        }
    }

    @Override
    public String getScreenName() {
        return Screen.SCREEN_CHATS_LIST;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search, menu);
        MenuItem searchItem = menu.findItem(R.id.actionSearch);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(this);

        MenuItemCompat.collapseActionView(searchItem);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase(Locale.getDefault());
//        presenter.getChats(newText, 1, Constants.DEFAULT_PAGE_MAX_SIZE);
        List<ChatsViewModel> filteredList = new ArrayList<>();
        for (ChatsViewModel chat : this.adapterList)
            for ( ChatUsersViewModel user : chat.getChatUsers()) {
                if (user.getDisplayName().toLowerCase(Locale.getDefault()).contains(newText)) {
                    filteredList.add(chat);
                    break;
                }
            }


        adapter.changeDisplayMessages(filteredList);
        adapter.notifyDataSetChanged();
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {

        Intent intent = new Intent(getActivity(), ChatMessagesActivity.class);
        intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
        intent.putExtra(Constants.ENTITY_CHAT, Parcels.wrap(adapter.getChatsList().get(position)));


        // When we open chat we should notify to all other fragments and activites that number of unread messages of this chat is not zero
        CompanyCountersChangedEvent event = new CompanyCountersChangedEvent();
        event.companyId = currentCompany.getId();
        event.unreadChatMessages = currentCompany.getNumUnreadChatMessages() - list.get(position).getNotificationsCount() > 0 ?
                currentCompany.getNumUnreadChatMessages() - list.get(position).getNotificationsCount() : 0;
        event.unreadProcessComments = currentCompany.getNumUnreadProcessComments();
        EventBus.getDefault().post(event);

        adapter.getChatsList().get(position).setNotificationsCount(0);
        adapter.notifyItemChanged(position);


        startActivity(intent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageFromDto message) {
        if (list == null)
            return;
        presenter.getChat(message.getChatId());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProcessCountersChangedEvent(CompanyCountersChangedEvent event) {
        if (currentCompany.getId() == event.companyId) {
            currentCompany.setNumUnreadProcesses(event.unreadProcessComments);
            currentCompany.setNumUnreadChatMessages(event.unreadChatMessages);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        srlChats.post(new Runnable() {
            @Override
            public void run() {
                srlChats.setRefreshing(true);
                presenter.getChats("", 1, Constants.DEFAULT_PAGE_SIZE);
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }



    @OnClick(R.id.fabChatCreate)
    public void openChatCreateParticipants() {
        Intent intent = new Intent(getActivity(), ChatCreateParticipantsSelectActivity.class);
        intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
        startActivity(intent);
    }



}
