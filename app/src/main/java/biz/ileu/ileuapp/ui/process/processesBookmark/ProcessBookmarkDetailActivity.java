package biz.ileu.ileuapp.ui.process.processesBookmark;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import org.parceler.Parcels;

import biz.ileu.ileuapi.models.bookmarks.BookmarkCategoryViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.menu.MenuActivity;
import biz.ileu.ileuapp.ui.process.processesList.ProcessesListFragment;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import biz.ileu.ileuapp.utils.ui.BaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProcessBookmarkDetailActivity extends BaseActivity implements SearchView.OnQueryTextListener {


    private CompanyWorkInfoListViewModel currentCompany = null;
    private BookmarkCategoryViewModel bookmarkCategory = null;
    @BindView(R.id.toolbar_process)
    Toolbar toolbar;

    @BindView(R.id.tvToolbarProcessTitle)
    TextView tvToolbarTitle;
    @BindView(R.id.tvToolbarProcessSubtitle)
    TextView tvToolbarSubtitle;
//
//    @BindView(R.id.toolbar)
//    Toolbar toolbar;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark_detail);
        ButterKnife.bind(this);

        bindViews();
        setData();


        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout

    }


    @Override
    public void bindViews() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvToolbarTitle.setText(R.string.process_bookmarks);
        tvToolbarSubtitle.setText("");
    }

    @Override
    public void setData() {
        bookmarkCategory = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_BOOKMARK_CATEGORY));
        currentCompany = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_COMPANY));
        Bundle args2 = new Bundle();
        args2.putParcelable(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
        args2.putParcelable(Constants.ENTITY_BOOKMARK_CATEGORY, Parcels.wrap(bookmarkCategory));
        args2.putString(Constants.PROCESS_FILTER, Constants.PROCESSES_FILTER_HASBOOKMARK);
        args2.putString(Constants.PROCESS_SORTING, Constants.PROCESS_SORTING_OUTDATED);

        // Create a new Fragment to be placed in the activity layout

        // Add the fragment to the 'fragment_container' FrameLayout
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragmentMainContainer, ProcessesListFragment.newInstance(args2)).commit();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}