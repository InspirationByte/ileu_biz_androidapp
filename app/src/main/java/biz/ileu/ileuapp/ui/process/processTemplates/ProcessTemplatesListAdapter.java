package biz.ileu.ileuapp.ui.process.processTemplates;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import biz.ileu.ileuapi.models.processtypes.ProcessTemplateViewModel;
import biz.ileu.ileuapp.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Daniq on 16.04.2017.
 */

public class ProcessTemplatesListAdapter extends RecyclerView.Adapter<ProcessTemplatesListAdapter.TemplateSectionViewHolder> {



    private List<ProcessTemplateListSection> sectionList;

    private TemplateListClick.OnTemplateNameClickListener listener;

    public ProcessTemplatesListAdapter(List<ProcessTemplateListSection> sectionList,
                                       TemplateListClick.OnTemplateNameClickListener listener) {
        this.sectionList = sectionList;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return sectionList.size();
    }

    @Override
    public TemplateSectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_process_template_type, parent, false);
        return new TemplateSectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TemplateSectionViewHolder holder, int position) {
        ProcessTemplateListSection section = sectionList.get(position);

        holder.tvRowItemProcessTemplateType.setText(section.type.getTitle());

        holder.llRowItemProcessTemplateType.removeAllViews();
        for (int i = 0; i < section.templates.size(); i++) {
            View view = LayoutInflater.from(holder.context).inflate(R.layout.row_item_process_template_name, null, false);
            TemplateNameViewHolder nameViewHolder = new TemplateNameViewHolder(view, section.templates.get(i));
            holder.llRowItemProcessTemplateType.addView(view);
        }
    }

    public void changeDisplayTemplates(List<ProcessTemplateListSection> processTemplateListSections) {
        this.sectionList = processTemplateListSections;
        notifyDataSetChanged();
    }

    public class TemplateSectionViewHolder extends RecyclerView.ViewHolder {

        Context context;

        @BindView(R.id.tvRowItemProcessTemplateType)
        TextView tvRowItemProcessTemplateType;
        @BindView(R.id.llRowItemProcessTemplateType)
        LinearLayout llRowItemProcessTemplateType;

        public TemplateSectionViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }
    }

    public class TemplateNameViewHolder {
        @BindView(R.id.tvRowItemProcessTemplateName)
        TextView tvRowItemProcessTemplateName;

        @BindView(R.id.llRowItemProcessTemplateName)
        LinearLayout llRowItemProcessTemplateName;

        public TemplateNameViewHolder(View view, final ProcessTemplateViewModel template) {
            ButterKnife.bind(this, view);

            this.tvRowItemProcessTemplateName.setText(template.getTitle());

            this.llRowItemProcessTemplateName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onTemplateClick(template);
                }
            });
        }
    }
}
