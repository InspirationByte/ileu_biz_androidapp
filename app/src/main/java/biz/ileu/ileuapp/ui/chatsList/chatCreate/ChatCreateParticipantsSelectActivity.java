package biz.ileu.ileuapp.ui.chatsList.chatCreate;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.chat.ChatsViewModel;
import biz.ileu.ileuapi.models.process.EmployeeList;
import biz.ileu.ileuapi.models.process.EmployeeViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.chatsList.chatMessages.ChatMessagesActivity;
import biz.ileu.ileuapp.ui.process.processTemplates.processCreate.processParticipants.ProcessCreateParticipantsAddActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import biz.ileu.ileuapp.utils.ui.EndlessRecyclerOnScrollListener;
import biz.ileu.ileuapp.utils.ui.ParticipantsCheckClick;
import biz.ileu.ileuapp.utils.ui.adapters.ParticipantAddAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ChatCreateParticipantsSelectActivity extends BaseActivity
        implements ParticipantsCheckClick.OnParticipantPositionCheckedUncheckedListener {

    @BindView(R.id.rlParticipants)
    RecyclerView rlParticipants;
    @BindView(R.id.srlChatCreateParticipantSelect)
    SwipeRefreshLayout srlChatCreateParticipantSelect;
    private CompanyWorkInfoListViewModel currentCompany;

    private List<Object> list;
    private ParticipantAddAdapter adapter;
    private EndlessRecyclerOnScrollListener listener;

    private CompositeSubscription compositeSubscription;

    private List<EmployeeViewModel> selectedEmployees;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_create_participants_select);
        ButterKnife.bind(this);
        bindViews();
        setData();
    }

    @Override
    public void bindViews() {
        initBaseToolbar(true);
        getSupportActionBar().setTitle(R.string.chats_available_contacts);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void setData() {
        compositeSubscription = new CompositeSubscription();

        currentCompany = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_COMPANY));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        list = new ArrayList<>();
        selectedEmployees = new ArrayList<>();
        adapter = new ParticipantAddAdapter(list, this);
        rlParticipants.setLayoutManager(layoutManager);
        rlParticipants.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        listener = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                request(current_page, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        };

        srlChatCreateParticipantSelect.post(new Runnable() {
            @Override
            public void run() {
                srlChatCreateParticipantSelect.setRefreshing(true);
                request(1, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        });

        srlChatCreateParticipantSelect.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                list.clear();
                listener.reset();
                adapter.notifyDataSetChanged();
                request(1, Constants.DEFAULT_PAGE_MAX_SIZE);

                srlChatCreateParticipantSelect.setRefreshing(true);
            }
        });
    }

    private void request(int page, int per) {
        Subscription subscription = RestClient.request()
                .getCompanyEmployees(PrefUtils.getToken(), currentCompany.getId(), page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<EmployeeList>() {
                    @Override
                    public void onCompleted() {
                        srlChatCreateParticipantSelect.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        srlChatCreateParticipantSelect.setRefreshing(false);
                        e.printStackTrace();
                        Toast.makeText(ChatCreateParticipantsSelectActivity.this, R.string.server_error, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onNext(EmployeeList employeeList) {
                        list.addAll(employeeList.getEmployeeViewModelList());
                        adapter.notifyDataSetChanged();
                        srlChatCreateParticipantSelect.setRefreshing(false);
                    }
                });

        compositeSubscription.add(subscription);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.participants_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.actionAddParticipants) {
            createChat();
        }

        return super.onOptionsItemSelected(item);
    }

    private void createChat() {
        if (selectedEmployees.size() == 0) {
            Toast.makeText(this, R.string.message_chat_create_select_participant, Toast.LENGTH_SHORT).show();
            return;
        }

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.chat_creating);

        int []userIds = new int[selectedEmployees.size()];
        for (int i = 0; i < selectedEmployees.size(); i++)
            userIds[i] = selectedEmployees.get(i).getId();

        Subscription subscription = RestClient.request()
                .createChat(PrefUtils.getToken(), currentCompany.getId(), userIds)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ChatsViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(ChatCreateParticipantsSelectActivity.this, R.string.message_chat_create_error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(ChatsViewModel chatsViewModel) {
                        progressDialog.dismiss();
                        Intent intent = new Intent(ChatCreateParticipantsSelectActivity.this, ChatMessagesActivity.class);
                        intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
                        intent.putExtra(Constants.ENTITY_CHAT, Parcels.wrap(chatsViewModel));

                        startActivity(intent);
                        finish();
                    }
                });

        compositeSubscription.add(subscription);
    }

    @Override
    public void OnPositionChecked(View v, int position) {
        selectedEmployees.add((EmployeeViewModel) list.get(position));
    }

    @Override
    public void OnPositionUnchecked(View v, int position) {
        selectedEmployees.remove(list.get(position));
    }
}
