package biz.ileu.ileuapp.ui.process.processComments;

/**
 * Created by Daniq on 04.05.2017.
 */

public interface ProcessCommentClick {
    interface OnItemClickListener {

        void onCommentClick(int position);



        void onDownloadFileClick(String fileName, String processId, String fileId);

        void onDownloadAudioClick(ProcessCommentsListAdapter.ProcessCommentAudioViewHolder audioViewHolder);
    }
}
