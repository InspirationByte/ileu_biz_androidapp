package biz.ileu.ileuapp.ui.login;

import biz.ileu.ileuapi.models.profile.ProfileInfo;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by Murager on 4/3/17.
 */

public interface LoginContract {

    interface LoginView extends BaseView {

        void loginLoading();

        void loginError(String error);

        void loginData(String token);

        void profileLoading();

        void profileError(String error);

        void profileLoaded(ProfileInfo profileInfo);

        void firebaseTokenLoading();

        void firebaseTokenIsSent();

    }

    interface LoginPresenter extends BasePresenter {

    }
}
