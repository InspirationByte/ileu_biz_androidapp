package biz.ileu.ileuapp.ui.process;

import biz.ileu.ileuapi.models.bookmarks.BookmarkCategoryViewModel;
import biz.ileu.ileuapi.models.process.ProcessUserStatsViewModel;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by Daniq on 07.06.2017.
 */

public interface ProcessContainerContract {

    interface ProcessContainerView extends BaseView {
        void processStatsLoaded(ProcessUserStatsViewModel processUserStatsViewModel);

        void categoryCreated(BookmarkCategoryViewModel categoryViewModel);
    }

    interface ProcessContainerPresenter extends BasePresenter {

    }

}
