package biz.ileu.ileuapp.ui.process.processComments;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.news.FileInfoViewModel;
import biz.ileu.ileuapi.models.process.CommentList;
import biz.ileu.ileuapi.models.process.CommentReadStateUpdatedViewModel;
import biz.ileu.ileuapi.models.process.DocumentAgreedByViewModel;
import biz.ileu.ileuapi.models.process.DocumentViewModel;
import biz.ileu.ileuapi.models.process.ParticipantViewModel;
import biz.ileu.ileuapi.models.process.PostCommentViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.process.ProcessCommentViewModel;
import biz.ileu.ileuapi.models.process.ProcessFileList;
import biz.ileu.ileuapi.models.process.ProcessFileViewModel;
import biz.ileu.ileuapi.models.processtypes.ProcessTemplateViewModel;
import biz.ileu.ileuapi.models.tables.DSResultWithTypeOfProcessRowDTO;
import biz.ileu.ileuapi.models.tables.ProcessRowDTO;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapi.models.usercontext.ContextInfoDTO;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.ui.attachedFile.AttachedFile;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniq on 13.04.2017.
 */

public class ProcessCommentsPresenter implements ProcessCommentsContract.ProcessCommentsPresenter{

    private ProcessCommentsContract.ProcessCommentsView processCommentsView;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private Process currentProcess;
    private CompanyWorkInfoListViewModel currentCompany;
    private ProcessCommentsListAdapter.ProcessCommentAudioViewHolder currentAudioViewHolder;

    MediaRecorder mediaRecorder;
    MediaPlayer mediaPlayer;
    File recordingAudioFile;

    Set<Integer> usersSet;


    public ProcessCommentsPresenter(ProcessCommentsContract.ProcessCommentsView processCommentsView, CompanyWorkInfoListViewModel currentCompany, Process currentProcess) {
        this.processCommentsView = processCommentsView;
        this.currentProcess = currentProcess;
        this.currentCompany = currentCompany;

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (currentAudioViewHolder != null)
                    currentAudioViewHolder.showStopped();
            }
        });

        usersSet = new HashSet<>();

        if (currentProcess.getCreator().getId() != PrefUtils.getCurrentUserId())
            usersSet.add(currentProcess.getCreator().getId());

        if (currentProcess.getExecutor().getId() != PrefUtils.getCurrentUserId())
            usersSet.add(currentProcess.getExecutor().getId());

        if (currentProcess.getResponsible().getId() != PrefUtils.getCurrentUserId())
            usersSet.add(currentProcess.getResponsible().getId());

        for (ParticipantViewModel participant: currentProcess.getParticipants())
            if (participant.getId() != PrefUtils.getCurrentUserId())
                usersSet.add(participant.getId());
    }

    public void getComments(int page, int per, final String tabId) {
        Subscription subscription = RestClient.request()
                .getProcessComments(PrefUtils.getToken(), currentCompany.getId(), currentProcess.getId(), page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CommentList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.loadCommentsError(e.getMessage());
                    }

                    @Override
                    public void onNext(CommentList commentList) {
                        processCommentsView.commentsLoaded(commentList.getProcessCommentViewModels(), tabId);
                    }
                });

        compositeSubscription.add(subscription);
    }
    public void getCompanyContext() {
        Subscription subscription = RestClient.request()
                .getCompanyContext(PrefUtils.getToken())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ContextInfoDTO>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.loadCommentsError(e.getMessage());
                    }

                    @Override
                    public void onNext(ContextInfoDTO contextInfoDTO) {
                        processCommentsView.companyContextLoaded(contextInfoDTO);
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void markCommentsAsRead() {
        Subscription subscription = RestClient.request()
                .markMessagesRead(PrefUtils.getToken(), currentCompany.getId(), currentProcess.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<CommentReadStateUpdatedViewModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(List<CommentReadStateUpdatedViewModel> commentReadStateUpdatedViewModels) {

                    }
                });
    }

    public void agreeDocument() {
        Subscription subscription = RestClient.request()
                .agreeDocument(PrefUtils.getToken(), currentProcess.getId(), currentCompany.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DocumentAgreedByViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.agreedError(e.getMessage());
                    }

                    @Override
                    public void onNext(DocumentAgreedByViewModel agreed) {
                        processCommentsView.successAgreed(agreed);
                    }
                });

        compositeSubscription.add(subscription);
    }
    public void saveDocument(DocumentViewModel storeDocumentData) {
        Subscription subscription = RestClient.request()
                .storeDocumentData(PrefUtils.getToken(), currentProcess.getId(), currentCompany.getId(), storeDocumentData)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DocumentViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.documentStoreError(e.getMessage());
                    }

                    @Override
                    public void onNext(DocumentViewModel document) {
                        processCommentsView.documentStored(document);
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void sendComment(String commentStr, int replyTo, int[] userIdsToNotify) {
        PostCommentViewModel newComment = new PostCommentViewModel();
        newComment.setProcessId(currentProcess.getId());
        newComment.setText(commentStr);
        newComment.setReplyTo(replyTo == 0 ? null : replyTo);

        if (userIdsToNotify == null)
            newComment.setUsers(null);
        else {
            Integer[] integers = new Integer[userIdsToNotify.length];
            for (int i = 0; i < userIdsToNotify.length; i++) {
                integers[i] = userIdsToNotify[i];
            }

            newComment.setUsers(integers);
        }


        Subscription subscription = RestClient.request()
                .postComment(PrefUtils.getToken(), currentCompany.getId(), newComment)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessCommentViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.loadCommentsError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProcessCommentViewModel processCommentViewModel) {
                        processCommentsView.commentSent(processCommentViewModel);
                    }
                });

        processCommentsView.sendingComment();
        compositeSubscription.add(subscription);
    }

    public void sendCommentWithAttachments(final String commentStr, List<AttachedFile> attachedFiles, final int replyTo, final int[] userIdsToNotify) {
        List<MultipartBody.Part> filesParts = new ArrayList<>();
        for (AttachedFile attachedFile: attachedFiles) {
            File file = attachedFile.getFile();

            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(attachedFile.getExtention());

            RequestBody body = MultipartBody.create(MediaType.parse(mimeType), file);

            MultipartBody.Part filePart = MultipartBody.Part.createFormData(file.getName(), file.getName(), body);
            filesParts.add(filePart);

            try {
                Log.d("Files", filePart.body().contentLength() + "");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Observable<List<FileInfoViewModel>> observable = RestClient.request().doUpload(PrefUtils.getToken(), currentCompany.getId(), filesParts);


        Subscription subscription = observable.flatMap(new Func1<List<FileInfoViewModel>, Observable<List<ProcessFileViewModel>>>() {
            @Override
            public Observable<List<ProcessFileViewModel>> call(List<FileInfoViewModel> fileInfoViewModels) {

                List<Integer> ids = new ArrayList<>();
                for (FileInfoViewModel fileInfo: fileInfoViewModels) {
                    ids.add(fileInfo.getId());
                }

                Log.d("STEP", "ADD PROCESS");

                return RestClient.request().addProcessMultiFiles(PrefUtils.getToken(), currentCompany.getId(), currentProcess.getId(), ids);
            }
        }).flatMap(new Func1<List<ProcessFileViewModel>, Observable<ProcessCommentViewModel>>() {
            @Override
            public Observable<ProcessCommentViewModel> call(List<ProcessFileViewModel> processFileViewModels) {

                String sendingComment = commentStr + " ";
                Log.d("STEP", "PROCESS FILE VIEW MODELS " + processFileViewModels.size());
//                for (int i = 0; i < processFileViewModels.size(); i++) {
//                    Log.d("STEP", "HREF " +  processFileViewModels.get(i).getFileLink());
//                    sendingComment += "<a href=\"" + processFileViewModels.get(i).getFileLink() + "\"> " + processFileViewModels.get(i).getFileName() + "</a>";
//                    if (i != processFileViewModels.size() - 1)
//                        sendingComment += ", ";
//                }


                PostCommentViewModel postComment = new PostCommentViewModel();
                postComment.setText(sendingComment);
                postComment.setProcessId(currentProcess.getId());
                postComment.setReplyTo(replyTo == 0 ? null : replyTo);
                postComment.setUsers(usersSet.toArray(new Integer[usersSet.size()]));

                if (userIdsToNotify == null)
                    postComment.setUsers(null);
                else {
                    Integer[] integers = new Integer[userIdsToNotify.length];
                    for (int i = 0; i < userIdsToNotify.length; i++)
                        integers[0] = userIdsToNotify[i];
                    postComment.setUsers(integers);
                }


                return RestClient.request().postComment(PrefUtils.getToken(), currentCompany.getId(), postComment);
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessCommentViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.sendingCommentError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProcessCommentViewModel processCommentViewModel) {
                        processCommentsView.commentSent(processCommentViewModel);
                    }
                });

        processCommentsView.sendingComment();
        compositeSubscription.add(subscription);
    }

    public void sendAudioMessage(AttachedFile attachedFile, final int replyTo, final int[] userIdsToNotify) {
        File file = attachedFile.getFile();

        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(attachedFile.getExtention());

        RequestBody body = MultipartBody.create(MediaType.parse(mimeType), file);

        MultipartBody.Part filePart = MultipartBody.Part.createFormData(file.getName(), file.getName(), body);

        List<MultipartBody.Part> filesParts = new ArrayList<>();
        filesParts.add(filePart);

        Observable<List<FileInfoViewModel>> observable = RestClient.request().doUpload(PrefUtils.getToken(), currentCompany.getId(), filesParts);

        Subscription subscription = observable.flatMap(new Func1<List<FileInfoViewModel>, Observable<ProcessCommentViewModel>>() {
            @Override
            public Observable<ProcessCommentViewModel> call(List<FileInfoViewModel> fileInfoViewModels) {

                List<Integer> ids = new ArrayList<>();
                for (FileInfoViewModel fileInfo: fileInfoViewModels) {
                    ids.add(fileInfo.getId());
                }

                Log.d("STEP", "ADD PROCESS");

                PostCommentViewModel audioComment = new PostCommentViewModel();
                audioComment.setAudioUploadId(ids.get(0));
                audioComment.setReplyTo(replyTo == 0 ? null : replyTo);
                audioComment.setUsers(usersSet.toArray(new Integer[usersSet.size()]));
                audioComment.setProcessId(currentProcess.getId());

                if (userIdsToNotify == null)
                    audioComment.setUsers(null);
                else {
                    Integer[] integers = new Integer[userIdsToNotify.length];
                    for (int i = 0; i < userIdsToNotify.length; i++)
                        integers[0] = userIdsToNotify[i];
                    audioComment.setUsers(integers);
                }

                Log.d("UPLOAD_ID", audioComment.getAudioUploadId() + "");
                return RestClient.request().postComment(PrefUtils.getToken(), currentCompany.getId(), audioComment);
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessCommentViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.sendingCommentError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProcessCommentViewModel processCommentViewModel) {
                        processCommentsView.commentSent(processCommentViewModel);
                    }
                });
        processCommentsView.sendingComment();
        compositeSubscription.add(subscription);
    }

    public void getProcessNameTemplate(int processNameId) {
        Subscription subscription = RestClient.request().getProcessNameTemplate(PrefUtils.getToken(),
                currentCompany.getId(), this.currentProcess.getProcessNameId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessTemplateViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.loadFilesError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProcessTemplateViewModel processTemplate) {
                        processCommentsView.processNameTemplatedLoaded(processTemplate);
                    }
                });

        compositeSubscription.add(subscription);
    }
    public void removeRow(int rowId) {
        Subscription subscription = RestClient.request().tableDeleteRow(PrefUtils.getToken(),
                currentProcess.getId(), rowId, currentCompany.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessRowDTO>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.loadFilesError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProcessRowDTO row) {
                        processCommentsView.processRowRemoved();
                    }
                });

        compositeSubscription.add(subscription);
    }
    public void updateProcess(int companyId, int processId) {
        Subscription subscription = RestClient.request().getProcessData(PrefUtils.getToken(),
                companyId, processId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Process>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.loadFilesError(e.getMessage());
                    }

                    @Override
                    public void onNext(Process process) {
                        processCommentsView.processLoaded(process);
                    }
                });

        compositeSubscription.add(subscription);
    }
    public void getProcessFiles() {
        Subscription subscription = RestClient.request().getProcessFiles(PrefUtils.getToken(),
                currentProcess.getId(), currentCompany.getId(), 1, Constants.DEFAULT_PAGE_MAX_SIZE)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessFileList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.loadFilesError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProcessFileList processFileList) {
                        processCommentsView.filesLoaded(processFileList.getProcessFileViewModelList());
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void downloadAudio(final ProcessCommentsListAdapter.ProcessCommentAudioViewHolder audioViewHolder) {
        if (currentAudioViewHolder != null) {
            currentAudioViewHolder.showStopped();
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                if (currentAudioViewHolder == audioViewHolder)
                    return;
            }


        }

        currentAudioViewHolder = audioViewHolder;

        final Handler handler = new Handler(processCommentsView.getContext().getMainLooper());
        Subscription subscription = RestClient.request()
                .getCommentAudioFile(PrefUtils.getToken(), currentCompany.getId(), currentProcess.getId(),
                        currentAudioViewHolder.commentViewModel.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.downloadError(e.getMessage());
                        if (currentAudioViewHolder != null)
                            currentAudioViewHolder.showDefault();
                    }

                    @Override
                    public void onNext(final ResponseBody body) {
                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... params) {
                                try {
                                    final File downloadedFile = new File(processCommentsView
                                            .getContext()
                                            .getExternalFilesDir(null) + File.separator + Constants.FILES_PATH + File.separator + audioViewHolder.commentViewModel.getId());

                                    downloadedFile.getParentFile().mkdirs();
                                    if (!downloadedFile.exists())
                                        downloadedFile.createNewFile();

                                    InputStream inputStream = null;
                                    OutputStream outputStream = null;

                                    try {
                                        byte[] fileReader = new byte[4096];

                                        long fileSize = body.contentLength();
                                        long fileSizeDownloaded = 0;

                                        inputStream = body.byteStream();
                                        outputStream = new FileOutputStream(downloadedFile);

                                        while (true) {
                                            int read = inputStream.read(fileReader);

                                            if (read == -1) {
                                                break;
                                            }

                                            outputStream.write(fileReader, 0, read);

                                            fileSizeDownloaded += read;

                                            Log.d("DOWNLOAD", "file download: " + fileSizeDownloaded + " of " + fileSize);
                                        }

                                        outputStream.flush();
                                        Runnable runnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    mediaPlayer.reset();
                                                    mediaPlayer.setDataSource(downloadedFile.getPath());
                                                    mediaPlayer.prepare();
                                                    mediaPlayer.start();
                                                    currentAudioViewHolder.showPlaying();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                    processCommentsView.audioPlayError(e.getMessage());
                                                    currentAudioViewHolder.showDefault();
                                                }
                                            }
                                        };
                                        handler.post(runnable);
                                    } catch (final IOException e) {
                                        e.printStackTrace();
                                        Runnable runnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                processCommentsView.downloadError(e.getMessage());
                                                if (currentAudioViewHolder != null)
                                                    currentAudioViewHolder.showDefault();
                                            }
                                        };
                                        handler.post(runnable);
                                    } finally {
                                        if (inputStream != null) {
                                            inputStream.close();
                                        }

                                        if (outputStream != null) {
                                            outputStream.close();
                                        }
                                    }
                                } catch (final IOException e) {
                                    e.printStackTrace();
                                    Runnable runnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            processCommentsView.downloadError(e.getMessage());
                                            if (currentAudioViewHolder != null)
                                                currentAudioViewHolder.showDefault();
                                        }
                                    };
                                    handler.post(runnable);
                                }

                                return null;
                            }
                        }.execute();
                    }
                });
        compositeSubscription.add(subscription);

    }

    public void downloadFile(final String fileName, String processId, String fileId) {
        final Handler handler = new Handler(processCommentsView.getContext().getMainLooper());
        int file = Integer.parseInt(fileId);
        int process = Integer.parseInt(processId);
        Subscription subscription = RestClient.request()
                .getProcessFile(PrefUtils.getToken(), currentCompany.getId(), file, process, false)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.downloadError(e.getMessage());
                    }

                    @Override
                    public void onNext(final ResponseBody body) {
                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... params) {
                                try {
                                    final File downloadedFile = new File(processCommentsView
                                            .getContext()
                                            .getExternalFilesDir(null) + File.separator + Constants.FILES_PATH + File.separator + fileName);

                                    downloadedFile.getParentFile().mkdirs();
                                    if (!downloadedFile.exists())
                                        downloadedFile.createNewFile();

                                    InputStream inputStream = null;
                                    OutputStream outputStream = null;

                                    try {
                                        byte[] fileReader = new byte[4096];

                                        long fileSize = body.contentLength();
                                        long fileSizeDownloaded = 0;

                                        inputStream = body.byteStream();
                                        outputStream = new FileOutputStream(downloadedFile);

                                        while (true) {
                                            int read = inputStream.read(fileReader);

                                            if (read == -1) {
                                                break;
                                            }

                                            outputStream.write(fileReader, 0, read);

                                            fileSizeDownloaded += read;

                                            Log.d("DOWNLOAD", "file download: " + fileSizeDownloaded + " of " + fileSize);
                                        }

                                        outputStream.flush();
                                        Runnable runnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                processCommentsView.downloadCompleted(downloadedFile);
                                            }
                                        };
                                        handler.post(runnable);
                                    } catch (final IOException e) {
                                        e.printStackTrace();
                                        Runnable runnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                processCommentsView.downloadError(e.getMessage());
                                            }
                                        };
                                        handler.post(runnable);
                                    } finally {
                                        if (inputStream != null) {
                                            inputStream.close();
                                        }

                                        if (outputStream != null) {
                                            outputStream.close();
                                        }
                                    }
                                } catch (final IOException e) {
                                    e.printStackTrace();
                                    Runnable runnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            processCommentsView.downloadError(e.getMessage());
                                        }
                                    };
                                    handler.post(runnable);
                                }

                                return null;
                            }
                        }.execute();
                    }
                });

        processCommentsView.downloadingFile();
        compositeSubscription.add(subscription);
    }

    public void startRecording() {
        try {
            setMediaRecorder();
            mediaRecorder.prepare();
            mediaRecorder.start();

        } catch (IOException e) {
            e.printStackTrace();
            processCommentsView.audioRecordError(e.getMessage());
        } catch (RuntimeException e) {
            e.printStackTrace();
            processCommentsView.audioRecordError(e.getMessage());
        }
    }

    public void stopRecording(int seconds, int replyTo, int[] userIdsToNotify) {
        try {
            mediaRecorder.stop();
            File fileToRename = new File(processCommentsView
                            .getContext()
                            .getExternalFilesDir(null)
                    + File.separator + Constants.AUDIOS_PATH + File.separator
                    + "IleuAudioMessage_" + seconds + "sec_proc" + currentProcess.getId() + ".amr");
            recordingAudioFile.renameTo(fileToRename);

            AttachedFile attachedFile = new AttachedFile(fileToRename.getAbsolutePath());
            sendAudioMessage(attachedFile, replyTo, userIdsToNotify);
        } catch (IllegalStateException e) {
            e.printStackTrace();
            processCommentsView.audioRecordError(e.getMessage());
        } catch (RuntimeException e) {
            e.printStackTrace();
            processCommentsView.audioRecordError(e.getMessage());
        }
    }

    public void cancelRecording() {
        if (recordingAudioFile.exists())
            recordingAudioFile.delete();
    }

    private void setMediaRecorder() throws IOException {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.DEFAULT);

        recordingAudioFile = new File(processCommentsView
                .getContext()
                .getExternalFilesDir(null) + File.separator + Constants.AUDIOS_PATH + File.separator + "temp.mp3");

        recordingAudioFile.getParentFile().mkdirs();
        if (!recordingAudioFile.exists())
            recordingAudioFile.createNewFile();


        mediaRecorder.setOutputFile(recordingAudioFile.getPath());
    }

    private boolean writeResponseBodyToDisk(ResponseBody body, String fileName) {
        try {
            // todo change the file location/name according to your needs
            File downloadedFile = new File(processCommentsView
                    .getContext()
                    .getExternalFilesDir(null) + File.separator + Constants.FILES_PATH + File.separator + fileName);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(downloadedFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d("DOWNLOAD", "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }
    public void loadDocument() {
        Subscription subscription = RestClient.request().getDocumentData(PrefUtils.getToken(),
                currentProcess.getId(), currentCompany.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DocumentViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.loadDocumetsError(e.getMessage());
                    }

                    @Override
                    public void onNext(DocumentViewModel processFileList) {
                        processCommentsView.documentsLoaded(processFileList);
                    }
                });

        compositeSubscription.add(subscription);
    }
    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();

        processCommentsView = null;
    }

    public void getRows(int page, int defaultPageMaxSize) {
        Subscription subscription = RestClient.request().tableGetRows(PrefUtils.getToken(),
                currentProcess.getId(), currentCompany.getId(), page, defaultPageMaxSize)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DSResultWithTypeOfProcessRowDTO>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.loadRowsError(e.getMessage());
                    }

                    @Override
                    public void onNext(DSResultWithTypeOfProcessRowDTO dsResult) {
                        processCommentsView.rowsLoaded(dsResult);
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void createRow(ProcessRowDTO row) {
        Subscription subscription = RestClient.request().tableAddRow(PrefUtils.getToken(),
                currentProcess.getId(), currentCompany.getId(), row.getColumnValues())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessRowDTO>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.loadRowsError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProcessRowDTO dsResult) {
                        processCommentsView.createdRow(dsResult);
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void updateRow(ProcessRowDTO row) {
        Subscription subscription = RestClient.request().tableUpdateRow(PrefUtils.getToken(),
                currentProcess.getId(), row.getId(), currentCompany.getId(), row.getColumnValues())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessRowDTO>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processCommentsView.loadRowsError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProcessRowDTO dsResult) {
                        processCommentsView.updatedRow(dsResult);
                    }
                });

        compositeSubscription.add(subscription);
    }

//    public void agreeTable() {
//        Subscription subscription = RestClient.request()
//                .agreeTable(PrefUtils.getToken(), currentProcess.getId(), currentCompany.getId())
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Subscriber<DocumentAgreedByViewModel>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        e.printStackTrace();
//                        processCommentsView.agreedError(e.getMessage());
//                    }
//
//                    @Override
//                    public void onNext(DocumentAgreedByViewModel agreed) {
//                        processCommentsView.successAgreed(agreed);
//                    }
//                });
//
//        compositeSubscription.add(subscription);
//    }
}
