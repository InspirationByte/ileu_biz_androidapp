package biz.ileu.ileuapp.ui.newsList.news;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.news.NewsCommentList;
import biz.ileu.ileuapi.models.news.NewsCommentViewModel;
import biz.ileu.ileuapi.models.news.NewsEntryViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniq on 16.04.2017.
 */

public class NewsPresenter implements NewsContract.NewsPresenter {

    private NewsContract.NewsView newsView;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private NewsEntryViewModel currentNews;

    public NewsPresenter(NewsContract.NewsView newsView, NewsEntryViewModel currentNews) {
        this.newsView = newsView;
        this.currentNews = currentNews;
    }

    public void getComments(int page, int per) {
        Subscription subscription = RestClient.request()
                .getEntryComments(PrefUtils.getToken(), currentNews.getId(), page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<NewsCommentList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        newsView.loadCommentsError(e.getMessage());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(NewsCommentList newsCommentList) {
                        newsView.commentsLoaded(newsCommentList.getNewsCommentViewModelList());
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void sendComment(String comment) {
        Subscription subscription = RestClient.request()
                .postComment(PrefUtils.getToken(), currentNews.getId(), comment.toString())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<NewsCommentViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        newsView.sendingCommentError(e.getMessage());
                    }

                    @Override
                    public void onNext(NewsCommentViewModel newsCommentViewModel) {
                        newsView.commentSent(newsCommentViewModel);
                    }
                });

        newsView.sendingComment();
        compositeSubscription.add(subscription);
    }


    public void downloadFile(final String fileName, String entryId, String fileId) {
        final Handler handler = new Handler(newsView.getContext().getMainLooper());
        int file = Integer.parseInt(fileId);
        int entry = Integer.parseInt(entryId);
        Subscription subscription = RestClient.request()
                .getNewsFile(PrefUtils.getToken(), file, entry, false)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        newsView.downloadError(e.getMessage());
                    }

                    @Override
                    public void onNext(final ResponseBody body) {
                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... params) {
                                try {
                                    final File downloadedFile = new File(newsView
                                            .getContext()
                                            .getExternalFilesDir(null) + File.separator + Constants.FILES_PATH + File.separator + fileName);

                                    downloadedFile.getParentFile().mkdirs();
                                    if (!downloadedFile.exists())
                                        downloadedFile.createNewFile();

                                    InputStream inputStream = null;
                                    OutputStream outputStream = null;

                                    try {
                                        byte[] fileReader = new byte[4096];

                                        long fileSize = body.contentLength();
                                        long fileSizeDownloaded = 0;

                                        inputStream = body.byteStream();
                                        outputStream = new FileOutputStream(downloadedFile);

                                        while (true) {
                                            int read = inputStream.read(fileReader);

                                            if (read == -1) {
                                                break;
                                            }

                                            outputStream.write(fileReader, 0, read);

                                            fileSizeDownloaded += read;

                                            Log.d("DOWNLOAD", "file download: " + fileSizeDownloaded + " of " + fileSize);
                                        }

                                        outputStream.flush();
                                        Runnable runnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                newsView.downloadCompleted(downloadedFile);
                                            }
                                        };
                                        handler.post(runnable);
                                    } catch (final IOException e) {
                                        e.printStackTrace();
                                        Runnable runnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                newsView.downloadError(e.getMessage());
                                            }
                                        };
                                        handler.post(runnable);
                                    } finally {
                                        if (inputStream != null) {
                                            inputStream.close();
                                        }

                                        if (outputStream != null) {
                                            outputStream.close();
                                        }
                                    }
                                } catch (final IOException e) {
                                    e.printStackTrace();
                                    Runnable runnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            newsView.downloadError(e.getMessage());
                                        }
                                    };
                                    handler.post(runnable);
                                }

                                return null;
                            }
                        }.execute();
                    }
                });

        newsView.downloadingFile();
        compositeSubscription.add(subscription);
    }

    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();
    }
}
