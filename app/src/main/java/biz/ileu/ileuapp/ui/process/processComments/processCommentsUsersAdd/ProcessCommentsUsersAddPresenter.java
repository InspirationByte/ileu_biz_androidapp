package biz.ileu.ileuapp.ui.process.processComments.processCommentsUsersAdd;

import android.util.Log;

import java.util.List;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.process.ParticipantViewModel;
import biz.ileu.ileuapi.models.process.UsersViewModel;
import biz.ileu.ileuapp.utils.PrefUtils;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by macair on 09.08.17.
 */

public class ProcessCommentsUsersAddPresenter {
    private ProcessCommentsUsersAddContract.ProcessCommentsUsersAddView userView;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public ProcessCommentsUsersAddPresenter(ProcessCommentsUsersAddContract.ProcessCommentsUsersAddView userView) {
        this.userView = userView;
    }

    public void getUsers(int companyId) {
        Subscription subscription = RestClient.request().getGroupedEmployees(PrefUtils.getToken(),
                companyId, false)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<UsersViewModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        userView.usersFail(e.getMessage());
                    }

                    @Override
                    public void onNext(List<UsersViewModel> users) {
                        userView.usersLoaded(users);
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void addUser(int userId, int processId,  int companyId, String message) {
        Subscription subscription = RestClient.request().addParticiapnt(PrefUtils.getToken(),
                userId, processId,companyId,  message)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ParticipantViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        userView.usersFail(e.getMessage());
                        Log.d("USER ADD ERROR", e.getMessage());
                    }

                    @Override
                    public void onNext(ParticipantViewModel users) {
                        Log.d("ParticipantViewModel:", String.valueOf(users.getId()));
                        Log.d("ParticipantViewModel:", String.valueOf(users.getDisplayName()));
                        userView.participantAdded(users);
                    }
                });

        compositeSubscription.add(subscription);
    }
}
