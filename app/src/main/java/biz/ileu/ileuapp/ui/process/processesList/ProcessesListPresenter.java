package biz.ileu.ileuapp.ui.process.processesList;

import java.util.List;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.bookmarks.BookmarkViewModel;
import biz.ileu.ileuapi.models.bookmarks.BookmarksCategoriesList;
import biz.ileu.ileuapi.models.process.ProcessList;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniq on 26.04.2017.
 */

public class ProcessesListPresenter implements ProcessesListContract.ProcessesListPresenter {

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private ProcessesListContract.ProcessesListView processesListView;

    private CompanyWorkInfoListViewModel currentCompany = null;
    private String filter;
    private String sorting;
    private int tabIndex;

    public ProcessesListPresenter(ProcessesListContract.ProcessesListView processesListView,
                                  CompanyWorkInfoListViewModel currentCompany,
                                  String filter, String sorting, int tabIndex) {
        this.processesListView = processesListView;
        this.currentCompany = currentCompany;
        this.filter = filter;
        this.sorting = sorting;
        this.tabIndex = tabIndex;

    }
    public void getProcesses(int page, int per, String additionalFilter, List<Integer> empIds, List<Integer> tempIds) {
        if (!additionalFilter.equals("")) getProcessBasic(page, per, filter + "~and~" + additionalFilter, empIds, tempIds);
        else getProcessBasic(page, per, filter, empIds, tempIds);
    }
    public void getProcesses(int page, int per) {
        String newFilter = filter;
        String addFilter = PrefUtils.getString(Constants.FILTER_STRING + tabIndex);
        if (!addFilter.isEmpty()) {
            newFilter += "~and~" + addFilter;
        }
        List<Integer> empIds = PrefUtils.getIntegerList(Constants.FILTER_EMP_IDS + tabIndex);
        List<Integer> tempIds = PrefUtils.getIntegerList(Constants.FILTER_TEMP_IDS + tabIndex);
        if (empIds != null && empIds.isEmpty())
            empIds = null;
        if (tempIds != null && tempIds.isEmpty())
            tempIds = null;

        getProcessBasic(page, per, newFilter, empIds, tempIds);
    }
    public void getProcessBasic(int page, int per, String fil, List<Integer> empIds, List<Integer> tempIds) {
        fil = "(" + fil + ")";
        Subscription subscription = RestClient.request()
                .getProcessesList(PrefUtils.getToken(), currentCompany.getId(), fil, sorting, page, per, empIds, tempIds)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processesListView.loadProcessesError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProcessList processList) {
                        processesListView.processesLoaded(processList.getProcessList());
                    }
                });
        processesListView.processesLoading();
        compositeSubscription.add(subscription);
    }
    public void getProgressWithCategory(int page, int per, int categoryId) {

        Subscription subscription = RestClient.request()
                .getBookmarkProcessesList(PrefUtils.getToken(), currentCompany.getId(),  categoryId, null, null, page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processesListView.loadProcessesError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProcessList processList) {
                        processesListView.processesLoaded(processList.getProcessList());
                    }
                });
        processesListView.processesLoading();
        compositeSubscription.add(subscription);
    }
    public void getBookmarkCategories(int page, int per) {

        Subscription subscription = RestClient.request()
                .getBookmarkCategories(PrefUtils.getToken(), currentCompany.getId(), page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BookmarksCategoriesList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processesListView.loadProcessesError(e.getMessage());
                    }

                    @Override
                    public void onNext(BookmarksCategoriesList bookmarksCategoriesList) {
                        processesListView.bookmarkCategoriesLoaded(bookmarksCategoriesList);
                    }
                });
        processesListView.processesLoading();
        compositeSubscription.add(subscription);
    }
    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();

        processesListView = null;
    }

    public void addBookmark(int categoryId, int processId) {
        Subscription subscription = RestClient.request()
                .addBookmark(PrefUtils.getToken(), currentCompany.getId(), categoryId, processId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BookmarkViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        processesListView.addBookmarkError(e.getMessage());
                    }

                    @Override
                    public void onNext(BookmarkViewModel bookmark) {
                        processesListView.addedBookmark(bookmark);
                    }
                });
        compositeSubscription.add(subscription);
    }
}
