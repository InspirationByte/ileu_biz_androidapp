package biz.ileu.ileuapp.ui.newsList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import biz.ileu.ileuapi.models.news.NewsEntryViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Daniq on 06.04.2017.
 */

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.NewsViewHolder>  {

    private List<NewsEntryViewModel> newsList;

    RecyclerViewClick.OnItemClickListener listener;

    public NewsListAdapter(List<NewsEntryViewModel> newsList, RecyclerViewClick.OnItemClickListener listener) {
        this.newsList = newsList;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_news, parent, false);
        return new NewsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        NewsEntryViewModel news = newsList.get(position);

        holder.tvRowNewsTitle.setText(news.getTitle());
        holder.tvRowNewsText.setText(Html.fromHtml(news.getText()));

        Picasso.with(holder.context)
                .load(Constants.BASE_URL + news.getAddedByAvatar())
                .into(holder.civRowNewsAvatar);

        holder.tvRowNewsAdderName.setText(news.getAddedByName());

        holder.tvRowNewsPublishedDate.setText(Constants.getFormattedDateTime(news.getPublicationTime()));


        holder.tvRowNewsCommentNum.setText(news.getNumComments() > 99 ? "+99" : news.getNumComments() + "");
    }



    public class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;

        @BindView(R.id.tvRowNewsTitle)
        TextView tvRowNewsTitle;

        @BindView(R.id.tvRowNewsText)
        TextView tvRowNewsText;

        @BindView(R.id.civRowNewsAvatar)
        CircleImageView civRowNewsAvatar;

        @BindView(R.id.tvRowNewsAdderName)
        TextView tvRowNewsAdderName;

        @BindView(R.id.tvRowNewsPublishedDate)
        TextView tvRowNewsPublishedDate;

        @BindView(R.id.tvRowNewsCommentsNum)
        TextView tvRowNewsCommentNum;

        public NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onItemClick(v, getAdapterPosition());
            }
        }
    }
}
