package biz.ileu.ileuapp.ui.chatsList;

import android.util.Log;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.chat.ChatList;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.utils.PrefUtils;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniq on 19.04.2017.
 */

public class ChatsListPresenter implements ChatsListContract.ChatsListPresenter {

    private ChatsListContract.ChatsListView chatsListView;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private CompanyWorkInfoListViewModel currentCompany;

    public ChatsListPresenter(ChatsListContract.ChatsListView chatsListView, CompanyWorkInfoListViewModel currentCompany) {
        this.chatsListView = chatsListView;
        this.currentCompany = currentCompany;
    }

    public void getChats(String searchText, int page, int per) {
        Log.d("CHAT_LIST_PAGE", "PAGE: " + page);
        Subscription subscription = RestClient.request()
                .getContactList(PrefUtils.getToken(), currentCompany.getId(), searchText, "LastMessageTime-desc", page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ChatList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        chatsListView.loadChatsError(e.getMessage());
                    }

                    @Override
                    public void onNext(ChatList chatList) {
                        chatsListView.chatsLoaded(chatList.getChatsViewModelList());
                    }
                });
        chatsListView.chatsLoading();
        compositeSubscription.add(subscription);
    }

    // Get single chat
    public void getChat(final int chatId) {
        Subscription subscription = RestClient.request()
                .getContact(PrefUtils.getToken(), currentCompany.getId(), "Id~eq~" + chatId, 1, 1)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ChatList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(ChatList chatList) {
                        if (chatList.getChatsViewModelList().size() > 0)
                            chatsListView.chatLoaded(chatList.getChatsViewModelList().get(0));
                    }
                });
        compositeSubscription.add(subscription);
    }

    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();

        chatsListView = null;
    }
}
