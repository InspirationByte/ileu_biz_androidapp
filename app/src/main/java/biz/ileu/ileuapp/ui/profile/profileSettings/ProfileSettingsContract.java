package biz.ileu.ileuapp.ui.profile.profileSettings;

import biz.ileu.ileuapi.models.profile.ProfileAvatarUpdateViewModel;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by Daniq on 25.04.2017.
 */

public interface ProfileSettingsContract {

    interface ProfileSettingsView extends BaseView{
        void savingProcess();

        void profileSaveError(String error);

        void profileSaved();

        void changingPassword();
        void changePasswordError(String error);
        void passwordChanged();

        void changingAvatar();
        void changeAvatarError(String error);
        void avatarChanged(ProfileAvatarUpdateViewModel profileAvatarUpdateViewModel);
    }

    interface ProfileSettingsPresenter extends BasePresenter {

    }

}
