package biz.ileu.ileuapp.ui.newsList;

/**
 * Created by Daniq on 03.09.2017.
 */

public interface NewsClick {
    interface OnItemClick {
        void onDownloadFileClick(String fileName, String fileId, String entryId);
    }
}
