package biz.ileu.ileuapp.ui.menu;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import biz.ileu.ileuapi.models.chat.MessageFromDto;
import biz.ileu.ileuapi.models.invitation.CompanyInvitationResultDTO;
import biz.ileu.ileuapi.models.process.ProcessCommentViewModel;
import biz.ileu.ileuapi.models.profile.ProfileInfo;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.company.CompaniesActivity;
import biz.ileu.ileuapp.ui.company.create.CompanyCreateActivity;
import biz.ileu.ileuapp.ui.profile.ProfileContract;
import biz.ileu.ileuapp.ui.profile.ProfilePresenter;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.events.CompanyCountersChangedEvent;
import biz.ileu.ileuapp.utils.events.ProcessClosedEvent;
import biz.ileu.ileuapp.utils.events.ProcessOpenedEvent;
import biz.ileu.ileuapp.utils.services.LocationTrackerService;
import biz.ileu.ileuapp.utils.services.SignalRService;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import biz.ileu.ileuapp.utils.ui.EndlessRecyclerOnScrollListener;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import ileu.lib.SlidingMenu;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import rx.subscriptions.CompositeSubscription;

@RuntimePermissions
public class MenuActivity extends BaseActivity
        implements View.OnClickListener, RecyclerViewClick.OnItemClickListener, MenuActivityContract.MenuActivityView, SearchView.OnQueryTextListener, ProfileContract.ProfileView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private SlidingMenu slidingMenu;

    private LeftMenuHolder holder;
    private LeftMenuAdapter leftMenuAdapter;
    private List<CompanyWorkInfoListViewModel> list = new ArrayList<>();

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private EndlessRecyclerOnScrollListener listener;
    private MenuActivityPresenter presenter;
    private ProfilePresenter profilePresenter;

    ProgressDialog progressDialog;

    private boolean firstOpen = true;

    private Timer timer;
    private TimerTask timerTask;

    public SignalRService signalRService;
    boolean serviceIsBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        bindViews();
        setData();
        setAdapter();
        initSlidingMenu();
        //MenuActivityPermissionsDispatcher.startLocationTrackingWithCheck(this);
        Intent intent = new Intent(this, SignalRService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void bindViews() {
        toolbar.setNavigationIcon(R.drawable.ic_format_list_bulleted_grey_24px);
        initBaseToolbar(true);
        View leftMenu = LayoutInflater.from(this).inflate(R.layout.left_menu, null, false);
        holder = new LeftMenuHolder(leftMenu);
    }

    @Override
    public void setData() {
        presenter = new MenuActivityPresenter(this);
        profilePresenter = new ProfilePresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.message_loading);
        profilePresenter.getProfile();


    }

    private void setAdapter() {
        leftMenuAdapter = new LeftMenuAdapter(list, this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        holder.recyclerView.setAdapter(leftMenuAdapter);
        holder.recyclerView.setLayoutManager(layoutManager);
        holder.svCompanies.setOnQueryTextListener(this);

        listener = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                presenter.getCompanies(current_page, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        };

        holder.srlCompanies.post(new Runnable() {
            @Override
            public void run() {
                progressDialog.show();
                presenter.getCompanies(1, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        });

        holder.srlCompanies.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                holder.srlCompanies.setRefreshing(true);
                listener.reset();
                list.clear();
                leftMenuAdapter.notifyDataSetChanged();
                presenter.getCompanies(1, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        });

        holder.recyclerView.addOnScrollListener(listener);

    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            SignalRService.LocalBinder binder = (SignalRService.LocalBinder) service;
            signalRService = binder.getService();
            serviceIsBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            serviceIsBound = false;
        }
    };

    @Override
    public void loadCompaniesError(String error) {
        progressDialog.dismiss();

        holder.srlCompanies.setRefreshing(false);
        Toast.makeText(MenuActivity.this, R.string.no_internet_error, Toast.LENGTH_LONG).show();

        if (firstOpen) {
            firstOpen = false;
            createFragment();
        }
    }

    @Override
    public void companiesLoaded(List<CompanyWorkInfoListViewModel> companiesList) {
        progressDialog.dismiss();

        holder.srlCompanies.setRefreshing(false);
        list.addAll(companiesList);
        leftMenuAdapter.notifyDataSetChanged();

        if (firstOpen) {
            firstOpen = false;

            int lastCompanyId = PrefUtils.getInt(PrefUtils.LAST_COMPANY_ID);
            CompanyWorkInfoListViewModel lastCompany = null;
            for (int i = 0; i < list.size(); i++) {
                CompanyWorkInfoListViewModel company = list.get(i);
                if (company.getId() == lastCompanyId) {
                    lastCompany = company;
                    leftMenuAdapter.setSelectedItemPosition(i);
                }
            }

            if (lastCompany != null)
                createFragment(lastCompany);
            else if (list.size() > 0) {
                PrefUtils.setData(PrefUtils.LAST_COMPANY_ID, list.get(0).getId());
                leftMenuAdapter.setSelectedItemPosition(0);
                createFragment(list.get(0));
            }
            else
                createFragment();
        } else {
            for (CompanyWorkInfoListViewModel company: list) {
                CompanyCountersChangedEvent event = new CompanyCountersChangedEvent();
                event.companyId = company.getId();
                event.unreadProcessComments = company.getNumUnreadProcessComments();
                event.unreadChatMessages = company.getNumUnreadChatMessages();

                EventBus.getDefault().post(event);
            }
        }
    }

    @Override
    public void companyLoaded(CompanyWorkInfoListViewModel loadedCompany) {
        CompanyCountersChangedEvent event = new CompanyCountersChangedEvent();
        event.companyId = loadedCompany.getId();
        event.unreadProcessComments = loadedCompany.getNumUnreadProcessComments();
        event.unreadChatMessages = loadedCompany.getNumUnreadChatMessages();

        EventBus.getDefault().post(event);
    }

    private void initSlidingMenu() {
        slidingMenu = new SlidingMenu(MenuActivity.this);
        slidingMenu.setMode(SlidingMenu.LEFT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        slidingMenu.setTouchModeBehind(SlidingMenu.TOUCHMODE_MARGIN);
        slidingMenu.setTouchmodeMarginThreshold(150);
        slidingMenu.setFadeDegree(0.0f);
        slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        slidingMenu.setMenu(holder.leftMenuHolder);
    }

    private void createFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentMainContainer, MenuFragment.newInstance())
                .commit();
    }

    private void createFragment(CompanyWorkInfoListViewModel selectedCompany) {
        Bundle args = new Bundle();
        args.putParcelable(Constants.ENTITY_COMPANY, Parcels.wrap(selectedCompany));
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentMainContainer, MenuFragment.newInstance(args))
                .commit();
    }


    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    public void startLocationTracking() {
        startService(new Intent(this, LocationTrackerService.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            slidingMenu.toggle();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivClose:
                slidingMenu.toggle();
                break;
            case R.id.ivAdd:

                this.showAddDialog();

                break;
            default:
                break;

        }

    }



    @Override
    public void onItemClick(View view, int position) {

        slidingMenu.toggle();
        if (!slidingMenu.isMenuShowing()) {
            PrefUtils.setData(PrefUtils.LAST_COMPANY_ID, list.get(position).getId());
            Log.d("SignalR", serviceIsBound + "");
            if (serviceIsBound)
                signalRService.connectToCompany(list.get(position).getId());
            //presenter.connectToCompany(list.get(position).getId());
            createFragment(list.get(position));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeSubscription != null && compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProcessCountersChangedEvent(CompanyCountersChangedEvent event) {
        Log.d("EVENT", "ACTIVITY");
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == event.companyId) {
                list.get(i).setNumUnreadProcessComments(event.unreadProcessComments);
                list.get(i).setNumUnreadChatMessages(event.unreadChatMessages);
                leftMenuAdapter.notifyItemChanged(i);
                break;
            }
        }
    }

    /**
     * When we receive new comment from process we should change the comment counters of process
     * @param data
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProcessCommentPostedEvent(ProcessCommentViewModel data) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == data.getCompanyId()) {
                list.get(i).setNumUnreadProcessComments(list.get(i).getNumUnreadProcessComments() + 1);

                // We should notify other fragments that counter had changed
                CompanyCountersChangedEvent event = new CompanyCountersChangedEvent();
                event.companyId = list.get(i).getId();
                event.unreadChatMessages = list.get(i).getNumUnreadChatMessages();
                event.unreadProcessComments = list.get(i).getNumUnreadProcessComments();

                EventBus.getDefault().post(event);
                leftMenuAdapter.notifyDataSetChanged();
                break;
            }
        }
    }

    /**
     * When we receive new message from chat we should change the chat counters of processes
     * @param message
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageFromDto message) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == message.getCompanyId()) {
                list.get(i).setNumUnreadChatMessages(list.get(i).getNumUnreadChatMessages() + 1);

                // We should notify other fragments that counter had changed
//                CompanyCountersChangedEvent event = new CompanyCountersChangedEvent();
//                event.companyId = list.get(i).getId();
//                event.unreadChatMessages = list.get(i).getNumUnreadChatMessages();
//                event.unreadProcessComments = list.get(i).getNumUnreadProcessComments();
//                EventBus.getDefault().post(event);

                presenter.getCompany(list.get(i).getId());
                leftMenuAdapter.notifyItemChanged(i);
                break;
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProcessOpenedEvent(ProcessOpenedEvent processOpenedEvent) {
        //presenter.connectToProcess(processOpenedEvent.processId);
        Log.d("SignalR", "PROCESS OPEN");
        if (serviceIsBound)
            signalRService.connectToProcess(processOpenedEvent.processId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProcessClosedEvent(ProcessClosedEvent event) {
        //presenter.disconnectFromProcess(event.processId);
        if (serviceIsBound)
            signalRService.disconnectFromProcess(event.processId);
    }



    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

        // We should be calling timer ever 10 second to refresh sidebar
        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                (new Handler(Looper.getMainLooper())).post(new Runnable() {
                    @Override
                    public void run() {
                        if (leftMenuAdapter != null && listener != null) {
                            listener.reset();
                            list.clear();
                            leftMenuAdapter.notifyDataSetChanged();
                            presenter.getCompanies(1, Constants.DEFAULT_PAGE_MAX_SIZE);
                        }
                    }
                });
            }
        };

        timer.schedule(timerTask, 1000 * 10, 1000 * 10);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

        // We should stop refresh call
        if (timer != null && timerTask != null) {
            timer.cancel();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MenuActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase(Locale.getDefault());

        List<CompanyWorkInfoListViewModel> filteredList = new ArrayList<>();
        for (CompanyWorkInfoListViewModel company : list)
            if (company.getName().toLowerCase(Locale.getDefault()).contains(newText))
                filteredList.add(company);

        leftMenuAdapter.changeDisplayCompanies(filteredList);
        return false;
    }

    @Override
    public void profileLoading() {

    }

    @Override
    public void profileError(String error) {

    }

    @Override
    public void profileData(ProfileInfo profileInfo) {
        Constants.currentUserId = profileInfo.getId();
    }

    @Override
    public void successInvited(CompanyInvitationResultDTO profileInfo) {

    }

    @Override
    public void logoutSuccess() {

    }

    @Override
    public void logoutError(String error) {

    }

    public class LeftMenuHolder {
        @BindView(R.id.leftMenuHolder)
        LinearLayout leftMenuHolder;

        @BindView(R.id.svCompanies)
        SearchView svCompanies;

        @BindView(R.id.ivClose)
        ImageView ivClose;


        @BindView(R.id.ivAdd)
        ImageView ivAdd;

        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        @BindView(R.id.srlCompanies)
        SwipeRefreshLayout srlCompanies;

        LeftMenuHolder(View view) {
            ButterKnife.bind(this, view);
            recyclerView.setLayoutManager(new LinearLayoutManager(MenuActivity.this));
            recyclerView.setHasFixedSize(true);
            ivClose.setOnClickListener(MenuActivity.this);
            ivAdd.setOnClickListener(MenuActivity.this);
        }
    }


    public void showAddDialog() {

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(MenuActivity.this);
        builderSingle.setTitle(R.string.choose)
                .setItems(R.array.add_array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent;
                        switch (which) {
                            case 0:
                                intent = new Intent(MenuActivity.this, CompaniesActivity.class);
                                startActivity(intent);
                                break;
                            case 1:
                                intent = new Intent(MenuActivity.this, CompanyCreateActivity.class);
                                startActivity(intent);
                                break;
                            default:
                                break;
                        }
                    }
                });


        builderSingle.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.show();
    }

}
