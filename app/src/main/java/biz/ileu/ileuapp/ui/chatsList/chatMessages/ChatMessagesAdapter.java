package biz.ileu.ileuapp.ui.chatsList.chatMessages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import biz.ileu.ileuapi.models.chat.ChatMessageViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Daniq on 19.04.2017.
 */

public class ChatMessagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int IS_MINE = 1;
    private final int NOT_MINE = 2;

    private List<ChatMessageViewModel> messagesList;


    public ChatMessagesAdapter(List<ChatMessageViewModel> messagesList) {
        this.messagesList = messagesList;

    }

    @Override
    public int getItemCount() {
        return messagesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessageViewModel message = messagesList.get(position);
        return message.isOwn() ? IS_MINE : NOT_MINE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == IS_MINE) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_chat_message_mine, parent, false);
            return new MyMessageViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_chat_message_not_mine, parent, false);
        return new NotMyMessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChatMessageViewModel message = messagesList.get(position);
        String msgTime = "";
        if (message.getMsgTime() != null) {
            msgTime = message.getMsgTime();
        }
        if (holder.getItemViewType() == IS_MINE) {
            MyMessageViewHolder myMessageViewHolder = (MyMessageViewHolder) holder;
            myMessageViewHolder.tvRowItemChatMessageMine.setText(message.getText());
            myMessageViewHolder.tvRowItemChatMessageMineDate.setText(Constants.getFormattedDateTime(msgTime));

            if (message.getState() == ChatMessageViewModel.STATE_SENT)
                myMessageViewHolder.llRowItemChatMessageMine
                        .setBackgroundResource(R.drawable.chat_message_comment_mine_bg_sent);

            else if (message.getState() == ChatMessageViewModel.STATE_SENDING)
                myMessageViewHolder.llRowItemChatMessageMine
                        .setBackgroundResource(R.drawable.chat_message_comment_mine_bg_sending);

            else if (message.getState() == ChatMessageViewModel.STATE_NOT_SENT)
                myMessageViewHolder.llRowItemChatMessageMine
                        .setBackgroundResource(R.drawable.chat_message_comment_mine_bg_not_sent);

        } else if (holder.getItemViewType() == NOT_MINE) {
            NotMyMessageViewHolder notMyMessageViewHolder = (NotMyMessageViewHolder) holder;
            notMyMessageViewHolder.tvRowItemChatMessageNotMine.setText(message.getText());
            notMyMessageViewHolder.tvRowItemChatMessageNotMineDate.setText(Constants.getFormattedDateTime(msgTime));

            Picasso.with(notMyMessageViewHolder.context)
                    .load(Constants.BASE_URL + message.getProfilePicture())
                    .into(notMyMessageViewHolder.civRowItemChatMessageNotMine);
        }

    }


    public void changeDisplayMessages(List<ChatMessageViewModel> chatMessageViewModels) {
        messagesList = chatMessageViewModels;
        notifyDataSetChanged();
    }

    public class MyMessageViewHolder extends RecyclerView.ViewHolder {

        Context context;

        @BindView(R.id.tvRowItemChatMessageMine)
        TextView tvRowItemChatMessageMine;

        @BindView(R.id.tvRowItemChatMessageMineDate)
        TextView tvRowItemChatMessageMineDate;

        @BindView(R.id.llRowItemChatMessageMine)
        LinearLayout llRowItemChatMessageMine;

        public MyMessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    public class NotMyMessageViewHolder extends RecyclerView.ViewHolder {

        Context context;

        @BindView(R.id.civRowItemChatMessageNotMine)
        CircleImageView civRowItemChatMessageNotMine;

        @BindView(R.id.tvRowItemChatMessageNotMineDate)
        TextView tvRowItemChatMessageNotMineDate;

        @BindView(R.id.tvRowItemChatMessageNotMine)
        TextView tvRowItemChatMessageNotMine;

        public NotMyMessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

}
