package biz.ileu.ileuapp.ui.process.processesBookmark;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import biz.ileu.ileuapi.models.bookmarks.BookmarkCategoryViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.process.ProcessViewOptionsClick;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Daniq on 06.04.2017.
 */

public class ProcessBookmarkAdapter extends RecyclerView.Adapter<ProcessBookmarkAdapter.ProcessBookmarkHolder> {


    private List<BookmarkCategoryViewModel> categoryList;

    private RecyclerViewClick.OnItemClickListener listener;
    private ProcessViewOptionsClick.OnOptionsClickListener optionsClickListener;

    public ProcessBookmarkAdapter(List<BookmarkCategoryViewModel> categoryList,
                                RecyclerViewClick.OnItemClickListener listener,
                                ProcessViewOptionsClick.OnOptionsClickListener optionsClickListener) {
        this.categoryList = categoryList;
        this.listener = listener;
        this.optionsClickListener = optionsClickListener;
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    @Override
    public ProcessBookmarkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_bookmark, parent, false);
        return new ProcessBookmarkHolder(row);
    }

    @Override
    public void onBindViewHolder(final ProcessBookmarkHolder holder, int position) {
        BookmarkCategoryViewModel category = categoryList.get(position);

        holder.tvRowBookmarkCategoryTitle.setText(category.getTitle() + " (" + category.getNumBookmarks() + ")");

    }



    public class ProcessBookmarkHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;

        @BindView(R.id.tvRowBookmarkCategoryTitle)
        TextView tvRowBookmarkCategoryTitle;




        public ProcessBookmarkHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
            itemView.setOnClickListener(this);

        }



        @Override
        public void onClick(View v) {

            if (listener != null) {
                listener.onItemClick(v, getAdapterPosition());
            }
        }
    }
}
