package biz.ileu.ileuapp.ui.menu;

import java.util.List;

import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by Daniq on 04.05.2017.
 */

public interface MenuActivityContract {
    interface MenuActivityView extends BaseView{

        void loadCompaniesError(String error);

        void companiesLoaded(List<CompanyWorkInfoListViewModel> list);

        void companyLoaded(CompanyWorkInfoListViewModel loadedCompany);
    }

    interface MenuActivityPresenter extends BasePresenter {

    }
}
