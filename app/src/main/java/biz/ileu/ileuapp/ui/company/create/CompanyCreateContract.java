package biz.ileu.ileuapp.ui.company.create;

import biz.ileu.ileuapi.models.profile.ProfileAvatarUpdateViewModel;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by macair on 08.08.17.
 */

public interface CompanyCreateContract {
    interface CompanyCreateView extends BaseView {
        public void createdCompany();
        public void failCreatingCompany(String e);

    }

    interface CompanyCreatePresenter extends BasePresenter {

    }

}
