package biz.ileu.ileuapp.ui.process.processesBookmark;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import biz.ileu.ileuapi.models.bookmarks.BookmarkCategoryViewModel;
import biz.ileu.ileuapi.models.bookmarks.BookmarkViewModel;
import biz.ileu.ileuapi.models.bookmarks.BookmarksCategoriesList;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.bookmarks.BookmarkCategoriesActivity;
import biz.ileu.ileuapp.ui.process.ProcessViewOptionsClick;
import biz.ileu.ileuapp.ui.process.processesList.ProcessesListContract;
import biz.ileu.ileuapp.ui.process.processesList.ProcessesListPresenter;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.cicerone.Screen;
import biz.ileu.ileuapp.utils.ui.BaseFragment;
import biz.ileu.ileuapp.utils.ui.EndlessRecyclerOnScrollListener;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProcessBookmarkFragment extends BaseFragment implements ProcessesListContract.ProcessesListView,
        RecyclerViewClick.OnItemClickListener,
        ProcessViewOptionsClick.OnOptionsClickListener  {

    private static final int FILTER_PROCESS = 9;
    @BindView(R.id.rlProcesses)
    RecyclerView rlProcesses;

    @BindView(R.id.srlProcesses)
    SwipeRefreshLayout srlProcesses;

    Unbinder unbinder;
    private CompanyWorkInfoListViewModel currentCompany = null;
    private String filter;
    private String sorting;

    private ProcessBookmarkAdapter adapter;
    private List<BookmarkCategoryViewModel> list;
    private EndlessRecyclerOnScrollListener listener;
    private boolean fromFilter = false;
    private BookmarksCategoriesList currentCategories;

    private ProcessesListPresenter presenter;
    private Menu menu;
    private MenuItem menuItem;

    public ProcessBookmarkFragment() {
        // Required empty public constructor
    }

    public static ProcessBookmarkFragment newInstance() {
        return new ProcessBookmarkFragment();
    }

    public static ProcessBookmarkFragment newInstance(Bundle bundle) {
        ProcessBookmarkFragment fragment = new ProcessBookmarkFragment();
        Bundle args = new Bundle();
        args.putAll(bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_processes_list, container, false);

        unbinder = ButterKnife.bind(this, view);

        setData();

//        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void bindViews() {

    }

    //    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.search, menu);
//        MenuItem searchItem = menu.findItem(R.id.actionSearch);
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
//
//        searchView.setOnQueryTextListener(this);
//
//        MenuItemCompat.collapseActionView(searchItem);
//        super.onCreateOptionsMenu(menu, inflater);
//    }
    private boolean isRefresh = true;
    @Override
    public void setData() {
        getActionBar().setTitle(getResources().getString(R.string.processes));

        Bundle args = getArguments();
        if (args != null && args.containsKey(Constants.ENTITY_COMPANY))
            currentCompany = Parcels.unwrap(args.getParcelable(Constants.ENTITY_COMPANY));
        filter = args.getString(Constants.PROCESS_FILTER);
        sorting = args.getString(Constants.PROCESS_SORTING);

        presenter = new ProcessesListPresenter(this, currentCompany, filter, sorting, 4);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rlProcesses.setLayoutManager(layoutManager);

        list = new ArrayList<>();
        adapter = new ProcessBookmarkAdapter(list, this, this);
        rlProcesses.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        DividerItemDecoration divider = new DividerItemDecoration(rlProcesses.getContext(),
                ((LinearLayoutManager) layoutManager).getOrientation());
        rlProcesses.addItemDecoration(divider);

        listener = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                isRefresh = false;
                presenter.getBookmarkCategories(current_page, Constants.DEFAULT_PAGE_SIZE);
            }
        };
//        rlProcesses.setRecyclerListener(listener);
        rlProcesses.addOnScrollListener(listener);
        srlProcesses.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                list.clear();
                adapter.notifyDataSetChanged();
                srlProcesses.setRefreshing(true);
                listener.reset();
                isRefresh = true;
                presenter.getBookmarkCategories(1, Constants.DEFAULT_PAGE_SIZE);
            }
        });

    }
    public void updateBookmarkList() {
        srlProcesses.post(new Runnable() {
            @Override
            public void run() {
                list.clear();
                srlProcesses.setRefreshing(true);
                presenter.getBookmarkCategories(1, Constants.DEFAULT_PAGE_SIZE);
            }
        });
    }
    @Override
    public void processesLoading() {
        if (isRefresh) list.clear();
        adapter.notifyDataSetChanged();
        if (!srlProcesses.isRefreshing())
            srlProcesses.setRefreshing(true);
    }

    @Override
    public void loadProcessesError(String error) {
        Toast.makeText(getActivity(), R.string.message_processes_load_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void processesLoaded(List<Process> processList) {

    }


    @Override
    public void addBookmarkError(String message) {

    }

    @Override
    public void addedBookmark(BookmarkViewModel bookmark) {

    }

    @Override
    public void bookmarkCategoriesLoaded(BookmarksCategoriesList bookmarksCategoriesList) {
        list.addAll(bookmarksCategoriesList.getBookmarkCategoryViewModelList());
        currentCategories = bookmarksCategoriesList;

        adapter.notifyDataSetChanged();
        srlProcesses.setRefreshing(false);
    }


    @Override
    public String getScreenName() {
        return Screen.SCREEN_PROCESSES_LIST;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.destroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!fromFilter)
            srlProcesses.post(new Runnable() {
                @Override
                public void run() {
                    list.clear();
                    srlProcesses.setRefreshing(true);
                    presenter.getBookmarkCategories(1, Constants.DEFAULT_PAGE_SIZE);
                }
            });
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onItemClick(View view, int position) {
            Intent intent = new Intent(getActivity(), ProcessBookmarkDetailActivity.class);
            intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
            intent.putExtra(Constants.ENTITY_BOOKMARK_CATEGORY, Parcels.wrap(list.get(position)));
            startActivity(intent);
    }


    @Override
    public void onBookmarkClick(int position) {

        Intent intent = new Intent(getActivity(), BookmarkCategoriesActivity.class);
        intent.putExtra(Constants.ENTITY_PROCESS, Parcels.wrap(list.get(position)));
        intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));

        startActivity(intent);
//        presenter.addBookmark(list.get(position).getId());
    }

    @Override
    public void onEditClick(int position) {

    }

    @Override
    public void onDeleteClick(int position) {

    }
    public void updateListWith(String filterS) {
        list.clear();
        fromFilter = true;
//        srlProcesses.setRefreshing(true);
        presenter.getProcesses(1, Constants.DEFAULT_PAGE_SIZE, filterS, null, null);
    }


    public boolean isFromFilter() {
        return fromFilter;
    }

    public void setFromFilter(boolean fromFilter) {
        this.fromFilter = fromFilter;
    }

}
