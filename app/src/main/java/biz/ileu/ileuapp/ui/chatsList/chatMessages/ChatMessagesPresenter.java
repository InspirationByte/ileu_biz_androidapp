package biz.ileu.ileuapp.ui.chatsList.chatMessages;


import android.util.Log;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.chat.ChatMessageList;
import biz.ileu.ileuapi.models.chat.ChatMessageViewModel;
import biz.ileu.ileuapi.models.chat.ChatsViewModel;
import biz.ileu.ileuapi.models.chat.CreateChatViewModel;
import biz.ileu.ileuapi.models.chat.MessageFromDto;
import biz.ileu.ileuapi.models.chat.PostMessageViewModel;
import biz.ileu.ileuapi.models.chat.UserChatStatusDto;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniq on 19.04.2017.
 */

public class ChatMessagesPresenter implements ChatMessagesContract.ChatMessagesPresenter {

    private ChatMessagesContract.ChatMessagesView chatMessagesView;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private CompanyWorkInfoListViewModel currentCompany;
    private ChatsViewModel currentChat;


    public ChatMessagesPresenter(ChatMessagesContract.ChatMessagesView chatMessagesView,
                                 final CompanyWorkInfoListViewModel currentCompany,
                                 ChatsViewModel currentChat) {
        this.chatMessagesView = chatMessagesView;
        this.currentCompany = currentCompany;
        this.currentChat = currentChat;
        Log.d("CHAT_ID", currentChat.getId() + "");
    }


    public void getMessages(int page, int per) {
        Subscription subscription = RestClient.request()
                .getChatMessages(PrefUtils.getToken(), currentCompany.getId(), currentChat.getId(), page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ChatMessageList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        chatMessagesView.loadMessagesError(e.getMessage());
                    }

                    @Override
                    public void onNext(ChatMessageList chatMessageList) {
                        chatMessagesView.messagesLoaded(chatMessageList.getChatMessageViewModels());
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void sendMessage(final ChatMessageViewModel chatMessageViewModel) {
        PostMessageViewModel postMessageViewModel = new PostMessageViewModel();
        postMessageViewModel.setChatId(currentChat.getId());
        postMessageViewModel.setMessage(chatMessageViewModel.getText());


        Subscription subscription = RestClient.request()
                .postMessage(PrefUtils.getToken(), postMessageViewModel)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MessageFromDto>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        chatMessagesView.messageSendError(chatMessageViewModel, e.getMessage());
                    }

                    @Override
                    public void onNext(MessageFromDto messageFromDto) {
                        chatMessagesView.messageIsSent(chatMessageViewModel);
                    }
                });

        chatMessagesView.messageIsSending(chatMessageViewModel);
        compositeSubscription.add(subscription);
    }


    public void createChat() {
        int []userIds = new int[currentChat.getChatUsers().size()];
        for (int i = 0; i < currentChat.getChatUsers().size(); i++) {
            userIds[i] = currentChat.getChatUsers().get(i).getUserId();
        }

        Subscription subscription = RestClient.request().createChat(PrefUtils.getToken(), currentCompany.getId(), userIds)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ChatsViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        chatMessagesView.chatCreateError(e.getMessage());
                    }

                    @Override
                    public void onNext(ChatsViewModel chatsViewModel) {
                        chatMessagesView.chatCreated(chatsViewModel);
                    }
                });
        chatMessagesView.chatCreating();
        compositeSubscription.add(subscription);
    }

    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();

        chatMessagesView = null;
    }
}
