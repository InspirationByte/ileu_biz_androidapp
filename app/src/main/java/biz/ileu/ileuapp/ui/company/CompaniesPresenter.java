package biz.ileu.ileuapp.ui.company;

import java.util.List;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.company.CompanyList;
import biz.ileu.ileuapi.models.news.NewsCommentList;
import biz.ileu.ileuapi.models.news.NewsEntryViewModel;
import biz.ileu.ileuapi.models.process.CommentReadStateUpdatedViewModel;
import biz.ileu.ileuapp.ui.newsList.news.NewsContract;
import biz.ileu.ileuapp.utils.PrefUtils;
import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by macair on 06.08.17.
 */

public class CompaniesPresenter implements CompaniesContract.CompaniesPresenter {
    private CompaniesContract.CompaniesView companiesView;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();


    public CompaniesPresenter(CompaniesContract.CompaniesView companiesView) {
        this.companiesView = companiesView;
    }

    public void getCompanies(int page, int per) {
        Subscription subscription = RestClient.request()
                .getCompaniesExceptJoined(PrefUtils.getToken(), page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CompanyList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        companiesView.loadCompaniesErrror(e.getMessage());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(CompanyList companyList) {
                        companiesView.companiesLoaded(companyList.getCompanyViewModelList());
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void join(int companyId) {
        Subscription subscription = RestClient.request()
                .joinCompanyRequest(PrefUtils.getToken(), companyId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        companiesView.joinError(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        companiesView.successJoined();
                    }
                });
        compositeSubscription.add(subscription);
    }

    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();
    }
}
