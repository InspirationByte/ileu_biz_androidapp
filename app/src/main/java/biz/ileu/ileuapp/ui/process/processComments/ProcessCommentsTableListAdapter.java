package biz.ileu.ileuapp.ui.process.processComments;

/**
 * Created by macair on 15.08.17.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.List;

import biz.ileu.ileuapi.models.tables.ProcessRowDTO;
import biz.ileu.ileuapp.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProcessCommentsTableListAdapter extends RecyclerView.Adapter<ProcessCommentsTableListAdapter.ProcessCommentTableViewHolder> {

    private List<ProcessRowDTO> rows;

    private ProcessCommentClick.OnItemClickListener listener;
    private ProcessCommentsPresenter presenter;

    public ProcessCommentsTableListAdapter(List<ProcessRowDTO> rows,
            ProcessCommentClick.OnItemClickListener listener, ProcessCommentsPresenter presenter) {
            this.rows = rows;
            this.listener = listener;

            this.presenter = presenter;
    }

    @Override
    public int getItemCount() {
            return this.rows.size();
    }
    public void addNew() {
        rows.add(new ProcessRowDTO());
        notifyItemChanged(rows.size() - 1);
    }
    @Override
    public ProcessCommentsTableListAdapter.ProcessCommentTableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_process_table, parent, false);
            return new ProcessCommentsTableListAdapter.ProcessCommentTableViewHolder(row);
    }

    @Override
    public void onBindViewHolder(final ProcessCommentsTableListAdapter.ProcessCommentTableViewHolder holder, int position) {
            final ProcessRowDTO row = rows.get(position);
            if (row.getId() == 0)
                return;
            int sizeColumn = row.getColumnValues().size();
            if (sizeColumn > 0)
                holder.etFirstColumn.setText(row.getColumnValues().get(0).getValue());
            if (sizeColumn > 1)
                holder.etSecondColumn.setText(row.getColumnValues().get(1).getValue());
            if (sizeColumn > 2)
                holder.etThirdColumn.setText(row.getColumnValues().get(2).getValue());

            holder.ivRemoveTableRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.removeRow(row.getId());
                }
            });
//            holder.ivRemoveTableRow.setOnClickListener(listener.onCommentClick(position));

    }

    public class ProcessCommentTableViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        Context context;


        @BindView(R.id.etFirstColumn)
        EditText etFirstColumn;
        @BindView(R.id.etSecondColumn)
        EditText etSecondColumn;
        @BindView(R.id.etThirdColumn)
        EditText etThirdColumn;
        @BindView(R.id.ivRemoveTableRow)
        ImageView ivRemoveTableRow;

        public ProcessCommentTableViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
//            if (listener != null)
//                listener.onCommentClick(getAdapterPosition());
        }

    }

}
