package biz.ileu.ileuapp.ui.login;

import android.util.Log;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.TokenData;
import biz.ileu.ileuapi.models.profile.ProfileInfo;
import biz.ileu.ileuapp.utils.PrefUtils;
import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Murager on 4/3/17.
 */

public class LoginPresenter implements LoginContract.LoginPresenter {

    private LoginContract.LoginView loginView;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public LoginPresenter(LoginContract.LoginView loginView) {
        this.loginView = loginView;
    }

    public void login(String grantType, String name, String password) {
        loginView.loginLoading();

        Log.d("My_data", name + "----" + password);

        Subscription subscription =
                RestClient.request().login(grantType, name, password)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<TokenData>() {
                    @Override
                    public void onCompleted() {
                        Log.d("Login_act", "complete");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Login_act", e.toString());
                        loginView.loginError(e.getMessage());
                    }

                    @Override
                    public void onNext(TokenData tokenData) {
                        Log.d("Login_act", tokenData.toString());
                        loginView.loginData(tokenData.getAccessToken());
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void getProfileInfo() {
        loginView.profileLoading();
        Subscription subscription = RestClient.request().getProfileInfo(PrefUtils.getToken())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProfileInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        loginView.profileError(e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileInfo profileInfo) {
                        loginView.profileLoaded(profileInfo);
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void sendFirebaseToken(String firebaseToken) {
        loginView.firebaseTokenLoading();

        Subscription subscription = RestClient.request().updateFirebaseToken(PrefUtils.getToken(), firebaseToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        Log.d("FIREBASE_UPDATE", "YES");
                        loginView.firebaseTokenIsSent();
                    }

                    @Override
                    public void onError(Throwable e) {
                        loginView.firebaseTokenIsSent();
                        Log.d("FIREBASE_UPDATE", "NO");
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                    }
                });

        compositeSubscription.add(subscription);
    }

    @Override
    public void destroy() {
        if (compositeSubscription != null && compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
        loginView = null;
    }
}
