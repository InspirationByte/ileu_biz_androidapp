package biz.ileu.ileuapp.ui.notificationsList;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.process.ProcessNotificationViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.process.processComments.ProcessCommentsActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.cicerone.Screen;
import biz.ileu.ileuapp.utils.events.CompanyCountersChangedEvent;
import biz.ileu.ileuapp.utils.ui.BaseFragment;
import biz.ileu.ileuapp.utils.ui.EndlessRecyclerOnScrollListener;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsListFragment extends BaseFragment
        implements NotificationsListContract.NotificationsListView, RecyclerViewClick.OnItemClickListener {


    @BindView(R.id.rlNotifications)
    RecyclerView rlNotifications;

    @BindView(R.id.srlNotifications)
    SwipeRefreshLayout srlNotifications;

    Unbinder unbinder;

    private CompanyWorkInfoListViewModel currentCompany = null;

    List<ProcessNotificationViewModel> list;
    NotificationsListAdapter adapter;
    private EndlessRecyclerOnScrollListener listener;

    private NotificationsListPresenter presenter;

    private ProgressDialog progressDialog;


    public NotificationsListFragment() {
        // Required empty public constructor
    }

    public static NotificationsListFragment newInstance() {
        return new NotificationsListFragment();
    }

    public static NotificationsListFragment newInstance(Bundle bundle) {
        NotificationsListFragment fragment = new NotificationsListFragment();
        Bundle args = new Bundle();
        args.putAll(bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notifications_list, container, false);
        unbinder = ButterKnife.bind(this, view);

        setData();
        return view;
    }

    @Override
    public void bindViews() {

    }

    @Override
    public void setData() {
        getActionBar().setTitle(getResources().getString(R.string.notifications));

        Bundle args = getArguments();
        if (args != null && args.containsKey(Constants.ENTITY_COMPANY))
            currentCompany = Parcels.unwrap(args.getParcelable(Constants.ENTITY_COMPANY));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rlNotifications.setLayoutManager(layoutManager);

        list = new ArrayList<>();
        adapter = new NotificationsListAdapter(list, this);
        rlNotifications.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if (currentCompany == null)
            return;

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.notifications_opening);

        presenter = new NotificationsListPresenter(this, currentCompany);

        listener = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                presenter.getNotifications(current_page, Constants.DEFAULT_PAGE_SIZE);
            }
        };

        srlNotifications.post(new Runnable() {
            @Override
            public void run() {
                srlNotifications.setRefreshing(true);
                presenter.getNotifications(1, Constants.DEFAULT_PAGE_SIZE);
            }
        });

        srlNotifications.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                list.clear();
                adapter.notifyDataSetChanged();
                srlNotifications.setRefreshing(true);
                listener.reset();

                presenter.getNotifications(1, Constants.DEFAULT_PAGE_SIZE);
            }
        });
    }

    @Override
    public String getScreenName() {
        return Screen.SCREEN_NOTIFICATIONS_LIST;
    }

    @Override
    public void loadNotificationsError(String error) {
        srlNotifications.setRefreshing(false);
        Toast.makeText(getActivity(), R.string.server_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void notificationsLoaded(List<ProcessNotificationViewModel> notificationList) {
        srlNotifications.setRefreshing(false);
        list.addAll(notificationList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void markNotificationError(String error) {
        progressDialog.dismiss();
        Toast.makeText(getActivity(), R.string.message_notification_open_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void notificationIsMarked(int notificationPosition) {
        progressDialog.dismiss();
        ProcessNotificationViewModel notification = list.get(notificationPosition);
        notification.setTotalUnreadMessages(0);
        adapter.notifyItemChanged(notificationPosition);

        CompanyCountersChangedEvent event = new CompanyCountersChangedEvent();
        event.companyId = currentCompany.getId();
        event.unreadChatMessages = currentCompany.getNumUnreadChatMessages();
        event.unreadProcessComments = currentCompany.getNumUnreadProcessComments() - 1 > 0 ?
                currentCompany.getNumUnreadProcessComments() - 1 : 0;
        EventBus.getDefault().post(event);

        presenter.loadProcess(notification.getProcessId());
    }

    @Override
    public void loadingProcess() {
        progressDialog.show();
    }

    @Override
    public void loadingProcessError(String error) {
        progressDialog.dismiss();
        Toast.makeText(getActivity(), R.string.message_notification_process_load_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void processLoaded(Process process) {
        progressDialog.dismiss();
        Log.d("PROCESS", process.getId() + "");
        Intent intent = new Intent(getActivity(), ProcessCommentsActivity.class);
        intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
        intent.putExtra(Constants.ENTITY_PROCESS, Parcels.wrap(process));
        intent.putExtra(Constants.PROCESS_IS_NEW, false);
        startActivity(intent);
    }

    @Override
    public void onItemClick(View view, int position) {
        presenter.markNotification(list.get(position).getProcessId(), position);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }



}
