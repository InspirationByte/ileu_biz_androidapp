package biz.ileu.ileuapp.ui.splash;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.parceler.Parcels;

import biz.ileu.ileuapi.models.chat.ChatsViewModel;
import biz.ileu.ileuapi.models.chat.MessageFromDto;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.chatsList.chatMessages.ChatMessagesActivity;
import biz.ileu.ileuapp.ui.login.LoginActivity;
import biz.ileu.ileuapp.ui.menu.MenuActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.services.IleuFirebaseMessagingService;
import biz.ileu.ileuapp.utils.services.SignalRService;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        // This piece of code is used to determine if we have opened this app via push message
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            String dataStr = bundle.getString("data");
            if (!TextUtils.isEmpty(dataStr)) {
                openChat(dataStr);
                return;
            }
        }




    }

    @Override
    protected void onResume() {
        super.onResume();

        openMainOrLogin();
    }

    private void openMainOrLogin() {


        Intent intent = null;
        if (PrefUtils.getString(PrefUtils.TOKEN).isEmpty())
            intent = new Intent(this, LoginActivity.class);
        else {
            Intent serviceIntent = new Intent(this, SignalRService.class);
            startService(serviceIntent);
            intent = new Intent(this, MenuActivity.class);
        };
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        final Handler handler = new Handler();
        final Intent finalIntent = intent;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(finalIntent);
                finish();
            }
        }, 3000);
    }


    private void openChat(String dataStr) {
        Intent serviceIntent = new Intent(this, SignalRService.class);
        startService(serviceIntent);

        Gson gson = new Gson();

        JsonParser parser = new JsonParser();
        JsonObject dataJson = parser.parse(dataStr).getAsJsonObject();

        MessageFromDto messageFromDto = gson.fromJson(dataJson.get("message"), MessageFromDto.class);
        CompanyWorkInfoListViewModel company = gson.fromJson(dataJson.get("company"), CompanyWorkInfoListViewModel.class);
        ChatsViewModel chat = gson.fromJson(dataJson.get("chat"), ChatsViewModel.class);

        Intent intentChat;
        intentChat = new Intent(this, ChatMessagesActivity.class);

        intentChat.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(company));
        intentChat.putExtra(Constants.ENTITY_CHAT, Parcels.wrap(chat));

        startActivity(intentChat);
        finish();
    }
}
