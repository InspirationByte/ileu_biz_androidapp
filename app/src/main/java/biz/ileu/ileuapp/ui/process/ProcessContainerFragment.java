package biz.ileu.ileuapp.ui.process;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import biz.ileu.ileuapi.models.bookmarks.BookmarkCategoryViewModel;
import biz.ileu.ileuapi.models.process.ProcessUserStatsViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.process.filter.ProcessesFilterActivity;
import biz.ileu.ileuapp.ui.process.processTemplates.ProcessTemplatesActivity;
import biz.ileu.ileuapp.ui.process.processesBookmark.ProcessBookmarkFragment;
import biz.ileu.ileuapp.ui.process.processesList.ProcessesListFragment;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.cicerone.Screen;
import biz.ileu.ileuapp.utils.ui.BaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.support.design.widget.TabLayout.MODE_SCROLLABLE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProcessContainerFragment extends BaseFragment implements TabLayout.OnTabSelectedListener, ProcessContainerContract.ProcessContainerView, SearchView.OnQueryTextListener {


    private static final int FILTER_PROCESS = 9;
    private static final int BOOKMARK_CATEGORY = 11;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    Unbinder unbinder;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.fabProcessCreate)
    FloatingActionButton fabProcessCreate;

    private FragmentAdapter fragmentAdapter;

    private List<Fragment> fragmentList = new ArrayList<>();

    private CompanyWorkInfoListViewModel currentCompany = null;

    private Menu menu;
    private MenuItem menuItem;
    private MenuItem searchItem;
    private ProcessContainerPresenter presenter;

    public ProcessContainerFragment() {
        // Required empty public constructor
    }

    public static ProcessContainerFragment newInstance() {
        return new ProcessContainerFragment();
    }

    public static ProcessContainerFragment newInstance(Bundle bundle) {
        ProcessContainerFragment fragment = new ProcessContainerFragment();
        Bundle args = new Bundle();
        args.putAll(bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_process_container, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (getArguments() != null && getArguments().containsKey(Constants.ENTITY_COMPANY))
            currentCompany = Parcels.unwrap(getArguments().getParcelable(Constants.ENTITY_COMPANY));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        bindViews();
        setData();
        setAdapter();
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.process_filter, menu);
        MenuItem filter = menu.findItem(R.id.actionFilter);
        this.menu = menu;
        this.menuItem = filter;
        filter.setVisible(true);
        MenuItem searchItemLocal = menu.findItem(R.id.actionSearch);
        this.searchItem = searchItemLocal;
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItemLocal);

        searchView.setOnQueryTextListener(this);

        MenuItemCompat.collapseActionView(searchItemLocal);
        super.onCreateOptionsMenu(menu, inflater);
    }


//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.search, menu);
//        MenuItem searchItem = menu.findItem(R.id.actionSearch);
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
//
////        searchView.setOnQueryTextListener(this);
//
//        MenuItemCompat.collapseActionView(searchItem);
//        super.onCreateOptionsMenu(menu, inflater);
//    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.actionFilter) {
            Intent intent = new Intent(getActivity(), ProcessesFilterActivity.class);
            intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
            intent.putExtra(Constants.ENTITY_CURRENT_TAB, tabLayout.getSelectedTabPosition());
            startActivityForResult(intent, FILTER_PROCESS);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FILTER_PROCESS) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                String filter = data.getStringExtra("FILTER");
                List<Integer> empIds = Parcels.unwrap(data.getParcelableExtra("EMPIDS"));
                List<Integer> tempIds = Parcels.unwrap(data.getParcelableExtra("TEMPIDS"));
                if (tabLayout.getSelectedTabPosition() == 0 || tabLayout.getSelectedTabPosition() == 1) {
                    ProcessesListFragment tempFragment = (ProcessesListFragment)fragmentList.get(tabLayout.getSelectedTabPosition());
                    tempFragment.updateListWith(filter, empIds, tempIds, true, tabLayout.getSelectedTabPosition());
                }
            }
        }
    }

    @Override
    public void bindViews() {
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.process_all)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.process_documents)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.process_files)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.process_tables)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.process_bookmarks)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.processes_outdated)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.processes_history)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//        tabLayout.setTabMode(TabLayout.GRAVITY_CENTER);
        tabLayout.setTabMode(MODE_SCROLLABLE);
        tabLayout.addOnTabSelectedListener(this);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    @Override
    public void setData() {
        presenter = new ProcessContainerPresenter(currentCompany, this);

        if (fragmentList.isEmpty() && currentCompany != null) {
            Bundle args1 = new Bundle();
            args1.putParcelable(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
            args1.putString(Constants.PROCESS_FILTER, Constants.PROCESSES_FILTER_MY);
            args1.putString(Constants.PROCESS_SORTING, Constants.PROCESS_SORTING_MY);
            args1.putInt(Constants.TAB_INDEX, 0);
            fragmentList.add(ProcessesListFragment.newInstance(args1));


            Bundle args2 = new Bundle();
            args2.putParcelable(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
            args2.putString(Constants.PROCESS_FILTER, Constants.PROCESSES_FILTER_HASDOCUMENT);
            args2.putString(Constants.PROCESS_SORTING, Constants.PROCESS_SORTING_OUTDATED);
            args2.putInt(Constants.TAB_INDEX, 1);
            fragmentList.add(ProcessesListFragment.newInstance(args2));

            Bundle args3 = new Bundle();
            args3.putParcelable(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
            args3.putString(Constants.PROCESS_FILTER, Constants.PROCESSES_FILTER_HASFILE);
            args3.putString(Constants.PROCESS_SORTING, Constants.PROCESS_SORTING_OUTDATED);
            args3.putInt(Constants.TAB_INDEX, 2);
            fragmentList.add(ProcessesListFragment.newInstance(args3));

            Bundle args4 = new Bundle();
            args4.putParcelable(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
            args4.putString(Constants.PROCESS_FILTER, Constants.PROCESSES_FILTER_HASTABLE);
            args4.putString(Constants.PROCESS_SORTING, Constants.PROCESS_SORTING_OUTDATED);
            args4.putInt(Constants.TAB_INDEX, 3);
            fragmentList.add(ProcessesListFragment.newInstance(args4));


            Bundle args5 = new Bundle();
            args5.putParcelable(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
            args5.putString(Constants.PROCESS_FILTER, Constants.PROCESSES_FILTER_HASBOOKMARK);
            args5.putString(Constants.PROCESS_SORTING, Constants.PROCESS_SORTING_OUTDATED);
            args5.putInt(Constants.TAB_INDEX, 4);
            fragmentList.add(ProcessBookmarkFragment.newInstance(args5));

            Bundle args6 = new Bundle();
            args6.putParcelable(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
            args6.putString(Constants.PROCESS_FILTER, Constants.PROCESSES_FILTER_OUTDATED);
            args6.putString(Constants.PROCESS_SORTING, Constants.PROCESS_SORTING_OUTDATED);
            args6.putInt(Constants.TAB_INDEX, 5);
            fragmentList.add(ProcessesListFragment.newInstance(args6));

            Bundle args7 = new Bundle();
            args7.putParcelable(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
            args7.putString(Constants.PROCESS_FILTER, Constants.PROCESSES_FILTER_HISTORY);
            args7.putString(Constants.PROCESS_SORTING, Constants.PROCESS_SORTING_HISTORY);
            args7.putInt(Constants.TAB_INDEX, 6);
            fragmentList.add(ProcessesListFragment.newInstance(args7));

            presenter.loadProcessStats();
        }
    }

    private void setAdapter() {
        fragmentAdapter = new FragmentAdapter(getChildFragmentManager(), fragmentList);
        viewPager.setOffscreenPageLimit(5);
        viewPager.setAdapter(fragmentAdapter);
    }

    @Override
    public String getScreenName() {
        return Screen.SCREEN_PROCESSES_CONTAINER;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getPosition() != 0 && tab.getPosition() != 5 && tab.getPosition() != 1) {
            menuItem.setVisible(false);
            searchItem.setVisible(false);
        } else {
            searchItem.setVisible(true);
            menuItem.setVisible(true);
        }
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    @OnClick(R.id.fabProcessCreate)
    public void openProcessTemplates() {
        if (tabLayout.getSelectedTabPosition() == 4) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Добавить закладку");

            // Set up the input
            final EditText input = new EditText(getActivity());
            input.setInputType(InputType.TYPE_CLASS_TEXT );
            builder.setView(input);

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String m_Text = input.getText().toString();
                    presenter.createBookmarkCategory(m_Text);
                }
            });
            builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        } else {
            Intent intent = new Intent(getActivity(), ProcessTemplatesActivity.class);
            intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
            startActivity(intent);
        }
    }

    @Override
    public void processStatsLoaded(ProcessUserStatsViewModel processUserStatsViewModel) {
//        TabLayout.Tab tab = tabLayout.getTabAt(0);
//        if (processUserStatsViewModel.getCurrentProcessCount() > 99)
//            tab.setText(getResources().getString(R.string.processes_my) + " +99");
//        else
//            tab.setText(getResources().getString(R.string.processes_my) + " " + processUserStatsViewModel.getCurrentProcessCount());
//
//        TabLayout.Tab tab2 = tabLayout.getTabAt(1);
//
//        if (processUserStatsViewModel.getOverdueProcessCount() + processUserStatsViewModel.getNearOverdueProcessCount() > 99)
//            tab2.setText(getResources().getString(R.string.processes_outdated) + " +99");
//        else
//            tab2.setText(getResources().getString(R.string.processes_outdated) + " " +
//                    (processUserStatsViewModel.getOverdueProcessCount() + processUserStatsViewModel.getNearOverdueProcessCount()));
        Log.d("PROCESS", processUserStatsViewModel.getCurrentProcessCount() + "");
        Log.d("PROCESS", processUserStatsViewModel.getOverdueProcessCount() + "");
        Log.d("PROCESS", processUserStatsViewModel.getNearOverdueProcessCount() + "");

    }

    @Override
    public void categoryCreated(BookmarkCategoryViewModel categoryViewModel) {
        ((ProcessBookmarkFragment)fragmentList.get(4)).updateBookmarkList();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        ProcessesListFragment tempFragment = (ProcessesListFragment)fragmentList.get(tabLayout.getSelectedTabPosition());
        tempFragment.searchText(newText);
        return false;
    }
}
