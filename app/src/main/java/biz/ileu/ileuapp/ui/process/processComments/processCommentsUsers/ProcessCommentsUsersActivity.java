package biz.ileu.ileuapp.ui.process.processComments.processCommentsUsers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import biz.ileu.ileuapi.models.process.ParticipantViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import biz.ileu.ileuapp.utils.ui.ParticipantsCheckClick;
import biz.ileu.ileuapp.utils.ui.adapters.ParticipantAddAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProcessCommentsUsersActivity extends BaseActivity implements ProcessCommentsUserContract.ProcessCommentsUserView, ParticipantsCheckClick.OnParticipantPositionCheckedUncheckedListener {

    @BindView(R.id.rlProcessUsers)
    RecyclerView rlProcessUsers;


    private Process currentProcess;
    private CompanyWorkInfoListViewModel currentCompany = null;
    private int countSelected = 0;
    private List<Object> usersList;
    private Set<ParticipantViewModel> participantViewModelSet;

    private ParticipantAddAdapter adapter;
    private ProcessCommentsUserPresenter presenter;

    private List<ParticipantViewModel> selectedParticipants;
    ProcessCommentsUsersActivity curr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_comments_users);
        ButterKnife.bind(this);
        bindViews();
        setData();
    }

    @Override
    public void bindViews() {
        initBaseToolbar(true);
        getSupportActionBar().setTitle(R.string.process_detail_participants);
        usersList = new ArrayList<>();
        adapter = new ParticipantAddAdapter(usersList, this);
        presenter = new ProcessCommentsUserPresenter(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rlProcessUsers.setLayoutManager(layoutManager);
        rlProcessUsers.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setData() {
        currentCompany = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_COMPANY));
        currentProcess = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_PROCESS));

        selectedParticipants = new ArrayList<>();
        participantViewModelSet = new LinkedHashSet<>();

        if (currentProcess.getExecutor() != null && currentProcess.getExecutor().getId() != PrefUtils.getCurrentUserId())
            participantViewModelSet.add(currentProcess.getExecutor());
        if (currentProcess.getCreator() != null && currentProcess.getCreator().getId() != PrefUtils.getCurrentUserId())
            participantViewModelSet.add(currentProcess.getCreator());
        if (currentProcess.getResponsible() != null && currentProcess.getResponsible().getId() != PrefUtils.getCurrentUserId())
            participantViewModelSet.add(currentProcess.getResponsible());

        for (ParticipantViewModel participantViewModel : currentProcess.getParticipants())
            if (participantViewModel.getId() != PrefUtils.getCurrentUserId())
                participantViewModelSet.add(participantViewModel);


        for (ParticipantViewModel participantViewModel : participantViewModelSet) {
            usersList.add(participantViewModel);
        }



        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.process_users, menu);
        if (this.currentProcess.getResponsible().getId() != PrefUtils.getCurrentUserId()) {
            MenuItem delete = menu.findItem(R.id.actionRemoveUser);
            delete.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.actionUsersSelect) {
            if (selectedParticipants.size() > 0) {
                Intent intent = new Intent();
                int[] ids = new int[selectedParticipants.size()];
                for (int i = 0; i < selectedParticipants.size(); i++)
                    ids[i] = selectedParticipants.get(i).getId();
                intent.putExtra(Constants.ENTITY_PROCESS_SELECTED_USERS, ids);
                setResult(RESULT_OK, intent);
                finish();
            } else {
                Toast.makeText(this, R.string.message_chat_create_select_participant, Toast.LENGTH_SHORT).show();
            }
        } else if (item.getItemId() == R.id.actionAddUser) {

        } else if (item.getItemId() == R.id.actionRemoveUser) {
            if (selectedParticipants.size() > 0) {
                int[] ids = new int[selectedParticipants.size()];
                int tempId;
                for (int i = 0; i < selectedParticipants.size(); i++) {
                    tempId = selectedParticipants.get(i).getId();
                    if (tempId == currentProcess.getExecutor().getId() ||
                            tempId == currentProcess.getResponsible().getId() ||
                            tempId == currentProcess.getCreator().getId()) {
                        Toast.makeText(this, R.string.message_process_comments_denied_delete, Toast.LENGTH_SHORT).show();
                        selectedParticipants.remove(i);
                    }
                }
                for (int i = 0; i < selectedParticipants.size(); i++) {
                    ids[i] = selectedParticipants.get(i).getId();
                    presenter.removeParticipants(ids[i], currentProcess.getId(), currentCompany.getId());
                }

            } else {
                Toast.makeText(this, R.string.message_chat_create_select_participant, Toast.LENGTH_SHORT).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnPositionChecked(View v, int position) {

        selectedParticipants.add((ParticipantViewModel) usersList.get(position));
        Log.d("SELECTED", selectedParticipants.size() + "");
    }

    @Override
    public void OnPositionUnchecked(View v, int position) {
        selectedParticipants.remove(usersList.get(position));
        Log.d("SELECTED", selectedParticipants.size() + "");
    }


    @Override
    public void successRemoved() {
        countSelected++;
        if (countSelected == selectedParticipants.size()) {
            Intent intent = new Intent();
            intent.putExtra(Constants.ENTITY_PROCESS_REMOVED_USERS, 1);
            setResult(RESULT_FIRST_USER, intent);
            Toast.makeText(this, R.string.success_removed, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void errorRemove(String e) {
        countSelected++;
        Toast.makeText(this, e, Toast.LENGTH_SHORT).show();
    }
}
