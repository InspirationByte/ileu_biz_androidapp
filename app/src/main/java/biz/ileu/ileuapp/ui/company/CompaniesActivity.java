package biz.ileu.ileuapp.ui.company;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.company.CompanyViewModel;
import biz.ileu.ileuapi.models.news.NewsCommentList;
import biz.ileu.ileuapi.models.news.NewsCommentViewModel;
import biz.ileu.ileuapi.models.news.NewsEntryViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.company.CompaniesContract;
import biz.ileu.ileuapp.ui.company.CompaniesPresenter;
import biz.ileu.ileuapp.ui.menu.MenuActivity;
import biz.ileu.ileuapp.ui.company.CompaniesListAdapter;
import biz.ileu.ileuapp.ui.newsList.NewsCommentsListAdapter;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import biz.ileu.ileuapp.utils.ui.EndlessRecyclerOnScrollListener;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class CompaniesActivity extends BaseActivity implements CompaniesContract.CompaniesView, RecyclerViewClick.OnItemClickListener, SearchView.OnQueryTextListener {

    @BindView(R.id.rlCompany)
    RecyclerView rlCompany;
    @BindView(R.id.srlCompanies)
    SwipeRefreshLayout srlCompanies;
    @BindView(R.id.svCompanies)
    SearchView svCompanies;

    private CompaniesPresenter presenter;

    private List<CompanyViewModel> companiesList;

    private CompaniesListAdapter adapter;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private ProgressDialog progressDialog;

    private EndlessRecyclerOnScrollListener listener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        ButterKnife.bind(this);
        bindViews();
        setData();
    }

    @Override
    public void bindViews() {
        initBaseToolbar(true);

        getSupportActionBar().setTitle(R.string.join);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void setData() {
        presenter = new CompaniesPresenter(this);
        svCompanies.setOnQueryTextListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);


        companiesList = new ArrayList<>();

        adapter = new CompaniesListAdapter(companiesList, this);
        rlCompany.setLayoutManager(layoutManager);
        rlCompany.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getResources().getString(R.string.process_comment_sending));

        listener = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                presenter.getCompanies(current_page, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        };

        srlCompanies.post(new Runnable() {
            @Override
            public void run() {
                srlCompanies.setRefreshing(true);
                presenter.getCompanies(1, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        });

        srlCompanies.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                companiesList.clear();
                adapter.notifyDataSetChanged();
                listener.reset();
                srlCompanies.setRefreshing(true);
                presenter.getCompanies(1, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        });

    }

//    @Override
//    public void commentSent(NewsCommentViewModel comment) {
//        companiesList.add(comment);
//        adapter.notifyDataSetChanged();
//        progressDialog.dismiss();
//        etNewsComment.setText("");
//        rlNewsComments.scrollToPosition(adapter.getItemCount() - 1);
//        Toast.makeText(this, R.string.process_comment_added, Toast.LENGTH_SHORT).show();
//    }

//
//    @OnClick(R.id.ibNewsCommentPost)
//    public void onViewClicked() {
//        if (etNewsComment.getText().toString().length() == 0) {
//            Toast.makeText(this, getResources().getString(R.string.process_comment_text_empty), Toast.LENGTH_SHORT).show();
//            etNewsComment.requestFocus();
//
//            return;
//        }
//
//        presenter.sendComment(etNewsComment.getText().toString());
//    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.destroy();
    }


    @Override
    public void loadCompaniesErrror(String error) {
        srlCompanies.setRefreshing(false);
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void companiesLoaded(List<CompanyViewModel> companies) {
        srlCompanies.setRefreshing(false);
        companiesList.addAll(companies);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void successJoined() {
        Toast.makeText(this, R.string.request_sended, Toast.LENGTH_SHORT).show();
        companiesList.clear();
        adapter.notifyDataSetChanged();
        listener.reset();
        srlCompanies.setRefreshing(true);
        presenter.getCompanies(1, Constants.DEFAULT_PAGE_MAX_SIZE);
    }

    @Override
    public void joinError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(View view, int position) {
        CompanyViewModel company = companiesList.get(position);
        if (company.hasSentJoinRequest()) {
            Toast.makeText(this, R.string.request_already_sended, Toast.LENGTH_SHORT).show();
            return;
        }
        presenter.join(company.getId());

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) { newText = newText.toLowerCase(Locale.getDefault());

        List<CompanyViewModel> filteredList = new ArrayList<>();
        for (CompanyViewModel company : this.companiesList)
            if (company.getName().toLowerCase(Locale.getDefault()).contains(newText))
                filteredList.add(company);

        adapter.changeDisplayCompanies(filteredList);

        return false;
    }

}
