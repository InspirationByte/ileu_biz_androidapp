package biz.ileu.ileuapp.ui.process.processComments.processCommentsUsersAdd;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import biz.ileu.ileuapi.models.process.UsersViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.ui.ParticipantsCheckClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by macair on 09.08.17.
 */

public class ProcessCommentsUserAdapter extends RecyclerView.Adapter<ProcessCommentsUserAdapter.ProcessCommentsUserViewHolder> {

    private List<UsersViewModel> objectList;

    private ParticipantsCheckClick.OnParticipantPositionCheckedUncheckedListener listener;

    public ProcessCommentsUserAdapter(List<UsersViewModel> objectList,
                                 ParticipantsCheckClick.OnParticipantPositionCheckedUncheckedListener listener) {
        this.objectList = objectList;
        this.listener = listener;

    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }

    @Override
    public ProcessCommentsUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_process_create_participants_add, parent, false);
        return new ProcessCommentsUserAdapter.ProcessCommentsUserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProcessCommentsUserViewHolder holder, int position) {
        UsersViewModel object = objectList.get(position);


        holder.tvRowParticipantAddSubtitle.setText(object.getPositionName());
        holder.tvRowParticipantAddTitle.setText(object.getDisplayName());

        Picasso.with(holder.context)
                .load(Constants.BASE_URL + object.getAvatar())
                .into(holder.civRowParticipantsAdd);

        holder.cbRowParticipantAdd.setVisibility(View.GONE);
    }



    public class ProcessCommentsUserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;

        @BindView(R.id.civRowParticipantsAdd)
        CircleImageView civRowParticipantsAdd;

        @BindView(R.id.tvRowParticipantAddTitle)
        TextView tvRowParticipantAddTitle;

        @BindView(R.id.tvRowParticipantAddSubtitle)
        TextView tvRowParticipantAddSubtitle;

        @BindView(R.id.llRowParticipantAdd)
        LinearLayout llRowParticipantAdd;


        @BindView(R.id.cbRowParticipantAdd)
        CheckBox cbRowParticipantAdd;



        public ProcessCommentsUserViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            ButterKnife.bind(this, itemView);
            llRowParticipantAdd.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.OnPositionChecked(v, getAdapterPosition());
        }


    }
}
