package biz.ileu.ileuapp.ui.bookmarks;

import java.util.List;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.bookmarks.BookmarkViewModel;
import biz.ileu.ileuapi.models.bookmarks.BookmarksCategoriesList;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.utils.PrefUtils;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by macair on 09.08.17.
 */

public class BookmakrCategoriesPresenter {

    private CompanyWorkInfoListViewModel currentCompany;

    private BookmarkCategoriesContract.BookmarkCategoriesView bookmarkView;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public BookmakrCategoriesPresenter(BookmarkCategoriesContract.BookmarkCategoriesView bookmarkView, CompanyWorkInfoListViewModel currentCompany) {
        this.bookmarkView = bookmarkView;
        this.currentCompany = currentCompany;
    }

    public void loadBookmarkCategories(int page, int per) {
        Subscription subscription = RestClient.request().getBookmarkCategories(PrefUtils.getToken(), this.currentCompany.getId(), page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BookmarksCategoriesList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        bookmarkView.loadingError(e.getMessage());

                    }

                    @Override
                    public void onNext(BookmarksCategoriesList categoriesList) {
                        bookmarkView.categoriesLoaded(categoriesList);
                    }
                });
        compositeSubscription.add(subscription);
    }
    public void addBookmark(int processId, int categoryId) {
        Subscription subscription = RestClient.request().addBookmark(PrefUtils.getToken(), currentCompany.getId(), categoryId, processId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BookmarkViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        bookmarkView.addingError(e.getMessage());

                    }

                    @Override
                    public void onNext(BookmarkViewModel bookmark) {
                        bookmarkView.successAdded();
                    }
                });
        compositeSubscription.add(subscription);
    }
    public void removeBookmark(int processId, int categoryId) {
        Subscription subscription = RestClient.request().removeBookmark(PrefUtils.getToken(), currentCompany.getId(), categoryId, processId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BookmarkViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        bookmarkView.addingError(e.getMessage());

                    }

                    @Override
                    public void onNext(BookmarkViewModel bookmark) {
                        bookmarkView.successRemoved();
                    }
                });
        compositeSubscription.add(subscription);
    }
    public void getBookmarkCategoriesFor(int processId) {
        Subscription subscription = RestClient.request().getBookmarkCategoriesFor(PrefUtils.getToken(), currentCompany.getId(), processId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Integer>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        bookmarkView.addingError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<Integer> categoriesIds) {
                        bookmarkView.categoriesIdsLoaded(categoriesIds);
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();
        bookmarkView = null;
    }

}
