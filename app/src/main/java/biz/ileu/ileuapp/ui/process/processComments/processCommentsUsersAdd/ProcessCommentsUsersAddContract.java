package biz.ileu.ileuapp.ui.process.processComments.processCommentsUsersAdd;

import java.util.List;

import biz.ileu.ileuapi.models.process.ParticipantViewModel;
import biz.ileu.ileuapi.models.process.UsersViewModel;

/**
 * Created by macair on 09.08.17.
 */

public interface ProcessCommentsUsersAddContract {
    public interface ProcessCommentsUsersAddView {
        public void usersLoaded(List<UsersViewModel> users);
        public void usersFail(String e);

        public void participantAdded(ParticipantViewModel users);
    }
}
