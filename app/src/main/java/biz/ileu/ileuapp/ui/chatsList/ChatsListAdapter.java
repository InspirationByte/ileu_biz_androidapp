package biz.ileu.ileuapp.ui.chatsList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import biz.ileu.ileuapi.models.chat.ChatsViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Daniq on 19.04.2017.
 */

public class ChatsListAdapter extends RecyclerView.Adapter<ChatsListAdapter.ChatViewHolder> {

    private RecyclerViewClick.OnItemClickListener listener;

    List<ChatsViewModel> chatsList;

    public ChatsListAdapter(List<ChatsViewModel> chatsList, RecyclerViewClick.OnItemClickListener listener) {
        this.chatsList = chatsList;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return chatsList.size();
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_chat, parent, false);
        return new ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        ChatsViewModel chat = chatsList.get(position);

        String chatDisplayName = "";
        for (int i = 0; i < chat.getChatUsers().size(); i++) {
            chatDisplayName += chat.getChatUsers().get(i).getDisplayName();
            if (i + 1 < chat.getChatUsers().size())
                chatDisplayName += ", ";
        }

        holder.tvRowChatName.setText(chatDisplayName);
        holder.tvRowChatLastMessage.setText(chat.getLastMessageText().length() > 64 ?
                chat.getLastMessageText().substring(0, 63) + "..." : chat.getLastMessageText());

        Picasso.with(holder.context)
                .load(Constants.BASE_URL + chat.getChatUsers().get(0).getProfilePicture())
                .into(holder.civRowChatAvatar);

        PrettyTime prettyTime = new PrettyTime(new Locale("ru"));




        if (chat.getLastMessageTime() != 0)
            holder.tvRowChatTime.setText(prettyTime.format(new Date(chat.getLastMessageTime())));
        else {
            holder.tvRowChatTime.setText("");
        }

        holder.tvBadge.setText("");

        if (chat.getNotificationsCount() == 0) {
            holder.rlBadge.setVisibility(View.GONE);
        }
        if (chat.getNotificationsCount() > 0) {
            holder.rlBadge.setVisibility(View.VISIBLE);
            holder.tvBadge.setText(chat.getNotificationsCount() + "");
        }
    }

    public List<ChatsViewModel> getChatsList() {
        return chatsList;
    }

    public void changeDisplayMessages(List<ChatsViewModel> chatsViewModels) {
        chatsList.clear();
        chatsList.addAll(chatsViewModels);
    }

    public class ChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;

        @BindView(R.id.civRowChatAvatar)
        CircleImageView civRowChatAvatar;

        @BindView(R.id.tvRowChatName)
        TextView tvRowChatName;

        @BindView(R.id.tvRowChatLastMessage)
        TextView tvRowChatLastMessage;

        @BindView(R.id.tvRowChatTime)
        TextView tvRowChatTime;

        @BindView(R.id.rlBadge)
        RelativeLayout rlBadge;

        @BindView(R.id.tvBadge)
        TextView tvBadge;

        public ChatViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.onItemClick(v, getAdapterPosition());
        }
    }

}
