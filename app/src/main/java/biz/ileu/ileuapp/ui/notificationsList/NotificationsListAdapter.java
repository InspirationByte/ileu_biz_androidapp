package biz.ileu.ileuapp.ui.notificationsList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import biz.ileu.ileuapi.models.process.ProcessNotificationViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Daniq on 06.04.2017.
 */

public class NotificationsListAdapter extends RecyclerView.Adapter<NotificationsListAdapter.NotificationViewHolder>{

    private List<ProcessNotificationViewModel> notificationList;
    private RecyclerViewClick.OnItemClickListener listener;

    public NotificationsListAdapter(List<ProcessNotificationViewModel> notificationList,
                                    RecyclerViewClick.OnItemClickListener listener) {
        this.notificationList = notificationList;
        this.listener = listener;
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_notification, parent, false);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        ProcessNotificationViewModel notification = notificationList.get(position);

        Picasso.with(holder.context)
                .load(Constants.BASE_URL + notification.getLastMessageAvatar())
                .into(holder.civRowAvatar);
        if (notification.getTotalUnreadMessages() <= 0)
            holder.indicator.setVisibility(View.GONE);
        else
            holder.indicator.setVisibility(View.VISIBLE);
        holder.tvNotificationTitle.setText(notification.getTitle());
        if (notification.getTotalUnreadMessages() > 1) {
            int numberOfUnread = notification.getTotalUnreadMessages() - 1;
            holder.tvNotificationMessage.setText(notification.getLastUnreadMessage() + " и еще "
                    + numberOfUnread + (numberOfUnread > 1 ? " комментария" : " комментарий"));
        }
        else
            holder.tvNotificationMessage.setText(notification.getLastUnreadMessage());
        holder.tvRowProcessId.setText(String.valueOf(notification.getProcessId()));
//        holder.tvRowProcessStatus.setText(notification.getStatus());
        holder.tvRowProcessDate.setText(Constants.getFormattedDateTime(notification.getEndDateTime()));
        holder.tvRowProcessDescription.setText(notification.getDescriptionText());
        holder.tvRowName.setText(notification.getLastMessageName());
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;

        @BindView(R.id.viewIndicator)
        RelativeLayout indicator;

        @BindView(R.id.tvNotificationTitle)
        TextView tvNotificationTitle;

        @BindView(R.id.tvRowProcessId)
        TextView tvRowProcessId;

        @BindView(R.id.tvRowProcessStatus)
        TextView tvRowProcessStatus;

        @BindView(R.id.tvRowProcessDate)
        TextView tvRowProcessDate;

        @BindView(R.id.tvRowProcessDescription)
        TextView tvRowProcessDescription;

        @BindView(R.id.tvRowName)
        TextView tvRowName;


        @BindView(R.id.tvNotificationMessage)
        TextView tvNotificationMessage;

        @BindView(R.id.civRowAvatar)
        CircleImageView civRowAvatar;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.onItemClick(v, getAdapterPosition());
        }
    }
}
