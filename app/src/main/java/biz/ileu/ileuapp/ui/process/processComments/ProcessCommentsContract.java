package biz.ileu.ileuapp.ui.process.processComments;

import android.content.ContentResolver;
import android.content.Context;

import java.io.File;
import java.util.List;

import biz.ileu.ileuapi.models.process.DocumentAgreedByViewModel;
import biz.ileu.ileuapi.models.process.DocumentViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.process.ProcessCommentViewModel;
import biz.ileu.ileuapi.models.process.ProcessFileViewModel;
import biz.ileu.ileuapi.models.processtypes.ProcessTemplateViewModel;
import biz.ileu.ileuapi.models.tables.DSResultWithTypeOfProcessRowDTO;
import biz.ileu.ileuapi.models.tables.ProcessRowDTO;
import biz.ileu.ileuapi.models.usercontext.ContextInfoDTO;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by Daniq on 13.04.2017.
 */

public interface ProcessCommentsContract {

    interface ProcessCommentsView extends BaseView{

        Context getContext();

        ContentResolver getResolver();

        void loadCommentsError(String error);
        void loadDocumetsError(String error);
        void documentsLoaded(DocumentViewModel document);

        void commentsLoaded(List<ProcessCommentViewModel> comments, String tabId);

        void sendingComment();

        void sendingCommentError(String error);

        void commentSent(ProcessCommentViewModel comment);

        void loadFilesError(String error);

        void filesLoaded(List<ProcessFileViewModel> processFileViewModels);

        void downloadingFile();
        void downloadingCancelled();
        void downloadError(String string);
        void downloadCompleted(File file);

        void audioRecordError(String error);

        void audioPlayError(String error);

        void processLoaded(Process process);
        void updateProccess(Process process);
        void agreedError(String message);

        void successAgreed(DocumentAgreedByViewModel agreed);

        void documentStored(DocumentViewModel document);

        void documentStoreError(String message);

        void loadRowsError(String message);

        void rowsLoaded(DSResultWithTypeOfProcessRowDTO dsResult);

        void processNameTemplatedLoaded(ProcessTemplateViewModel processTemplate);

        void processRowRemoved();

        void updatedRow(ProcessRowDTO dsResult);

        void createdRow(ProcessRowDTO dsResult);

        void companyContextLoaded(ContextInfoDTO contextInfoDTO);
    }

    interface ProcessCommentsPresenter extends BasePresenter {

    }
}
