package biz.ileu.ileuapp.ui.process.processComments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import biz.ileu.ileuapi.models.process.ParticipantViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.process.ProcessFileViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;

/**
 * Created by Daniq on 26.04.2017.
 */

public class ProcessDetailViewHolder implements  ProcessDetailViewContract.ProcessDetailView {

    private Context context;

    @BindView(R.id.tvProcessTitle)
    TextView tvProcessTitle;

    @BindView(R.id.tvProcessId)
    TextView tvProcessId;

    @BindView(R.id.tvProcessStatus)
    TextView tvProcessStatus;

    @BindView(R.id.tvProcessDate)
    TextView tvProcessDate;

    @BindView(R.id.tvProcessDescription)
    TextView tvProcessDescription;

    @BindView(R.id.ivProcessDetailClose)
    ImageView ivProcessDetailClose;

    @BindView(R.id.llProcessParticipants)
    LinearLayout llProcessParticipants;

    @BindView(R.id.glProcessFiles)
    GridLayout glProcessFiles;

    @BindView(R.id.btnProcessCompleted)
    Button btnProcessCompleted;

    @BindView(R.id.btnProcessFinishNegative)
    Button btnProcessFinishNegative;

    @BindView(R.id.btnProcessFinishPositive)
    Button btnProcessFinishPositive;

    @BindView(R.id.btnProcessChangeDate)
    Button btnProcessChangeDate;

    @BindView(R.id.btnProcessAddToCart)
    Button btnProcessAddToCart;





    private CompanyWorkInfoListViewModel currentCompany;
    private Process currentProcess;
    private PopupWindow popupWindow;
    final Calendar myCalendar = Calendar.getInstance();

    private List<ProcessFileViewModel> fileViewModels;
    private ProcessDetailViewPresenter presenter;
    private ProcessCommentsActivity activity;
    EditText input;
    ProcessDetailViewHolder(ProcessCommentsActivity processCommentsActivity, View view, Process currentProcess, PopupWindow popupWindow,  CompanyWorkInfoListViewModel currentCompany) {
        this.currentProcess = currentProcess;
        context = view.getContext();
        this.popupWindow = popupWindow;
        this.currentCompany = currentCompany;
        this.activity = processCommentsActivity;
        ButterKnife.bind(this, view);
        fileViewModels = new ArrayList<>();
        presenter = new ProcessDetailViewPresenter(this, currentProcess, currentCompany);
        setViews();
    }
    public void setCurrentProcess(Process currentProcess) {
        this.currentProcess = currentProcess;
    }
    private void setViews() {
        tvProcessTitle.setText(currentProcess.getTitle());
        tvProcessId.setText("#" + currentProcess.getId());

        tvProcessStatus.setText(currentProcess.getStatusName());
        if (currentProcess.getStartDateTime() != null)
            tvProcessDate.setText(Constants.getFormattedDateTime(currentProcess.getStartDateTime()) + " - "
                    + Constants.getFormattedDateTime(currentProcess.getEndDateTime()));
        else
            tvProcessDate.setText("");

        String descriptionToShow = "";
        if (currentProcess.getDescription() != null)
            descriptionToShow = currentProcess.getDescription();
        else
            descriptionToShow = currentProcess.getDescriptionText();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            tvProcessDescription.setText(Html.fromHtml(descriptionToShow, Html.FROM_HTML_MODE_COMPACT));
        else
            tvProcessDescription.setText(Html.fromHtml(descriptionToShow));

        setParticipants();
        setButtons();
        ivProcessDetailClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null && popupWindow.isShowing())
                    popupWindow.dismiss();
            }
        });
    }
    public void setButtons() {
        btnProcessCompleted.setVisibility(View.GONE);
        btnProcessFinishNegative.setVisibility(View.GONE);
        btnProcessFinishPositive.setVisibility(View.GONE);
        btnProcessChangeDate.setVisibility(View.GONE);
        btnProcessAddToCart.setVisibility(View.GONE);
        if (currentProcess.getExecutor().getId() == Constants.currentUserId) {
            if (currentProcess.getStatus() == 0 || currentProcess.getStatus() == 1 || currentProcess.getStatus() == 2)
                btnProcessCompleted.setVisibility(View.VISIBLE);
        }
        if (currentProcess.getResponsible().getId() == Constants.currentUserId) {
            if (currentProcess.getStatus() == 3 || currentProcess.getStatus() == 8) {
                btnProcessFinishNegative.setVisibility(View.VISIBLE);
                btnProcessFinishPositive.setVisibility(View.VISIBLE);
            }
            btnProcessChangeDate.setVisibility(View.VISIBLE);
            btnProcessAddToCart.setVisibility(View.VISIBLE);

        }
    }
    public void setParticipants() {
        llProcessParticipants.removeAllViews();
        if (currentProcess.getCreator() != null) {
            View creatorView = LayoutInflater.from(context).inflate(R.layout.process_detail_view_participant_layout, null, false);
            ProcessParticipantViewHolder creator = new ProcessParticipantViewHolder(creatorView);

            creator.tvParticipant.setText(currentProcess.getCreator().getDisplayName());
            creator.tvParticipantType.setText(R.string.process_detail_participant_type_creator);

            Picasso.with(context)
                    .load(Constants.BASE_URL + currentProcess.getCreator().getProfilePicture())
                    .into(creator.civParticipant);

            if (currentProcess.getCreator().isIsExecuting()) {
                creator.civParticipant.setBorderWidth(8);
                creator.civParticipant.setBorderColor(Color.parseColor("#0092ff"));
            }

            llProcessParticipants.addView(creatorView);
        }

        if (currentProcess.getExecutor() != null) {
            View executorView = LayoutInflater.from(context).inflate(R.layout.process_detail_view_participant_layout, null, false);
            ProcessParticipantViewHolder executor = new ProcessParticipantViewHolder(executorView);

            executor.tvParticipant.setText(currentProcess.getExecutor().getDisplayName());
            executor.tvParticipantType.setText(R.string.process_detail_participant_type_executor);

            Picasso.with(context)
                    .load(Constants.BASE_URL + currentProcess.getExecutor().getProfilePicture())
                    .into(executor.civParticipant);
            if (currentProcess.getStatus() == 0 || currentProcess.getStatus() == 2) {
                executor.civParticipant.setBorderWidth(8);
                executor.civParticipant.setBorderColor(Color.parseColor("#FBC02D"));
            } else if (currentProcess.getExecutor().isIsExecuting()) {
                executor.civParticipant.setBorderWidth(8);
                executor.civParticipant.setBorderColor(Color.parseColor("#0092ff"));
            }



            llProcessParticipants.addView(executorView);
        }

        if (currentProcess.getResponsible() != null) {
            View responsibleView = LayoutInflater.from(context).inflate(R.layout.process_detail_view_participant_layout, null, false);
            ProcessParticipantViewHolder responsible = new ProcessParticipantViewHolder(responsibleView);

            responsible.tvParticipant.setText(currentProcess.getResponsible().getDisplayName());
            responsible.tvParticipantType.setText(R.string.process_detail_participant_type_responsive);

            Picasso.with(context)
                    .load(Constants.BASE_URL + currentProcess.getResponsible().getProfilePicture())
                    .into(responsible.civParticipant);
            if (currentProcess.getStatus() == 3 || currentProcess.getStatus() == 8) {
                responsible.civParticipant.setBorderWidth(8);
                responsible.civParticipant.setBorderColor(Color.parseColor("#FBC02D"));
            }
            llProcessParticipants.addView(responsibleView);
        }

        if (currentProcess.getParticipants() != null) {
            Set<ParticipantViewModel> set = new LinkedHashSet<>();
            set.addAll(currentProcess.getParticipants());
            for (ParticipantViewModel participantViewModel : set) {
                View participantView = LayoutInflater.from(context).inflate(R.layout.process_detail_view_participant_layout, null, false);
                ProcessParticipantViewHolder participant = new ProcessParticipantViewHolder(participantView);
                participant.tvParticipant.setText(participantViewModel.getDisplayName());
                participant.tvParticipantType.setText(R.string.process_detail_participant_type_participant);
                Picasso.with(context)
                        .load(Constants.BASE_URL + participantViewModel.getProfilePicture())
                        .into(participant.civParticipant);
                if (participantViewModel.isIsExecuting()) {
                    participant.civParticipant.setBorderWidth(8);
                    participant.civParticipant.setBorderColor(Color.parseColor("#0092ff"));
                }
                llProcessParticipants.addView(participantView);
            }
        }
    }
    @OnClick(R.id.btnProcessCompleted)
    public void processCompletedClicked() {
        presenter.processCompleted();
    }
    @OnClick(R.id.btnProcessFinishNegative)
    public void processFinishNegativeClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Причина");

        // Set up the input
        input = new EditText(context);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);

        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.processFinishNegative(input.getText().toString());
            }
        });
        builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }
    @OnClick(R.id.btnProcessFinishPositive)
    public void processFinishPositiveClicked() {

        presenter.processFinishPositive();

    }
    @OnClick(R.id.btnProcessAddToCart)
    public void processAddToCartClicked() {

        presenter.processAddToCart();

    }
    @OnClick(R.id.btnProcessChangeDate)
    public void processChangeDateClicked()  {
//        String date;
        String startDateS = currentProcess.getStartDateTime();
        String endDateS = currentProcess.getStartDateTime();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Продлить");

        // Set up the input
        input = new EditText(context);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        input.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(context, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String start_dt = input.getText().toString();
                DateFormat formatter = new SimpleDateFormat("MM/dd/yy");
                Date parsedDate = new Date();
                try {
                    parsedDate = (Date) formatter.parse(start_dt);
                } catch (ParseException p) {

                }

                SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");
                String finalString = newFormat.format(parsedDate) + Constants.getCurrentTimezoneOffset();
//                String  m_Text = input.getText().toString();

                presenter.processChangeDate(finalString);
            }
        });
        builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        input.setText(sdf.format(myCalendar.getTime()));
    }
    public void setFiles(List<ProcessFileViewModel> fileViewModels) {
        this.fileViewModels.clear();
        this.fileViewModels.addAll(fileViewModels);

        glProcessFiles.removeAllViews();

        for (ProcessFileViewModel model: this.fileViewModels) {

            View view = LayoutInflater.from(context).inflate(R.layout.item_process_file, glProcessFiles, false);
            ProcessFileViewHolder holder = new ProcessFileViewHolder(view, model);

            glProcessFiles.addView(view);
        }
    }

    public void addFile(ProcessFileViewModel fileViewModel) {
        this.fileViewModels.add(fileViewModel);

            View view = LayoutInflater.from(context).inflate(R.layout.item_process_file, glProcessFiles, false);
            ProcessFileViewHolder holder = new ProcessFileViewHolder(view, fileViewModel);

            glProcessFiles.addView(view);

    }

    public void removeFile(int fileId) {
        for (Iterator<ProcessFileViewModel> iterator = fileViewModels.iterator(); iterator.hasNext();) {
            ProcessFileViewModel processFileViewModel = iterator.next();
            if (processFileViewModel.getId() == fileId) {
                iterator.remove();
                break;
            }
        }
        List<ProcessFileViewModel> processFileViewModels = new ArrayList<>();
        processFileViewModels.addAll(fileViewModels);
        setFiles(processFileViewModels);
    }

    public void setStatus(int status) {
        tvProcessStatus.setText(Constants.getStatusByInt(status));
        currentProcess.setStatus(status);
        setButtons();
    }

    @Override
    public void failedResult(String e) {
        Toast.makeText(context, e, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void successChanged(ResponseBody responseBody) {
        Gson gson = new Gson();
        Map<String, String> myMap = null;
        try {
            myMap = gson.fromJson(responseBody.string(), new TypeToken<Map<String, String>>(){}.getType());

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (myMap.containsKey("error")) {
            failedResult(myMap.get("error"));
        } else {
//            activity.updateProcess();
            popupWindow.dismiss();
        }

    }
    public void updateButtons(Process p) {
        currentProcess = p;
        setButtons();
    }

    public class ProcessFileViewHolder implements View.OnClickListener {
        @BindView(R.id.ivProcessFile)
        public ImageView ivProcessFile;

        @BindView(R.id.tvProcessFile)
        public TextView tvProcessFile;

        @BindView(R.id.llProcessFile)
        public LinearLayout llProcessFile;

        public ProcessFileViewModel processFileViewModel;

        ProcessFileViewHolder(View view, ProcessFileViewModel processFileViewModel) {
            ButterKnife.bind(this, view);
            this.processFileViewModel = processFileViewModel;

            tvProcessFile.setText(processFileViewModel.getFileName().length() < 10 ?
                    processFileViewModel.getFileName() : processFileViewModel.getFileName().substring(0, 9) + "...");
            if (processFileViewModel.getFileMime().equals("application/pdf"))
                ivProcessFile.setImageResource(R.mipmap.ic_pdf_dark_grey);
            else if (processFileViewModel.getFileMime().equals("video/mp4") ||
                    processFileViewModel.getFileMime().equals("video/3gpp") ||
                    processFileViewModel.getFileMime().equals("video/mpeg") ||
                    processFileViewModel.getFileMime().equals("video/x-ms-wmv") ||
                    processFileViewModel.getFileMime().equals("video/x-flv") ||
                    processFileViewModel.getFileMime().equals("application/x-troff-msvideo") ||
                    processFileViewModel.getFileMime().equals("video/avi"))
                ivProcessFile.setImageResource(R.mipmap.ic_video_dark_grey);
            else if (processFileViewModel.getFileMime().startsWith("audio/"))
                ivProcessFile.setImageResource(R.mipmap.ic_audio_dark_grey);
            else
                ivProcessFile.setImageResource(R.mipmap.ic_file_dark_grey);

            llProcessFile.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            ProcessCommentsActivityPermissionsDispatcher.downloadFileWithCheck((ProcessCommentsActivity) context,
                    processFileViewModel.getFileName(),
                    currentProcess.getId() + "", processFileViewModel.getId() + "");
        }
    }

    public class ProcessParticipantViewHolder {
        @BindView(R.id.tvProcessDetailParticipantName)
        TextView tvParticipant;

        @BindView(R.id.tvProcessDetailParticipantType)
        TextView tvParticipantType;

        @BindView(R.id.civProcessDetailParticipant)
        CircleImageView civParticipant;

        public ProcessParticipantViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
