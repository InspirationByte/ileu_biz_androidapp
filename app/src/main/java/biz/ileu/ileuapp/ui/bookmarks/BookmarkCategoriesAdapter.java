package biz.ileu.ileuapp.ui.bookmarks;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import biz.ileu.ileuapi.models.bookmarks.BookmarkCategoryViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.ui.BookmarksCategoryCheckClick;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by macair on 09.08.17.
 */

public class BookmarkCategoriesAdapter  extends RecyclerView.Adapter<BookmarkCategoriesAdapter.BookmarkCategoriesViewHolder> {

    private List<BookmarkCategoryViewModel> objectList;

    private BookmarksCategoryCheckClick.OnBookmarkCategoryPositionCheckedUncheckedListener listener;

    private List<Integer> selectedIndexes;
    private List<Integer> selectedIds;

    public BookmarkCategoriesAdapter(List<BookmarkCategoryViewModel> objectList,
                                     BookmarksCategoryCheckClick.OnBookmarkCategoryPositionCheckedUncheckedListener listener, List<Integer> selectedIds) {
        this.objectList = objectList;
        this.listener = listener;
        this.selectedIds = selectedIds;
        selectedIndexes = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }

    @Override
    public BookmarkCategoriesAdapter.BookmarkCategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_bookmark_category, parent, false);
        return new BookmarkCategoriesAdapter.BookmarkCategoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookmarkCategoriesAdapter.BookmarkCategoriesViewHolder holder, int position) {
        BookmarkCategoryViewModel object = objectList.get(position);


        holder.tvRowBookmarkCategoryTitle.setText(object.getTitle());

//        if ()
        if (selectedIndexes.contains(position)) {
            holder.cbRowBookmarkCategoryAdd.setChecked(true);
        }
        else
            holder.cbRowBookmarkCategoryAdd.setChecked(false);
    }

    public List<Integer> getSelectedIds() {
        return selectedIds;
    }

    public void setSelectedIds(List<Integer> selectedIds) {
        for (int i = 0; i < this.objectList.size(); i++) {
            if (selectedIds.contains(this.objectList.get(i).getId())) {
                selectedIndexes.add(i);
            }
        }
        this.selectedIds = selectedIds;
    }


    public class BookmarkCategoriesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;

        @BindView(R.id.tvRowBookmarkCategoryTitle)
        TextView tvRowBookmarkCategoryTitle;

        @BindView(R.id.cbRowBookmarkCategoryAdd)
        CheckBox cbRowBookmarkCategoryAdd;

        @BindView(R.id.llRowBookmarkCategoryAdd)
        LinearLayout llRowBookmarkCategoryAdd;



        public BookmarkCategoriesViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            ButterKnife.bind(this, itemView);
            llRowBookmarkCategoryAdd.setOnClickListener(this);
            cbRowBookmarkCategoryAdd.setClickable(false);
        }

        @Override
        public void onClick(View v) {
            cbRowBookmarkCategoryAdd.setChecked(!cbRowBookmarkCategoryAdd.isChecked());
            changedCheckedState(v, cbRowBookmarkCategoryAdd.isChecked());
        }

        private void changedCheckedState(View v, boolean isChecked) {
            if (listener == null)
                return;

            if (isChecked) {
                selectedIndexes.add(getAdapterPosition());
                listener.OnPositionChecked(v, getAdapterPosition());
            }
            else {
                selectedIndexes.remove(Arrays.asList(getAdapterPosition()));
                listener.OnPositionUnchecked(v, getAdapterPosition());
            }
        }
    }
}
