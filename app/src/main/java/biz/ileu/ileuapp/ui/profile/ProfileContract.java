package biz.ileu.ileuapp.ui.profile;

import biz.ileu.ileuapi.models.invitation.CompanyInvitationResultDTO;
import biz.ileu.ileuapi.models.profile.ProfileInfo;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by Daniq on 06.04.2017.
 */

public interface ProfileContract {

    interface ProfileView extends BaseView {
        void profileLoading();

        void profileError(String error);

        void profileData(ProfileInfo profileInfo);

        void successInvited(CompanyInvitationResultDTO profileInfo);

        void logoutSuccess();

        void logoutError(String error);
    }

    interface ProfilePresenter extends BasePresenter {

    }
}
