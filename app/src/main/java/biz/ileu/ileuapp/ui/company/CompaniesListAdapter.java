package biz.ileu.ileuapp.ui.company;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;

import biz.ileu.ileuapi.models.company.CompanyViewModel;
import biz.ileu.ileuapi.models.news.NewsCommentViewModel;
import biz.ileu.ileuapi.models.news.NewsEntryViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Daniq on 06.04.2017.
 */

public class CompaniesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private List<CompanyViewModel> companies;

    private int selectedItemPosition = -1;
    private RecyclerViewClick.OnItemClickListener listener;
    public CompaniesListAdapter(List<CompanyViewModel> companies, RecyclerViewClick.OnItemClickListener listener) {

        this.listener = listener;
        this.companies = companies;
    }
    public void changeDisplayCompanies(List<CompanyViewModel> companies) {
        this.companies = companies;
        notifyDataSetChanged();
    }
    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder = null;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_company, parent, false);
        holder = new CompaniesViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CompaniesViewHolder item = (CompaniesViewHolder) holder;
        CompanyViewModel company = companies.get(position);


        Picasso.with(item.context)
                .load(Constants.IMAGE_BASE_URL + company.getLogo())
                .into(item.civRowCompanyLogo);
        if (company.hasSentJoinRequest()) {
            item.llRequestSended.setVisibility(View.VISIBLE);
        } else {
            item.llRequestSended.setVisibility(View.GONE);

        }
//        item.llRequestSended.
        item.tvRowCompanyName.setText(company.getName());

    }


    class CompaniesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        Context context;

        @BindView(R.id.civRowCompanyLogo)
        CircleImageView civRowCompanyLogo;

        @BindView(R.id.tvRowCompanyName)
        TextView tvRowCompanyName;

        @BindView(R.id.llRequestSended)
        LinearLayout llRequestSended;

        public CompaniesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (listener != null) {
                listener.onItemClick(view, getAdapterPosition());
            }

        }
    }

}
