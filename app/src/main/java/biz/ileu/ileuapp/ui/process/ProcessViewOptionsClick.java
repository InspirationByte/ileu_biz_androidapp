package biz.ileu.ileuapp.ui.process;

import android.view.View;

/**
 * Created by Daniq on 26.04.2017.
 */

public interface ProcessViewOptionsClick {

    interface OnOptionsClickListener {
        void onBookmarkClick(int position);

        void onEditClick(int position);

        void onDeleteClick(int position);
    }

}
