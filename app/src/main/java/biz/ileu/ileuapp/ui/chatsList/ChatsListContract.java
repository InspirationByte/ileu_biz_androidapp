package biz.ileu.ileuapp.ui.chatsList;

import java.util.List;

import biz.ileu.ileuapi.models.chat.ChatsViewModel;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by Daniq on 19.04.2017.
 */

public interface ChatsListContract {

    interface ChatsListView extends BaseView {
        void chatsLoading();

        void loadChatsError(String error);

        void chatsLoaded(List<ChatsViewModel> chatsList);

        void chatLoaded(ChatsViewModel chat);
    }

    interface ChatsListPresenter extends BasePresenter {

    }

}
