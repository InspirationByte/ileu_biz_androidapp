package biz.ileu.ileuapp.ui.process.processTemplates.processCreate;

import java.util.List;

import biz.ileu.ileuapi.models.process.CreateProcessViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.process.ProcessCreateStatus;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;
import biz.ileu.ileuapp.utils.ui.attachedFile.AttachedFile;

/**
 * Created by Daniq on 16.04.2017.
 */

public interface ProcessCreateContract {

    interface ProcessCreateView extends BaseView {
        void processCreating();

        void processCreateError(String error);

        void processCreated(ProcessCreateStatus processCreateStatus);

        void processLoading();

        void processLoaded(Process process);

        void processLoadError(String error);
    }

    interface ProcessCreatePresenter extends BasePresenter {
        void createProcess(CreateProcessViewModel createProcessViewModel, List<AttachedFile> attachedFiles);

        void getProcess(int id);
    }

}
