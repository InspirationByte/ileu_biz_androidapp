package biz.ileu.ileuapp.ui.process.processTemplates;

import java.util.ArrayList;
import java.util.List;

import biz.ileu.ileuapi.models.processtypes.ProcessTemplateViewModel;
import biz.ileu.ileuapi.models.processtypes.ProcessTypeViewModel;

/**
 * Created by Daniq on 16.04.2017.
 */

public class ProcessTemplateListSection {
    public ProcessTypeViewModel type;

    public List<ProcessTemplateViewModel> templates;

    public ProcessTemplateListSection() {
        templates = new ArrayList<>();
    }

    public ProcessTemplateListSection(ProcessTemplateListSection sectionToCopy) {
        templates = new ArrayList<>();

        this.type = sectionToCopy.type;
        this.templates.addAll(sectionToCopy.templates);
    }

}
