package biz.ileu.ileuapp.ui.register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import biz.ileu.ileuapi.models.TokenData;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.menu.MenuActivity;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity implements RegisterContract.RegisterView {

    @BindView(R.id.etRegisterEmail)
    EditText etRegisterEmail;
    @BindView(R.id.etRegisterPassword)
    EditText etRegisterPassword;
    @BindView(R.id.etRegisterConfirm)
    EditText etRegisterConfirm;
    @BindView(R.id.btnRegister)
    Button btnRegister;

    ProgressDialog progressDialog;

    public RegisterPresenter presenter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setData();
        ButterKnife.bind(this);

    }

    @Override
    public void bindViews() {

    }

    @Override
    public void setData() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.register_message);

        presenter = new RegisterPresenter(this);
    }


    @Override
    public void registerLoading() {
        progressDialog.show();
    }

    @Override
    public void registerError(String error) {
        progressDialog.dismiss();
    }

    @Override
    public void registerComplete(TokenData tokenData) {
        progressDialog.dismiss();
        Log.d("Login_init_log", tokenData.getAccessToken());
        PrefUtils.setData(PrefUtils.TOKEN, tokenData.getAccessToken());
        Log.d("Register_firebase", "FIREBASE: " + PrefUtils.getFirebaseToken());
        presenter.sendFirebaseToken(PrefUtils.getFirebaseToken());
    }

    @Override
    public void firebaseTokenLoading() {
        progressDialog.show();
    }

    @Override
    public void firebaseTokenIsSent() {
        progressDialog.dismiss();
        proceed();
    }

    private void proceed() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btnRegister)
    public void register() {

        String email = etRegisterEmail.getText().toString();
        String password = etRegisterPassword.getText().toString();
        String confirm = etRegisterConfirm.getText().toString();

        if (email.isEmpty()) {
            Toast.makeText(this, R.string.message_email_empty, Toast.LENGTH_SHORT).show();
            etRegisterEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            Toast.makeText(this, R.string.message_password_empty, Toast.LENGTH_SHORT).show();
            etRegisterPassword.requestFocus();
            return;
        }

        if (confirm.isEmpty()) {
            Toast.makeText(this, R.string.confirm_password, Toast.LENGTH_SHORT).show();
            etRegisterConfirm.requestFocus();
            return;
        }


        if (!password.equals(confirm)) {
            Toast.makeText(this, R.string.passwords_not_equal, Toast.LENGTH_SHORT).show();
            etRegisterPassword.requestFocus();
            return;
        }

        presenter.register(email, password, confirm);

    }
}
