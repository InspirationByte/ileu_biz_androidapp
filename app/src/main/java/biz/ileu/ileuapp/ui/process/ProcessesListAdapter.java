package biz.ileu.ileuapp.ui.process;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.squareup.picasso.Picasso;

import java.util.List;

import biz.ileu.ileuapi.models.process.ParticipantViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Daniq on 06.04.2017.
 */

public class ProcessesListAdapter extends RecyclerView.Adapter<ProcessesListAdapter.ProcessViewHolder> {


    private List<Process> processList;

    private RecyclerViewClick.OnItemClickListener listener;
    private ProcessViewOptionsClick.OnOptionsClickListener optionsClickListener;

    public ProcessesListAdapter(List<Process> processList,
                                RecyclerViewClick.OnItemClickListener listener,
                                ProcessViewOptionsClick.OnOptionsClickListener optionsClickListener) {
        this.processList = processList;
        this.listener = listener;
        this.optionsClickListener = optionsClickListener;
    }

    @Override
    public int getItemCount() {
        return processList.size();
    }

    @Override
    public ProcessViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_process, parent, false);
        return new ProcessViewHolder(row);
    }

    @Override
    public void onBindViewHolder(final ProcessViewHolder holder, int position) {
        Process process = processList.get(position);

        holder.swipeRowProcess.setSwipeEnabled(false);

        holder.tvRowProcessName.setText(process.getProcessName().length() < 24 ?
                process.getProcessName() : process.getProcessName().substring(0, 23) + "...");

        if (!process.isHasBookmark())
            holder.ivRowProcessHasBookmark.setVisibility(View.GONE);
        else
            holder.ivRowProcessHasBookmark.setVisibility(View.VISIBLE);

        holder.tvRowProcessId.setText("#" + process.getId());

        holder.tvRowProcessStatus.setText(process.getStatusName());
        holder.tvRowProcessDate.setText(Constants.getFormattedDateTime(process.getEndDateTime()));

        holder.tvRowProcessDescription.setText(process.getDescriptionText());

        holder.llRowProcessParticipants.removeAllViews();

        // Adding creator
        if (process.getCreator().getId() == process.getResponsible().getId()) {
            addParticipantAvatar(process.getCreator(), holder, process.getStatus() == 3 || process.getStatus() == 8);
        } else if (process.getCreator().getId() == process.getExecutor().getId()) {
            addParticipantAvatar(process.getCreator(), holder, process.getStatus() == 0 || process.getStatus() == 2);
        } else {
            addParticipantAvatar(process.getCreator(), holder, false);

        }

        // Add executor if it is not creator
        if (process.getCreator().getId() != process.getExecutor().getId()) {
            if (process.getExecutor().getId() == process.getResponsible().getId())
                addParticipantAvatar(process.getExecutor(), holder, (process.getStatus() == 0 || process.getStatus() == 2 || process.getStatus() == 3 || process.getStatus() == 8));
            else
                addParticipantAvatar(process.getExecutor(), holder, (process.getStatus() == 0 || process.getStatus() == 2));

        }

        // Add responsible if it is not creator and not executor
        if (process.getResponsible().getId() != process.getExecutor().getId() &&
                process.getResponsible().getId() != process.getCreator().getId())
            addParticipantAvatar(process.getResponsible(), holder, process.getStatus() == 3 || process.getStatus() == 8);

        // Add else
        for (ParticipantViewModel participant: process.getParticipants()) {
            if (participant.getId() != process.getCreator().getId() && participant.getId() != process.getResponsible().getId() && participant.getId() != process.getExecutor().getId() ) {
                addParticipantAvatar(participant, holder, false);
            }
        }

        holder.ivRowProcessMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.swipeRowProcess.getOpenStatus() == SwipeLayout.Status.Open) {
                    holder.swipeRowProcess.close();
                } else {
                    holder.swipeRowProcess.open();
                }
            }
        });
    }

    public void changeProcesses(List<Process> list) {
        this.processList.clear();
        this.processList.addAll(list);
    }
    private void addParticipantAvatar(ParticipantViewModel participant, ProcessViewHolder holder, boolean hasBorder) {
        int wantedHeight = 36;
        int wantedWidth = 36;

        int heightDpi = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                wantedHeight,
                holder.context.getResources().getDisplayMetrics());

        int widthDpi = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                wantedWidth,
                holder.context.getResources().getDisplayMetrics());

        CircleImageView civCreator = new CircleImageView(holder.context);
        RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(heightDpi, widthDpi);


        int wantedMarginLeft = 8;
        int marginLeft = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                wantedMarginLeft, holder.context.getResources().getDisplayMetrics());
        params.setMargins(marginLeft, 0, 0, 0);


        civCreator.setLayoutParams(params);
        if (hasBorder) {
            civCreator.setBorderColor(Color.parseColor("#FBC02D"));
            civCreator.setBorderWidth(5);
        } else if (participant.isIsExecuting()) {
            civCreator.setBorderColor(Color.parseColor("#0092ff"));
            civCreator.setBorderWidth(5);
        } else {
            civCreator.setBorderWidth(0);
        }

        holder.llRowProcessParticipants.addView(civCreator);
        Picasso.with(holder.context)
                .load(Constants.BASE_URL + participant.getProfilePicture())
                .into(civCreator);
    }


    public class ProcessViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;

        @BindView(R.id.tvRowProcessName)
        TextView tvRowProcessName;

        @BindView(R.id.ivRowProcessHasBookmark)
        ImageView ivRowProcessHasBookmark;

        @BindView(R.id.tvRowProcessId)
        TextView tvRowProcessId;

        @BindView(R.id.ivRowProcessMore)
        ImageView ivRowProcessMore;

        @BindView(R.id.tvRowProcessStatus)
        TextView tvRowProcessStatus;

        @BindView(R.id.tvRowProcessDate)
        TextView tvRowProcessDate;

        @BindView(R.id.tvRowProcessDescription)
        TextView tvRowProcessDescription;

        @BindView(R.id.llRowProcessParticipants)
        LinearLayout llRowProcessParticipants;

        @BindView(R.id.swipeRowProcess)
        SwipeLayout swipeRowProcess;

        @BindView(R.id.llRowProcessBottomView)
        LinearLayout llRowProcessBottomView;

        @BindView(R.id.llRowProcessSurfaceView)
        LinearLayout llRowProcessSurfaceView;

        @BindView(R.id.ibRowProcessBookmark)
        ImageButton ibRowProcessBookmark;

        @BindView(R.id.ibRowProcessEdit)
        ImageButton ibRowProcessEdit;

        @BindView(R.id.ibRowProcessDelete)
        ImageButton ibRowProcessDelete;


        public ProcessViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
            itemView.setOnClickListener(this);

            ibRowProcessBookmark.setOnClickListener(this);
            ibRowProcessEdit.setOnClickListener(this);
            ibRowProcessDelete.setOnClickListener(this);
        }



        @Override
        public void onClick(View v) {

            if (v.getId() == R.id.ibRowProcessBookmark) {
                if (optionsClickListener != null)
                    optionsClickListener.onBookmarkClick(getAdapterPosition());
                return;
            }

            if (v.getId() == R.id.ibRowProcessEdit) {
                if (optionsClickListener != null)
                    optionsClickListener.onEditClick(getAdapterPosition());
                return;
            }

            if (v.getId() == R.id.ibRowProcessDelete) {
                if (optionsClickListener != null)
                    optionsClickListener.onDeleteClick(getAdapterPosition());
                return;
            }

            if (listener != null) {
                listener.onItemClick(v, getAdapterPosition());
            }
        }
    }
}
