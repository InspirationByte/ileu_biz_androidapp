package biz.ileu.ileuapp.ui.menu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Murager on 4/4/17.
 */

public class LeftMenuAdapter extends RecyclerView.Adapter<LeftMenuAdapter.LeftMenuViewHolder> {

    private List<CompanyWorkInfoListViewModel> leftMenuList;

    private RecyclerViewClick.OnItemClickListener listener;

    private int selectedItemPosition = -1;

    public LeftMenuAdapter(List<CompanyWorkInfoListViewModel> leftMenuList) {
        this.leftMenuList = leftMenuList;
    }

    public LeftMenuAdapter(List<CompanyWorkInfoListViewModel> leftMenuList, RecyclerViewClick.OnItemClickListener listener) {
        this.leftMenuList = leftMenuList;
        this.listener = listener;
    }

    @Override
    public LeftMenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_left_menu, parent, false);
        return new LeftMenuViewHolder(row);
    }

    @Override
    public void onBindViewHolder(LeftMenuViewHolder holder, int position) {
        CompanyWorkInfoListViewModel company = leftMenuList.get(position);

        holder.tvRowCompanyIsOwn.setVisibility(company.isOwn() ? View.VISIBLE : View.GONE);


        holder.tvName.setText(company.getName());
        Picasso.with(holder.context).load(Constants.IMAGE_BASE_URL + company.getLogo()).into(holder.civRowCompany);


        holder.ivRowCompanyProcessChatMessages.setImageResource(company.getNumUnreadChatMessages() > 0 ?
                R.drawable.ic_chat_message_orange_24px : R.drawable.ic_chat_message_grey_24px);

        holder.ivRowCompanyProcessComments.setImageResource(company.getNumUnreadProcessComments() > 0 ?
                R.drawable.ic_message_orange_24px : R.drawable.ic_message_grey_24px);

        holder.ivRowCompanyProcessOverdues.setImageResource(company.getNumOverdues() > 0 ?
                R.drawable.ic_info_outline_orange_24px : R.drawable.ic_info_outline_grey_24px);

        holder.ivRowCompanyJoinRequests.setImageResource(company.getJoinRequests() > 0 ?
                R.drawable.ic_person_add_orange_24px : R.drawable.ic_person_add_grey_24px);

        holder.tvRowCompanyProcessChatMessages.setText(company.getNumUnreadChatMessages() > 99 ? "99+" : "" + company.getNumUnreadChatMessages());
        holder.tvRowCompanyProcessComments.setText(company.getNumUnreadProcessComments() > 99 ? "99+" : "" + company.getNumUnreadProcessComments());
        holder.tvRowCompanyProcessOverdues.setText(company.getNumOverdues() > 99 ? "99+" : "" + company.getNumOverdues());
        holder.tvRowCompanyJoinRequests.setText(company.getJoinRequests() > 99 ? "99+" : "" + company.getJoinRequests());

        holder.rlRowCompanySelectedIndicator.setVisibility((selectedItemPosition > -1 && selectedItemPosition == position) ?
                View.VISIBLE : View.GONE);
    }

    public void changeDisplayCompanies(List<CompanyWorkInfoListViewModel> companyWorkInfoListViewModels) {
        this.leftMenuList = companyWorkInfoListViewModels;
        notifyDataSetChanged();
    }

    public void setSelectedItemPosition(int newPosition) {
        selectedItemPosition = newPosition;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return leftMenuList == null ? 0 : leftMenuList.size();
    }

    public class LeftMenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;

        @BindView(R.id.tvRowCompanyName)
        TextView tvName;

        @BindView(R.id.civRowCompany)
        CircleImageView civRowCompany;

        @BindView(R.id.tvRowCompanyIsOwn)
        TextView tvRowCompanyIsOwn;

        @BindView(R.id.tvRowCompanyProcessChatMessages)
        TextView tvRowCompanyProcessChatMessages;

        @BindView(R.id.ivRowCompanyProcessChatMessages)
        ImageView ivRowCompanyProcessChatMessages;

        @BindView(R.id.tvRowCompanyProcessComments)
        TextView tvRowCompanyProcessComments;

        @BindView(R.id.ivRowCompanyProcessComments)
        ImageView ivRowCompanyProcessComments;

        @BindView(R.id.ivRowCompanyProcessOverdues)
        ImageView ivRowCompanyProcessOverdues;

        @BindView(R.id.tvRowCompanyProcessOverdues)
        TextView tvRowCompanyProcessOverdues;

        @BindView(R.id.tvRowCompanyJoinRequests)
        TextView tvRowCompanyJoinRequests;

        @BindView(R.id.ivRowCompanyJoinRequests)
        ImageView ivRowCompanyJoinRequests;

        @BindView(R.id.rlRowCompanySelectedIndicator)
        View rlRowCompanySelectedIndicator;

        public LeftMenuViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (listener != null) {
                listener.onItemClick(view, getAdapterPosition());
            }

            selectedItemPosition = getAdapterPosition();
            notifyDataSetChanged();
        }
    }
}
