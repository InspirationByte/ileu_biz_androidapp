package biz.ileu.ileuapp.ui.process.processTemplates;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.processtypes.ProcessTemplateViewModel;
import biz.ileu.ileuapi.models.processtypes.ProcessTypeViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.process.processTemplates.processCreate.ProcessCreateActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ProcessTemplatesActivity extends BaseActivity implements TemplateListClick.OnTemplateNameClickListener, SearchView.OnQueryTextListener {

    @BindView(R.id.rlProcessTemplates)
    RecyclerView rlProcessTemplates;

    private CompanyWorkInfoListViewModel currentCompany;

    HashMap<String, ProcessTemplateViewModel> processTemplatesMap;

    ProgressDialog progressDialog;

        private List<ProcessTemplateListSection> list;
        private ProcessTemplatesListAdapter adapter;

    private CompositeSubscription compositeSubscription;

    private int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_templates);
        ButterKnife.bind(this);
        bindViews();
        setData();
    }

    @Override
    public void bindViews() {
        initBaseToolbar(true);
        getSupportActionBar().setTitle(R.string.process_templates);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void setData() {
        compositeSubscription = new CompositeSubscription();

        currentCompany = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_COMPANY));

        processTemplatesMap = new HashMap<>();
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.process_templates_loading);

        list = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rlProcessTemplates.setLayoutManager(layoutManager);
        adapter = new ProcessTemplatesListAdapter(list, this);
        rlProcessTemplates.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        getSections();
    }

    private void getSections() {
        progressDialog.show();

        Subscription subscription = RestClient.request()
                .getStartingProcessTypes(PrefUtils.getToken(), currentCompany.getId(), false, null)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<ProcessTypeViewModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(ProcessTemplatesActivity.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(List<ProcessTypeViewModel> processTypeViewModels) {
                        for (ProcessTypeViewModel type: processTypeViewModels) {
                            ProcessTemplateListSection section = new ProcessTemplateListSection();
                            section.type = type;
                            list.add(section);
                        }
                        loadTemplates(0);
                    }
                });

        compositeSubscription.add(subscription);
    }

    private void loadTemplates(final int i) {
        final ProcessTemplateListSection section = list.get(i);
        ProcessTypeViewModel type = section.type;

        Subscription subscription = RestClient.request()
                .getStartingProcessNames(PrefUtils.getToken(), currentCompany.getId(), type.getId(), false, null)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<ProcessTemplateViewModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        Toast.makeText(ProcessTemplatesActivity.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(List<ProcessTemplateViewModel> processTemplateViewModels) {
                        section.templates.addAll(processTemplateViewModels);

                        if (i + 1 >= list.size()) {
                            progressDialog.dismiss();
                            adapter.notifyDataSetChanged();
                        } else {
                            loadTemplates(i + 1);
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase(Locale.getDefault());

        List<ProcessTemplateListSection> filteredList = new ArrayList<>();
        for (ProcessTemplateListSection section : list) {
            ProcessTemplateListSection sectionClone = new ProcessTemplateListSection(section);
            if (section.type.getTitle().toLowerCase(Locale.getDefault()).contains(newText))
                filteredList.add(sectionClone);
            else {
                List<ProcessTemplateViewModel> newSectionTemplatesList = new ArrayList<>();
                for (ProcessTemplateViewModel sectionTemplate : sectionClone.templates) {
                    if (sectionTemplate.getTitle().toLowerCase(Locale.getDefault()).contains(newText))
                        newSectionTemplatesList.add(sectionTemplate);
                }
                if (newSectionTemplatesList.size() > 0)
                    filteredList.add(sectionClone);
                sectionClone.templates = newSectionTemplatesList;
            }
        }

        adapter.changeDisplayTemplates(filteredList);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.search, menu);
        MenuItem searchItem = menu.findItem(R.id.actionSearch);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(this);

        MenuItemCompat.collapseActionView(searchItem);


        return true;
    }

    @Override
    public void onTemplateClick(ProcessTemplateViewModel template) {
        Intent intent = new Intent(this, ProcessCreateActivity.class);
        intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
        intent.putExtra(Constants.ENTITY_PROCESS_TEMPLATE, Parcels.wrap(template));
        startActivity(intent);
    }


}
