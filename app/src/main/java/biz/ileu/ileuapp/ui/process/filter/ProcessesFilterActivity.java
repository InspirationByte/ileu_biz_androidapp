package biz.ileu.ileuapp.ui.process.filter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.androidbuts.multispinnerfilter.SpinnerListener;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.process.EmployeeList;
import biz.ileu.ileuapi.models.process.EmployeeViewModel;
import biz.ileu.ileuapi.models.processtypes.ProcessNameGroupedViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.process.processTemplates.ProcessTemplateListSection;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by macair on 14.08.17.
 */

public class ProcessesFilterActivity extends BaseActivity {

    @BindView(R.id.etProcessNumber)
    EditText etProcessNumber;
    @BindView(R.id.etProcessTitle)
    EditText etProcessTitle;
    @BindView(R.id.etProcessTemplate)
    MultiSpinnerSearch spProcessTemplate;
    @BindView(R.id.etProcessParticipants)
    MultiSpinnerSearch spProcessParticipants;
    @BindView(R.id.spDocumentState)
    Spinner mssDocumentState;
    @BindView(R.id.tvDocState)
    TextView tvDocState;
    private List<ProcessTemplateListSection> list;
    private List<ProcessNameGroupedViewModel> templates;
    private List<String> templatesTitle;
    private CompositeSubscription compositeSubscription;
    private CompanyWorkInfoListViewModel currentCompany;
    private EmployeeList employeeList;
    private int tabIndex;

    @Override
    public void bindViews() {
        initBaseToolbar(true);
        getSupportActionBar().setTitle(R.string.process_filter);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getActionBar().setTitle(R.string.process_filter);

    }
    ProgressDialog progressDialog;

    @Override
    public void setData() {

        progressDialog = new ProgressDialog(this);
        list = new ArrayList<>();
        templates = new ArrayList<>();
        templatesTitle = new ArrayList<>();
        compositeSubscription = new CompositeSubscription();
        loadTemplates();
        getParticipants();
        String pname = PrefUtils.getString(Constants.FILTER_PNAME + tabIndex);
        if (!pname.isEmpty()) {
            etProcessNumber.setText(pname);
        }
        String desc = PrefUtils.getString(Constants.FILTER_DESC  + tabIndex);
        if (!desc.isEmpty()) {
            etProcessTitle.setText(desc);
        }
        if (tabIndex == 1) {
            ArrayAdapter<String> adapter;
            String[] list = getResources().getStringArray(R.array.document_state);
            adapter = new ArrayAdapter<String>(getApplicationContext(),
                    R.layout.compony_type_item, list);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mssDocumentState.setAdapter(adapter);
            String docState = PrefUtils.getString(Constants.FILTER_DOC_STATES);
            if (docState != "") {
                mssDocumentState.setSelection(Integer.parseInt(docState));
            }
        } else {
            tvDocState.setVisibility(View.GONE);
            mssDocumentState.setVisibility(View.GONE);
        }
    }
    private void getParticipants() {
        Subscription subscription = RestClient.request()
                .getCompanyEmployees(PrefUtils.getToken(), currentCompany.getId(), 1, 9999)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<EmployeeList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(ProcessesFilterActivity.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(EmployeeList employeelist) {
                        updateSpinnerParticipants(employeelist);
                    }
                });
    }

    private void updateSpinnerParticipants(EmployeeList employeeList) {
        this.employeeList = employeeList;
        List<String> employeeListName = new ArrayList<>();
        for (EmployeeViewModel emp : this.employeeList.getEmployeeViewModelList()) {
            employeeListName.add(emp.getNameSurname());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, employeeListName);

        final List<KeyPairBoolData> listArray = new ArrayList<KeyPairBoolData>();
        List<Integer> selectedParticipants = PrefUtils.getIntegerList(Constants.FILTER_EMP_IDS + tabIndex);
        for(int i=0; i<employeeListName.size(); i++) {
            KeyPairBoolData h = new KeyPairBoolData();
            h.setId(i+1);
            h.setName(employeeListName.get(i));
            if (selectedParticipants != null && selectedParticipants.contains(employeeList.getEmployeeViewModelList().get(i).getId())) {
                h.setSelected(true);
            } else
                h.setSelected(false);
            listArray.add(h);
        }

/***
 * -1 is no by default selection
 * 0 to length will select corresponding values
 */
        spProcessParticipants.setItems(listArray,  -1, new SpinnerListener() {
            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {

                for(int i=0; i<items.size(); i++) {
                    if(items.get(i).isSelected()) {
                        Log.i("TAG", i + " : " + items.get(i).getName() + " : " + items.get(i).isSelected());
                    }
                }
            }
        });
    }


    private void loadTemplates() {

        Subscription subscription = RestClient.request()
                .getViewableGroupedProcessNames(PrefUtils.getToken(), currentCompany.getId(), false)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<ProcessNameGroupedViewModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        Toast.makeText(ProcessesFilterActivity.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(List<ProcessNameGroupedViewModel> processTemplateViewModels) {
                        templates.addAll(processTemplateViewModels);
                        progressDialog.dismiss();
                        updateSpinner();

                    }
                });
        compositeSubscription.add(subscription);
    }

    private void updateSpinner() {

        for (ProcessNameGroupedViewModel temp : templates) {
            templatesTitle.add(temp.getTitle());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, templatesTitle);

        final List<KeyPairBoolData> listArray = new ArrayList<KeyPairBoolData>();

        List<Integer> selectedTemplates = PrefUtils.getIntegerList(Constants.FILTER_TEMP_IDS + tabIndex);
        for(int i=0; i<templatesTitle.size(); i++) {
            KeyPairBoolData h = new KeyPairBoolData();
            h.setId(i+1);
            h.setName(templatesTitle.get(i));
            if (selectedTemplates != null && selectedTemplates.contains(templates.get(i).getId())) {
                h.setSelected(true);
            } else
                h.setSelected(false);
            listArray.add(h);
        }

        spProcessTemplate.setItems(listArray,  -1, new SpinnerListener() {
            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {

                for(int i=0; i<items.size(); i++) {
                    if(items.get(i).isSelected()) {
                        Log.i("TAG", i + " : " + items.get(i).getName() + " : " + items.get(i).isSelected());
                    }
                }
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);
        currentCompany = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_COMPANY));
        tabIndex = getIntent().getIntExtra(Constants.ENTITY_CURRENT_TAB,0);
        bindViews();
        setData();
    }

    @OnClick(R.id.btnClearFilter)
    public void processClearFilter() {
        etProcessNumber.setText("");
        etProcessTitle.setText("");
        spProcessTemplate.clear();
        if (tabIndex == 1) mssDocumentState.setSelected(false);
        spProcessParticipants.clear();
        PrefUtils.setData(Constants.FILTER_PNAME + tabIndex, "");
        PrefUtils.setData(Constants.FILTER_DESC + tabIndex, "");
        PrefUtils.setData(Constants.FILTER_STRING + tabIndex, "");
        PrefUtils.setData(Constants.FILTER_EMP_IDS + tabIndex, new ArrayList<Integer>());
        PrefUtils.setData(Constants.FILTER_TEMP_IDS + tabIndex, new ArrayList<Integer>());
        if (tabIndex == 1) PrefUtils.setData(Constants.FILTER_DOC_STATES, "");
    }
    public void search() {
        String number = etProcessNumber.getText().toString();
        String title = etProcessTitle.getText().toString();
        String filter = "";
        boolean b = false;
        if (!number.equals("")) {
            filter += "(IdProcessName~contains~'" + number + "')";
            b = true;
            PrefUtils.setData(Constants.FILTER_PNAME + tabIndex, number);
        }
        if (!title.equals("")) {
            if (b)
                filter += "~and~";
            b = true;
            filter += "(DescriptionText~contains~'" + title + "')";
            PrefUtils.setData(Constants.FILTER_DESC + tabIndex, title);
        }
        List<Long> selectedIds = spProcessTemplate.getSelectedIds();
        List<Integer> tempIds = new ArrayList<Integer>();
        for (int i = 0; i < selectedIds.size(); i++) {
            tempIds.add(templates.get(spProcessTemplate.getSelectedIds().get(i).intValue() - 1).getId());
        }



        PrefUtils.setData(Constants.FILTER_TEMP_IDS + tabIndex, tempIds);
        if (tabIndex == 1) {
            int selectedDocumentState = ((int) mssDocumentState.getSelectedItemPosition());
            if (selectedDocumentState > 0) {
                PrefUtils.setData(Constants.FILTER_DOC_STATES, String.valueOf(selectedDocumentState));
                if (b)
                    filter += "~and~";
                filter += "(DocumentState~eq~'" + (selectedDocumentState) + "')";
            }
        }
        selectedIds = spProcessParticipants.getSelectedIds();
        List<Integer> empIds =
                new ArrayList<Integer>();
        for (int i = 0; i < selectedIds.size(); i++) {
            empIds.add(employeeList.getEmployeeViewModelList().get(spProcessParticipants.getSelectedIds().get(i).intValue() - 1).getId());
        }

        PrefUtils.setData(Constants.FILTER_STRING + tabIndex, filter);
        PrefUtils.setData(Constants.FILTER_EMP_IDS + tabIndex, empIds);
//
//        boolean[] selected2 = spProcessParticipants.getSelected();
//        for (int i = 0; i < employeeList.getEmployeeViewModelList().size(); i++) {
//            if (selected2[i]) {
//                empIds.add(employeeList.getEmployeeViewModelList().get(i).getId());
//            }
//        }



        Intent intent = new Intent();
        intent.putExtra("FILTER", filter);
        intent.putExtra("EMPIDS", Parcels.wrap(empIds));
        intent.putExtra("TEMPIDS", Parcels.wrap(tempIds));
        setResult(RESULT_OK, intent);
    }
    @OnClick(R.id.btnProcessSearch)
    public void processSearchClicked() {
        search();
        finish();

    }

    public void clearFilter() {
        PrefUtils.setData(Constants.FILTER_STRING + tabIndex, "");
        PrefUtils.setData(Constants.FILTER_EMP_IDS + tabIndex, new ArrayList<Integer>());

    }

    @Override
    public void finish() {
        search();
        super.finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
           finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
