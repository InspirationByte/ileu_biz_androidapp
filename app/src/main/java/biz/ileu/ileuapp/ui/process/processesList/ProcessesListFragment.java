package biz.ileu.ileuapp.ui.process.processesList;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import biz.ileu.ileuapi.models.bookmarks.BookmarkCategoryViewModel;
import biz.ileu.ileuapi.models.bookmarks.BookmarkViewModel;
import biz.ileu.ileuapi.models.bookmarks.BookmarksCategoriesList;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.bookmarks.BookmarkCategoriesActivity;
import biz.ileu.ileuapp.ui.process.ProcessViewOptionsClick;
import biz.ileu.ileuapp.ui.process.ProcessesListAdapter;
import biz.ileu.ileuapp.ui.process.processComments.ProcessCommentsActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.cicerone.Screen;
import biz.ileu.ileuapp.utils.events.CompanyCountersChangedEvent;
import biz.ileu.ileuapp.utils.ui.BaseFragment;
import biz.ileu.ileuapp.utils.ui.EndlessRecyclerOnScrollListener;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProcessesListFragment extends BaseFragment implements ProcessesListContract.ProcessesListView,
        RecyclerViewClick.OnItemClickListener,
        ProcessViewOptionsClick.OnOptionsClickListener, SearchView.OnQueryTextListener {

    private static final int FILTER_PROCESS = 9;
    private static final int BOOKMARK_CATEGORY = 11;
    private static final int PROCESS_COMMENTS = 12;
    @BindView(R.id.rlProcesses)
    RecyclerView rlProcesses;

    @BindView(R.id.srlProcesses)
    SwipeRefreshLayout srlProcesses;

    Unbinder unbinder;
    private CompanyWorkInfoListViewModel currentCompany = null;

    private BookmarkCategoryViewModel bookmarkCategory = null;
    private int tabIndex = -1;
    private String filter;
    private String sorting;
    private String filterS = "";
    private List<Integer> empIds;
    private List<Integer> tempIds;
    private ProcessesListAdapter adapter;
    private List<Process> list;
    private List<Process> adapterList;
    private EndlessRecyclerOnScrollListener listener;
    private boolean fromFilter = false;
    private boolean filt = false;
    private int currentPosition = 0;
    private int bookmarkPosition = 0;
    private ProcessesListPresenter presenter;
    private Menu menu;
    private MenuItem menuItem;

    public ProcessesListFragment() {
        // Required empty public constructor
    }

    public static ProcessesListFragment newInstance() {
        return new ProcessesListFragment();
    }

    public static ProcessesListFragment newInstance(Bundle bundle) {
        ProcessesListFragment fragment = new ProcessesListFragment();
        Bundle args = new Bundle();
        args.putAll(bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_processes_list, container, false);

        unbinder = ButterKnife.bind(this, view);

        setData();


//        setHasOptionsMenu(true);
//        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void bindViews() {

    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.search, menu);
//        MenuItem searchItem = menu.findItem(R.id.actionSearch);
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
//
//        searchView.setOnQueryTextListener(this);
//
//        MenuItemCompat.collapseActionView(searchItem);
//        super.onCreateOptionsMenu(menu, inflater);
//    }
    private boolean isRefresh = true;
    @Override
    public void setData() {
        getActionBar().setTitle(getResources().getString(R.string.processes));


        Bundle args = getArguments();
        if (args != null && args.containsKey(Constants.ENTITY_COMPANY))
            currentCompany = Parcels.unwrap(args.getParcelable(Constants.ENTITY_COMPANY));

        if (args != null && args.containsKey(Constants.ENTITY_BOOKMARK_CATEGORY))
            bookmarkCategory = Parcels.unwrap(args.getParcelable(Constants.ENTITY_BOOKMARK_CATEGORY));

        if (args != null && args.containsKey(Constants.TAB_INDEX))
            tabIndex = args.getInt(Constants.TAB_INDEX);

        filter = args.getString(Constants.PROCESS_FILTER);
        sorting = args.getString(Constants.PROCESS_SORTING);

        presenter = new ProcessesListPresenter(this, currentCompany, filter, sorting, tabIndex);
        filterS = PrefUtils.getString(Constants.FILTER_STRING + tabIndex);
        empIds  = PrefUtils.getList(Constants.FILTER_EMP_IDS + tabIndex);
        tempIds = PrefUtils.getList(Constants.FILTER_TEMP_IDS + tabIndex);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rlProcesses.setLayoutManager(layoutManager);

        list = new ArrayList<>();
        adapterList = new ArrayList<>();
        adapter = new ProcessesListAdapter(list, this, this);
        rlProcesses.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        DividerItemDecoration divider = new DividerItemDecoration(rlProcesses.getContext(),
                ((LinearLayoutManager) layoutManager).getOrientation());
        rlProcesses.addItemDecoration(divider);

        listener = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                isRefresh = false;
                if (bookmarkCategory == null) {
                     presenter.getProcesses(current_page, Constants.DEFAULT_PAGE_SIZE, filterS, empIds, tempIds);
                }
                else
                    presenter.getProgressWithCategory(current_page, Constants.DEFAULT_PAGE_SIZE, bookmarkCategory.getId());
            }
        };
//        rlProcesses.setRecyclerListener(listener);
        rlProcesses.addOnScrollListener(listener);
        srlProcesses.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                list.clear();
                adapterList.clear();
                adapter.notifyDataSetChanged();
                srlProcesses.setRefreshing(true);
                listener.reset();
                isRefresh = true;

                if (bookmarkCategory == null) {
                    presenter.getProcesses(1, Constants.DEFAULT_PAGE_SIZE, filterS, empIds, tempIds);
                }
                else
                    presenter.getProgressWithCategory(1, Constants.DEFAULT_PAGE_SIZE, bookmarkCategory.getId());
            }
        });

    }

    public List<Process> getList() {
        return list;
    }

    @Override
    public void processesLoading() {
        if (isRefresh) {
            list.clear();
            adapterList.clear();
        }
        adapter.notifyDataSetChanged();
        if (!srlProcesses.isRefreshing())
            srlProcesses.setRefreshing(true);
    }

    @Override
    public void loadProcessesError(String error) {
        Toast.makeText(getActivity(), R.string.message_processes_load_error, Toast.LENGTH_SHORT).show();
    }

    public int getBookmarkPosition() {
        return bookmarkPosition;
    }

    public void setBookmarkPosition(int bookmarkPosition) {
        this.bookmarkPosition = bookmarkPosition;
    }

    @Override
    public void processesLoaded(List<Process> processList) {
//        list.clear();
//        adapterList.clear();
        list.addAll(processList);
        adapterList.addAll(processList);
        adapter.notifyDataSetChanged();
        srlProcesses.setRefreshing(false);
    }

    @Override
    public void addBookmarkError(String message) {

    }

    @Override
    public void addedBookmark(BookmarkViewModel bookmark) {

    }

    @Override
    public void bookmarkCategoriesLoaded(BookmarksCategoriesList bookmarksCategoriesList) {

    }


    @Override
    public String getScreenName() {
        return Screen.SCREEN_PROCESSES_LIST;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.destroy();
    }

    @Override
    public void onStart() {
        super.onStart();
            srlProcesses.post(new Runnable() {
                @Override
                public void run() {
                    srlProcesses.setRefreshing(true);
                    list.clear();
                    adapterList.clear();
                    if (bookmarkCategory == null) {
                        presenter.getProcesses(1, Constants.DEFAULT_PAGE_SIZE, filterS, empIds, tempIds);
                    }
                    else
                        presenter.getProgressWithCategory(1, Constants.DEFAULT_PAGE_SIZE, bookmarkCategory.getId());
                }
            });

    }
    public void updateList() {
        presenter.getProcesses(1, Constants.DEFAULT_PAGE_SIZE);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onItemClick(View view, int position) {
        if (list.size() > 0) {
            Intent intent = new Intent(getActivity(), ProcessCommentsActivity.class);
            currentPosition = position;
            intent.putExtra(Constants.ENTITY_PROCESS, Parcels.wrap(list.get(position)));
            intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));

            // When we are opening process all unread messages considered as read.
            CompanyCountersChangedEvent event = new CompanyCountersChangedEvent();
            event.companyId = currentCompany.getId();
            event.unreadChatMessages = currentCompany.getNumUnreadChatMessages();
            event.unreadProcessComments = currentCompany.getNumUnreadProcessComments() - list.get(position).getUnreadCommentsCount() > 0 ?
                    currentCompany.getNumUnreadProcessComments() - list.get(position).getUnreadCommentsCount() : 0;
            EventBus.getDefault().post(event);

            list.get(position).getUnreadCommentsCount();

            startActivityForResult(intent, PROCESS_COMMENTS);
        }
    }


    @Override
    public void onBookmarkClick(int position) {

        Intent intent = new Intent(getActivity(), BookmarkCategoriesActivity.class);
        intent.putExtra(Constants.ENTITY_PROCESS, Parcels.wrap(list.get(position)));
        intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
        bookmarkPosition = position;
        startActivityForResult(intent, BOOKMARK_CATEGORY);
//        presenter.addBookmark(list.get(position).getId());
    }

    public ProcessesListAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void onEditClick(int position) {

    }

    @Override
    public void onDeleteClick(int position) {

    }
    public void updateListWith(String filterS, List<Integer> empIds, List<Integer> tempIds, boolean filt, int tab) {
//        list.clear();
        this.filterS = filterS;
        this.empIds = empIds;
        this.tempIds = tempIds;
        this.filt = true;
        this.tabIndex = tab;
//        this.filterS = filterS;
//        fromFilter = true;
//        this.filt = filt;
//        this.empIds = empIds;
//        this.tempIds = tempIds;
//        PrefUtils.setData("is_filter_" + tab, 1);
//        PrefUtils.setData("filter_text_" + tab, filterS);
//        PrefUtils.setData("emp_ids_" + tab, empIds);
//        PrefUtils.setData("temp_ids_" + tab, tempIds);
//        srlProcesses.setRefreshing(true);
    }

    public boolean isFilt() {
        return filt;
    }

    public void setFilt(boolean filt) {
        this.filt = filt;
    }

    public boolean isFromFilter() {
        return fromFilter;
    }

    public void setFromFilter(boolean fromFilter) {
        this.fromFilter = fromFilter;
    }

    public boolean searchText(String newText) {
        if (!newText.matches("-?\\d+(\\.\\d+)?"))
            newText = newText.toLowerCase(Locale.getDefault());

        List<Process> filteredList = new ArrayList<>();
        for (Process process : this.adapterList) {
            if (process.getIdProcessName().toLowerCase(Locale.getDefault()).contains(newText))
                filteredList.add(process);
        }
        Log.d("FILTERED_LIST_SIZE", String.valueOf(filteredList.size()));
        adapter.changeProcesses(filteredList);
        adapter.notifyDataSetChanged();
        return false;
    }
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.process_filter, menu);
        MenuItem filter = menu.findItem(R.id.actionFilter);

        filter.setVisible(true);
        MenuItem searchItemLocal = menu.findItem(R.id.actionSearch);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItemLocal);

        searchView.setOnQueryTextListener(this);

        MenuItemCompat.collapseActionView(searchItemLocal);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BOOKMARK_CATEGORY) {
            if (resultCode == Activity.RESULT_OK) {
                list.get(bookmarkPosition).setHasBookmark(data.getBooleanExtra("HASBOOKMARK", false));
                adapter.notifyItemChanged(bookmarkPosition);
            }
        }
        if (requestCode == PROCESS_COMMENTS) {
            if (resultCode == Activity.RESULT_OK) {
                Process pr = Parcels.unwrap(data.getParcelableExtra(Constants.ENTITY_PROCESS));
                list.set(currentPosition, pr);
                adapter.notifyItemChanged(currentPosition);
            }
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
