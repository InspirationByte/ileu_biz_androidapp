package biz.ileu.ileuapp.ui.newsList.news;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.parceler.Parcels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.news.NewsCommentList;
import biz.ileu.ileuapi.models.news.NewsCommentViewModel;
import biz.ileu.ileuapi.models.news.NewsEntryViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.menu.MenuActivity;
import biz.ileu.ileuapp.ui.newsList.NewsClick;
import biz.ileu.ileuapp.ui.newsList.NewsCommentsListAdapter;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import biz.ileu.ileuapp.utils.ui.EndlessRecyclerOnScrollListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class NewsActivity extends BaseActivity implements NewsContract.NewsView, NewsClick.OnItemClick {

    @BindView(R.id.rlNewsComments)
    RecyclerView rlNewsComments;
    @BindView(R.id.etNewsComment)
    EditText etNewsComment;
    @BindView(R.id.ibNewsCommentPost)
    ImageButton ibNewsCommentPost;
    @BindView(R.id.srlNewsComments)
    SwipeRefreshLayout srlNewsComments;
    @BindView(R.id.ibNewsCommentAttach)
    ImageButton ibNewsCommentAttach;
    @BindView(R.id.ibNewsCommentVoice)
    ImageButton ibNewsCommentVoice;
    @BindView(R.id.llNewsCommentsAttachments)
    LinearLayout llNewsCommentsAttachments;

    private NewsPresenter presenter;

    private NewsEntryViewModel currentNews;

    private List<NewsCommentViewModel> commentsList;

    private NewsCommentsListAdapter adapter;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private ProgressDialog progressDialog;

    private EndlessRecyclerOnScrollListener listener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);

        bindViews();
        setData();
    }

    @Override
    public void bindViews() {
        initBaseToolbar(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void setData() {
        currentNews = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_NEWS));
        getSupportActionBar().setTitle(currentNews.getTitle().length() < 24 ?
                currentNews.getTitle() : currentNews.getTitle().substring(0, 23));


        presenter = new NewsPresenter(this, currentNews);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);


        commentsList = new ArrayList<>();

        adapter = new NewsCommentsListAdapter(currentNews, commentsList, this);
        rlNewsComments.setLayoutManager(layoutManager);
        rlNewsComments.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getResources().getString(R.string.process_comment_sending));

        listener = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                presenter.getComments(current_page, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        };

        srlNewsComments.post(new Runnable() {
            @Override
            public void run() {
                srlNewsComments.setRefreshing(true);
                presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        });

        srlNewsComments.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                commentsList.clear();
                adapter.notifyDataSetChanged();
                listener.reset();
                srlNewsComments.setRefreshing(true);
                presenter.getComments(1, Constants.DEFAULT_PAGE_MAX_SIZE);
            }
        });

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void loadCommentsError(String error) {
        srlNewsComments.setRefreshing(false);
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void commentsLoaded(List<NewsCommentViewModel> comments) {
        srlNewsComments.setRefreshing(false);
        commentsList.addAll(comments);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void sendingComment() {
        progressDialog.show();
    }

    @Override
    public void sendingCommentError(String error) {
        progressDialog.dismiss();
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void commentSent(NewsCommentViewModel comment) {
        commentsList.add(comment);
        adapter.notifyDataSetChanged();
        progressDialog.dismiss();
        etNewsComment.setText("");
        rlNewsComments.scrollToPosition(adapter.getItemCount() - 1);
        Toast.makeText(this, R.string.process_comment_added, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void downloadingFile() {
        progressDialog.setTitle(R.string.process_file_downloading);
        progressDialog.show();
    }

    @Override
    public void downloadError(String string) {
        Toast.makeText(this, R.string.message_process_file_download_error, Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
    }

    @Override
    public void downloadCompleted(File file) {
        progressDialog.dismiss();
        Intent myIntent = new Intent(Intent.ACTION_VIEW);
        String type = "*/*";
        String extension = MimeTypeMap.getFileExtensionFromUrl(file.getPath());
        if (extension != null)
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        myIntent.setDataAndType(Uri.fromFile(file), type);

        Intent intent = Intent.createChooser(myIntent, getResources().getString(R.string.process_file_choose_type));
        startActivity(intent);
    }


    @OnClick(R.id.ibNewsCommentPost)
    public void onViewClicked() {
        if (etNewsComment.getText().toString().length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.process_comment_text_empty), Toast.LENGTH_SHORT).show();
            etNewsComment.requestFocus();

            return;
        }

        presenter.sendComment(etNewsComment.getText().toString());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.destroy();
    }


    @Override
    public void onDownloadFileClick(String fileName, String fileId, String entryId) {
        Log.d("NEWS_FILE", "ID: " + fileId + " ENTRY: " + entryId);
        presenter.downloadFile(fileName, entryId, fileId);
    }
}
