package biz.ileu.ileuapp.ui.chatsList.chatMessages;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import biz.ileu.ileuapi.models.chat.ChatMessageViewModel;
import biz.ileu.ileuapi.models.chat.ChatsViewModel;
import biz.ileu.ileuapi.models.chat.MessageFromDto;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.IleuApp;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.services.IleuFirebaseMessagingService;
import biz.ileu.ileuapp.utils.services.SignalRService;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import biz.ileu.ileuapp.utils.ui.EndlessRecyclerOnScrollListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatMessagesActivity extends BaseActivity implements ChatMessagesContract.ChatMessagesView, SearchView.OnQueryTextListener {

    @BindView(R.id.civToolbarChatMessagesAvatar)
    CircleImageView civToolbarChatMessagesAvatar;
    @BindView(R.id.tvToolbarChatMessagesName)
    TextView tvToolbarChatMessagesName;
    @BindView(R.id.toolbar_chat_messages)
    Toolbar toolbarChatMessages;
    @BindView(R.id.rlChatMessages)
    RecyclerView rlChatMessages;
    @BindView(R.id.etChatMessage)
    EditText etChatMessage;
    @BindView(R.id.ibChatMessagesVoice)
    ImageButton ibChatMessagesVoice;
    @BindView(R.id.ibChatMessagesPost)
    ImageButton ibChatMessagesPost;

    private ChatsViewModel currentChat;
    private CompanyWorkInfoListViewModel currentCompany;
    private List<ChatMessageViewModel> list;
    private ChatMessagesAdapter adapter;

    private EndlessRecyclerOnScrollListener listener;

    private ChatMessagesPresenter presenter;

    private ProgressDialog progressDialog;

    private SignalRService signalRService;
    boolean serviceIsBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_messages);
        ButterKnife.bind(this);
        bindViews();
        setData();

        Intent intent = new Intent(this, SignalRService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void bindViews() {
        setSupportActionBar(toolbarChatMessages);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void setData() {
        currentCompany = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_COMPANY));
        currentChat = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_CHAT));
        if (IleuFirebaseMessagingService.instance != null) {
            IleuFirebaseMessagingService.instance.clearNotifications(currentChat.getId());
        }
        IleuApp mMyApp = (IleuApp) this.getApplicationContext();
        mMyApp.currentChatId = currentChat.getId();

        presenter = new ChatMessagesPresenter(this, currentCompany, currentChat);

        tvToolbarChatMessagesName.setText(currentChat.getChatUsers().get(0).getDisplayName());
        Picasso.with(this).load(Constants.BASE_URL + currentChat.getChatUsers().get(0).getProfilePicture())
                .into(civToolbarChatMessagesAvatar);


        list = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//        layoutManager.setReverseLayout(true);
//        layoutManager.setStackFromEnd(true);
        rlChatMessages.setLayoutManager(layoutManager);
        adapter = new ChatMessagesAdapter(list);
        rlChatMessages.setAdapter(adapter);
        adapter.notifyDataSetChanged();

//        listener = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
//            @Override
//            public void onLoadMore(int current_page) {
//
//                presenter.getMessages(current_page, Constants.DEFAULT_PAGE_SIZE);
//            }
//        };
//        rlProcesses.setRecyclerListener(listener);
//        rlChatMessages.addOnScrollListener(listener);

        presenter.getMessages(1, Constants.DEFAULT_PAGE_MAX_SIZE);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.chat_creating);

        if (currentChat.getId() == 0) {
            presenter.createChat();
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            SignalRService.LocalBinder binder = (SignalRService.LocalBinder) service;
            signalRService = binder.getService();
            serviceIsBound = true;

            signalRService.connectToCompany(currentCompany.getId());
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            serviceIsBound = false;
        }
    };

    @OnClick({R.id.ibChatMessagesVoice, R.id.ibChatMessagesPost})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ibChatMessagesVoice:
                break;
            case R.id.ibChatMessagesPost:
                sendMessage();
                break;
        }
    }

    @Override
    public void loadMessagesError(String error) {
        Toast.makeText(this, R.string.server_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void messagesLoaded(List<ChatMessageViewModel> messagesList) {
        list.addAll(messagesList);
        adapter.notifyDataSetChanged();

        rlChatMessages.scrollToPosition(list.size() - 1);
    }

    @Override
    public void messageIsSending(ChatMessageViewModel message) {
        message.setState(ChatMessageViewModel.STATE_SENDING);
        adapter.notifyItemChanged(list.indexOf(message));
    }

    @Override
    public void messageSendError(ChatMessageViewModel message, String error) {
        Toast.makeText(this, R.string.message_chat_mesage_send_error, Toast.LENGTH_SHORT).show();
        message.setState(ChatMessageViewModel.STATE_NOT_SENT);
        adapter.notifyItemChanged(list.indexOf(message));
    }

    @Override
    public void messageIsSent(ChatMessageViewModel message) {
        message.setState(ChatMessageViewModel.STATE_SENT);
        adapter.notifyItemChanged(list.indexOf(message));
        etChatMessage.setText("");
    }

    @Override
    public void chatCreating() {
        progressDialog.show();
    }

    @Override
    public void chatCreateError(String error) {
        progressDialog.dismiss();
        Toast.makeText(this, R.string.message_chat_create_error, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void chatCreated(ChatsViewModel chatsViewModel) {
        currentChat = chatsViewModel;
        progressDialog.dismiss();
    }

    public void sendMessage() {
        String messageStr = etChatMessage.getText().toString();
        if (messageStr.isEmpty()) {
            Toast.makeText(this, R.string.chat_message_empty, Toast.LENGTH_SHORT).show();
            etChatMessage.requestFocus();
            return;
        }

        ChatMessageViewModel message = new ChatMessageViewModel();
        message.setOwn(true);
        message.setText(messageStr);

        list.add(message);
        presenter.sendMessage(message);
        adapter.notifyDataSetChanged();
        etChatMessage.setText("");

        rlChatMessages.getLayoutManager().scrollToPosition(list.size() - 1);


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageFromDto message) {
        if (list == null || currentChat == null)
            return;

        if (message.getChatId() == currentChat.getId()) {
            ChatMessageViewModel chatMessage = new ChatMessageViewModel();
            chatMessage.setOwn(false);
            chatMessage.setText(message.getText());
            chatMessage.setProfilePicture(message.getProfilePicture());
            chatMessage.setDisplayName(message.getDisplayName());
            Log.d("CHAT MESSAGE RESULT", message.getText());
            list.add(chatMessage);
            adapter.notifyDataSetChanged();
            rlChatMessages.getLayoutManager().scrollToPosition(list.size() - 1);
        }

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase(Locale.getDefault());

        List<ChatMessageViewModel> filteredList = new ArrayList<>();
        for (ChatMessageViewModel messageViewModel: list)
            if (messageViewModel.getDisplayName().toLowerCase(Locale.getDefault()).contains(newText))
                filteredList.add(messageViewModel);
        adapter.changeDisplayMessages(filteredList);
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);


    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

        IleuApp mMyApp = (IleuApp) this.getApplicationContext();
        mMyApp.currentChatId = 0;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);
        MenuItem searchItem = menu.findItem(R.id.actionSearch);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(this);

        MenuItemCompat.collapseActionView(searchItem);

        return true;
    }


}
