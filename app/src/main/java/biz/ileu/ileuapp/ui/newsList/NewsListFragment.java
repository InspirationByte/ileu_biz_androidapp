package biz.ileu.ileuapp.ui.newsList;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.news.NewsEntryViewModel;
import biz.ileu.ileuapi.models.news.NewsList;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.newsList.news.NewsActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.cicerone.Screen;
import biz.ileu.ileuapp.utils.ui.BaseFragment;
import biz.ileu.ileuapp.utils.ui.EndlessRecyclerOnScrollListener;
import biz.ileu.ileuapp.utils.ui.RecyclerViewClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsListFragment extends BaseFragment implements RecyclerViewClick.OnItemClickListener {


    @BindView(R.id.rlNews)
    RecyclerView rlNews;

    @BindView(R.id.srlNews)
    SwipeRefreshLayout srlNews;

    private boolean isRefresh = false;
    Unbinder unbinder;
    private CompanyWorkInfoListViewModel currentCompany;

    private List<NewsEntryViewModel> list;
    private NewsListAdapter adapter;

    private CompositeSubscription compositeSubscription = null;

    private EndlessRecyclerOnScrollListener listener;


    public NewsListFragment() {
        // Required empty public constructor
    }


    public static NewsListFragment newInstance() {
        return new NewsListFragment();
    }

    public static NewsListFragment newInstance(Bundle bundle) {
        NewsListFragment fragment = new NewsListFragment();
        Bundle args = new Bundle();
        args.putAll(bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        bindViews();
        setData();
        return view;
    }

    @Override
    public void bindViews() {

    }

    @Override
    public void setData() {
        getActionBar().setTitle(getResources().getString(R.string.news));

        compositeSubscription = new CompositeSubscription();

        Bundle args = getArguments();

        if (args != null && args.containsKey(Constants.ENTITY_COMPANY))
            currentCompany = Parcels.unwrap(args.getParcelable(Constants.ENTITY_COMPANY));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rlNews.setLayoutManager(layoutManager);

        list = new ArrayList<>();
        adapter = new NewsListAdapter(list, this);
        rlNews.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if (currentCompany == null)
            return;

        listener = new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (isRefresh)
                    return;
                request(current_page, Constants.DEFAULT_PAGE_SIZE);
            }
        };

        srlNews.post(new Runnable() {
            @Override
            public void run() {
                if (isRefresh)
                    return;
                srlNews.setRefreshing(true);
                request(1, Constants.DEFAULT_PAGE_SIZE);
            }
        });

        srlNews.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isRefresh = true;
                list.clear();
                adapter.notifyDataSetChanged();
                srlNews.setRefreshing(true);
                listener.reset();
                requestRefresh(1, Constants.DEFAULT_PAGE_SIZE);
            }
        });

        rlNews.addOnScrollListener(listener);


    }

    private void request(int page, int per) {
        Subscription subscription = RestClient.request()
                .getNewsList(PrefUtils.getToken(), currentCompany.getId(), "PublicationTime-desc", page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<NewsList>() {
                    @Override
                    public void onCompleted() {
                        srlNews.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(NewsList newsList) {
                        if (!isRefresh) {
                            list.addAll(newsList.getNewsEntryViewModelList());
                            adapter.notifyDataSetChanged();
                        }
                    }
                });

        compositeSubscription.add(subscription);
    }

    private void requestRefresh(int page, int per) {
        Subscription subscription = RestClient.request()
                .getNewsList(PrefUtils.getToken(), currentCompany.getId(), "PublicationTime-desc", page, per)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<NewsList>() {
                    @Override
                    public void onCompleted() {
                        isRefresh = false;
                        srlNews.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(NewsList newsList) {

                        list.clear();
                        list.addAll(newsList.getNewsEntryViewModelList());
                        adapter.notifyDataSetChanged();
                    }
                });

        compositeSubscription.add(subscription);
    }
    @Override
    public String getScreenName() {
        return Screen.SCREEN_NEWS_LIST;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();
    }

    @Override
    public void onItemClick(View view, int position) {
            NewsEntryViewModel selectedNews = list.get(position);
            Intent intent = new Intent(getActivity(), NewsActivity.class);
            intent.putExtra(Constants.ENTITY_NEWS, Parcels.wrap(selectedNews));

            startActivity(intent);
    }
}
