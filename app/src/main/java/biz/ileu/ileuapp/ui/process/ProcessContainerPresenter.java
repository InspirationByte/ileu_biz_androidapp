package biz.ileu.ileuapp.ui.process;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.bookmarks.BookmarkCategoryViewModel;
import biz.ileu.ileuapi.models.process.ProcessUserStatsViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.utils.PrefUtils;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniq on 07.06.2017.
 */

public class ProcessContainerPresenter implements  ProcessContainerContract.ProcessContainerPresenter {

    private CompanyWorkInfoListViewModel currentCompany;

    private ProcessContainerContract.ProcessContainerView processContainerView;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public ProcessContainerPresenter(CompanyWorkInfoListViewModel currentCompany, ProcessContainerContract.ProcessContainerView processContainerView) {
        this.currentCompany = currentCompany;
        this.processContainerView = processContainerView;

    }

    public void loadProcessStats() {
        Subscription subscription = RestClient.request().getProcessStats(PrefUtils.getToken(), currentCompany.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProcessUserStatsViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ProcessUserStatsViewModel processUserStatsViewModel) {
                        processContainerView.processStatsLoaded(processUserStatsViewModel);
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void createBookmarkCategory(String title) {
        Subscription subscription = RestClient.request().createBookmarkCategory(PrefUtils.getToken(), currentCompany.getId(), title)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BookmarkCategoryViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(BookmarkCategoryViewModel categoryViewModel) {
                        processContainerView.categoryCreated(categoryViewModel);
                    }
                });
        compositeSubscription.add(subscription);
    }
    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();
        processContainerView = null;
    }
}
