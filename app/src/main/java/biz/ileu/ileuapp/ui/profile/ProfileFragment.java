package biz.ileu.ileuapp.ui.profile;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.invitation.CompanyInvitationResultDTO;
import biz.ileu.ileuapi.models.invitation.InvitationResultDTO;
import biz.ileu.ileuapi.models.profile.ProfileInfo;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.IleuApp;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.login.LoginActivity;
import biz.ileu.ileuapp.ui.menu.MenuActivity;
import biz.ileu.ileuapp.ui.profile.profileSettings.ProfileSettingsActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.cicerone.Screen;
import biz.ileu.ileuapp.utils.services.SignalRService;
import biz.ileu.ileuapp.utils.ui.BaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment implements ProfileContract.ProfileView {


    @BindView(R.id.civProfileAvatar)
    CircleImageView civProfileAvatar;
    @BindView(R.id.tvProfileName)
    TextView tvProfileName;
    @BindView(R.id.llProfileSettings)
    LinearLayout llProfileSettings;

    @BindView(R.id.llProfileInvite)
    LinearLayout llProfileInvite;

    @BindView(R.id.llProfileLogout)
    LinearLayout llProfileLogout;

    Unbinder unbinder;

    private ProfilePresenter presenter;
    private long firstTouchTS;
    private ProfileInfo profileInfo;
    private CompanyWorkInfoListViewModel currentCompany;
    private int clickCount = 0;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        this.clickCount = 0;
        presenter = new ProfilePresenter(this);
        bindViews();
        unbinder = ButterKnife.bind(this, view);
        setData();
        return view;
    }

    @Override
    public void bindViews() {

    }

    @Override
    public void setData() {
        getActionBar().setTitle(getResources().getString(R.string.profile));
        presenter.getProfile();
    }

    public CompanyWorkInfoListViewModel getCurrentCompany() {
        return currentCompany;
    }

    public void setCurrentCompany(CompanyWorkInfoListViewModel currentCompany) {
        this.currentCompany = currentCompany;
    }

    @Override
    public String getScreenName() {
        return Screen.SCREEN_PROFILE;
    }

    @Override
    public void profileLoading() {

    }

    @Override
    public void profileError(String error) {
        Toast.makeText(getActivity(), "Error: " + error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void profileData(ProfileInfo profileInfo) {
        this.profileInfo = profileInfo;

        Picasso.with(getActivity())
                .load(Constants.BASE_URL + profileInfo.getProfileImage())
                .into(civProfileAvatar);
        if (profileInfo.getFirstname() == null || profileInfo.getFirstname().equals("") ||
            profileInfo.getSurname() == null || profileInfo.getSurname().equals("")
                ) {
            tvProfileName.setText(profileInfo.getEmail());
        } else {
            tvProfileName.setText(profileInfo.getFirstname() + " " + profileInfo.getSurname());

        }

    }

    @Override
    public void successInvited(CompanyInvitationResultDTO profileInfo) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Результат");

// Set up the input
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
        llp.setMargins(20, 0, 20, 0); // llp.setMargins(left, top, right, bottom);
        final EditText input = new EditText(getActivity());
        final TextView tvResult = new TextView(getActivity());
        tvResult.setLayoutParams(llp);
        final LinearLayout llBuilder = new LinearLayout(getActivity());
        llBuilder.setOrientation(LinearLayout.HORIZONTAL);
//        input.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS );
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        String res = "";
        for (InvitationResultDTO ires : profileInfo.getResult()) {
            if (ires.getPhone() != null) {
                res += ires.getPhone() + ": " + ires.getMessage() + "\n";
            } else {
                res += ires.getEmail() + ": " + ires.getMessage() + "\n";
            }
        }
        tvResult.setText(res);
        llBuilder.addView(tvResult);
        builder.setView(llBuilder);

// Set up the buttons
        final AlertDialog mAlertDialog = builder.create();
        mAlertDialog.show();
    }


    @Override
    public void onDestroyView() {
        presenter.destroy();
        super.onDestroyView();
    }

    @OnClick(R.id.llProfileSettings)
    public void onSettingsClicked() {
        Intent intent = new Intent(getActivity(), ProfileSettingsActivity.class);
        intent.putExtra(Constants.ENTITY_PROFILE_INFO, Parcels.wrap(profileInfo));
        startActivityForResult(intent, Constants.PROFILE_CHANGE_CODE);
    }

    @OnClick(R.id.llProfileInvite)
    public void onInviteClicked() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Пригласить участника");

// Set up the input
        final EditText input = new EditText(getActivity());
        final LinearLayout llBuilder = new LinearLayout(getActivity());
        llBuilder.setOrientation(LinearLayout.HORIZONTAL);
//        input.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS );
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT );
        input.setHint(R.string.email_or_phone);
        input.setSingleLine(false);
        input.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        llBuilder.addView(input);
        builder.setView(llBuilder);
        builder.setNeutralButton("+ ЕЩЕ", null);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String m_Text = input.getText().toString();

                String[] temp;
                temp = m_Text.split("\n");
                presenter.invite(temp, currentCompany.getId());

            }
        });
        builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog mAlertDialog = builder.create();
        mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button b = mAlertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        input.setText(input.getText() + "\n");
                        input.setSelection(input.getText().length());
                    }
                });
            }
        });
        mAlertDialog.show();


    }
    @OnClick(R.id.civProfileAvatar)
    public void Onclick() {
        if (++clickCount == 5) {
            clickCount = 0;
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Изменить настройки сервера");

            // Set up the input
            final EditText input = new EditText(getActivity());
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //                m_Text = input.getText().toString();
                    RestClient.setBaseUrl(input.getText().toString());
                    PrefUtils.clearAllPrefsExceptFirebaseToken();
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    getActivity().finish();
                    IleuApp.INSTANCE.updateRetrofit();
                }
            });
            builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();

                }
            });

            builder.show();
        }
    }

    @OnClick(R.id.llProfileLogout)
    public void onLogoutClicked() {
        presenter.logout();
    }

    @Override
    public void logoutSuccess() {
        SignalRService signalRService = ((MenuActivity) getActivity()).signalRService;
        if (signalRService != null)
            signalRService.destroyConnection();

        PrefUtils.clearAllPrefsExceptFirebaseToken();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void logoutError(String error) {
        Toast.makeText(getActivity(), "Error: " + error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.PROFILE_CHANGE_CODE) {
            presenter.getProfile();
        }
    }
}
