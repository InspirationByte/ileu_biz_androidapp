package biz.ileu.ileuapp.ui.register;

import android.util.Log;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapi.models.TokenData;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniq on 14.04.2017.
 */

public class RegisterPresenter implements RegisterContract.RegisterPresenter {

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private RegisterContract.RegisterView registerView;

    public RegisterPresenter(RegisterContract.RegisterView registerView) {
        this.registerView = registerView;
    }

    public void register(final String email, final String password, String confirmation) {

        Call<ResponseBody> call = RestClient.request().register(email, password, confirmation);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.code() != 200) {
                    registerView.registerError("");
                    return;
                }

                Subscription subscription = RestClient.request()
                        .login(Constants.GRANT_TYPE, email, password)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<TokenData>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                registerView.registerError(e.getMessage());
                            }

                            @Override
                            public void onNext(TokenData tokenData) {
                                registerView.registerComplete(tokenData);
                            }
                        });

                compositeSubscription.add(subscription);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                registerView.registerError(t.getMessage());
            }
        });


        registerView.registerLoading();
    }

    public void sendFirebaseToken(String firebaseToken) {
        registerView.firebaseTokenLoading();

        Subscription subscription = RestClient.request().updateFirebaseToken(PrefUtils.getToken(), firebaseToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        Log.d("FIREBASE_UPDATE", "YES");
                        registerView.firebaseTokenIsSent();
                    }

                    @Override
                    public void onError(Throwable e) {
                        registerView.firebaseTokenIsSent();
                        Log.d("FIREBASE_UPDATE", "NO");
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                    }
                });

        compositeSubscription.add(subscription);
    }

    @Override
    public void destroy() {
        if (compositeSubscription != null && compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
        registerView = null;
    }
}
