package biz.ileu.ileuapp.ui.register;

import biz.ileu.ileuapi.models.TokenData;
import biz.ileu.ileuapp.utils.ui.BasePresenter;
import biz.ileu.ileuapp.utils.ui.BaseView;

/**
 * Created by Daniq on 14.04.2017.
 */

public interface RegisterContract {

    interface RegisterView extends BaseView {
        void registerLoading();

        void registerError(String error);

        void registerComplete(TokenData tokenData);

        void firebaseTokenLoading();

        void firebaseTokenIsSent();
    }

    interface RegisterPresenter extends BasePresenter {

    }

}
