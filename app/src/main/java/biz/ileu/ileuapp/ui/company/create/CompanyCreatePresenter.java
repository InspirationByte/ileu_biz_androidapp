package biz.ileu.ileuapp.ui.company.create;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapp.utils.PrefUtils;
import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by macair on 08.08.17.
 */

public class CompanyCreatePresenter implements CompanyCreateContract.CompanyCreatePresenter {

    private final CompanyCreateContract.CompanyCreateView companyView;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public CompanyCreatePresenter(CompanyCreateContract.CompanyCreateView companyView) {
        this.companyView = companyView;
    }

    @Override
    public void destroy() {

    }

    public void createCompany(String companyName, String companyBIN, String companyWebsite,String dateFound,  String companyEmail, String companyDesc, long companyType, boolean isAddBasicProccess, boolean isAddDataProccess) {
        Subscription subscription = RestClient.request()
                .createMyCompany(PrefUtils.getToken(), companyName, companyBIN, (int)companyType, companyEmail, companyWebsite, dateFound, companyDesc, isAddBasicProccess, isAddDataProccess)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        companyView.failCreatingCompany(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        companyView.createdCompany();
                    }
                });
        compositeSubscription.add(subscription);

    }
}
