package biz.ileu.ileuapp.ui.newsList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import biz.ileu.ileuapi.models.company.CompanyViewModel;
import biz.ileu.ileuapi.models.news.FileInfoViewModel;
import biz.ileu.ileuapi.models.news.NewsCommentViewModel;
import biz.ileu.ileuapi.models.news.NewsEntryViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.process.processComments.ProcessCommentsListAdapter;
import biz.ileu.ileuapp.utils.Constants;
import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Daniq on 06.04.2017.
 */

public class NewsCommentsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private NewsEntryViewModel news;
    private List<NewsCommentViewModel> comments;

    private NewsClick.OnItemClick listener;

    public NewsCommentsListAdapter(NewsEntryViewModel news, List<NewsCommentViewModel> comments, NewsClick.OnItemClick listener) {
        this.news = news;
        this.comments = comments;

        this.listener = listener;
    }




    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return comments.size() + 1;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder = null;
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_header_layout, parent, false);
            holder = new CommentsHeaderViewHolder(view);
        } else if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_news_comment, parent, false);
            holder = new CommentsViewHolder(view);
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CommentsHeaderViewHolder) {
            CommentsHeaderViewHolder header = (CommentsHeaderViewHolder) holder;
            header.tvNewsTitle.setText(news.getTitle());

            Picasso.with(header.context)
                    .load(Constants.BASE_URL + news.getAddedByAvatar())
                    .into(header.civNewsAvatar);

            header.tvNewsAdderName.setText(news.getAddedByName());
            header.tvNewsPublishedTime.setText(Constants.getFormattedDateTime(news.getPublicationTime()));
            header.tvNewsText.setText(Html.fromHtml(news.getText()));
            header.tvNewsText.setMovementMethod(LinkMovementMethod.getInstance());

            header.llNewsFiles.removeAllViews();
            for (FileInfoViewModel fileInfo: news.getFiles()) {
                View view = null;
                view = LayoutInflater.from(header.context).inflate(R.layout.item_process_comment_file,
                        header.llNewsFiles, false);
                NewsFileViewHolder fileHolder = new NewsFileViewHolder(view,
                        fileInfo.getFileName(), fileInfo.getFileLink());

                header.llNewsFiles.addView(view);
            }
        }

        else if (holder instanceof  CommentsViewHolder) {
            CommentsViewHolder item = (CommentsViewHolder) holder;
            NewsCommentViewModel comment = comments.get(position - 1);


            Picasso.with(item.context)
                    .load(Constants.BASE_URL + comment.getAddedByAvatar())
                    .into(item.civRowNewsCommentAvatar);

            item.tvRowNewsCommentName.setText(comment.getAddedByName());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            item.tvRowNewsCommentPublishedDate.setText(simpleDateFormat.format(comment.getTimeAsDate()));
            item.tvRowNewsCommentText.setText(comment.getText());
        }
    }



    class CommentsHeaderViewHolder extends RecyclerView.ViewHolder {

        Context context;

        @BindView(R.id.tvNewsTitle)
        TextView tvNewsTitle;

        @BindView(R.id.civNewsAvatar)
        CircleImageView civNewsAvatar;

        @BindView(R.id.tvNewsAdderName)
        TextView tvNewsAdderName;

        @BindView(R.id.tvNewsPublishedTime)
        TextView tvNewsPublishedTime;

        @BindView(R.id.tvNewsText)
        TextView tvNewsText;

        @BindView(R.id.llNewsFiles)
        LinearLayout llNewsFiles;

        public CommentsHeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    class CommentsViewHolder extends RecyclerView.ViewHolder {

        Context context;

        @BindView(R.id.civRowNewsCommentAvatar)
        CircleImageView civRowNewsCommentAvatar;

        @BindView(R.id.tvRowNewsCommentName)
        TextView tvRowNewsCommentName;

        @BindView(R.id.tvRowNewsCommentPublishedDate)
        TextView tvRowNewsCommentPublishedDate;

        @BindView(R.id.tvRowNewsCommentText)
        TextView tvRowNewsCommentText;

        public CommentsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    public class NewsFileViewHolder {
        @BindView(R.id.tvProcessCommentFile)
        public TextView tvProcessCommentFile;

        @BindView(R.id.ivProcessCommentFile)
        public ImageView ivProcessCommentFile;

        @BindView(R.id.llProcessCommentFile)
        public LinearLayout llProcessCommentFile;

        public NewsFileViewHolder(final View view, final String name, final String link) {
            ButterKnife.bind(this, view);

            tvProcessCommentFile.setText(name.length() < 12 ? name : name.substring(0, 11) + "...");
            if (name.endsWith(".pdf"))
                ivProcessCommentFile.setImageResource(R.mipmap.ic_pdf);
            else if (name.endsWith(".avi") ||
                    name.endsWith(".3gp") ||
                    name.endsWith(".mp4") ||
                    name.endsWith(".wmv") || name.endsWith(".mpeg"))
                ivProcessCommentFile.setImageResource(R.mipmap.ic_video);
            else if (name.endsWith(".mp3") ||
                    name.endsWith(".amr"))
                ivProcessCommentFile.setImageResource(R.mipmap.ic_audio);

            Map<String, List<String>> queries = Constants.getQueryParams(link);
            String entryId = "";
            String fileId = "";

            entryId = queries.get("entryId").get(0);
            fileId = queries.get("fileId").get(0);

            final String finelEntryId = entryId;
            final String finalFileId = fileId;

            llProcessCommentFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDownloadFileClick(name, finalFileId, finelEntryId);
                }
            });

        }


    }
}
