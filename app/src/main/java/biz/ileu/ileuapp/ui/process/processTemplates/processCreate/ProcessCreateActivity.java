package biz.ileu.ileuapp.ui.process.processTemplates.processCreate;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.parceler.Parcels;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimeZone;

import biz.ileu.ileuapi.models.process.CreateProcessColumnViewModel;
import biz.ileu.ileuapi.models.process.CreateProcessViewModel;
import biz.ileu.ileuapi.models.process.EmployeeViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.process.ProcessCreateStatus;
import biz.ileu.ileuapi.models.processtypes.ProcessTemplateViewModel;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.process.processComments.ProcessCommentsActivity;
import biz.ileu.ileuapp.ui.process.processTemplates.processCreate.processParticipants.ProcessCreateParticipantsAddActivity;
import biz.ileu.ileuapp.ui.process.processTemplates.processCreate.processParticipants.ProcessCreateResponsibleSelectActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.events.ProcessOpenedEvent;
import biz.ileu.ileuapp.utils.ui.BaseActivity;
import biz.ileu.ileuapp.utils.ui.attachedFile.AttachedFile;
import biz.ileu.ileuapp.utils.ui.attachedFile.AttachedFileViewClickListener;
import biz.ileu.ileuapp.utils.ui.attachedFile.AttachedFileViewHolder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import skedgo.datetimerangepicker.DateTimeRangePickerActivity;

@RuntimePermissions
public class ProcessCreateActivity extends BaseActivity implements ProcessCreateContract.ProcessCreateView, AttachedFileViewClickListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.etProcessCreateTitle)
    EditText etProcessCreateTitle;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.etProcessCreateDescription)
    EditText etProcessCreateDescription;
    @BindView(R.id.ibProcessCreateAttach)
    ImageButton ibProcessCreateAttach;
    @BindView(R.id.ibProcessCreateAddParticipants)
    ImageButton ibProcessCreateAddParticipants;
    @BindView(R.id.ibProcessCreateSetTime)
    ImageButton ibProcessCreateSetTime;

    @BindView(R.id.llProcessCreateResponsibleInfo)
    LinearLayout llProcessCreateResponsibleInfo;
    @BindView(R.id.llProcessCreateResponsible)
    LinearLayout llProcessCreateResponsible;

    @BindView(R.id.llProcessCreateParticipantInfo)
    LinearLayout llProcessCreateParticipantInfo;
    @BindView(R.id.llProcessCreateParticipant)
    LinearLayout llProcessCreateParticipant;
    @BindView(R.id.tvProcessCreateStartDate)
    TextView tvProcessCreateStartDate;
    @BindView(R.id.tvProcessCreateEndDate)
    TextView tvProcessCreateEndDate;
    @BindView(R.id.llProcessCreateStartDateEndDate)
    LinearLayout llProcessCreateStartDateEndDate;
    @BindView(R.id.llProcessCreateExecutorInfo)
    LinearLayout llProcessCreateExecutorInfo;
    @BindView(R.id.llProcessCreateExecutor)
    LinearLayout llProcessCreateExecutor;

    @BindView(R.id.llProcessCreateAttachmentsInfo)
    LinearLayout llProcessCreateAttachmentsInfo;
    @BindView(R.id.llProcessCreateAttachments)
    LinearLayout llProcessCreateAttachments;

    private CompanyWorkInfoListViewModel currentCompany;
    private ProcessTemplateViewModel processTemplate;

    private EmployeeViewModel responsibleEmployee;
    private EmployeeViewModel executorEmployee;
    private List<EmployeeViewModel> selectedParticipants;

    private Date startDateTime;
    private Date endDateTime;
    SimpleDateFormat dateFormat;

    private ArrayList<String> selectedFilesPaths;
    private ArrayList<AttachedFile> attachedFiles;


    ProcessCreatePresenter presenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_create);
        ButterKnife.bind(this);

        bindViews();
        setData();
    }

    @Override
    public void bindViews() {
        initBaseToolbar(true);
        getSupportActionBar().setTitle(R.string.process_create);
    }

    @Override
    public void setData() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.process_creating);
        currentCompany = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_COMPANY));
        processTemplate = Parcels.unwrap(getIntent().getParcelableExtra(Constants.ENTITY_PROCESS_TEMPLATE));
        selectedParticipants = new ArrayList<>();
        attachedFiles = new ArrayList<>();
        selectedFilesPaths = new ArrayList<>();
        presenter = new ProcessCreatePresenter(this, currentCompany);


        // Setting up a date
        Calendar calendar = Calendar.getInstance();
        startDateTime = calendar.getTime();
        calendar.add(Calendar.DATE, 1);
        endDateTime = calendar.getTime();
        dateFormat = new SimpleDateFormat("HH:mm dd.MM.yyyy");
        llProcessCreateStartDateEndDate.setVisibility(View.VISIBLE);
        tvProcessCreateStartDate.setText(dateFormat.format(startDateTime));
        tvProcessCreateEndDate.setText(dateFormat.format(endDateTime));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.process_create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.actionProcessCreate) {
            createProcess();
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.ibProcessCreateAttach, R.id.ibProcessCreateAddParticipants, R.id.ibProcessCreateSetTime})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ibProcessCreateAttach:
                ProcessCreateActivityPermissionsDispatcher.attachFilesWithCheck(this);
                break;
            case R.id.ibProcessCreateAddParticipants:
                showParticipantsDialog();
                break;
            case R.id.ibProcessCreateSetTime:
                showDateTimePicker();
                break;
        }
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    public void attachFiles() {
        FilePickerBuilder.getInstance()
                .setMaxCount(3)
                .setSelectedFiles(selectedFilesPaths)
                .setActivityTheme(R.style.CustomTheme)
                .addVideoPicker()
                .enableDocSupport(true)
                .pickFile(this);
    }

    @OnPermissionDenied(Manifest.permission.READ_EXTERNAL_STORAGE)
    void showDeniedForStorage() {
        Toast.makeText(this, R.string.message_permission_denied, Toast.LENGTH_SHORT).show();
    }

    private void showDateTimePicker() {
        Intent intent = new Intent(this, DateTimeRangePickerActivity.class);
        intent.putExtra("timeZone", TimeZone.getDefault().getID());
        intent.putExtra("startTimeInMillis", startDateTime.getTime());
        intent.putExtra("endTimeInMillis", endDateTime.getTime());
        startActivityForResult(intent, Constants.PROCESS_CREATE_SET_DATETIME);
    }

    private void showParticipantsDialog() {
        final String[] options = {
                getString(R.string.process_create_set_responsible),
                getString(R.string.process_create_set_executor),
                getString(R.string.process_create_add_participants),};

        ListView optionsList = new ListView(this);
        optionsList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, options));
        optionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(ProcessCreateActivity.this, ProcessCreateResponsibleSelectActivity.class);
                    intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
                    intent.putExtra("requestCode", Constants.PROCESS_CREATE_SET_RESPONSIBLE_CODE);

                    startActivityForResult(intent, Constants.PROCESS_CREATE_SET_RESPONSIBLE_CODE);
                } else if (position == 1) {
                    Intent intent = new Intent(ProcessCreateActivity.this, ProcessCreateResponsibleSelectActivity.class);
                    intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
                    intent.putExtra("requestCode", Constants.PROCESS_CREATE_SET_EXECUTOR_CODE);

                    startActivityForResult(intent, Constants.PROCESS_CREATE_SET_EXECUTOR_CODE);
                } else if (position == 2) {
                    Intent intent = new Intent(ProcessCreateActivity.this, ProcessCreateParticipantsAddActivity.class);
                    intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));

                    startActivityForResult(intent, Constants.PROCESS_CREATE_ADD_PARTICIPANTS_CODE);
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(optionsList);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            if (requestCode == Constants.PROCESS_CREATE_SET_RESPONSIBLE_CODE) {
                responsibleEmployee = Parcels.unwrap(data.getParcelableExtra(Constants.ENTITY_EMPLOYEE));
                llProcessCreateResponsible.setVisibility(View.VISIBLE);

                llProcessCreateResponsibleInfo.removeAllViews();
                View view = LayoutInflater.from(this).inflate(R.layout.row_item_process_create_participant, null, false);
                EmployeeViewHolder holder = new EmployeeViewHolder(view, responsibleEmployee, EmployeeViewHolder.EMPLOYEE_RESPONSIBLE);

                llProcessCreateResponsibleInfo.addView(view);
            } else if (requestCode == Constants.PROCESS_CREATE_SET_EXECUTOR_CODE) {

                executorEmployee = Parcels.unwrap(data.getParcelableExtra(Constants.ENTITY_EMPLOYEE));
                llProcessCreateExecutor.setVisibility(View.VISIBLE);

                llProcessCreateExecutorInfo.removeAllViews();
                View view = LayoutInflater.from(this).inflate(R.layout.row_item_process_create_participant, null, false);
                EmployeeViewHolder holder = new EmployeeViewHolder(view, executorEmployee, EmployeeViewHolder.EMPLOYEE_EXECUTOR);
                llProcessCreateExecutorInfo.addView(view);

            } else if (requestCode == Constants.PROCESS_CREATE_ADD_PARTICIPANTS_CODE) {

                ArrayList<Parcelable> parcelables = data.getParcelableArrayListExtra(Constants.ENTITY_EMPLOYEES);

                if (parcelables.size() > 0)
                    llProcessCreateParticipant.setVisibility(View.VISIBLE);
                else
                    llProcessCreateParticipant.setVisibility(View.GONE);
                llProcessCreateParticipantInfo.removeAllViews();
                selectedParticipants.clear();

                for (Parcelable parcelable : parcelables) {
                    EmployeeViewModel employeeViewModel = (EmployeeViewModel) Parcels.unwrap(parcelable);
                    selectedParticipants.add(employeeViewModel);


                    View view = LayoutInflater.from(this).inflate(R.layout.row_item_process_create_participant, null, false);
                    EmployeeViewHolder holder = new EmployeeViewHolder(view, employeeViewModel, EmployeeViewHolder.EMPLOYEE_PARTICIPANT);

                    llProcessCreateParticipantInfo.addView(view);
                }

            } else if (requestCode == Constants.PROCESS_CREATE_SET_DATETIME) {

                long startLong = data.getLongExtra("startTimeInMillis", 0);
                long endLong = data.getLongExtra("endTimeInMillis", 0);

                if (startLong != 0 && endLong != 0) {

                    startDateTime = new Date(startLong);
                    endDateTime = new Date(endLong);

                    llProcessCreateStartDateEndDate.setVisibility(View.VISIBLE);
                    tvProcessCreateStartDate.setText(dateFormat.format(startDateTime));
                    tvProcessCreateEndDate.setText(dateFormat.format(endDateTime));

                }

            } else if (requestCode == FilePickerConst.REQUEST_CODE_DOC && data != null) {
                List<String> filePaths = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS);

                llProcessCreateAttachmentsInfo.removeAllViews();
                attachedFiles.clear();
                for (String path: filePaths) {
                    AttachedFile attachedFile = new AttachedFile(path);
                    attachedFiles.add(attachedFile);
                    View attachedFileView = LayoutInflater.from(this).inflate(R.layout.item_attached_file, null, false);
                    AttachedFileViewHolder holder = new AttachedFileViewHolder(attachedFileView, attachedFile, this);
                    llProcessCreateAttachmentsInfo.addView(attachedFileView);
                }

                if (attachedFiles.size() > 0)
                    llProcessCreateAttachments.setVisibility(View.VISIBLE);
                else
                    llProcessCreateAttachments.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onAttachedFileClick(AttachedFile attachedFile, View v) {
        attachedFiles.remove(attachedFile);
        llProcessCreateAttachmentsInfo.removeView(v);

        if (attachedFiles.size() == 0)
            llProcessCreateAttachments.setVisibility(View.GONE);
    }


    public void createProcess() {
        String title = etProcessCreateTitle.getText().toString();
        String description = etProcessCreateDescription.getText().toString();


        if (title.isEmpty()) {
            Toast.makeText(this, R.string.message_process_title_empty, Toast.LENGTH_SHORT).show();
            etProcessCreateTitle.requestFocus();
            return;
        }

        if (description.isEmpty()) {
            Toast.makeText(this, R.string.message_process_description_empty, Toast.LENGTH_SHORT).show();
            etProcessCreateDescription.requestFocus();
            return;
        }


        if (executorEmployee == null) {
            Toast.makeText(this, R.string.message_precess_executor_empty, Toast.LENGTH_SHORT).show();
            return;
        }


        if (startDateTime == null || endDateTime == null) {
            Toast.makeText(this, R.string.message_process_start_end_datetime_empty, Toast.LENGTH_SHORT).show();
            return;
        }


        CreateProcessViewModel createProcessViewModel = new CreateProcessViewModel();
        createProcessViewModel.setTitle(title);

        createProcessViewModel.setDescription(description);
        if (responsibleEmployee != null)
            createProcessViewModel.setResponsibleUser(responsibleEmployee.getId());
        else
            createProcessViewModel.setResponsibleUser(PrefUtils.getCurrentUserId());
        createProcessViewModel.setExecutingUser(executorEmployee.getId());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");
        createProcessViewModel.setStartDateTime(format.format(startDateTime) + Constants.getCurrentTimezoneOffset());
        createProcessViewModel.setEndDateTime(format.format(endDateTime) + Constants.getCurrentTimezoneOffset());
        createProcessViewModel.setProcessNameId(processTemplate.getId());

        int[] participantIds = new int[selectedParticipants.size()];
        for (int i = 0; i < selectedParticipants.size(); i++)
            participantIds[i] = selectedParticipants.get(i).getId();
        createProcessViewModel.setParticipants(participantIds);

        createProcessViewModel.setColumnsValues(new ArrayList<CreateProcessColumnViewModel>());

        presenter.createProcess(createProcessViewModel, attachedFiles);
    }

    @Override
    public void processCreating() {
        progressDialog.show();
    }

    @Override
    public void processCreateError(String error) {
        progressDialog.dismiss();
        Toast.makeText(this, R.string.server_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void processCreated(ProcessCreateStatus processCreateStatus) {
        progressDialog.dismiss();

        if (processCreateStatus.isError()) {
            Toast.makeText(this, processCreateStatus.getStatusText(), Toast.LENGTH_LONG).show();
        } else {
            presenter.getProcess(processCreateStatus.getId());
        }
    }

    @Override
    public void processLoading() {
        progressDialog.show();
    }

    @Override
    public void processLoaded(Process process) {
        progressDialog.dismiss();

        Log.d("DATE", process.getEndDateTime() + "");
        Toast.makeText(getApplicationContext(), R.string.message_process_created, Toast.LENGTH_SHORT).show();
        Log.d("PROCESS", process.getId() + "");
        Log.d("PROCESS", process.getStartDateTime() + "");
        Log.d("PROCESS", process.getProcessName() + "");
        Log.d("PROCESS", process.getDescriptionText() + "");
        Log.d("PROCESS", process.getTitle() + "");
        Intent intent = new Intent(this, ProcessCommentsActivity.class);
        intent.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(currentCompany));
        intent.putExtra(Constants.ENTITY_PROCESS, Parcels.wrap(process));
        intent.putExtra(Constants.PROCESS_IS_NEW, true);
        startActivity(intent);
        finish();
    }

    @Override
    public void processLoadError(String error) {
        Toast.makeText(this, R.string.message_process_load_error, Toast.LENGTH_SHORT).show();
    }



    public class EmployeeViewHolder implements View.OnClickListener {
        Context context;

        public static final int EMPLOYEE_RESPONSIBLE = 0;
        public static final int EMPLOYEE_EXECUTOR = 1;
        public static final int EMPLOYEE_PARTICIPANT = 2;

        @BindView(R.id.civRowProcessCreateParticipantResponsible)
        CircleImageView civRowProcessCreateParticipantResponsible;

        @BindView(R.id.tvRowProcessCreateParticipantResponsibleName)
        TextView tvRowProcessCreateParticipantResponsibleName;

        @BindView(R.id.tvRowProcessCreateParticipantResponsibleEmail)
        TextView tvRowProcessCreateParticipantResponsibleEmail;

        @BindView(R.id.ibRowProcessCreateParticipantRemove)
        ImageButton ibRowProcessCreateParticipantRemove;

        private int employeeType = EMPLOYEE_PARTICIPANT;
        private EmployeeViewModel employeeViewModel;

        private View view;

        public EmployeeViewHolder(View view, EmployeeViewModel employeeViewModel, int employeeType)  {
            ButterKnife.bind(this, view);
            this.employeeType = employeeType;
            this.employeeViewModel = employeeViewModel;
            this.view = view;

            tvRowProcessCreateParticipantResponsibleEmail.setText(employeeViewModel.getEmail());
            tvRowProcessCreateParticipantResponsibleName.setText(employeeViewModel.getNameSurname());

            Picasso.with(view.getContext())
                    .load(Constants.IMAGE_BASE_URL + employeeViewModel.getProfilePicture())
                    .into(civRowProcessCreateParticipantResponsible);

            ibRowProcessCreateParticipantRemove.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (employeeType == EMPLOYEE_RESPONSIBLE) {
                responsibleEmployee = null;
                llProcessCreateResponsibleInfo.removeView(view);
                llProcessCreateResponsible.setVisibility(View.GONE);
            } else if (employeeType == EMPLOYEE_EXECUTOR) {
                executorEmployee = null;
                llProcessCreateExecutorInfo.removeView(view);
                llProcessCreateExecutor.setVisibility(View.GONE);
            } else if (employeeType == EMPLOYEE_PARTICIPANT) {
                selectedParticipants.remove(employeeViewModel);
                llProcessCreateParticipantInfo.removeView(view);
                if (selectedParticipants.size() == 0)
                    llProcessCreateParticipant.setVisibility(View.GONE);
            }

        }

        ;
    }
}
