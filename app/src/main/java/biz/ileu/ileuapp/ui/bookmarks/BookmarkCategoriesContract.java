package biz.ileu.ileuapp.ui.bookmarks;

import java.util.List;

import biz.ileu.ileuapi.models.bookmarks.BookmarksCategoriesList;

/**
 * Created by macair on 09.08.17.
 */

public interface BookmarkCategoriesContract {

    public interface BookmarkCategoriesView {

        void categoriesLoaded(BookmarksCategoriesList categoriesList);

        void loadingError(String message);

        void successAdded();

        void addingError(String message);

        void categoriesIdsLoaded(List<Integer> categoriesIds);

        void successRemoved();
    }

}
