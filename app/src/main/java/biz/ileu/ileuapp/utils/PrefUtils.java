package biz.ileu.ileuapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * Created by Murager on 4/3/17.
 */

public class PrefUtils {

    public static final String TOKEN = "TOKEN";
    public static final String FIREBASE_TOKEN = "FIREBASE_TOKEN";
    public static final String BEARER = "Bearer ";
    public static final String LAST_COMPANY_ID = "LAST_COMPANY_ID";
    public static final String CURRENT_USER_ID = "CURRENT_USER_ID";

    private static SharedPreferences sharedPreferences;

    private static SharedPreferences.Editor editor;

    private Context context;

    private PrefUtils() {
    }

    public static void init(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
    }

    public static String getToken() {
        return BEARER + sharedPreferences.getString(TOKEN, "");
    }

    public static String getFirebaseToken() { return sharedPreferences.getString(FIREBASE_TOKEN, ""); }

    public static int getCurrentUserId() { return sharedPreferences.getInt(CURRENT_USER_ID, 0); }

    public static void setData(String key, String data) {
        editor.putString(key, data);
        editor.apply();
    }

    public static void setData(String key, int data) {
        editor.putInt(key, data);
        editor.apply();
    }

    public static String getString(String key) {
        return sharedPreferences.getString(key, "");
    }
    public static List<Integer> getIntegerList(String key) {
        Gson gson = new Gson();
        return gson.fromJson(sharedPreferences.getString(key, ""), new TypeToken<List<Integer>>(){}.getType());
    }

    public static int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public static void clearAllPrefsExceptFirebaseToken() {
        String firebaseToken = getFirebaseToken();
        editor.clear();
        editor.commit();
        setData(FIREBASE_TOKEN, firebaseToken);
    }

    public static void clearAllPrefs() {
        editor.clear();
        editor.commit();
    }

    public static void setData(String key, List<Integer> value) {
        Gson gson = new Gson();
        editor.putString(key, gson.toJson(value));
        editor.apply();
    }

    public static List<Integer> getList(String key) {
        Gson gson = new Gson();
        return gson.fromJson(sharedPreferences.getString(key, ""), new TypeToken<List<Integer>>(){}.getType());
    }
}
