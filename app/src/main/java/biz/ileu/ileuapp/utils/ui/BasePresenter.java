package biz.ileu.ileuapp.utils.ui;

/**
 * Created by Murager on 4/3/17.
 */

public interface BasePresenter {

    void destroy();
}
