package biz.ileu.ileuapp.utils.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import biz.ileu.ileuapp.utils.PrefUtils;

/**
 * Created by Daniq on 27.05.2017.
 */

public class IleuFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("FIREBASE_TOKEN", "Accepted_token: " + refreshedToken);

        PrefUtils.setData(PrefUtils.FIREBASE_TOKEN, refreshedToken);
    }
}
