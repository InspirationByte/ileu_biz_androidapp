package biz.ileu.ileuapp.utils.services;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.Date;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapp.utils.PrefUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Daniq on 18.04.2017.
 */

public class LocationTrackerService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static LocationTrackerService currentService;

    public static final long INTERVAL = 10 * 60 * 1000;

    private GoogleApiClient googleApiClient;

    private LocationRequest locationRequest;


    public Location lastLocation;

    @Override
    public void onCreate() {
        super.onCreate();
        currentService = this;

        Log.d("LOCATION", "STARTED");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (googleApiClient != null && googleApiClient.isConnected())
            googleApiClient.disconnect();
        Log.d("LOCATION", "STOPPED");
    }



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // calling super is required when extending from LocationBaseService
        super.onStartCommand(intent, flags, startId);
        startTracking();
        Log.d("LOCATION", "STARTED");


        // Return type is depends on your requirements
        return START_STICKY;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(INTERVAL);
        //locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //Отправка данных координат с установленным периодоом времени

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("LOCATION", "Permission not granted");
            stopSelf();
        }
        else {
            Log.d("LOCATION", "Permission granted");
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        //Если координаты не пусты то сохранить их в памяти телефона и отправить их на сервет
        if (location != null) {
            lastLocation = location;
            //Вывод данных в консоле для проверки
            Log.d("LOCATION", location.getLatitude() + "   " + location.getLongitude());

            //Если команда на отправку данных не отменена, то отправлят данные
            sendLocation(location.getLatitude(), location.getLongitude());
        }
    }

    private void startTracking() {
        Log.d("LOCATION", "startTracking");

        //Проверка на присутстве в телефоне Google Play сервиса
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {

            //Создание клиента для получения координат
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            if (!googleApiClient.isConnected() || !googleApiClient.isConnecting()) {//Проверка на соединение
                googleApiClient.connect(); //Соединение с сервисом
            }
        } else {
            Log.e("LOCATION", "unable to connect to google play services.");
        }
    }


    private void sendLocation(final double lat, final double lon) {
        Call<ResponseBody> call = RestClient.request().updateGeoCoords(PrefUtils.getToken(), lat, lon);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("LOCATION", "SENT: " + lat + " " + lon);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("LOCATION", "FAILED_TO_SEND");
            }
        });
    }

}
