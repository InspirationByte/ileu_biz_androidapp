package biz.ileu.ileuapp.utils.ui.attachedFile;

import android.content.Context;

import java.io.File;

/**
 * Created by Daniq on 13.04.2017.
 */

public class AttachedFile {

    public AttachedFile(String path) {
        this.path = path;
        file = new File(this.path);
    }

    private String path;

    private File file;

    private Context context;

    public String getPath() {
        return path;
    }

    public File getFile() {
        return file;
    }



    public String getExtention() {
        return path.substring(path.lastIndexOf(".") + 1, path.length()).toLowerCase();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AttachedFile)
            return this.path.equals(((AttachedFile) obj).getPath());

        return false;
    }
}
