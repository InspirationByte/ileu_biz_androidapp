package biz.ileu.ileuapp.utils.ui;

import android.view.View;

/**
 * Created by Daniq on 16.04.2017.
 */

public interface ParticipantsCheckClick {

    interface OnParticipantPositionCheckedUncheckedListener {
        void OnPositionChecked(View v, int position);

        void OnPositionUnchecked(View v, int position);
    }

}
