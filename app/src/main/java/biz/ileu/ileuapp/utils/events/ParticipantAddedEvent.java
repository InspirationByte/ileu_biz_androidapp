package biz.ileu.ileuapp.utils.events;

import biz.ileu.ileuapi.models.process.ParticipantViewModel;

/**
 * Created by Daniq on 07.06.2017.
 */

public class ParticipantAddedEvent {
    public ParticipantViewModel participantViewModel;
}
