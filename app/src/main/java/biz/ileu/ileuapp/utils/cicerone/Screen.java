package biz.ileu.ileuapp.utils.cicerone;

/**
 * Created by Daniq on 03.04.2017.
 */

public class Screen {

    public static final String SCREEN_PROCESSES_CONTAINER = "SCREEN_PROCESSES_CONTAINER";
    public static final String SCREEN_PROCESSES_LIST = "SCREEN_PROCESSES_LIST";
    public static final String SCREEN_NOTIFICATIONS_LIST = "SCREEN_NOTIFICATIONS_LIST";
    public static final String SCREEN_CHATS_LIST = "SCREEN_CHATS_LIST";
    public static final String SCREEN_NEWS_LIST = "SCREEN_NEWS_LIST";
    public static final String SCREEN_PROFILE = "SCREEN_PROFILE";
}
