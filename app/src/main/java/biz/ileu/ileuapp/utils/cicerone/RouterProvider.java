package biz.ileu.ileuapp.utils.cicerone;

import ru.terrakok.cicerone.Router;

public interface RouterProvider {
    Router getRouter();
}
