package biz.ileu.ileuapp.utils.events;

/**
 * Created by Daniq on 05.05.2017.
 */

public class CompanyCountersChangedEvent {
    public int companyId;

    public int unreadChatMessages;

    public int unreadProcessComments;
}

