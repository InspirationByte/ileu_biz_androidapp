package biz.ileu.ileuapp.utils.ui.attachedFile;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import biz.ileu.ileuapp.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Daniq on 13.04.2017.
 */

public class AttachedFileViewHolder implements View.OnClickListener{

    @BindView(R.id.ivAttachedFile)
    public ImageView ivAttachedFile;

    @BindView(R.id.tvAttachedFile)
    public TextView tvAttachedFile;

    private AttachedFile attachedFile;

    private Context context;

    private AttachedFileViewClickListener listener;

    public AttachedFileViewHolder(View view, AttachedFile attachedFile, AttachedFileViewClickListener listener) {
        ButterKnife.bind(this, view);
        context = view.getContext();
        this.attachedFile = attachedFile;
        this.listener = listener;
        view.setOnClickListener(this);

        setViews();
    }

    private void setViews() {
        if (attachedFile.getExtention().equals("pdf"))
            ivAttachedFile.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_pdf));
        else if (attachedFile.getExtention().equals("avi") ||
                attachedFile.getExtention().equals("mp4") ||
                attachedFile.getExtention().equals("3gp") ||
                attachedFile.getExtention().equals("wmv"))
            ivAttachedFile.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_video));
        else
            ivAttachedFile.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_file));


        String fileName = attachedFile.getFile().getName();
        tvAttachedFile.setText(fileName.length() > 12 ? fileName.substring(0, 11) + "..." : fileName);
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onAttachedFileClick(attachedFile, v);
        }
    }
}
