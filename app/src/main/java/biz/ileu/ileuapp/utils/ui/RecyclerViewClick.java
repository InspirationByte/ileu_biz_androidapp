package biz.ileu.ileuapp.utils.ui;

import android.view.View;

/**
 * Created by Murager on 4/4/17.
 */

public interface RecyclerViewClick {

    interface OnItemClickListener {

        void onItemClick(View view, int position);
    }

}
