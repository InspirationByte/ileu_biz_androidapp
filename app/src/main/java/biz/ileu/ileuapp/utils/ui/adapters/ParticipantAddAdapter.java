package biz.ileu.ileuapp.utils.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import biz.ileu.ileuapi.models.process.EmployeeViewModel;
import biz.ileu.ileuapi.models.process.ParticipantViewModel;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.utils.ui.ParticipantsCheckClick;
import biz.ileu.ileuapp.utils.Constants;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Daniq on 16.04.2017.
 */

public class ParticipantAddAdapter extends RecyclerView.Adapter<ParticipantAddAdapter.ParticipantViewHolder> {

    private List<Object> objectList;

    private ParticipantsCheckClick.OnParticipantPositionCheckedUncheckedListener listener;

    private List<Integer> selectedIndexes;

    public ParticipantAddAdapter(List<Object> objectList,
                                 ParticipantsCheckClick.OnParticipantPositionCheckedUncheckedListener listener) {
        this.objectList = objectList;
        this.listener = listener;

        selectedIndexes = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }

    @Override
    public ParticipantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_process_create_participants_add, parent, false);
        return new ParticipantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ParticipantViewHolder holder, int position) {
        Object object = objectList.get(position);

        if (object instanceof EmployeeViewModel) {
            EmployeeViewModel employeeViewModel = (EmployeeViewModel) object;

            holder.tvRowParticipantAddSubtitle.setText(employeeViewModel.getEmail());
            holder.tvRowParticipantAddTitle.setText(employeeViewModel.getNameSurname());

            Picasso.with(holder.context)
                    .load(Constants.IMAGE_BASE_URL + employeeViewModel.getProfilePicture())
                    .into(holder.civRowParticipantsAdd);
        } else if (object instanceof ParticipantViewModel) {
            ParticipantViewModel participantViewModel = (ParticipantViewModel) object;

            holder.tvRowParticipantAddTitle.setText(participantViewModel.getDisplayName());
            holder.tvRowParticipantAddSubtitle.setText("");

            Picasso.with(holder.context)
                    .load(Constants.BASE_URL + participantViewModel.getProfilePicture())
                    .into(holder.civRowParticipantsAdd);
        }

        if (selectedIndexes.contains(position))
            holder.cbRowParticipantAdd.setChecked(true);
        else
            holder.cbRowParticipantAdd.setChecked(false);
    }



    public class ParticipantViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;

        @BindView(R.id.civRowParticipantsAdd)
        CircleImageView civRowParticipantsAdd;

        @BindView(R.id.tvRowParticipantAddTitle)
        TextView tvRowParticipantAddTitle;

        @BindView(R.id.tvRowParticipantAddSubtitle)
        TextView tvRowParticipantAddSubtitle;

        @BindView(R.id.cbRowParticipantAdd)
        CheckBox cbRowParticipantAdd;

        @BindView(R.id.llRowParticipantAdd)
        LinearLayout llRowParticipantAdd;



        public ParticipantViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            ButterKnife.bind(this, itemView);
            llRowParticipantAdd.setOnClickListener(this);
            cbRowParticipantAdd.setClickable(false);
        }

        @Override
        public void onClick(View v) {
            cbRowParticipantAdd.setChecked(!cbRowParticipantAdd.isChecked());
            changedCheckedState(v, cbRowParticipantAdd.isChecked());
        }

        private void changedCheckedState(View v, boolean isChecked) {
            if (listener == null)
                return;

            if (isChecked) {
                selectedIndexes.add(getAdapterPosition());
                listener.OnPositionChecked(v, getAdapterPosition());
            }
            else {
                selectedIndexes.remove(Arrays.asList(getAdapterPosition()));
                listener.OnPositionUnchecked(v, getAdapterPosition());
            }
        }
    }

}
