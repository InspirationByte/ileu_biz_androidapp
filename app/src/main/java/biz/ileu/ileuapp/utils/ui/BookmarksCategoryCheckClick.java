package biz.ileu.ileuapp.utils.ui;

import android.view.View;

/**
 * Created by macair on 09.08.17.
 */

public class BookmarksCategoryCheckClick {
    public interface OnBookmarkCategoryPositionCheckedUncheckedListener {
        void OnPositionChecked(View v, int position);

        void OnPositionUnchecked(View v, int position);
    }
}
