package biz.ileu.ileuapp.utils.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import biz.ileu.ileuapp.utils.services.LocationTrackerService;

/**
 * Created by Daniq on 30.05.2017.
 */

public class IleuBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("ILEU_RECEIVER", "RECEIVED");
        context.startService(new Intent(context, LocationTrackerService.class));
    }
}
