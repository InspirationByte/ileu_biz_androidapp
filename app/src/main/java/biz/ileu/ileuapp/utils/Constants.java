package biz.ileu.ileuapp.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by Daniq on 06.04.2017.
 */

public class Constants {
    public static final String BASE_URL = "http://ileu.biz";
    public static final String IMAGE_BASE_URL = BASE_URL + "/Content/images/usermedia/";

    public static final String FILES_PATH = "files";
    public static final String AUDIOS_PATH = "audio";

    public static final String GRANT_TYPE = "password";

    public static final String FRAGMENT_TAG = "FRAGMENT_TAG";

    public static final int DEFAULT_PAGE_SIZE = 15;
    public static final int MIDDLE_PAGE_SIZE = 30;
    public static final int DEFAULT_DELAY = 1000;
    public static final int DEFAULT_PAGE_MAX_SIZE = 99999;

    public static final String FILTER_PNAME = "FILTER_PNAME_";
    public static final String FILTER_DESC = "FILTER_DESC_";
    public static final String FILTER_EMP_IDS = "FILTER_EMP_IDS_";
    public static final String FILTER_TEMP_IDS = "FILTER_TEMP_IDS_";
    public static final String FILTER_STRING = "FILTER_STRING_";
    public static final String TAB_INDEX = "TAB_INDEX";
    // Process types
    public static final String PROCESS_FILTER = "PROCESS_FILTER";
    public static final String PROCESSES_FILTER_MY = "(Status~eq~0~or~Status~eq~2~or~Status~eq~3)";
    public static final String PROCESSES_FILTER_OUTDATED = "(Status~eq~0~or~Status~eq~2~or~Status~eq~3)~and~(Hidden~eq~0~or~UnreadCommentsCount~gt~0)~and~IsOverDue~eq~2";
    public static final String PROCESSES_FILTER_HISTORY = "(Hidden~eq~0~or~UnreadCommentsCount~gt~0)~and~(Status~eq~4~or~Status~eq~5)";
    public static final String PROCESSES_FILTER_HASFILE = "(FilesCount~neq~0)~and~(Status~eq~0~or~Status~eq~2~or~Status~eq~3)";
    public static final String PROCESSES_FILTER_HASDOCUMENT = "(DocumentState~eq~1~or~DocumentState~eq~2~or~DocumentState~eq~3)~and~(Status~eq~0~or~Status~eq~2~or~Status~eq~3)";
    public static final String PROCESSES_FILTER_HASTABLE = "(HasTable~eq~true)~and~(Status~eq~0~or~Status~eq~2~or~Status~eq~3)";
    public static final String PROCESSES_FILTER_HASBOOKMARK = "(HasBookmark~eq~true)~and~(Status~eq~0~or~Status~eq~2~or~Status~eq~3)";

    // Process sortings
    public static final String PROCESS_SORTING = "PROCESS_SORTING";
    public static final String PROCESS_SORTING_MY = "LastUpdateTime-desc";
    public static final String PROCESS_SORTING_OUTDATED = "LastUpdateTime-desc";
    public static final String PROCESS_SORTING_HISTORY = "LastUpdateTime-desc";

    // Entity Constants
    public static final String ENTITY_COMPANY = "ENTITY_COMPANY";
    public static final String ENTITY_NEWS = "ENTITY_NEWS";
    public static final String ENTITY_PROCESS = "ENTITY_PROCESS";
    public static final String ENTITY_PROCESS_SELECTED_USERS = "ENTITY_PROCESS_SELECTED_USERS";
    public static final String ENTITY_PROCESS_TEMPLATE = "ENTITY_PROCESS_TEMPLATE";
    public static final String ENTITY_EMPLOYEE = "ENTITY_EMPLOYEE";
    public static final String ENTITY_EMPLOYEES = "ENTITY_EMPLOYEES";
    public static final String ENTITY_CHAT = "ENTITY_CHAT";
    public static final String ENTITY_PROFILE_INFO = "ENTITY_PROFILE_INFO";
    public static final String ENTITY_CURRENT_TAB = "ENTITY_CURRENT_TAB";

    // Intent extras
    public static final String PROCESS_IS_NEW = "PROCESS_IS_NEW";

    // Intent Status CODES
    public static final int PROCESS_CREATE_SET_RESPONSIBLE_CODE = 1;
    public static final int PROCESS_CREATE_SET_EXECUTOR_CODE = 2;
    public static final int PROCESS_CREATE_ADD_PARTICIPANTS_CODE = 3;
    public static final int PROCESS_CREATE_SET_DATETIME = 4;
    public static final int PROFILE_CHANGE_CODE = 5;
    public static final String ENTITY_PROCESS_REMOVED_USERS = "ENTITY_PROCESS_REMOVED_USERS";
    public static final String CHAT = "ПЕРЕПИСКА";
    public static final String DOCUMENTS = "ДОКУМЕНТЫ";
    public static final String FILES = "ФАЙЛЫ";
    public static final String TABLES = "ТАБЛИЦЫ";
    public static final String ENTITY_BOOKMARK_CATEGORY = "BOOKMARK_CATEGORY";
    public static final String FILTER_DOC_STATES = "FILTER_DOC_STATES";
    public static final String ENTITY_POSITION = "ENTITY_POSITION";
    public static final String ENTITY_PROCESS_COMMENTS = "ENTITY_PROCESS_COMMENTS";
    public static int currentUserId;

    public static String getStatusByInt(int i) {
        if (i == 0)
            return "Запущен";
        else if (i == 2)
            return "Доработка исполнителя";
        else if (i == 3)
            return "Выполнено";
        else if (i == 4)
            return "Завершено положительно";
        else if (i == 5)
            return "Завершено отрицательно";
        else if (i == 6)
            return "В корзине";

        return "НЕ ИСПОЛЬЗУЕТСЯ, ДЛЯ СОВМЕСТИМОСТИ";
    }

    public static Date getDateFromString(String datetime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            return format.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFormattedDateTime(String datetime) {
        Date date = getDateFromString(datetime);

        if (date == null)
            date = new Date();

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");

        return format.format(date);
    }

    public static String getCurrentTimezoneOffset() {

        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        int offsetInMillis = tz.getOffset(cal.getTimeInMillis());

        String offset = String.format("%02d:%02d", Math.abs(offsetInMillis / 3600000), Math.abs((offsetInMillis / 60000) % 60));
        offset = (offsetInMillis >= 0 ? "+" : "-") + offset;

        return offset;
    }


    public static Map<String, List<String>> getQueryParams(String url) {
        try {
            Map<String, List<String>> params = new HashMap<String, List<String>>();
            String[] urlParts = url.split("\\?");
            if (urlParts.length > 1) {
                String query = urlParts[1];
                for (String param : query.split("&")) {
                    String[] pair = param.split("=");
                    String key = URLDecoder.decode(pair[0], "UTF-8");
                    String value = "";
                    if (pair.length > 1) {
                        value = URLDecoder.decode(pair[1], "UTF-8");
                    }

                    List<String> values = params.get(key);
                    if (values == null) {
                        values = new ArrayList<String>();
                        params.put(key, values);
                    }
                    values.add(value);
                }
            }

            return params;
        } catch (UnsupportedEncodingException ex) {
            throw new AssertionError(ex);
        }
    }


}
