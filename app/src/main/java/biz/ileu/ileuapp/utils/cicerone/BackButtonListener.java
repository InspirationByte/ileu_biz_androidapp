package biz.ileu.ileuapp.utils.cicerone;


public interface BackButtonListener {
    boolean onBackPressed();
}
