package biz.ileu.ileuapp.utils.ui;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;

import biz.ileu.ileuapp.R;

/**
 * Created by Daniq on 03.04.2017.
 */

public abstract class BaseFragment extends Fragment{

    protected Toolbar toolbar;

    public abstract void bindViews();
    public abstract void setData();
    public abstract String getScreenName();


    public void initBaseToolbar(View v,  boolean isArrowVisible) {
        toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        ((BaseActivity)getActivity()).setSupportActionBar(toolbar);
        getActionBar().setHomeButtonEnabled(isArrowVisible);
        getActionBar().setDisplayHomeAsUpEnabled(isArrowVisible);
    }

    public ActionBar getActionBar() {
        return ((BaseActivity) getActivity()).getSupportActionBar();
    }


}
