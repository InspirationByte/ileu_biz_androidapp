package biz.ileu.ileuapp.utils.ui;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import biz.ileu.ileuapp.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Daniq on 03.04.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private Toolbar toolbar;

    public abstract void bindViews();

    public abstract void setData();


    public Toolbar getToolbar() {
        return toolbar;
    }

    public void initBaseToolbar(boolean isArrowVisible) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(isArrowVisible);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isArrowVisible);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
