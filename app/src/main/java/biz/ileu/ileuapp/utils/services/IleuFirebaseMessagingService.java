package biz.ileu.ileuapp.utils.services;

import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.parceler.Parcels;

import biz.ileu.ileuapi.models.chat.ChatsViewModel;
import biz.ileu.ileuapi.models.chat.MessageFromDto;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModel;
import biz.ileu.ileuapp.IleuApp;
import biz.ileu.ileuapp.R;
import biz.ileu.ileuapp.ui.chatsList.chatMessages.ChatMessagesActivity;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;

/**
 * Created by Daniq on 27.05.2017.
 */

public class IleuFirebaseMessagingService extends FirebaseMessagingService {

    MessageFromDto messageFromDto;
    CompanyWorkInfoListViewModel company;
    ChatsViewModel chat;

    public static IleuFirebaseMessagingService instance;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (PrefUtils.getString(PrefUtils.TOKEN).isEmpty())
            return;

        instance = this;
        Gson gson = new Gson();
        if (remoteMessage.getData().size() > 0) {
            try {
                Log.d("FIREBASE_DATA", remoteMessage.getData() + "");
                String dataStr = remoteMessage.getData().get("data");

                JsonParser parser = new JsonParser();
                JsonObject dataJson = parser.parse(dataStr).getAsJsonObject();

                messageFromDto = gson.fromJson(dataJson.get("message"), MessageFromDto.class);
                company = gson.fromJson(dataJson.get("company"), CompanyWorkInfoListViewModel.class);
                chat = gson.fromJson(dataJson.get("chat"), ChatsViewModel.class);

            IleuApp mMyApp = (IleuApp) this.getApplicationContext();
            if (mMyApp.currentChatId != 0 && chat.getId() == mMyApp.currentChatId)
                return;
            if (messageFromDto.getFromUserId() != PrefUtils.getCurrentUserId()) {
                sendNotification(messageFromDto.getDisplayName(), company.getName() + ": " + messageFromDto.getText());
            }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sendNotification(final String title, final String message) {
        if (messageFromDto == null || chat == null || company == null)
            return;

        Intent serviceIntent = new Intent(this, SignalRService.class);
        startService(serviceIntent);


        Intent intentNotification;
        intentNotification = new Intent(IleuFirebaseMessagingService.this, ChatMessagesActivity.class);

        intentNotification.putExtra(Constants.ENTITY_COMPANY, Parcels.wrap(company));
        intentNotification.putExtra(Constants.ENTITY_CHAT, Parcels.wrap(chat));

        int requestCode = 0;

        PendingIntent pendingIntent = PendingIntent.getActivity(IleuFirebaseMessagingService.this, requestCode,
                intentNotification, PendingIntent.FLAG_ONE_SHOT);


        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(IleuFirebaseMessagingService.this)
                .setSmallIcon(R.mipmap.ic_app_icon_circle)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(sound)
                .setContentIntent(pendingIntent);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(IleuFirebaseMessagingService.this);
        notificationManager.notify(0, noBuilder.build()); //0 = ID of notification


    }

    public void clearNotifications(int chatId) {
        if (chat != null && chat.getId() == chatId) {
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(IleuFirebaseMessagingService.this);
            notificationManager.cancelAll();
        }
    }
}
