package biz.ileu.ileuapp.utils.ui.attachedFile;

import android.view.View;

/**
 * Created by Daniq on 13.04.2017.
 */

public interface AttachedFileViewClickListener {
    void onAttachedFileClick(AttachedFile attachedFile, View v);
}
