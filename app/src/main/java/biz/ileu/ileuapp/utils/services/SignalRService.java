package biz.ileu.ileuapp.utils.services;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.IntDef;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.ExecutionException;

import biz.ileu.ileuapi.models.chat.CreateChatViewModel;
import biz.ileu.ileuapi.models.chat.MessageFromDto;
import biz.ileu.ileuapi.models.chat.UserChatStatusDto;
import biz.ileu.ileuapi.models.process.ParticipantChangeViewModel;
import biz.ileu.ileuapi.models.process.ParticipantViewModel;
import biz.ileu.ileuapi.models.process.ProcessCommentViewModel;
import biz.ileu.ileuapi.models.process.ProcessFileViewModel;
import biz.ileu.ileuapp.ui.menu.MenuActivityPresenter;
import biz.ileu.ileuapp.utils.Constants;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.events.FileRemovedEvent;
import biz.ileu.ileuapp.utils.events.ParticipantAddedEvent;
import biz.ileu.ileuapp.utils.events.ParticipantRemovedEvent;
import biz.ileu.ileuapp.utils.events.ProcessStatusChangedEvent;
import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.Credentials;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.LogLevel;
import microsoft.aspnet.signalr.client.Logger;
import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.http.Request;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport;

public class SignalRService extends Service {

    private final IBinder mBinder = new LocalBinder();
    public class LocalBinder extends Binder {
        public SignalRService getService() {
            // Return this instance of LocalService so clients can call public methods
            return SignalRService.this;
        }
    }


    private HubConnection connection;

    private HubProxy chatProxy;
    private HubProxy processProxy;
    private int lastCompanyId = 0;
    private int lastProcessId = 0;

    private boolean reconnect = true;

    private Logger logger = new Logger() {
        @Override
        public void log(String s, LogLevel logLevel) {
            Log.d("SignalR", s);
        }
    };


    public SignalRService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("SignalR", "Service Started");
        startSignalR();
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private void startSignalR() {
        if (connection != null && connection.getState().name().equals(ConnectionState.Connected.name()))
            return;

        Platform.loadPlatformComponent(new AndroidPlatformComponent());
        connection = new HubConnection(Constants.BASE_URL + "/", "", true, logger);
        chatProxy = connection.createHubProxy("ChatMessageHub");
        processProxy = connection.createHubProxy("ViewProcessHub");

        Credentials credentials = new Credentials() {
            @Override
            public void prepareRequest(Request request) {
                request.addHeader("Authorization", PrefUtils.getToken());
            }
        };
        connection.setCredentials(credentials);

        chatProxy.subscribe(new Object() {

            public void onChatMessage(MessageFromDto data) {
                Log.d("SignalR_Message", data.getText()+ " ");
                EventBus.getDefault().post(data);
            }

            public void chatContactStatusUpdate(UserChatStatusDto data) {
                Log.d("SignalR_STATUS", data.getId() + " " + data.getStatusText());
            }

            public void chatCreatedEvent(CreateChatViewModel data) {
                Log.d("SignalR_CREATED", "Chat created " + data.getChatId());
            }

            public void chatContactsOutDated() {
                Log.d("SignalR", "Contacts Outdated");
            }
        });

        processProxy.subscribe(new Object() {
            public void onCommentPosted(ProcessCommentViewModel data) {
                Log.d("SignalR_Process", "MSG: " + data.getText());
                EventBus.getDefault().post(data);
            }

            public void onFileAdded(ProcessFileViewModel data) {
                Log.d("SignalR_Process", "File: " + data.getFileName());
                EventBus.getDefault().post(data);
            }

            public void onFileRemoved(int data) {
                Log.d("SignalR_Process", "File_Remove: " + data);
                FileRemovedEvent event = new FileRemovedEvent();
                event.fileId = data;
                EventBus.getDefault().post(event);
            }

            public void onStatusChanged(int status) {
                Log.d("SignalR_Process", "Status: " + status);
                ProcessStatusChangedEvent event = new ProcessStatusChangedEvent();
                event.status = status;
                EventBus.getDefault().post(event);
            }

            public void onParticipantAdded(ParticipantViewModel data) {
                Log.d("SignalR_Process", "Participant_add: " + data.getId());
                ParticipantAddedEvent event = new ParticipantAddedEvent();
                event.participantViewModel = data;
                EventBus.getDefault().post(event);
            }

            public void onParticipantRemoved(int id) {
                Log.d("SignalR_Process", "Participant_remove: " + id);
                ParticipantRemovedEvent event = new ParticipantRemovedEvent();
                event.participantId = id;
                EventBus.getDefault().post(event);
            }

            public void onEmployeesChanged(ParticipantChangeViewModel data) {
                Log.d("SignalR_Process", "Participant_change: " + data.toString());
                EventBus.getDefault().post(data);
            }
        });

        connection.connected(new Runnable() {
            @Override
            public void run() {
                Log.d("SignalR", "CONNECTED");
                if (lastCompanyId != 0)
                    (new ChatConnectionTask()).execute(lastCompanyId);
                if (lastProcessId != 0)
                    (new ProcessConnectionTask()).execute(lastProcessId);
            }
        });

        connection.start(new ServerSentEventsTransport(logger))
                .done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.d("SignalR", "START");
                    }
                });

        connection.error(new ErrorCallback() {
            @Override
            public void onError(Throwable throwable) {
                Log.d("SignalR_ERROR", "ERROR: " + throwable.getMessage());
            }
        });

        connection.closed(new Runnable() {
            @Override
            public void run() {
                Log.d("SignalR", "DISCONNECTED");
                if (!reconnect)
                    return;


                final Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        ReconnectTask task = new ReconnectTask();
                        task.execute();
                    }
                }, 3000);

            }
        });
    }

    public void connectToCompany(int companyId) {
        if (lastCompanyId != companyId) {
            ChatConnectionTask task = new ChatConnectionTask();
            task.execute(companyId);
        }
    }

    public void connectToProcess(int processId) {
        if (lastProcessId != processId) {
            ProcessConnectionTask task = new ProcessConnectionTask();
            task.execute(processId);
        }
    }

    public void disconnectFromProcess(int processId) {
        ProcessDisconnectionTask task = new ProcessDisconnectionTask();
        task.execute(processId);
    }

    public void destroyConnection() {
        if (connection != null && connection.getState() == ConnectionState.Connected) {
            reconnect = false;
            connection.disconnect();
            connection.stop();
        }
    }

    class ProcessConnectionTask extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... params) {
            int processId = params[0];
            lastProcessId = processId;

            if (connection != null && connection.getState() == ConnectionState.Connected) {
                try {
                    processProxy.invoke("joinGroup", processId).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    class ProcessDisconnectionTask extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... params) {
            int processId = params[0];
            lastProcessId = 0;

            if (connection != null && connection.getState() == ConnectionState.Connected) {
                try {
                    processProxy.invoke("leaveGroup", processId).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }


    class ChatConnectionTask extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... params) {
            int companyId = params[0];
            lastCompanyId = companyId;
            if (connection != null && connection.getState() == ConnectionState.Connected) {
                try {
                    chatProxy.invoke("joinGroup", "com" + companyId).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    class ReconnectTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Log.d("SignalR", "Reconnecting");

            startSignalR();

            return null;
        }
    }
}
