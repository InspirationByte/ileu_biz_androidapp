package biz.ileu.ileuapp;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import net.danlew.android.joda.JodaTimeAndroid;

import biz.ileu.ileuapi.RestClient;
import biz.ileu.ileuapp.ui.login.LoginActivity;
import biz.ileu.ileuapp.utils.PrefUtils;
import biz.ileu.ileuapp.utils.cicerone.LocalCiceroneHolder;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Daniq on 03.04.2017.
 */

public class IleuApp extends Application {

    public static IleuApp INSTANCE;

    private Cicerone<Router> cicerone;

    public int currentChatId = 0;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        PrefUtils.init(this);
        INSTANCE = this;
        cicerone = Cicerone.create();
        Timber.plant(new Timber.DebugTree());
        JodaTimeAndroid.init(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath).build());

        RestClient.initRetrofit(this, LoginActivity.class);
    }


    public NavigatorHolder getNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }

    public Router getRouter() {
        return cicerone.getRouter();
    }

    public LocalCiceroneHolder getCiceroneHolder() {
        return new LocalCiceroneHolder();
    }

    public void updateRetrofit() {
        RestClient.initRetrofit(this, LoginActivity.class);
    }

}
