package biz.ileu.ileuapi;

import com.google.gson.JsonObject;

import java.util.List;

import biz.ileu.ileuapi.models.TokenData;
import biz.ileu.ileuapi.models.bookmarks.BookmarkCategoryViewModel;
import biz.ileu.ileuapi.models.bookmarks.BookmarkViewModel;
import biz.ileu.ileuapi.models.bookmarks.BookmarksCategoriesList;
import biz.ileu.ileuapi.models.chat.ChatList;
import biz.ileu.ileuapi.models.chat.ChatMessageList;
import biz.ileu.ileuapi.models.chat.ChatsViewModel;
import biz.ileu.ileuapi.models.chat.GroupMessageViewModel;
import biz.ileu.ileuapi.models.chat.MessageFromDto;
import biz.ileu.ileuapi.models.chat.PostMessageViewModel;
import biz.ileu.ileuapi.models.company.CompanyList;
import biz.ileu.ileuapi.models.company.CompanyReviewStatsViewModel;
import biz.ileu.ileuapi.models.company.JoinCompanyRequestList;
import biz.ileu.ileuapi.models.invitation.CompanyInvitationDTO;
import biz.ileu.ileuapi.models.invitation.CompanyInvitationResultDTO;
import biz.ileu.ileuapi.models.news.FileInfoViewModel;
import biz.ileu.ileuapi.models.news.NewsCommentList;
import biz.ileu.ileuapi.models.news.NewsCommentViewModel;
import biz.ileu.ileuapi.models.news.NewsEntryViewModel;
import biz.ileu.ileuapi.models.news.NewsList;
import biz.ileu.ileuapi.models.news.PostNewsEntryViewModel;
import biz.ileu.ileuapi.models.process.CommentList;
import biz.ileu.ileuapi.models.process.CommentReadStateUpdatedViewModel;
import biz.ileu.ileuapi.models.process.CommentReadStateViewModel;
import biz.ileu.ileuapi.models.process.CommentTypeUpdatedViewModel;
import biz.ileu.ileuapi.models.process.CreateProcessViewModel;
import biz.ileu.ileuapi.models.process.CreateRelatedProcessViewModel;
import biz.ileu.ileuapi.models.process.DocumentAgreedByViewModel;
import biz.ileu.ileuapi.models.process.DocumentViewModel;
import biz.ileu.ileuapi.models.process.EmployeeList;
import biz.ileu.ileuapi.models.process.EventEndDateLogViewModel;
import biz.ileu.ileuapi.models.process.ParticipantChangeViewModel;
import biz.ileu.ileuapi.models.process.ParticipantViewModel;
import biz.ileu.ileuapi.models.process.PostCommentViewModel;
import biz.ileu.ileuapi.models.process.Process;
import biz.ileu.ileuapi.models.process.ProcessCommentViewModel;
import biz.ileu.ileuapi.models.process.ProcessCreateStatus;
import biz.ileu.ileuapi.models.process.ProcessCreateStatusOfRelatedProcessViewModel;
import biz.ileu.ileuapi.models.process.ProcessFileList;
import biz.ileu.ileuapi.models.process.ProcessFileViewModel;
import biz.ileu.ileuapi.models.process.ProcessList;
import biz.ileu.ileuapi.models.process.ProcessNotificationList;
import biz.ileu.ileuapi.models.process.ProcessUserStatsViewModel;
import biz.ileu.ileuapi.models.process.RelatedProcessViewModel;
import biz.ileu.ileuapi.models.process.TargetProcessViewModel;
import biz.ileu.ileuapi.models.process.UsersViewModel;
import biz.ileu.ileuapi.models.processtypes.ProcessNameGroupedViewModel;
import biz.ileu.ileuapi.models.processtypes.ProcessTemplateViewModel;
import biz.ileu.ileuapi.models.processtypes.ProcessTypeViewModel;
import biz.ileu.ileuapi.models.profile.ProfileAvatarUpdateViewModel;
import biz.ileu.ileuapi.models.profile.ProfileInfo;
import biz.ileu.ileuapi.models.tables.DSResultWithTypeOfProcessRowDTO;
import biz.ileu.ileuapi.models.tables.ProcessColumnValueDTO;
import biz.ileu.ileuapi.models.tables.ProcessRowDTO;
import biz.ileu.ileuapi.models.uploads.FileInfoList;
import biz.ileu.ileuapi.models.usercontext.CompanyWorkInfoListViewModelList;
import biz.ileu.ileuapi.models.usercontext.ContextInfoDTO;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;


/**
 * Created by Daniq on 22.02.2017.
 *
 *
 * Where token is asked. You should pass token String like this: "Bearer YOUR_GIANT_TOKEN"
 */
public interface IleuService {

    @FormUrlEncoded
    @POST("api/Account/Register")
    Call<ResponseBody> register(@Field("Email") String email,
                                @Field("Password") String password,
                                @Field("ConfirmPassword") String confirmPassword);

    @FormUrlEncoded
    @POST("Token")
    Observable<TokenData> login(@Field("grant_type") String grantType,
                                @Field("username") String email,
                                @Field("password") String password);

    @POST("api/Account/Logout")
    Observable<ResponseBody> logout(@Header("Authorization") String token);



    @FormUrlEncoded
    @POST("api/UserContext/UpdateDeviceToken")
    Observable<ResponseBody> updateFirebaseToken(@Header("Authorization") String token,
                                                 @Field("DeviceToken") String firebaseToken);

    @FormUrlEncoded
    @POST("api/UserContext/UpdateGeoCoords")
    Call<ResponseBody> updateGeoCoords(@Header("Authorization") String token,
                                             @Field("Latitude") double latitude,
                                             @Field("Longitude") double longitude);

    // Bookmarks

    @POST("api/Bookmarks/GetCategories")
    @FormUrlEncoded
    Observable<BookmarksCategoriesList> getBookmarkCategories(@Header("Authorization") String token,
                                                              @Query("companyId") int companyId,
                                                              @Field("Page") int page,
                                                              @Field("PageSize") int pageSize);

    @POST("api/Bookmarks/GetBookmarkCategoriesFor")
    Observable<List<Integer>> getBookmarkCategoriesFor(@Header("Authorization") String token,
                                                              @Query("companyId") int companyId,
                                                              @Query("processId") int processId
                                                             );
    @POST("api/Bookmarks/AddBookmark")
    Observable<BookmarkViewModel> addBookmark(@Header("Authorization") String token,
                                              @Query("companyId") int companyId,
                                              @Query("categoryId") int categoryId,
                                              @Query("processId") int processId);
    @POST("api/Bookmarks/RemoveBookmark")
    Observable<BookmarkViewModel> removeBookmark(@Header("Authorization") String token,
                                              @Query("companyId") int companyId,
                                              @Query("categoryId") int categoryId,
                                              @Query("processId") int processId);

    @POST("api/Bookmarks/CreateCategory")
    Observable<BookmarkCategoryViewModel> createBookmarkCategory(@Header("Authorization") String token,
                                                                 @Query("companyId") int companyId,
                                                                 @Query("title") String title);




    // Bookmarks end.

    // Profile
    @GET("api/Profile/GetProfileInfo")
    Observable<ProfileInfo> getProfileInfo(@Header("Authorization") String token);

    @POST("api/Profile/ChangeStatus")
    Observable<ResponseBody> changeStatus(@Header("Authorization") String token,
                                          @Query("companyId") int companyId,
                                          @Query("statusType") int statusType, @Query("text") String text);

    @FormUrlEncoded
    @POST("api/Profile/SaveProfileInfo")
    Observable<ProfileInfo> saveProfileInfo(@Header("Authorization") String token,
                                            @Field("Firstname") String firstName,
                                            @Field("Surname") String surname,
                                            @Field("PhoneNumber") String phoneNumber);

    @FormUrlEncoded
    @POST("api/Profile/ChangePassword")
    Observable<JsonObject> changePassword(@Header("Authorization") String token,
                                          @Field("OldPassword") String oldPassword,
                                          @Field("NewPassword") String newPassword,
                                          @Field("ConfirmPassword") String confirmPassword);
    @POST("api/Profile/SaveAvatar")
    @Multipart
    Observable<ProfileAvatarUpdateViewModel> saveAvatar(@Header("Authorization") String token, @Part MultipartBody.Part body);

    // Profile end.

    // Uploads
    @FormUrlEncoded
    @POST("api/Uploads/GetUploadsList")
    Observable<FileInfoList> getUploadsList(@Header("Authorization") String token,
                                            @Query("checkProcessId") int checkProcessId,
                                            @Query("filesOfProcessId") int filesOfProcessId,
                                            @Field("Page") int page,
                                            @Field("PageSize") int pageSize);

    @POST("api/Uploads/RemoveUpload")
    Observable<ResponseBody> removeUpload(@Header("Authorization") String token,
                                          @Query("uploadId") int uploadId);

    @POST("api/Uploads/DoUpload")
    @Multipart
    Observable<List<FileInfoViewModel>> doUpload(@Header("Authorization") String token, @Query("companyId") int companyId, @Part List<MultipartBody.Part> files);

    @GET("api/Uploads/DoDownload")
    Observable<ResponseBody> doDownload(@Header("Authorization") String token, @Query("fileId") int fileId);
    // Uploads end


    // Process
    @FormUrlEncoded
    @POST("api/Process/GetProcessList")
    Observable<ProcessList> getProcessesList(@Header("Authorization") String token,
                                             @Query("companyId") int companyId,
                                             @Field("Filter") String filter,
                                             @Field("Sort") String sort,
                                             @Field("Page") int page,
                                             @Field("PageSize") int pageSize,
                                             @Field("EmpIds") List<Integer> empId,
                                             @Field("ProcessTemplateIds") List<Integer>  processTemplateIds
                                             );

    @FormUrlEncoded
    @POST("api/Bookmarks/GetProcessList")
    Observable<ProcessList> getBookmarkProcessesList(@Header("Authorization") String token,
                                             @Query("companyId") int companyId,
                                                     @Query("categoryId") int categoryId,
                                             @Field("Filter") String filter,
                                             @Field("Sort") String sort,
                                             @Field("Page") int page,
                                             @Field("PageSize") int pageSize);

    @FormUrlEncoded
    @POST("api/Process/GetProcessNotifications")
    Observable<ProcessNotificationList> getProcessNotifications(@Header("Authorization") String token,
                                                                @Query("companyId") int companyId,
                                                                @Field("Page") int page,
                                                                @Field("PageSize") int pageSize);

    @GET("api/Process/GetProcessData")
    Observable<Process> getProcessData(@Header("Authorization") String token,
                                       @Query("companyId") int companyId,
                                       @Query("processId") int processId);


    @GET("api/Process/GetProcessStats")
    Observable<ProcessUserStatsViewModel> getProcessStats(@Header("Authorization") String token,
                                                          @Query("companyId") int companyId);

    @POST("api/Process/CreateProcess")
    Observable<ProcessCreateStatus> createProcess(@Header("Authorization") String token,
                                                  @Query("companyId") int companyId,
                                                  @Body CreateProcessViewModel createProcessViewModel);

    @POST("api/Process/CreateRelatedProcess")
    Observable<ProcessCreateStatusOfRelatedProcessViewModel> createRelatedProcess(@Header("Authorization") String token,
                                                                                  @Body CreateRelatedProcessViewModel createRelatedProcessViewModel);

    @POST("api/Process/CreateTargetProcess")
    Observable<ProcessCreateStatusOfRelatedProcessViewModel> createTargetProcess(@Header("Authorization") String token,
                                                                                 @Body CreateRelatedProcessViewModel createRelatedProcessViewModel);

    @GET("api/Process/GetComment")
    Observable<ProcessCommentViewModel> getComment(@Header("Authorization") String token,
                                                   @Query("processId") int processId,
                                                   @Query("id") int id);

    @GET("api/Process/GetReplyComments")
    Observable<List<ProcessCommentViewModel>> getReplyComments(@Header("Authorization") String token,
                                                               @Query("processId") int processId,
                                                               @Query("id") int id);

    @GET("api/Process/GetCommentReadStatus")
    Observable<List<CommentReadStateViewModel>> getCommentReadStatus(@Header("Authorization") String token,
                                                                     @Query("processId") int processId,
                                                                     @Query("id") int id);
    @POST("api/Process/MarkMessagesRead")
    Observable<List<CommentReadStateUpdatedViewModel>> markMessagesRead(@Header("Authorization") String token,
                                                                        @Query("companyId") int companyId,
                                                                        @Query("processId") int processId);

    @FormUrlEncoded
    @POST("api/Process/GetProcessComments")
    Observable<CommentList> getProcessComments(@Header("Authorization") String token,
                                               @Query("companyId") int companyId,
                                               @Query("processId") int processId,
                                               @Field("Page") int page,
                                               @Field("PageSize") int pageSize);

    @POST("api/Process/PostComment")
    Observable<ProcessCommentViewModel> postComment(@Header("Authorization") String token,
                                                    @Query("companyId") int companyId,
                                                    @Body PostCommentViewModel postCommentViewModel);

    @GET("api/Process/GetCommentAudioFile")
    Observable<ResponseBody> getCommentAudioFile(@Header("Authorization") String token,
                                                 @Query("companyId") int companyId,
                                                 @Query("processId") int processId,
                                                 @Query("id") int commentId);

    @POST("api/Process/SetCommentIsSolution")
    Observable<CommentTypeUpdatedViewModel> setCommentIsSolution(@Header("Authorization") String token,
                                                                 @Query("processId") int processId,
                                                                 @Query("commentId") int commentId,
                                                                 @Query("isSolution") boolean isSolution);

    @POST("api/Process/SetCommentIsApprovedSolution")
    Observable<CommentTypeUpdatedViewModel> setCommentIsApprovedSolution(@Header("Authorization") String token,
                                                                         @Query("processId") int processId,
                                                                         @Query("commentId") int commentId,
                                                                         @Query("approve") boolean approve);

    @GET("api/Process/GetRelatedProcessList")
    Observable<List<RelatedProcessViewModel>> getRelatedProcessList(@Header("Authorization") String token,
                                                                    @Query("processId") int processId,
                                                                    @Query("registrational") boolean registrational);

    @POST("api/Process/AddRelatedProcess")
    Observable<RelatedProcessViewModel> addRelatedProcess(@Header("Authorization") String token,
                                                          @Query("relatedProcessId") int relatedProcessId,
                                                          @Query("processId") int processId,
                                                          @Query("matchTarget") boolean matchTarget);

    @POST("api/Process/RemoveRelatedProcess")
    Observable<ResponseBody> removeRelatedProcess(@Header("Authorization") String token,
                                                  @Query("relatedProcessId") int relatedProcessId,
                                                  @Query("processId") int processId);

    @POST("api/Process/AddTargetProcessName")
    Observable<TargetProcessViewModel> addTargetProcessName(@Header("Authorization") String token,
                                                            @Query("targetProcessNameId") int targetProcessNameId,
                                                            @Query("processId") int processId);

    @POST("api/Process/RemoveTargetProcess")
    Observable<ResponseBody> removeTargetProcess(@Header("Authorization") String token,
                                                 @Query("targetProcessNameId") int targetProcessNameId,
                                                 @Query("processId") int processId);

    @POST("api/Process/AddParticipant")
    Observable<ParticipantViewModel> addParticiapnt(@Header("Authorization") String token,
                                                    @Query("participantId") int participantId,
                                                    @Query("processId") int processId,
                                                    @Query("companyId") int companyId,
                                                    @Query("messageText") String messageText);

    @POST("api/Process/RemoveParticipant")
    Observable<ResponseBody> removeParticipant(@Header("Authorization") String token,
                                               @Query("participantId") int participantId,
                                               @Query("processId") int processId,
                                               @Query("companyId") int companyId);

    @POST("api/Process/ExitFromProcess")
    Observable<ResponseBody> exitFromProcess(@Header("Authorization") String token,
                                             @Query("processId") int processId,
                                             @Query("messageText") String messageText);

    @POST("api/Process/ChangeEmployees")
    Observable<ParticipantChangeViewModel> changeEmplyees(@Header("Authorization") String token,
                                                          @Query("processId") int processId,
                                                          @Query("newExecutorId") int newExecutorId,
                                                          @Query("newResponsibleId") int newResponsibleId);

    @POST("api/Process/GetCompanyEmployeesExceptProcess")
    Observable<EmployeeList> getCompanyEmployeesExceptProcess(@Header("Authorization") String token,
                                                              @Query("processId") int processId,
                                                              @Field("Page") int page,
                                                              @Field("PageSize") int pageSize);

    @POST("api/Process/ChangeStartDateTime")
    Observable<ResponseBody> changeStartDateTime(@Header("Authorization") String token,
                                                 @Query("processId") int processId,
                                                 @Query("newDateTime") String newDateTime);

    @POST("api/Process/SaveEndDateTime")
    Observable<EventEndDateLogViewModel> saveEndDateTime(@Header("Authorization") String token,
                                                         @Query("endDateTime") String endDateTime,
                                                         @Query("processId") int processId,
                                                         @Query("companyId") int companyId
                                                         );

    @POST("api/Process/ChangeTitle")
    Observable<ResponseBody> changeTitle(@Header("Authorization") String token,
                                         @Query("processId") int processId,
                                         @Query("NewTitle") String newTitle);

    @POST("api/Process/ToggleProcessHidden")
    Observable<ResponseBody> toggleProcessHidden(@Header("Authorization") String token,
                                                 @Query("processId") int processId,
                                                 @Query("hide") boolean hide);

    @POST("api/Process/UnhideAllProcesses")
    Observable<ResponseBody> unhideAllProcesses();

    @GET("api/Process/GetDocumentData")
    Observable<DocumentViewModel> getDocumentData(@Header("Authorization") String token,
                                                  @Query("processId") int processId,
                                                  @Query("companyId") int companyId);

    @GET("api/Process/GetDocumentHistory")
    Observable<List<DocumentViewModel>> getDocumentHistory(@Header("Authorization") String token,
                                                           @Query("processId") int processId);

    @POST("api/Process/StoreDocumentData")
    Observable<DocumentViewModel> storeDocumentData(@Header("Authorization") String token,
                                                    @Query("processId") int processId,
                                                    @Query("companyId") int companyId,
                                                    @Body DocumentViewModel documentViewModel);

    @POST("api/Process/AgreeDocument")
    Observable<DocumentAgreedByViewModel> agreeDocument(@Header("Authorization") String token,
                                                        @Query("processId") int processId,
                                                        @Query("companyId") int companyId);

    @POST("api/Process/AddProcessFile")
    Observable<ProcessFileViewModel> addProcessFile(@Header("Authorization") String token,
                                                    @Query("processId") int processId,
                                                    @Query("uploadId") int uploadId);

    @POST("api/Process/AddProcessMultiFiles")
    Observable<List<ProcessFileViewModel>> addProcessMultiFiles(@Header("Authorization") String token,
                                                                @Query("companyId") int companyId,
                                                                @Query("processId") int processId,
                                                                @Body List<Integer> ids);

    @POST("api/Process/RemoveProcessFile")
    Observable<ResponseBody> removeProcessFile(@Header("Authorization") String token,
                                               @Query("processId") int processId,
                                               @Query("fileId") int fileId);

    @FormUrlEncoded
    @POST("api/Process/GetProcessFiles")
    Observable<ProcessFileList> getProcessFiles(@Header("Authorization") String token,
                                                @Query("processId") int processId,
                                                @Query("companyId") int companyId,
                                                @Field("Page") int page,
                                                @Field("PageSize") int pageSize);

    @POST("api/Process/GetFile")
    Observable<ResponseBody> getProcessFile(@Header("Authorization") String token,
                                            @Query("companyId") int companyId,
                                            @Query("fileId") int fileId,
                                            @Query("processId") int processId,
                                            @Query("onlyDownload") boolean onlyDownload);


    @POST("api/Process/FinishProcess")
    Observable<ResponseBody> finishProcess(@Header("Authorization") String token,
                                           @Query("processId") int processId,
                                           @Query("companyId") int companyId);

    @POST("api/Process/CompletePositive")
    Observable<ResponseBody> completePositive(@Header("Authorization") String token,
                                              @Query("processId") int processId,
                                              @Query("companyId") int companyId);

    @POST("api/Process/CompleteNegative")
    Observable<ResponseBody> completeNegative(@Header("Authorization") String token,
                                              @Query("processId") int processId,
                                              @Query("reasonText") String reasonText,
                                              @Query("companyId") int companyId);

    @POST("api/Process/ExecutorRevision")
    Observable<ResponseBody> executorRevision(@Header("Authorization") String token,
                                              @Query("processId") int processId);

    @POST("api/Process/ReStartProcess")
    Observable<ResponseBody> restartProcess(@Header("Authorization") String token,
                                            @Query("processId") int processId,
                                            @Query("reasonId") int reasonId);

    @POST("api/Process/RecycleProcess")
    Observable<ResponseBody> recycleProcess(@Header("Authorization") String token,
                                            @Query("processId") int processId,
                                            @Query("companyId") int companyId);

    @POST("api/Process/ApproveProcess")
    Observable<ResponseBody> approceProcess(@Header("Authorization") String token,
                                            @Query("processId") int processId);
    // Process end.




    // UserContext
    @GET("api/UserContext/GetCompanyContext")
    Observable<ContextInfoDTO> getCompanyContext(@Header("Authorization") String token);


    @FormUrlEncoded
    @POST("api/UserContext/GetJoinedCompanyListWithInfo")
    Observable<CompanyWorkInfoListViewModelList> getJoinedCompanyListWithInfo(@Header("Authorization") String token,
                                                                              @Field("Page") int page,
                                                                              @Field("PageSize") int pageSize);

    @FormUrlEncoded
    @POST("api/UserContext/GetJoinedCompanyListWithInfo")
    Observable<CompanyWorkInfoListViewModelList> getJoinedCompanyListWithInfoWithFilter(@Header("Authorization") String token,
                                                                                        @Field("Filter") String filter,
                                                                                        @Field("Page") int page,
                                                                                        @Field("PageSize") int pageSize);



    @GET("api/UserContext/GetAccessFlags")
    Observable<Integer> getAccessFlags(@Header("Authorization") String token,
                                       @Query("group") int group,
                                       @Query("privilegeName") String privilegeName);

    @GET("api/UserContext/GetAccessFlagsForResource")
    Observable<Integer> getAccessFlagsForResource(@Header("Authorization") String token,
                                                  @Query("group") int group,
                                                  @Query("resourceId") int resourceId);

    // UserContext end.

    // ProcessTypes

    @GET("api/ProcessTypes/GetProcessNameTemplate")
    Observable<ProcessTemplateViewModel> getProcessNameTemplate(@Header("Authorization") String token,
                                                                @Query("companyId") int companyId,
                                                                @Query("processNameId") int processNameId);

    @GET("api/ProcessTypes/GetAllProcessTypes")
    Observable<List<ProcessTypeViewModel>> getAllProcessTypes(@Header("Authorization") String token,
                                                              @Query("companyId") int companyId,
                                                              @Query("registrational") Boolean registrational,
                                                              @Query("hasTable") Boolean hasTable);

    @GET("api/ProcessTypes/GetAllProcessNames")
    Observable<List<ProcessTemplateViewModel>> getAllProcessNames(@Header("Authorization") String token,
                                                                  @Query("companyId") int companyId,
                                                                  @Query("processTypeId") int processTypeId,
                                                                  @Query("registrational") Boolean registrational,
                                                                  @Query("hasTable") Boolean hasTable);

    @GET("api/ProcessTypes/GetStartingProcessTypes")
    Observable<List<ProcessTypeViewModel>> getStartingProcessTypes(@Header("Authorization") String token,
                                                                   @Query("companyId") int companyId,
                                                                   @Query("registrational") Boolean registrational,
                                                                   @Query("hasTable") Boolean hasTable);

    @GET("api/ProcessTypes/GetStartingProcessNames")
    Observable<List<ProcessTemplateViewModel>> getStartingProcessNames(@Header("Authorization") String token,
                                                                       @Query("companyId") int companyId,
                                                                       @Query("processTypeId") int processTypeId,
                                                                       @Query("registrational") Boolean registrational,
                                                                       @Query("hasTable") Boolean hasTable);

    @GET("api/ProcessTypes/GetViewableProcessTypes")
    Observable<List<ProcessTypeViewModel>> getViewableProcessTypes(@Header("Authorization") String token,
                                                                   @Query("registrational") boolean registrational,
                                                                   @Query("hasTable") boolean hasTable);

    @GET("api/ProcessTypes/GetViewableProcessNames")
    Observable<List<ProcessTemplateViewModel>> getViewableProcessNames(@Header("Authorization") String token,
                                                                       @Query("processTypeId") int processTypeId,
                                                                       @Query("registrational") boolean registrational,
                                                                       @Query("hasTable") boolean hasTable);


    @GET("api/ProcessTypes/GetStartingGroupedProcessNames")
    Observable<List<ProcessNameGroupedViewModel>> getStartingGroupedProcessNames(@Header("Authorization") String token,
                                                                                 @Query("registrational") boolean registrational);

    @GET("api/ProcessTypes/GetViewableGroupedProcessNames")
    Observable<List<ProcessNameGroupedViewModel>> getViewableGroupedProcessNames(@Header("Authorization") String token,
                                                                                 @Query("companyId") int companyId,
                                                                                 @Query("registrational") boolean registrational);

    @GET("api/ProcessTypes/GetAllGroupedProcessNames")
    Observable<List<ProcessNameGroupedViewModel>> getAllGroupedProcessNames(@Header("Authorization") String token,
                                                                            @Query("registrational") boolean registrational);

    // ProcessTypes end.

    // News
    @FormUrlEncoded
    @POST("api/News/GetNewsList")
    Observable<NewsList> getNewsList(@Header("Authorization") String token,
                                     @Query("companyId") int companyId,
                                     @Field("Sort") String sort,
                                     @Field("Page") int page,
                                     @Field("PageSize") int pageSize);

    @FormUrlEncoded
    @POST("api/News/AddNewsEntry")
    Observable<ResponseBody> addNewsEntry(@Header("Authorization") String token,
                                          @Query("companyId") int companyId,
                                          @Body PostNewsEntryViewModel postNewsEntryViewModel);

    @GET("api/News/GetNewsEntryData")
    Observable<NewsEntryViewModel> getNewsEntryData(@Header("Authorization") String token,
                                                    @Query("entryId") int entryId);

    @POST("api/News/EditNewsEntry")
    Observable<ResponseBody> editNewsEntry(@Header("Authorization") String token,
                                           @Body PostNewsEntryViewModel postNewsEntryViewModel);

    @POST("api/News/DeleteNewsEntry")
    Observable<ResponseBody> deleteNewsEntry(@Header("Authorization") String token,
                                             @Query("entryId") int entryId);

    @POST("api/News/GetFile")
    Observable<ResponseBody> getNewsFile(@Header("Authorization") String token,
                                         @Query("fileId") int fileId,
                                         @Query("entryId") int entryId,
                                         @Query("onlyDownload") boolean onlyDownload);

    @FormUrlEncoded
    @POST("api/News/GetEntryComments")
    Observable<NewsCommentList> getEntryComments(@Header("Authorization") String token,
                                                 @Query("entryId") int entryId,
                                                 @Field("Page") int page,
                                                 @Field("PageSize") int pageSize);

    @POST("api/News/PostComment")
    Observable<NewsCommentViewModel> postComment(@Header("Authorization") String token,
                                                 @Query("entryId") int entryId,
                                                 @Query("text") String text);
    // News end.

    // Company
    @POST("api/Company/GetGroupedEmployees")
    Observable<List<UsersViewModel>> getGroupedEmployees(@Header("Authorization") String token,
                                                         @Query("companyId") int companyId,
                                                         @Query("showDeleted") boolean showDeleted);

    @FormUrlEncoded
    @POST("api/Company/GetMyOwnCompanyList")
    Observable<CompanyList> getMyOwnCompanyList(@Header("Authorization") String token,
                                                @Field("Page") int page,
                                                @Field("PageSize") int pageSize);

    @FormUrlEncoded
    @POST("api/Company/GetJoinedCompanyList")
    Observable<CompanyList> getJoinedCompanyList(@Header("Authorization") String token,
                                                 @Field("Page") int page,
                                                 @Field("PageSize") int pageSize);

    @POST("api/Company/GetCompanyReviewStats")
    Observable<CompanyReviewStatsViewModel> getCompanyReviewStats();

    @FormUrlEncoded
    @POST("api/Company/GetCompaniesExceptJoined")
    Observable<CompanyList> getCompaniesExceptJoined(@Header("Authorization") String token,
                                                     @Field("Page") int page,
                                                     @Field("PageSize") int pageSize);

    @POST("api/Company/JoinCompanyRequest")
    Observable<ResponseBody> joinCompanyRequest(@Header("Authorization") String token,
                                                @Query("companyId") int companyId);

    @FormUrlEncoded
    @POST("api/Company/GetCompanyJoinRequests")
    Observable<JoinCompanyRequestList> getCompanyJoinRequests(@Header("Authorization") String token,
                                                              @Field("Page") int page,
                                                              @Field("PageSize") int pageSize);

    @POST("api/Company/DeclineJoinRequest")
    Observable<ResponseBody> declineJoinRequest(@Header("Authorization") String token,
                                                @Query("requestId") int requestId,
                                                @Query("declineReason") String declineReason);

    @POST("api/Company/AcceptJoinRequest")
    Observable<ResponseBody> acceptJoinRequest(@Header("Authorization") String token,
                                               @Query("requestId") int requestId);

    @POST("api/Company/DeleteEmployee")
    Observable<ResponseBody> deleteEmployee(@Header("Authorization") String token,
                                            @Query("employeeId") int employeeId,
                                            @Query("replaceExecutorId") int replaceExecutorId,
                                            @Query("replaceResponsibleId") int replaceResponsibleId);

    @FormUrlEncoded
    @POST("api/Company/GetCompanyEmployees")
    Observable<EmployeeList> getCompanyEmployees(@Header("Authorization") String token,
                                                 @Query("companyId") int companyId,
                                                 @Field("Page") int page,
                                                 @Field("PageSize") int pageSize);

    @FormUrlEncoded
    @POST("api/Company/CreateMyCompany")
    Observable<ResponseBody> createMyCompany(@Header("Authorization") String token,
                                             @Field("Name") String name,
                                             @Field("Bin") String bin,
                                             @Field("CompanyTypeId") int companyTypeId,
                                             @Field("Email") String email,
                                             @Field("Website") String website,
                                             @Field("DateFound") String dateFound,
                                             @Field("Desc") String desc,
                                             @Field("AddBasicProcesses") boolean addBasicProcesses,
                                             @Field("AddDatabaseProcesses") boolean addDatabaseProcesses);
    // Company end.

    // Chat
    @FormUrlEncoded
    @POST("api/Chat/GetContactList")
    Observable<ChatList> getContactList(@Header("Authorization") String token,
                                        @Query("companyId") int companyId,
                                        @Query("searchText") String searchText,
                                        @Field("Sort") String sort,
                                        @Field("Page") int page,
                                        @Field("PageSize") int pageSize);


    @FormUrlEncoded
    @POST("api/Chat/GetContactList")
    Observable<ChatList> getContact(@Header("Authorization") String token,
                                        @Query("companyId") int companyId,
                                        @Field("Filter") String filter,
                                        @Field("Page") int page,
                                        @Field("PageSize") int pageSize);


    @FormUrlEncoded
    @POST("api/Chat/GetChatMessages")
    Observable<ChatMessageList> getChatMessages(@Header("Authorization") String token,
                                                @Query("companyId") int companyId,
                                                @Query("chatId") int chatId,
                                                @Field("Page") int page,
                                                @Field("PageSize") int pageSize);

    @POST("api/Chat/SendMessageToGroup")
    Observable<ResponseBody> sendMessageToGroup(@Header("Authorization") String token,
                                                @Body GroupMessageViewModel groupMessageViewModel);

    @POST("api/Chat/SendMessageToAll")
    Observable<ResponseBody> sendMessageToAll(@Header("Authorization") String token, @Query("message") String message);


    @POST("api/Chat/CreateChat")
    Observable<ChatsViewModel> createChat(@Header("Authorization") String token,
                                          @Query("companyId") int companyId,
                                          @Body int[] userIds);

    @POST("api/Chat/PostMessage")
    Observable<MessageFromDto> postMessage(@Header("Authorization") String token, @Body PostMessageViewModel postMessageViewModel);


    // Chat end.

    @FormUrlEncoded
    @POST("api/Process/TableGetRows")
    Observable<DSResultWithTypeOfProcessRowDTO> tableGetRows(@Header("Authorization") String token,
                                                             @Query("processId") int processId,
                                                             @Query("companyId") int companyId,
                                                             @Field("Page") int page,
                                                             @Field("PageSize") int pageSize);


    @POST("api/Process/TableAddRow")
    Observable<ProcessRowDTO> tableAddRow(@Header("Authorization") String token,
                                          @Query("processId") int processId,
                                          @Query("companyId") int companyId,
                                          @Body List<ProcessColumnValueDTO> row);

    @POST("api/Process/TableUpdateRow")
    Observable<ProcessRowDTO> tableUpdateRow(@Header("Authorization") String token,
                                          @Query("processId") int processId,
                                          @Query("rowId") int rowId,
                                          @Query("companyId") int companyId,
                                          @Body List<ProcessColumnValueDTO> row);

    @POST("api/Process/TableDeleteRow")
    Observable<ProcessRowDTO> tableDeleteRow(@Header("Authorization") String token,
                                             @Query("processId") int processId,
                                             @Query("Id") int rowId,
                                             @Query("companyId") int companyId);

    @POST("api/Company/SendInvitationMessages")
    Observable<CompanyInvitationResultDTO> sendInvitationMessages(@Header("Authorization") String token,
                                                                  @Query("companyId") int companyId,
                                                                  @Body CompanyInvitationDTO destinations);


}
