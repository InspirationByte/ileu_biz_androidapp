package biz.ileu.ileuapi;

import android.content.Context;
import android.content.Intent;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Daniq on 03.04.2017.
 */

public class RestClient {


    private static String BASE_URL = "http://ileu.biz/";

    private static final int TIMEOUT_IN_SECONDS = 60;

    private static OkHttpClient okHttpClient;


    private static Retrofit retrofit;

    private RestClient() {

    }
    public static void setBaseUrl(String baseUrl) {
        BASE_URL = baseUrl;
    }

    private static HttpLoggingInterceptor getInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                : HttpLoggingInterceptor.Level.NONE);
        return interceptor;
    }

    public static class TokenInterceptor implements Interceptor {

        private Context context;
        private Class loginActivityClass;

        public TokenInterceptor(Context context, Class loginActivityClass) {
            this.context = context;
            this.loginActivityClass = loginActivityClass;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Response response = chain.proceed(request);

            if (response.code() == 403 || response.code() == 401) {
                Intent intent = new Intent(context, loginActivityClass);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
            return response;
        }
    }


    public static void initRetrofit(Context context, Class loginActivityClass) {
        okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(getInterceptor())
                .addInterceptor(new TokenInterceptor(context, loginActivityClass))
                .connectTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }


    public static IleuService request() {
        return retrofit.create(IleuService.class);
    }

}
