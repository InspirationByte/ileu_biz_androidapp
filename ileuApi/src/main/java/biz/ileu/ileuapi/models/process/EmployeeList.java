package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
public class EmployeeList {
    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<EmployeeViewModel> employeeViewModelList;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<EmployeeViewModel> getEmployeeViewModelList() {
        return employeeViewModelList;
    }

    public void setEmployeeViewModelList(List<EmployeeViewModel> employeeViewModelList) {
        this.employeeViewModelList = employeeViewModelList;
    }
}
