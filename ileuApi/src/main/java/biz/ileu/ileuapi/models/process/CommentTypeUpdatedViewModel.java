package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class CommentTypeUpdatedViewModel {

    /**
     * Id : 1
     * Type : 0
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("Type")
    private int type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
