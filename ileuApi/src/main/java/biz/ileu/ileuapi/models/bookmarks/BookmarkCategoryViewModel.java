package biz.ileu.ileuapi.models.bookmarks;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Daniq on 26.04.2017.
 */
@Parcel
public class BookmarkCategoryViewModel {

    /**
     * Id : 1
     * Title : sample string 2
     * UserProfileId : 3
     * CompanyId : 4
     * NumBookmarks : 5
     * NumNotifications : 6
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("Title")
    private String title;
    @SerializedName("UserProfileId")
    private int userProfileId;
    @SerializedName("CompanyId")
    private int companyId;
    @SerializedName("NumBookmarks")
    private int numBookmarks;
    @SerializedName("NumNotifications")
    private int numNotifications;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(int userProfileId) {
        this.userProfileId = userProfileId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getNumBookmarks() {
        return numBookmarks;
    }

    public void setNumBookmarks(int numBookmarks) {
        this.numBookmarks = numBookmarks;
    }

    public int getNumNotifications() {
        return numNotifications;
    }

    public void setNumNotifications(int numNotifications) {
        this.numNotifications = numNotifications;
    }
}
