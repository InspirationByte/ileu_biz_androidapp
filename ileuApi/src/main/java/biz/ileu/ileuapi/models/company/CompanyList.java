package biz.ileu.ileuapi.models.company;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
public class CompanyList {
    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<CompanyViewModel> companyViewModelList;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<CompanyViewModel> getCompanyViewModelList() {
        return companyViewModelList;
    }

    public void setCompanyViewModelList(List<CompanyViewModel> companyViewModelList) {
        this.companyViewModelList = companyViewModelList;
    }
}
