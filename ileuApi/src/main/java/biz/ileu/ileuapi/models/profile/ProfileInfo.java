package biz.ileu.ileuapi.models.profile;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Daniq on 22.02.2017.
 */
@Parcel
public class ProfileInfo {

    /**
     * Id : 1
     * Email : sample string 2
     * Firstname : sample string 3
     * Surname : sample string 4
     * UserPosition : sample string 5
     * DateOfBirth : 2017-02-22T15:09:01.6823839+06:00
     * ProfileImage : sample string 6
     * IsMale : true
     * PhoneNumber : sample string 8
     * IsIleuAdmin : true
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("Email")
    private String email;
    @SerializedName("Firstname")
    private String firstname;
    @SerializedName("Surname")
    private String surname;
    @SerializedName("UserPosition")
    private String userPosition;
    @SerializedName("DateOfBirth")
    private String dateOfBirth;
    @SerializedName("ProfileImage")
    private String profileImage;
    @SerializedName("IsMale")
    private boolean male;
    @SerializedName("PhoneNumber")
    private String phoneNumber;
    @SerializedName("IsIleuAdmin")
    private boolean isIleuAdmin;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserPosition() {
        return userPosition;
    }

    public void setUserPosition(String userPosition) {
        this.userPosition = userPosition;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isIsIleuAdmin() {
        return isIleuAdmin;
    }

    public void setIsIleuAdmin(boolean isIleuAdmin) {
        this.isIleuAdmin = isIleuAdmin;
    }
}
