package biz.ileu.ileuapi.models.chat;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 24.02.2017.
 */
public class GroupMessageViewModel {

    /**
     * Message : sample string 1
     * UserIds : [1,2]
     */

    @SerializedName("Message")
    private String message;
    @SerializedName("UserIds")
    private int[] userIds;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int[] getUserIds() {
        return userIds;
    }

    public void setUserIds(int[] userIds) {
        this.userIds = userIds;
    }
}
