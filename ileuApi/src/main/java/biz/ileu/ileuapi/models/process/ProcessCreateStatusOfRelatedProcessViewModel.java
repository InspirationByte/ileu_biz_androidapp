package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class ProcessCreateStatusOfRelatedProcessViewModel {

    /**
     * Id : 1
     * StatusText : sample string 2
     * IsError : true
     * Data : {"Id":1,"Description":"sample string 2","ProcessStatus":0,"StatusColor":"blue","IsRegistrational":true,"StatusName":"Запущен"}
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("StatusText")
    private String statusText;
    @SerializedName("IsError")
    private boolean error;
    @SerializedName("Data")
    private RelatedProcessViewModel data;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public RelatedProcessViewModel getData() {
        return data;
    }

    public void setData(RelatedProcessViewModel data) {
        this.data = data;
    }


}
