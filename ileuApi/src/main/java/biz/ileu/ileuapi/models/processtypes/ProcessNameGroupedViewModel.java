package biz.ileu.ileuapi.models.processtypes;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class ProcessNameGroupedViewModel {

    /**
     * Id : 1
     * TypeId : 2
     * TypeTitle : sample string 3
     * Title : sample string 4
     * IsRegistrational : true
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("TypeId")
    private int typeId;
    @SerializedName("TypeTitle")
    private String typeTitle;
    @SerializedName("Title")
    private String title;
    @SerializedName("IsRegistrational")
    private boolean registrational;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getTypeTitle() {
        return typeTitle;
    }

    public void setTypeTitle(String typeTitle) {
        this.typeTitle = typeTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isRegistrational() {
        return registrational;
    }

    public void setRegistrational(boolean registrational) {
        this.registrational = registrational;
    }
}
