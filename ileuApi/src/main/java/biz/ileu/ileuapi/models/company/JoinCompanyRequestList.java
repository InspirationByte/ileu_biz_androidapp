package biz.ileu.ileuapi.models.company;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 24.02.2017.
 */
public class JoinCompanyRequestList {
    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<JoinCompanyRequestViewModel> companyViewModelList;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<JoinCompanyRequestViewModel> getCompanyViewModelList() {
        return companyViewModelList;
    }

    public void setCompanyViewModelList(List<JoinCompanyRequestViewModel> companyViewModelList) {
        this.companyViewModelList = companyViewModelList;
    }
}
