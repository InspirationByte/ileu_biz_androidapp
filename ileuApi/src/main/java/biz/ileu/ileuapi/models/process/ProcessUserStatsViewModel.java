package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 07.06.2017.
 */

public class ProcessUserStatsViewModel {

    /**
     * CurrentProcessCount : 1
     * OverdueProcessCount : 2
     * NearOverdueProcessCount : 3
     * UnreadCommentsCount : 4
     * UnreadCommentsActiveCount : 5
     * ProcessPotentialSolutionsCount : 6
     */

    @SerializedName("CurrentProcessCount")
    private int currentProcessCount;
    @SerializedName("OverdueProcessCount")
    private int overdueProcessCount;
    @SerializedName("NearOverdueProcessCount")
    private int nearOverdueProcessCount;
    @SerializedName("UnreadCommentsCount")
    private int unreadCommentsCount;
    @SerializedName("UnreadCommentsActiveCount")
    private int unreadCommentsActiveCount;
    @SerializedName("ProcessPotentialSolutionsCount")
    private int processPotentialSolutionsCount;

    public int getCurrentProcessCount() {
        return currentProcessCount;
    }

    public void setCurrentProcessCount(int currentProcessCount) {
        this.currentProcessCount = currentProcessCount;
    }

    public int getOverdueProcessCount() {
        return overdueProcessCount;
    }

    public void setOverdueProcessCount(int overdueProcessCount) {
        this.overdueProcessCount = overdueProcessCount;
    }

    public int getNearOverdueProcessCount() {
        return nearOverdueProcessCount;
    }

    public void setNearOverdueProcessCount(int nearOverdueProcessCount) {
        this.nearOverdueProcessCount = nearOverdueProcessCount;
    }

    public int getUnreadCommentsCount() {
        return unreadCommentsCount;
    }

    public void setUnreadCommentsCount(int unreadCommentsCount) {
        this.unreadCommentsCount = unreadCommentsCount;
    }

    public int getUnreadCommentsActiveCount() {
        return unreadCommentsActiveCount;
    }

    public void setUnreadCommentsActiveCount(int unreadCommentsActiveCount) {
        this.unreadCommentsActiveCount = unreadCommentsActiveCount;
    }

    public int getProcessPotentialSolutionsCount() {
        return processPotentialSolutionsCount;
    }

    public void setProcessPotentialSolutionsCount(int processPotentialSolutionsCount) {
        this.processPotentialSolutionsCount = processPotentialSolutionsCount;
    }
}
