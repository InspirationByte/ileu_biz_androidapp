package biz.ileu.ileuapi.models.news;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
public class PostNewsEntryViewModel {

    /**
     * Id : 1
     * Title : sample string 2
     * Text : sample string 3
     * Files : [1,2]
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("Title")
    private String title;
    @SerializedName("Text")
    private String text;
    @SerializedName("Files")
    private int[] files;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int[] getFiles() {
        return files;
    }

    public void setFiles(int[] files) {
        this.files = files;
    }
}
