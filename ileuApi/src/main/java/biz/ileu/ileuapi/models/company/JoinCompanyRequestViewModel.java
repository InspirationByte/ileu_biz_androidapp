package biz.ileu.ileuapi.models.company;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 24.02.2017.
 */
public class JoinCompanyRequestViewModel {

    /**
     * Id : 1
     * UserProfileId : 2
     * DisplayName : sample string 3
     * RequestDateTime : 2017-02-24T00:06:19.366477+06:00
     * RequestStatus : 0
     * DeclineReason : sample string 5
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("UserProfileId")
    private int userProfileId;
    @SerializedName("DisplayName")
    private String displayName;
    @SerializedName("RequestDateTime")
    private String requestDateTime;
    @SerializedName("RequestStatus")
    private int requestStatus;
    @SerializedName("DeclineReason")
    private String declineReason;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(int userProfileId) {
        this.userProfileId = userProfileId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(String requestDateTime) {
        this.requestDateTime = requestDateTime;
    }

    public int getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(int requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getDeclineReason() {
        return declineReason;
    }

    public void setDeclineReason(String declineReason) {
        this.declineReason = declineReason;
    }
}
