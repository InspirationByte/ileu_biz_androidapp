package biz.ileu.ileuapi.models.news;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
public class NewsCommentList {
    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<NewsCommentViewModel> newsCommentViewModelList;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<NewsCommentViewModel> getNewsCommentViewModelList() {
        return newsCommentViewModelList;
    }

    public void setNewsCommentViewModelList(List<NewsCommentViewModel> newsCommentViewModelList) {
        this.newsCommentViewModelList = newsCommentViewModelList;
    }
}
