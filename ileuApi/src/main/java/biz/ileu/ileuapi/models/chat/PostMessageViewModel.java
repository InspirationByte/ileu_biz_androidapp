package biz.ileu.ileuapi.models.chat;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 24.02.2017.
 */
public class PostMessageViewModel {

    /**
     * ChatId : 1
     * Message : sample string 2
     */

    @SerializedName("ChatId")
    private int chatId;
    @SerializedName("Message")
    private String message;

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
