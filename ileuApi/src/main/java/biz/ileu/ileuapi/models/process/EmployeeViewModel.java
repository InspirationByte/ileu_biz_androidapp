package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Daniq on 23.02.2017.
 */
@Parcel
public class EmployeeViewModel {

    /**
     * Id : 1
     * NameSurname : sample string 2
     * Email : sample string 3
     * Spec : sample string 4
     * Position : sample string 5
     * ProfilePicture : sample string 6
     * PhoneNumber : sample string 7
     * Viewable : true
     * IsOwner : true
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("NameSurname")
    private String nameSurname;
    @SerializedName("Email")
    private String email;
    @SerializedName("Spec")
    private String spec;
    @SerializedName("Position")
    private String position;
    @SerializedName("ProfilePicture")
    private String profilePicture;
    @SerializedName("PhoneNumber")
    private String phoneNumber;
    @SerializedName("Viewable")
    private boolean viewable;
    @SerializedName("IsOwner")
    private boolean owner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isViewable() {
        return viewable;
    }

    public void setViewable(boolean viewable) {
        this.viewable = viewable;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }


}
