package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class ProcessCreateStatus {
    @SerializedName("Id")
    private int id;

    @SerializedName("StatusText")
    private String statusText;

    @SerializedName("IsError")
    private boolean error;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}