package biz.ileu.ileuapi.models.usercontext;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Daniq on 22.02.2017.
 */
@Parcel
public class CompanyWorkInfoListViewModel {

    /**
     * Id : 1
     * Name : sample string 2
     * Logo : sample string 3
     * NumNotifications : 4
     * NumOverdues : 5
     * JoinRequests : 6
     * UsersWithoutPosition : 7
     * UsersWithoutSpecs : 8
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("Name")
    private String name;
    @SerializedName("IsOwn")
    private boolean own;
    @SerializedName("Logo")
    private String logo;
    @SerializedName("NumNotifications")
    private int numNotifications;
    @SerializedName("NumOverdues")
    private int numOverdues;
    @SerializedName("JoinRequests")
    private int joinRequests;
    @SerializedName("UsersWithoutPosition")
    private int usersWithoutPosition;
    @SerializedName("UsersWithoutSpecs")
    private int usersWithoutSpecs;

    @SerializedName("NumUnreadProcessComments")
    private int numUnreadProcessComments;
    @SerializedName("TotalUsers")
    private int totalUsers;
    @SerializedName("NumUnreadChatMessages")
    private int numUnreadChatMessages;

    @SerializedName("NumUnreadProcesses")
    private int numUnreadProcesses;

    public int getNumUnreadProcesses() {
        return numUnreadProcesses;
    }

    public void setNumUnreadProcesses(int numUnreadProcesses) {
        this.numUnreadProcesses = numUnreadProcesses;
    }

    public int getNumUnreadChatMessages() {
        return numUnreadChatMessages;
    }

    public void setNumUnreadChatMessages(int numUnreadChatMessages) {
        this.numUnreadChatMessages = numUnreadChatMessages;
    }

    public int getNumUnreadProcessComments() {
        return numUnreadProcessComments;
    }

    public void setNumUnreadProcessComments(int numUnreadProcessComments) {
        this.numUnreadProcessComments = numUnreadProcessComments;
    }

    public int getTotalUsers() {
        return totalUsers;
    }

    public void setTotalUsers(int totalUsers) {
        this.totalUsers = totalUsers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getNumNotifications() {
        return numNotifications;
    }

    public void setNumNotifications(int numNotifications) {
        this.numNotifications = numNotifications;
    }

    public int getNumOverdues() {
        return numOverdues;
    }

    public void setNumOverdues(int numOverdues) {
        this.numOverdues = numOverdues;
    }

    public int getJoinRequests() {
        return joinRequests;
    }

    public void setJoinRequests(int joinRequests) {
        this.joinRequests = joinRequests;
    }

    public int getUsersWithoutPosition() {
        return usersWithoutPosition;
    }

    public void setUsersWithoutPosition(int usersWithoutPosition) {
        this.usersWithoutPosition = usersWithoutPosition;
    }

    public int getUsersWithoutSpecs() {
        return usersWithoutSpecs;
    }

    public void setUsersWithoutSpecs(int usersWithoutSpecs) {
        this.usersWithoutSpecs = usersWithoutSpecs;
    }

    public boolean isOwn() {
        return own;
    }

    public void setOwn(boolean own) {
        this.own = own;
    }
}
