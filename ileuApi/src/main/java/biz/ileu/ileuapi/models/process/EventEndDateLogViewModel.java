package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class EventEndDateLogViewModel {

    /**
     * When : 2017-02-23T16:07:10.1917886+06:00
     * Value : 2017-02-23T16:07:10.1917886+06:00
     */

    @SerializedName("When")
    private String when;
    @SerializedName("Value")
    private String value;

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
