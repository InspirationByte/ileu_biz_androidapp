package biz.ileu.ileuapi.models.uploads;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import biz.ileu.ileuapi.models.news.FileInfoViewModel;

/**
 * Created by Daniq on 24.02.2017.
 */
public class FileInfoList {
    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<FileInfoViewModel> fileInfoViewModels;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<FileInfoViewModel> getFileInfoViewModels() {
        return fileInfoViewModels;
    }

    public void setFileInfoViewModels(List<FileInfoViewModel> fileInfoViewModels) {
        this.fileInfoViewModels = fileInfoViewModels;
    }
}
