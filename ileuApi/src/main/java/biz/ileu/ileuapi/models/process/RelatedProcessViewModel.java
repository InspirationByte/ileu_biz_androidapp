package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class RelatedProcessViewModel {

    /**
     * Id : 1
     * Description : sample string 2
     * ProcessStatus : 0
     * StatusColor : blue
     * IsRegistrational : true
     * StatusName : Запущен
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("Description")
    private String description;
    @SerializedName("ProcessStatus")
    private int processStatus;
    @SerializedName("StatusColor")
    private String statusColor;
    @SerializedName("IsRegistrational")
    private boolean registrational;
    @SerializedName("StatusName")
    private String statusName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(int processStatus) {
        this.processStatus = processStatus;
    }

    public String getStatusColor() {
        return statusColor;
    }

    public void setStatusColor(String statusColor) {
        this.statusColor = statusColor;
    }

    public boolean isRegistrational() {
        return registrational;
    }

    public void setRegistrational(boolean registrational) {
        this.registrational = registrational;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
