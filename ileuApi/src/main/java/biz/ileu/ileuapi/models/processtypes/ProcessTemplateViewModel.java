package biz.ileu.ileuapi.models.processtypes;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

import biz.ileu.ileuapi.models.process.EmployeeSpecViewModel;
import biz.ileu.ileuapi.models.process.ProcessNameColumnViewModel;

/**
 * Created by Daniq on 23.02.2017.
 */
@Parcel
public class ProcessTemplateViewModel {

    /**
     * Id : 1
     * ProcessTypeId : 1
     * Title : sample string 2
     * HasAdditionalTiming : true
     * TargetProcessNames : []
     * ProcessColumns : [{"Id":1,"Name":"sample string 2","ProcessNameId":3,"OrderPos":4,"SpecId":5,"SpecTitle":"sample string 6","Required":true},{"Id":1,"Name":"sample string 2","ProcessNameId":3,"OrderPos":4,"SpecId":5,"SpecTitle":"sample string 6","Required":true}]
     * HasDocument : true
     * DocumentAgreeSpecs : [{"Id":1,"Title":"sample string 2"},{"Id":1,"Title":"sample string 2"}]
     * IsRegistrational : true
     * HasCompletionTime : true
     * IsSolutionsOptional : true
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("ProcessTypeId")
    private int processTypeId;
    @SerializedName("Title")
    private String title;
    @SerializedName("HasAdditionalTiming")
    private boolean hasAdditionalTiming;
    @SerializedName("HasDocument")
    private boolean hasDocument;
    @SerializedName("IsRegistrational")
    private boolean registrational;
    @SerializedName("HasCompletionTime")
    private boolean hasCompletionTime;
    @SerializedName("IsSolutionsOptional")
    private boolean solutionsOptional;
    @SerializedName("ProcessColumns")
    private List<ProcessNameColumnViewModel> processColumns;
    @SerializedName("DocumentAgreeSpecs")
    private List<EmployeeSpecViewModel> documentAgreeSpecs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProcessTypeId() {
        return processTypeId;
    }

    public void setProcessTypeId(int processTypeId) {
        this.processTypeId = processTypeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isHasAdditionalTiming() {
        return hasAdditionalTiming;
    }

    public void setHasAdditionalTiming(boolean hasAdditionalTiming) {
        this.hasAdditionalTiming = hasAdditionalTiming;
    }

    public boolean isHasDocument() {
        return hasDocument;
    }

    public void setHasDocument(boolean hasDocument) {
        this.hasDocument = hasDocument;
    }

    public boolean isRegistrational() {
        return registrational;
    }

    public void setRegistrational(boolean registrational) {
        this.registrational = registrational;
    }

    public boolean isHasCompletionTime() {
        return hasCompletionTime;
    }

    public void setHasCompletionTime(boolean hasCompletionTime) {
        this.hasCompletionTime = hasCompletionTime;
    }

    public boolean isSolutionsOptional() {
        return solutionsOptional;
    }

    public void setSolutionsOptional(boolean solutionsOptional) {
        this.solutionsOptional = solutionsOptional;
    }

    public List<ProcessNameColumnViewModel> getProcessColumns() {
        return processColumns;
    }

    public void setProcessColumns(List<ProcessNameColumnViewModel> processColumns) {
        this.processColumns = processColumns;
    }
    public int getOrderPosById(int id) {
        for (ProcessNameColumnViewModel column : this.getProcessColumns()) {
            if (column.getId() == id)
                return column.getOrderPos();
        }
        return -1;
    }

    public int getIdByOrderPos(int orderPos) {
        for (ProcessNameColumnViewModel column : this.getProcessColumns()) {
            if (column.getOrderPos() == orderPos)
                return column.getId();
        }
        return -1;
    }
    public List<EmployeeSpecViewModel> getDocumentAgreeSpecs() {
        return documentAgreeSpecs;
    }

    public void setDocumentAgreeSpecs(List<EmployeeSpecViewModel> documentAgreeSpecs) {
        this.documentAgreeSpecs = documentAgreeSpecs;
    }


}
