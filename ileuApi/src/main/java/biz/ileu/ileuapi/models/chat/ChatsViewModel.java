package biz.ileu.ileuapi.models.chat;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Daniq on 24.02.2017.
 */
@Parcel
public class ChatsViewModel {

    /**
     * Id : 1
     * LastMessageTime : 2
     * LastMessageText : sample string 3
     * ChatUsers : [{"UserId":1,"DisplayName":"sample string 2","ProfilePicture":"sample string 3","IsOnline":true,"StatusText":"sample string 5","ShouldDisplayStatus":true},{"UserId":1,"DisplayName":"sample string 2","ProfilePicture":"sample string 3","IsOnline":true,"StatusText":"sample string 5","ShouldDisplayStatus":true}]
     * ChatGuid : 7a73a2ad-77c9-4ebf-9e3a-d86a059add90
     * NotificationsCount : 5
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("LastMessageTime")
    private long lastMessageTime;
    @SerializedName("LastMessageText")
    private String lastMessageText;
    @SerializedName("ChatGuid")
    private String chatGuid;
    @SerializedName("NotificationsCount")
    private int notificationsCount;
    @SerializedName("ChatUsers")
    private List<ChatUsersViewModel> chatUsers;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(long lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public String getLastMessageText() {
        return lastMessageText;
    }

    public void setLastMessageText(String lastMessageText) {
        this.lastMessageText = lastMessageText;
    }

    public String getChatGuid() {
        return chatGuid;
    }

    public void setChatGuid(String chatGuid) {
        this.chatGuid = chatGuid;
    }

    public int getNotificationsCount() {
        return notificationsCount;
    }

    public void setNotificationsCount(int notificationsCount) {
        this.notificationsCount = notificationsCount;
    }

    public List<ChatUsersViewModel> getChatUsers() {
        return chatUsers;
    }

    public void setChatUsers(List<ChatUsersViewModel> chatUsers) {
        this.chatUsers = chatUsers;
    }

}
