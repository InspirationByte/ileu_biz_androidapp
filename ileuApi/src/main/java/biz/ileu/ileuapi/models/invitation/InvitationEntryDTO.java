package biz.ileu.ileuapi.models.invitation;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macair on 22.08.17.
 */

public class InvitationEntryDTO {
    @SerializedName("Email")
    private String email;

    @SerializedName("Phone")
    private String phone;

    @SerializedName("DesiredSpec")
    private String despiredSpec;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDespiredSpec() {
        return despiredSpec;
    }

    public void setDespiredSpec(String despiredSpec) {
        this.despiredSpec = despiredSpec;
    }


    @Override
    public String toString() {
        return "InvitationEntryDTO{" +
                "email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", despiredSpec='" + despiredSpec + '\'' +
                '}';
    }
}
