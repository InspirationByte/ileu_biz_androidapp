package biz.ileu.ileuapi.models.chat;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Daniq on 20.04.2017.
 */

@Parcel
public class UserChatStatusDto {

    @SerializedName("Id")
    private int id;

    @SerializedName("StatusText")
    private String statusText;

    @SerializedName("ShouldDisplayStatus")
    private boolean shouldDisplayStatus;

    @SerializedName("IsOnline")
    private boolean online;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public boolean isShouldDisplayStatus() {
        return shouldDisplayStatus;
    }

    public void setShouldDisplayStatus(boolean shouldDisplayStatus) {
        this.shouldDisplayStatus = shouldDisplayStatus;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}
