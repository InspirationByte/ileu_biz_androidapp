package biz.ileu.ileuapi.models.tables;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by macair on 15.08.17.
 */

public class DSResultWithTypeOfProcessRowDTO {
    @SerializedName("Id")
    private List<AggregateResult> AggregateResults;
    @SerializedName("Data")
    private List<ProcessRowDTO> data;
    @SerializedName("Errors")
    private Object errors;
    @SerializedName("Total")
    private int total;

    public List<AggregateResult> getAggregateResults() {
        return AggregateResults;
    }

    public void setAggregateResults(List<AggregateResult> aggregateResults) {
        AggregateResults = aggregateResults;
    }

    public List<ProcessRowDTO> getData() {
        return data;
    }

    public void setData(List<ProcessRowDTO> data) {
        this.data = data;
    }

    public Object getErrors() {
        return errors;
    }

    public void setErrors(Object errors) {
        this.errors = errors;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
