package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Daniq on 23.02.2017.
 */
@Parcel
public class EmployeeSpecViewModel {
    @SerializedName("Id")
    private int id;

    @SerializedName("Title")
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
