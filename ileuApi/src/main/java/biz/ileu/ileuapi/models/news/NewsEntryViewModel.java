package biz.ileu.ileuapi.models.news;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
@Parcel
public class NewsEntryViewModel {

    /**
     * Id : 1
     * Title : sample string 2
     * Text : sample string 3
     * PublicationTime : 2017-02-23T23:26:44.6880548+06:00
     * AddedByName : sample string 5
     * AddedByAvatar : sample string 6
     * IsGlobal : true
     * NumComments : 8
     * UserIsAuthor : true
     * Files : [{"Id":1,"FileName":"sample string 2","FileLink":"sample string 3","UploadDate":"2017-02-23T23:26:44.6880548+06:00","References":5,"IsUsedHere":true,"SizeInBytes":7,"UploadId":8},{"Id":1,"FileName":"sample string 2","FileLink":"sample string 3","UploadDate":"2017-02-23T23:26:44.6880548+06:00","References":5,"IsUsedHere":true,"SizeInBytes":7,"UploadId":8}]
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("Title")
    private String title;
    @SerializedName("Text")
    private String text;
    @SerializedName("PublicationTime")
    private String publicationTime;
    @SerializedName("AddedByName")
    private String addedByName;
    @SerializedName("AddedByAvatar")
    private String addedByAvatar;
    @SerializedName("IsGlobal")
    private boolean global;
    @SerializedName("NumComments")
    private int numComments;
    @SerializedName("UserIsAuthor")
    private boolean userIsAuthor;
    @SerializedName("Files")
    private List<FileInfoViewModel> files;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPublicationTime() {
        return publicationTime;
    }

    public void setPublicationTime(String publicationTime) {
        this.publicationTime = publicationTime;
    }

    public String getAddedByName() {
        return addedByName;
    }

    public void setAddedByName(String addedByName) {
        this.addedByName = addedByName;
    }

    public String getAddedByAvatar() {
        return addedByAvatar;
    }

    public void setAddedByAvatar(String addedByAvatar) {
        this.addedByAvatar = addedByAvatar;
    }

    public boolean isGlobal() {
        return global;
    }

    public void setGlobal(boolean global) {
        this.global = global;
    }

    public int getNumComments() {
        return numComments;
    }

    public void setNumComments(int numComments) {
        this.numComments = numComments;
    }

    public boolean isUserIsAuthor() {
        return userIsAuthor;
    }

    public void setUserIsAuthor(boolean userIsAuthor) {
        this.userIsAuthor = userIsAuthor;
    }

    public List<FileInfoViewModel> getFiles() {
        return files;
    }

    public void setFiles(List<FileInfoViewModel> files) {
        this.files = files;
    }


}
