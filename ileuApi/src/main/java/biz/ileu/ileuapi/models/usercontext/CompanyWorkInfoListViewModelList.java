package biz.ileu.ileuapi.models.usercontext;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 22.02.2017.
 */
public class CompanyWorkInfoListViewModelList {

    /**
     * AggregateResults : null
     * Data : [{"Id":1,"Name":"sample string 2","Logo":"sample string 3","NumNotifications":4,"NumOverdues":5,"JoinRequests":6,"UsersWithoutPosition":7,"UsersWithoutSpecs":8},{"Id":1,"Name":"sample string 2","Logo":"sample string 3","NumNotifications":4,"NumOverdues":5,"JoinRequests":6,"UsersWithoutPosition":7,"UsersWithoutSpecs":8}]
     * Errors : {}
     * Total : 2
     */

    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<CompanyWorkInfoListViewModel> companies;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<CompanyWorkInfoListViewModel> getCompanies() {
        return companies;
    }

    public void setCompanies(List<CompanyWorkInfoListViewModel> companies) {
        this.companies = companies;
    }
}
