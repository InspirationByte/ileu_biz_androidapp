package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
public class CommentList {
    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<ProcessCommentViewModel> processCommentViewModels;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ProcessCommentViewModel> getProcessCommentViewModels() {
        return processCommentViewModels;
    }

    public void setProcessCommentViewModels(List<ProcessCommentViewModel> processCommentViewModels) {
        this.processCommentViewModels = processCommentViewModels;
    }
}
