package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class CommentReadStateViewModel {

    /**
     * Id : 1
     * IsRead : true
     * ReadTime : 3
     * EmployeeId : 4
     * EmployeeName : sample string 5
     * ProfilePicture : sample string 6
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("IsRead")
    private boolean read;
    @SerializedName("ReadTime")
    private int readTime;
    @SerializedName("EmployeeId")
    private int employeeId;
    @SerializedName("EmployeeName")
    private String employeeName;
    @SerializedName("ProfilePicture")
    private String profilePicture;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public int getReadTime() {
        return readTime;
    }

    public void setReadTime(int readTime) {
        this.readTime = readTime;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
}
