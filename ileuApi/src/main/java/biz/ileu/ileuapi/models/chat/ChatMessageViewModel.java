package biz.ileu.ileuapi.models.chat;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 24.02.2017.
 */
public class ChatMessageViewModel {

    /**
     * Text : sample string 1
     * MsgTime : 2
     * DisplayName : sample string 3
     * ProfilePicture : sample string 4
     * IsOwn : true
     */

    @SerializedName("Text")
    private String text;
    @SerializedName("MsgTime")
    private String msgTime;
    @SerializedName("DisplayName")
    private String displayName;
    @SerializedName("ProfilePicture")
    private String profilePicture;
    @SerializedName("IsOwn")
    private boolean own;

    public static final int STATE_SENT = 0;
    public static final int STATE_SENDING = 1;
    public static final int STATE_NOT_SENT = 2;

    private transient int state = STATE_SENT;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public boolean isOwn() {
        return own;
    }

    public void setOwn(boolean own) {
        this.own = own;
    }
}
