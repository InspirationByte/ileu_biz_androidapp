package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
public class ProcessFileList {
    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<ProcessFileViewModel> processFileViewModelList;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ProcessFileViewModel> getProcessFileViewModelList() {
        return processFileViewModelList;
    }

    public void setProcessFileViewModelList(List<ProcessFileViewModel> processFileViewModelList) {
        this.processFileViewModelList = processFileViewModelList;
    }
}
