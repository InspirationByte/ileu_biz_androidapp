package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
public class ParticipantChangeViewModel {
    @SerializedName("Executor")
    private ParticipantViewModel executor;

    @SerializedName("Responsible")
    private ParticipantViewModel responsible;

    @SerializedName("AddedParticipants")
    private List<ParticipantViewModel> addedParticipants;

    public ParticipantViewModel getExecutor() {
        return executor;
    }

    public void setExecutor(ParticipantViewModel executor) {
        this.executor = executor;
    }

    public ParticipantViewModel getResponsible() {
        return responsible;
    }

    public void setResponsible(ParticipantViewModel responsible) {
        this.responsible = responsible;
    }

    public List<ParticipantViewModel> getAddedParticipants() {
        return addedParticipants;
    }

    public void setAddedParticipants(List<ParticipantViewModel> addedParticipants) {
        this.addedParticipants = addedParticipants;
    }
}
