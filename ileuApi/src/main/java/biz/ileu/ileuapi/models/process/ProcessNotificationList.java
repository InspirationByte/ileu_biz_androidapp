package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 14.04.2017.
 */

public class ProcessNotificationList {

    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<ProcessNotificationViewModel> notificationList;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ProcessNotificationViewModel> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<ProcessNotificationViewModel> notificationList) {
        this.notificationList = notificationList;
    }
}
