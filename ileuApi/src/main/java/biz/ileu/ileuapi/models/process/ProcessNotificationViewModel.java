package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 14.04.2017.
 */

public class ProcessNotificationViewModel {


    /**
     * ProcessId : 1
     * DescriptionText : sample string 2
     * Status : 0
     * EndDateTime : 2017-04-14T14:40:50.5287402+06:00
     * IsOverDue : 4
     * TotalUnreadMessages : 5
     * LastUnreadMessage : sample string 6
     */

    @SerializedName("ProcessId")
    private int processId;
    @SerializedName("DescriptionText")
    private String descriptionText;
    @SerializedName("LastMessageFromAvatar")
    private String lastMessageAvatar;
    @SerializedName("LastMessageFromName")
    private String lastMessageName;

    @SerializedName("Title")
    private String title;
    @SerializedName("Status")
    private int status;
    @SerializedName("EndDateTime")
    private String endDateTime;
    @SerializedName("IsOverDue")
    private int isOverDue;
    @SerializedName("TotalUnreadMessages")
    private int totalUnreadMessages;
    @SerializedName("LastUnreadMessage")
    private String lastUnreadMessage;

    public int getProcessId() {
        return processId;
    }

    public void setProcessId(int processId) {
        this.processId = processId;
    }

    public String getDescriptionText() {
        return descriptionText;
    }

    public void setDescriptionText(String descriptionText) {
        this.descriptionText = descriptionText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public int getIsOverDue() {
        return isOverDue;
    }

    public void setIsOverDue(int isOverDue) {
        this.isOverDue = isOverDue;
    }

    public int getTotalUnreadMessages() {
        return totalUnreadMessages;
    }

    public void setTotalUnreadMessages(int totalUnreadMessages) {
        this.totalUnreadMessages = totalUnreadMessages;
    }

    public String getLastUnreadMessage() {
        return lastUnreadMessage;
    }

    public void setLastUnreadMessage(String lastUnreadMessage) {
        this.lastUnreadMessage = lastUnreadMessage;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLastMessageName() {
        return lastMessageName;
    }

    public void setLastMessageName(String lastMessageName) {
        this.lastMessageName = lastMessageName;
    }

    public String getLastMessageAvatar() {
        return lastMessageAvatar;
    }

    public void setLastMessageAvatar(String lastMessageAvatar) {
        this.lastMessageAvatar = lastMessageAvatar;
    }
}
