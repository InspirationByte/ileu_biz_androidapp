package biz.ileu.ileuapi.models.chat;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 24.02.2017.
 */
public class ChatMessageList {
    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<ChatMessageViewModel> chatMessageViewModels;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ChatMessageViewModel> getChatMessageViewModels() {
        return chatMessageViewModels;
    }

    public void setChatMessageViewModels(List<ChatMessageViewModel> chatMessageViewModels) {
        this.chatMessageViewModels = chatMessageViewModels;
    }
}
