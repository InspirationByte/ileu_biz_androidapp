package biz.ileu.ileuapi.models.news;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Daniq on 23.02.2017.
 */
public class NewsCommentViewModel {

    /**
     * Id : 1
     * EntryId : 2
     * Text : sample string 3
     * Time : 4
     * AddedByName : sample string 5
     * AddedByAvatar : sample string 6
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("EntryId")
    private int entryId;
    @SerializedName("Text")
    private String text;
    @SerializedName("Time")
    private long time;
    @SerializedName("AddedByName")
    private String addedByName;
    @SerializedName("AddedByAvatar")
    private String addedByAvatar;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getTime() {
        return time;
    }

    public Date getTimeAsDate() {
        Date d = new Date(this.time);

        return d;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getAddedByName() {
        return addedByName;
    }

    public void setAddedByName(String addedByName) {
        this.addedByName = addedByName;
    }

    public String getAddedByAvatar() {
        return addedByAvatar;
    }

    public void setAddedByAvatar(String addedByAvatar) {
        this.addedByAvatar = addedByAvatar;
    }
}
