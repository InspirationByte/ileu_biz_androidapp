package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
public class CreateProcessViewModel {

    @SerializedName("Title")
    private String title;

    @SerializedName("StartDateTime")
    private String startDateTime;

    @SerializedName("EndDateTime")
    private String endDateTime;

    @SerializedName("EventStartDate")
    private String EventStartDate;

    @SerializedName("EventEndDate")
    private String eventEndDate;

    @SerializedName("ExecutingUser")
    private int executingUser;

    @SerializedName("ResponsibleUser")
    private int responsibleUser;

    @SerializedName("Description")
    private String description;

    @SerializedName("ProcessTypeId")
    private int processTypeId;

    @SerializedName("ProcessNameId")
    private int processNameId;

    @SerializedName("RelatedProcesses")
    private int[] relatedProcesses;

    @SerializedName("Participants")
    private int[] participants;

    @SerializedName("DynamicTargetProcessNames")
    private int[] dynamicTargetProcessNames;

    @SerializedName("ColumnsValues")
    private List<CreateProcessColumnViewModel> columnsValues;

    @SerializedName("FileUploads")
    private int[] fileUploads;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getEventStartDate() {
        return EventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        EventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public int getExecutingUser() {
        return executingUser;
    }

    public void setExecutingUser(int executingUser) {
        this.executingUser = executingUser;
    }

    public int getResponsibleUser() {
        return responsibleUser;
    }

    public void setResponsibleUser(int responsibleUser) {
        this.responsibleUser = responsibleUser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProcessTypeId() {
        return processTypeId;
    }

    public void setProcessTypeId(int processTypeId) {
        this.processTypeId = processTypeId;
    }

    public int getProcessNameId() {
        return processNameId;
    }

    public void setProcessNameId(int processNameId) {
        this.processNameId = processNameId;
    }

    public int[] getRelatedProcesses() {
        return relatedProcesses;
    }

    public void setRelatedProcesses(int[] relatedProcesses) {
        this.relatedProcesses = relatedProcesses;
    }

    public int[] getParticipants() {
        return participants;
    }

    public void setParticipants(int[] participants) {
        this.participants = participants;
    }

    public int[] getDynamicTargetProcessNames() {
        return dynamicTargetProcessNames;
    }

    public void setDynamicTargetProcessNames(int[] dynamicTargetProcessNames) {
        this.dynamicTargetProcessNames = dynamicTargetProcessNames;
    }

    public List<CreateProcessColumnViewModel> getColumnsValues() {
        return columnsValues;
    }

    public void setColumnsValues(List<CreateProcessColumnViewModel> columnsValues) {
        this.columnsValues = columnsValues;
    }

    public int[] getFileUploads() {
        return fileUploads;
    }

    public void setFileUploads(int[] fileUploads) {
        this.fileUploads = fileUploads;
    }
}
