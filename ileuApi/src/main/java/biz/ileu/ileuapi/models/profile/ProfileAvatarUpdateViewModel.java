package biz.ileu.ileuapi.models.profile;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 25.04.2017.
 */

public class ProfileAvatarUpdateViewModel {

    /**
     * Id : 1
     * ProfileImage : sample string 2
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("ProfileImage")
    private String profileImage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
}
