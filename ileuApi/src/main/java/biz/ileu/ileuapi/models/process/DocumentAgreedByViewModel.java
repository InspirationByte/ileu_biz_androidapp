package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class DocumentAgreedByViewModel {

    /**
     * ProfileId : 1
     * ProfileName : sample string 2
     * SpecId : 3
     * SpecName : sample string 4
     * IsFullyAgreed : true
     */

    @SerializedName("ProfileId")
    private int profileId;
    @SerializedName("ProfileName")
    private String profileName;
    @SerializedName("SpecId")
    private int specId;
    @SerializedName("SpecName")
    private String specName;
    @SerializedName("IsFullyAgreed")
    private boolean fullyAgreed;

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public int getSpecId() {
        return specId;
    }

    public void setSpecId(int specId) {
        this.specId = specId;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public boolean isFullyAgreed() {
        return fullyAgreed;
    }

    public void setFullyAgreed(boolean fullyAgreed) {
        this.fullyAgreed = fullyAgreed;
    }
}
