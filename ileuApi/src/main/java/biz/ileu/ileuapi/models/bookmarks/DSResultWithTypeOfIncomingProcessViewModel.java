package biz.ileu.ileuapi.models.bookmarks;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import biz.ileu.ileuapi.models.tables.AggregateResult;

/**
 * Created by macair on 26.08.17.
 */

public class DSResultWithTypeOfIncomingProcessViewModel {

    @SerializedName("AggregateResults")
    private List<AggregateResult> aggregateResultList;
    @SerializedName("Data")
    private List<IncomingProcessViewModel> categoryId;
    @SerializedName("AddedTime")
    private String addedTime;
    @SerializedName("IleuProcessId")
    private int ileuProcessId;
    @SerializedName("UserProfileId")
    private int userProfileId;
}
