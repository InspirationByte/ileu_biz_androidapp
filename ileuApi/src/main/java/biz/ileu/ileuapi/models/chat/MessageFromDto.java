package biz.ileu.ileuapi.models.chat;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 24.02.2017.
 */
public class MessageFromDto {

    /**
     * ChatId : 1
     * Text : sample string 2
     * ProfilePicture : sample string 3
     * DisplayName : sample string 4
     * MsgTime : 2017-02-24T10:28:49.2350977+06:00
     * FromUserId : 6
     * CompanyId : 7
     */

    @SerializedName("ChatId")
    private int chatId;
    @SerializedName("Text")
    private String text;
    @SerializedName("ProfilePicture")
    private String profilePicture;
    @SerializedName("DisplayName")
    private String displayName;
    @SerializedName("MsgTime")
    private String msgTime;
    @SerializedName("FromUserId")
    private int fromUserId;
    @SerializedName("CompanyId")
    private int companyId;

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public int getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(int fromUserId) {
        this.fromUserId = fromUserId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }
}
