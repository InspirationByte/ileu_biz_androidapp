package biz.ileu.ileuapi.models.tables;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by macair on 15.08.17.
 */

public class ProcessRowDTO {
    @SerializedName("Id")
    private int id;
    @SerializedName("AddedById")
    private int addedById;
    @SerializedName("ColumnValues")
    private List<ProcessColumnValueDTO> columnValues;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAddedById() {
        return addedById;
    }

    public void setAddedById(int addedById) {
        this.addedById = addedById;
    }

    public List<ProcessColumnValueDTO> getColumnValues() {
        return columnValues;
    }

    public void setColumnValues(List<ProcessColumnValueDTO> columnValues) {
        this.columnValues = columnValues;
    }
}
