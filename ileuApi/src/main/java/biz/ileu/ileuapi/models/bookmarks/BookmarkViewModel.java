package biz.ileu.ileuapi.models.bookmarks;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macair on 09.08.17.
 */

public class BookmarkViewModel {



    @SerializedName("Id")
    private int id;
    @SerializedName("CategoryId")
    private int categoryId;
    @SerializedName("AddedTime")
    private String addedTime;
    @SerializedName("IleuProcessId")
    private int ileuProcessId;
    @SerializedName("UserProfileId")
    private int userProfileId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(String addedTime) {
        this.addedTime = addedTime;
    }

    public int getIleuProcessId() {
        return ileuProcessId;
    }

    public void setIleuProcessId(int ileuProcessId) {
        this.ileuProcessId = ileuProcessId;
    }

    public int getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(int userProfileId) {
        this.userProfileId = userProfileId;
    }
}
