package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
public class CreateRelatedProcessViewModel {
    @SerializedName("Title")
    private String title;

    @SerializedName("RelatedProcessStartDateTime")
    private String relatedProcessStartDateTime;

    @SerializedName("RelatedProcessEndDateTime")
    private String relatedProcessEndDateTime;

    @SerializedName("RelatedProcessEventStartDate")
    private String relatedProcessEventStartDate;

    @SerializedName("RelatedProcessEventEndDate")
    private String relatedProcessEventEndDate;

    @SerializedName("ExecutingUser")
    private int executingUser;

    @SerializedName("ResponsibleUser")
    private int responsibleUser;

    @SerializedName("Description")
    private String description;

    @SerializedName("ProcessNameId")
    private int processNameId;

    @SerializedName("Participants")
    private int[] participants;

    @SerializedName("RelatedProcesses")
    private int[] relatedProcesses;

    @SerializedName("DynamicTargetProcessNames")
    private int[] dynamicTargetProcessNames;

    @SerializedName("ProcessTypeId")
    private int processTypeId;

    @SerializedName("ParentProcessId")
    private int parentProcessId;

    @SerializedName("ColumnsValues")
    private List<CreateProcessColumnViewModel> columnsValues;

    @SerializedName("DefaultResponsibleUser")
    private UsersViewModel defaultResponsibleUser;

    @SerializedName("FileUploads")
    private int[] fileUploads;

    @SerializedName("Guid")
    private String guid;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelatedProcessStartDateTime() {
        return relatedProcessStartDateTime;
    }

    public void setRelatedProcessStartDateTime(String relatedProcessStartDateTime) {
        this.relatedProcessStartDateTime = relatedProcessStartDateTime;
    }

    public String getRelatedProcessEndDateTime() {
        return relatedProcessEndDateTime;
    }

    public void setRelatedProcessEndDateTime(String relatedProcessEndDateTime) {
        this.relatedProcessEndDateTime = relatedProcessEndDateTime;
    }

    public String getRelatedProcessEventStartDate() {
        return relatedProcessEventStartDate;
    }

    public void setRelatedProcessEventStartDate(String relatedProcessEventStartDate) {
        this.relatedProcessEventStartDate = relatedProcessEventStartDate;
    }

    public String getRelatedProcessEventEndDate() {
        return relatedProcessEventEndDate;
    }

    public void setRelatedProcessEventEndDate(String relatedProcessEventEndDate) {
        this.relatedProcessEventEndDate = relatedProcessEventEndDate;
    }

    public int getExecutingUser() {
        return executingUser;
    }

    public void setExecutingUser(int executingUser) {
        this.executingUser = executingUser;
    }

    public int getResponsibleUser() {
        return responsibleUser;
    }

    public void setResponsibleUser(int responsibleUser) {
        this.responsibleUser = responsibleUser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProcessNameId() {
        return processNameId;
    }

    public void setProcessNameId(int processNameId) {
        this.processNameId = processNameId;
    }

    public int[] getParticipants() {
        return participants;
    }

    public void setParticipants(int[] participants) {
        this.participants = participants;
    }

    public int[] getRelatedProcesses() {
        return relatedProcesses;
    }

    public void setRelatedProcesses(int[] relatedProcesses) {
        this.relatedProcesses = relatedProcesses;
    }

    public int[] getDynamicTargetProcessNames() {
        return dynamicTargetProcessNames;
    }

    public void setDynamicTargetProcessNames(int[] dynamicTargetProcessNames) {
        this.dynamicTargetProcessNames = dynamicTargetProcessNames;
    }

    public int getProcessTypeId() {
        return processTypeId;
    }

    public void setProcessTypeId(int processTypeId) {
        this.processTypeId = processTypeId;
    }

    public int getParentProcessId() {
        return parentProcessId;
    }

    public void setParentProcessId(int parentProcessId) {
        this.parentProcessId = parentProcessId;
    }

    public List<CreateProcessColumnViewModel> getColumnsValues() {
        return columnsValues;
    }

    public void setColumnsValues(List<CreateProcessColumnViewModel> columnsValues) {
        this.columnsValues = columnsValues;
    }

    public UsersViewModel getDefaultResponsibleUser() {
        return defaultResponsibleUser;
    }

    public void setDefaultResponsibleUser(UsersViewModel defaultResponsibleUser) {
        this.defaultResponsibleUser = defaultResponsibleUser;
    }

    public int[] getFileUploads() {
        return fileUploads;
    }

    public void setFileUploads(int[] fileUploads) {
        this.fileUploads = fileUploads;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }
}
