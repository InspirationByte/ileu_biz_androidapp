package biz.ileu.ileuapi.models.tables;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macair on 15.08.17.
 */

public class ProcessColumnValueDTO {
    @SerializedName("ColumnId")
    private int columnId;
    @SerializedName("Value")
    private String value;
    @SerializedName("AddedById")
    private int addedById;
    @SerializedName("AddedBy")
    private String addedBy;

    public int getColumnId() {
        return columnId;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getAddedById() {
        return addedById;
    }

    public void setAddedById(int addedById) {
        this.addedById = addedById;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }
}
