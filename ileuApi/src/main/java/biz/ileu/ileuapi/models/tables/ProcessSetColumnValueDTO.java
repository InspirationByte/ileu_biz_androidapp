package biz.ileu.ileuapi.models.tables;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macair on 15.08.17.
 */

public class ProcessSetColumnValueDTO {
    @SerializedName("ColumnId")
    private int columnId;
    @SerializedName("Value")
    private String value;

    public int getColumnId() {
        return columnId;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
