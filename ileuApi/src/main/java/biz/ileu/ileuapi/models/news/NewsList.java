package biz.ileu.ileuapi.models.news;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
public class NewsList {
    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<NewsEntryViewModel> newsEntryViewModelList;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<NewsEntryViewModel> getNewsEntryViewModelList() {
        return newsEntryViewModelList;
    }

    public void setNewsEntryViewModelList(List<NewsEntryViewModel> newsEntryViewModelList) {
        this.newsEntryViewModelList = newsEntryViewModelList;
    }
}
