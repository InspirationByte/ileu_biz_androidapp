package biz.ileu.ileuapi.models.chat;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 24.02.2017.
 */
public class CreateChatViewModel {

    /**
     * CompanionIds : [1,2]
     * ChatGuid : 2b5bc702-c072-4089-af2a-4fdcde955bbc
     * ChatId : 2
     */

    @SerializedName("ChatGuid")
    private String chatGuid;
    @SerializedName("ChatId")
    private int chatId;
    @SerializedName("CompanionIds")
    private int[] companionIds;

    public String getChatGuid() {
        return chatGuid;
    }

    public void setChatGuid(String chatGuid) {
        this.chatGuid = chatGuid;
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    public int[] getCompanionIds() {
        return companionIds;
    }

    public void setCompanionIds(int[] companionIds) {
        this.companionIds = companionIds;
    }
}
