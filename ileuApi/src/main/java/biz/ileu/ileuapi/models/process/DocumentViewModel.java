package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
public class DocumentViewModel {

    /**
     * DocumentText : sample string 1
     * FromName : sample string 2
     * ToName : sample string 3
     * AgreedBy : [{"ProfileId":1,"ProfileName":"sample string 2","SpecId":3,"SpecName":"sample string 4","IsFullyAgreed":true},{"ProfileId":1,"ProfileName":"sample string 2","SpecId":3,"SpecName":"sample string 4","IsFullyAgreed":true}]
     * SpecsToAgree : [{"Id":1,"Title":"sample string 2"},{"Id":1,"Title":"sample string 2"}]
     * IsFullyAgreed : true
     * IsCanAgree : true
     * When : 2017-02-23T16:34:12.4149874+06:00
     * WhoseId : 7
     * WhoseName : sample string 8
     */

    @SerializedName("DocumentText")
    private String documentText;
    @SerializedName("FromName")
    private String fromName;
    @SerializedName("ToName")
    private String toName;
    @SerializedName("IsFullyAgreed")
    private boolean fullyAgreed;
    @SerializedName("IsCanAgree")
    private boolean canAgree;
    @SerializedName("When")
    private String when;
    @SerializedName("WhoseId")
    private int whoseId;
    @SerializedName("WhoseName")
    private String whoseName;
    @SerializedName("AgreedBy")
    private List<DocumentAgreedByViewModel> agreedBy;
    @SerializedName("SpecsToAgree")
    private List<EmployeeSpecViewModel> specsToAgree;

    public String getDocumentText() {
        return documentText;
    }

    public void setDocumentText(String documentText) {
        this.documentText = documentText;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public boolean isFullyAgreed() {
        return fullyAgreed;
    }

    public void setFullyAgreed(boolean fullyAgreed) {
        this.fullyAgreed = fullyAgreed;
    }

    public boolean isCanAgree() {
        return canAgree;
    }

    public void setCanAgree(boolean canAgree) {
        this.canAgree = canAgree;
    }

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }

    public int getWhoseId() {
        return whoseId;
    }

    public void setWhoseId(int whoseId) {
        this.whoseId = whoseId;
    }

    public String getWhoseName() {
        return whoseName;
    }

    public void setWhoseName(String whoseName) {
        this.whoseName = whoseName;
    }

    public List<DocumentAgreedByViewModel> getAgreedBy() {
        return agreedBy;
    }

    public void setAgreedBy(List<DocumentAgreedByViewModel> agreedBy) {
        this.agreedBy = agreedBy;
    }

    public List<EmployeeSpecViewModel> getSpecsToAgree() {
        return specsToAgree;
    }

    public void setSpecsToAgree(List<EmployeeSpecViewModel> specsToAgree) {
        this.specsToAgree = specsToAgree;
    }


}
