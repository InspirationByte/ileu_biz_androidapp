package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Daniq on 23.02.2017.
 */
@Parcel
public class TargetProcessViewModel {
    @SerializedName("Id")
    private int id;

    @SerializedName("ProcessName")
    private String processName;

    @SerializedName("IsDynamic")
    private boolean dynamic;

    @SerializedName("ProcessStatus")
    private int processStatus;

    @SerializedName("ProcessTypeId")
    private int processTypeId;

    @SerializedName("HasAdditionalTiming")
    private boolean hasAdditionalTiming;

    @SerializedName("ParentProcessId")
    private int parentProcessId;

    @SerializedName("StartedProccessId")
    private int startedProccessId;

    @SerializedName("StatusName")
    private String statusName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public boolean isDynamic() {
        return dynamic;
    }

    public void setDynamic(boolean dynamic) {
        this.dynamic = dynamic;
    }

    public int getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(int processStatus) {
        this.processStatus = processStatus;
    }

    public int getProcessTypeId() {
        return processTypeId;
    }

    public void setProcessTypeId(int processTypeId) {
        this.processTypeId = processTypeId;
    }

    public boolean isHasAdditionalTiming() {
        return hasAdditionalTiming;
    }

    public void setHasAdditionalTiming(boolean hasAdditionalTiming) {
        this.hasAdditionalTiming = hasAdditionalTiming;
    }

    public int getParentProcessId() {
        return parentProcessId;
    }

    public void setParentProcessId(int parentProcessId) {
        this.parentProcessId = parentProcessId;
    }

    public int getStartedProccessId() {
        return startedProccessId;
    }

    public void setStartedProccessId(int startedProccessId) {
        this.startedProccessId = startedProccessId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
