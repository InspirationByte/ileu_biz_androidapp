package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Daniq on 23.02.2017.
 */
@Parcel
public class ProcessNameColumnViewModel   {
    @SerializedName("Id")
    private int id;

    @SerializedName("Name")
    private String name;

    @SerializedName("ProcessNameId")
    private int processNameId;

    @SerializedName("OrderPos")
    private int orderPos;

    @SerializedName("SpecId")
    private int specId;

    @SerializedName("SpecTitle")
    private String specTitle;

    @SerializedName("Required")
    private boolean required;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProcessNameId() {
        return processNameId;
    }

    public void setProcessNameId(int processNameId) {
        this.processNameId = processNameId;
    }

    public int getOrderPos() {
        return orderPos;
    }

    public void setOrderPos(int orderPos) {
        this.orderPos = orderPos;
    }

    public int getSpecId() {
        return specId;
    }

    public void setSpecId(int specId) {
        this.specId = specId;
    }

    public String getSpecTitle() {
        return specTitle;
    }

    public void setSpecTitle(String specTitle) {
        this.specTitle = specTitle;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

}
