package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
public class PostCommentViewModel {

    /**
     * ProcessId : 1
     * Text : sample string 2
     * ReplyTo : 3
     * ReplyBefore : 2017-02-23T15:26:35.0629789+06:00
     * Users : [1,2]
     * NotifyAllIfNoUsers : true
     * IsPotentialSolution : true
     * IsOfftopicOrOther : true
     */

    @SerializedName("ProcessId")
    private int processId;
    @SerializedName("Text")
    private String text;
    @SerializedName("ReplyTo")
    private Integer replyTo;
    @SerializedName("ReplyBefore")
    private String replyBefore;
    @SerializedName("NotifyAllIfNoUsers")
    private boolean notifyAllIfNoUsers;
    @SerializedName("IsPotentialSolution")
    private boolean potentialSolution;
    @SerializedName("IsOfftopicOrOther")
    private boolean offtopicOrOther;
    @SerializedName("Users")
    private Integer[] users;
    @SerializedName("AudioUploadId")
    private int audioUploadId;

    public int getAudioUploadId() {
        return audioUploadId;
    }

    public void setAudioUploadId(int audioUploadId) {
        this.audioUploadId = audioUploadId;
    }

    public int getProcessId() {
        return processId;
    }

    public void setProcessId(int processId) {
        this.processId = processId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(Integer replyTo) {
        this.replyTo = replyTo;
    }

    public String getReplyBefore() {
        return replyBefore;
    }

    public void setReplyBefore(String replyBefore) {
        this.replyBefore = replyBefore;
    }

    public boolean isNotifyAllIfNoUsers() {
        return notifyAllIfNoUsers;
    }

    public void setNotifyAllIfNoUsers(boolean notifyAllIfNoUsers) {
        this.notifyAllIfNoUsers = notifyAllIfNoUsers;
    }

    public boolean isPotentialSolution() {
        return potentialSolution;
    }

    public void setPotentialSolution(boolean potentialSolution) {
        this.potentialSolution = potentialSolution;
    }

    public boolean isOfftopicOrOther() {
        return offtopicOrOther;
    }

    public void setOfftopicOrOther(boolean offtopicOrOther) {
        this.offtopicOrOther = offtopicOrOther;
    }

    public Integer[] getUsers() {
        return users;
    }

    public void setUsers(Integer[] users) {
        this.users = users;
    }
}
