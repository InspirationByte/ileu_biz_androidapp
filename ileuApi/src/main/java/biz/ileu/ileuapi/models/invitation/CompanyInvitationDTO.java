package biz.ileu.ileuapi.models.invitation;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macair on 22.08.17.
 */

public class CompanyInvitationDTO {
    @SerializedName("Destinations")
    private List<InvitationEntryDTO> destinations;

    public CompanyInvitationDTO() {
        destinations = new ArrayList<>();
    }

    public List<InvitationEntryDTO> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<InvitationEntryDTO> destinations) {
        this.destinations = destinations;
    }

    @Override
    public String toString() {
        String str = "CompanyInvitationDTO: ";
        for (InvitationEntryDTO temp : destinations) {
            str += temp.toString();
        }
        return str;
    }

}
