package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class ProcessFileViewModel {

    /**
     * Id : 1
     * FileName : sample string 2
     * FileLink : sample string 3
     * DisplayName : sample string 4
     * UploadDate : 2017-02-23T16:43:36.7018046+06:00
     * ProfilePicture : sample string 6
     * VersionCount : 7
     * SizeInBytes : 8
     * UploadId : 9
     * FileMime : sample string 10
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("FileName")
    private String fileName;
    @SerializedName("FileLink")
    private String fileLink;
    @SerializedName("DisplayName")
    private String displayName;
    @SerializedName("UploadDate")
    private String uploadDate;
    @SerializedName("ProfilePicture")
    private String profilePicture;
    @SerializedName("VersionCount")
    private int versionCount;
    @SerializedName("SizeInBytes")
    private int sizeInBytes;
    @SerializedName("UploadId")
    private int uploadId;
    @SerializedName("FileMime")
    private String fileMime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileLink() {
        return fileLink;
    }

    public void setFileLink(String fileLink) {
        this.fileLink = fileLink;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public int getVersionCount() {
        return versionCount;
    }

    public void setVersionCount(int versionCount) {
        this.versionCount = versionCount;
    }

    public int getSizeInBytes() {
        return sizeInBytes;
    }

    public void setSizeInBytes(int sizeInBytes) {
        this.sizeInBytes = sizeInBytes;
    }

    public int getUploadId() {
        return uploadId;
    }

    public void setUploadId(int uploadId) {
        this.uploadId = uploadId;
    }

    public String getFileMime() {
        return fileMime;
    }

    public void setFileMime(String fileMime) {
        this.fileMime = fileMime;
    }
}
