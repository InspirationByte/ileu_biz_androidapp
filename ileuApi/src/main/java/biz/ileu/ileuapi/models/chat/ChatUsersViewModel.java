package biz.ileu.ileuapi.models.chat;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Daniq on 24.02.2017.
 */
@Parcel
public class ChatUsersViewModel {

    /**
     * UserId : 1
     * DisplayName : sample string 2
     * ProfilePicture : sample string 3
     * IsOnline : true
     * StatusText : sample string 5
     * ShouldDisplayStatus : true
     */

    @SerializedName("UserId")
    private int userId;
    @SerializedName("DisplayName")
    private String displayName;
    @SerializedName("ProfilePicture")
    private String profilePicture;
    @SerializedName("IsOnline")
    private boolean online;
    @SerializedName("StatusText")
    private String statusText;
    @SerializedName("ShouldDisplayStatus")
    private boolean shouldDisplayStatus;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public boolean isShouldDisplayStatus() {
        return shouldDisplayStatus;
    }

    public void setShouldDisplayStatus(boolean shouldDisplayStatus) {
        this.shouldDisplayStatus = shouldDisplayStatus;
    }
}
