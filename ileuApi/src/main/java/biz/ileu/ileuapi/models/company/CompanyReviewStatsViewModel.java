package biz.ileu.ileuapi.models.company;

/**
 * Created by Daniq on 24.02.2017.
 */
public class CompanyReviewStatsViewModel {

    /**
     * joinRequests : 1
     * usersWithoutPosition : 2
     * usersWithoutSpecs : 3
     */

    private int joinRequests;
    private int usersWithoutPosition;
    private int usersWithoutSpecs;

    public int getJoinRequests() {
        return joinRequests;
    }

    public void setJoinRequests(int joinRequests) {
        this.joinRequests = joinRequests;
    }

    public int getUsersWithoutPosition() {
        return usersWithoutPosition;
    }

    public void setUsersWithoutPosition(int usersWithoutPosition) {
        this.usersWithoutPosition = usersWithoutPosition;
    }

    public int getUsersWithoutSpecs() {
        return usersWithoutSpecs;
    }

    public void setUsersWithoutSpecs(int usersWithoutSpecs) {
        this.usersWithoutSpecs = usersWithoutSpecs;
    }
}
