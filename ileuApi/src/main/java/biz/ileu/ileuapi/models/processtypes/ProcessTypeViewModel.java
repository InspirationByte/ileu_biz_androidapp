package biz.ileu.ileuapi.models.processtypes;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class ProcessTypeViewModel {

    /**
     * Id : 1
     * Title : sample string 2
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("Title")
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
