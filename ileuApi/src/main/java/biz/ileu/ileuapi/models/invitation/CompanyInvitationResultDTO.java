package biz.ileu.ileuapi.models.invitation;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macair on 22.08.17.
 */

public class CompanyInvitationResultDTO {
    @SerializedName("Results")
    private List<InvitationResultDTO> result;

    public CompanyInvitationResultDTO() {
        result = new ArrayList<>();
    }

    public List<InvitationResultDTO> getResult() {
        return result;
    }

    public void setResult(List<InvitationResultDTO> destinations) {
        this.result = destinations;
    }

    @Override
    public String toString() {
        String str = "CompanyInvitationResultDTO: ";
        for (InvitationResultDTO temp : result) {
            str += temp.toString();
        }
        return str;
    }
}
