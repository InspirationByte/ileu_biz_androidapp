package biz.ileu.ileuapi.models.chat;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniq on 24.02.2017.
 */
public class ChatList {
    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<ChatsViewModel> chatsViewModelList;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ChatsViewModel> getChatsViewModelList() {
        return chatsViewModelList;
    }

    public void setChatsViewModelList(List<ChatsViewModel> chatsViewModelList) {
        this.chatsViewModelList = chatsViewModelList;
    }
}
