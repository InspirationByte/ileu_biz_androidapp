package biz.ileu.ileuapi.models.company;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class CompanyViewModel {

    /**
     * Id : 1
     * Name : sample string 2
     * Logo : sample string 3
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("Name")
    private String name;
    @SerializedName("Logo")
    private String logo;
    @SerializedName("HasSentJoinRequest")
    private boolean sentJoinRequest;

    @SerializedName("isOwn")
    private boolean own;

    public boolean isOwn() {
        return own;
    }

    public void setOwn(boolean own) {
        this.own = own;
    }

    public boolean hasSentJoinRequest() {
        return sentJoinRequest;
    }

    public void setSentJoinRequest(boolean sentJoinRequest) {
        this.sentJoinRequest = sentJoinRequest;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
