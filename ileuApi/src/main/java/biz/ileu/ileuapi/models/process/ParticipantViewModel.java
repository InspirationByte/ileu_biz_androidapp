package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Daniq on 23.02.2017.
 */
@Parcel
public class ParticipantViewModel {
    /**
     * Id : 1
     * DisplayName : sample string 2
     * ProfilePicture : sample string 3
     * IsExecuting : true
     * IsOverDue : 5
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("DisplayName")
    private String displayName;
    @SerializedName("ProfilePicture")
    private String profilePicture;
    @SerializedName("IsExecuting")
    private boolean isExecuting;
    @SerializedName("IsOverDue")
    private int isOverDue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public boolean isIsExecuting() {
        return isExecuting;
    }

    public void setIsExecuting(boolean isExecuting) {
        this.isExecuting = isExecuting;
    }

    public int getIsOverDue() {
        return isOverDue;
    }

    public void setIsOverDue(int isOverDue) {
        this.isOverDue = isOverDue;
    }


    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ParticipantViewModel)
            return this.getId() == ((ParticipantViewModel) obj).getId();
        return false;
    }
}
