package biz.ileu.ileuapi.models.bookmarks;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Daniq on 26.04.2017.
 */
@Parcel
public class BookmarksCategoriesList {

    @SerializedName("Total")
    private int total;
    @SerializedName("Data")
    private List<BookmarkCategoryViewModel> bookmarkCategoryViewModelList;


    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<BookmarkCategoryViewModel> getBookmarkCategoryViewModelList() {
        return bookmarkCategoryViewModelList;
    }

    public void setBookmarkCategoryViewModelList(List<BookmarkCategoryViewModel> bookmarkCategoryViewModelList) {
        this.bookmarkCategoryViewModelList = bookmarkCategoryViewModelList;
    }
}
