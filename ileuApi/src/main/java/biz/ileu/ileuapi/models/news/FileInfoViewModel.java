package biz.ileu.ileuapi.models.news;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Daniq on 23.02.2017.
 */
@Parcel
public class FileInfoViewModel {

    /**
     * Id : 1
     * FileName : sample string 2
     * FileLink : sample string 3
     * UploadDate : 2017-02-23T23:26:44.6880548+06:00
     * References : 5
     * IsUsedHere : true
     * SizeInBytes : 7
     * UploadId : 8
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("FileName")
    private String fileName;
    @SerializedName("FileLink")
    private String fileLink;
    @SerializedName("UploadDate")
    private String uploadDate;
    @SerializedName("References")
    private int references;
    @SerializedName("IsUsedHere")
    private boolean usedHere;
    @SerializedName("SizeInBytes")
    private int sizeInBytes;
    @SerializedName("UploadId")
    private int uploadId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileLink() {
        return fileLink;
    }

    public void setFileLink(String fileLink) {
        this.fileLink = fileLink;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public int getReferences() {
        return references;
    }

    public void setReferences(int references) {
        this.references = references;
    }

    public boolean isUsedHere() {
        return usedHere;
    }

    public void setUsedHere(boolean usedHere) {
        this.usedHere = usedHere;
    }

    public int getSizeInBytes() {
        return sizeInBytes;
    }

    public void setSizeInBytes(int sizeInBytes) {
        this.sizeInBytes = sizeInBytes;
    }

    public int getUploadId() {
        return uploadId;
    }

    public void setUploadId(int uploadId) {
        this.uploadId = uploadId;
    }
}
