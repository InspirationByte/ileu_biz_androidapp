package biz.ileu.ileuapi.models.tables;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macair on 15.08.17.
 */

public class AggregateResult {
    @SerializedName("Value")
    private Object value;
    @SerializedName("Member")
    private String member;
    @SerializedName("FormattedValue")
    private Object formattedValue;
    @SerializedName("ItemCount")
    private int itemCount;
    @SerializedName("Caption")
    private String Caption;
    @SerializedName("FunctionName")
    private String functionName;
    @SerializedName("AggregateMethodName")
    private String aggregateMethodName;

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public Object getFormattedValue() {
        return formattedValue;
    }

    public void setFormattedValue(Object formattedValue) {
        this.formattedValue = formattedValue;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public String getCaption() {
        return Caption;
    }

    public void setCaption(String caption) {
        Caption = caption;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getAggregateMethodName() {
        return aggregateMethodName;
    }

    public void setAggregateMethodName(String aggregateMethodName) {
        this.aggregateMethodName = aggregateMethodName;
    }
}
