package biz.ileu.ileuapi.models.usercontext;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 22.02.2017.
 */
public class ContextInfoDTO {

    /**
     * CompanyId : 1
     * CompanyName : sample string 2
     * SpecId : 3
     * PositionId : 4
     * IsCompanyOwner : true
     * IsCompanyEmployee : true
     * CanStartProcesses : true
     * CanCreateReports : true
     * CanUseChat : true
     * AllowedToAddOrEditEmployees : true
     */

    @SerializedName("CompanyId")
    private int companyId;
    @SerializedName("CompanyName")
    private String companyName;
    @SerializedName("SpecId")
    private int specId;
    @SerializedName("PositionId")
    private int positionId;
    @SerializedName("IsCompanyOwner")
    private boolean companyOwner;
    @SerializedName("IsCompanyEmployee")
    private boolean companyEmployee;
    @SerializedName("CanStartProcesses")
    private boolean canStartProcesses;
    @SerializedName("CanCreateReports")
    private boolean canCreateReports;
    @SerializedName("CanUseChat")
    private boolean canUseChat;
    @SerializedName("AllowedToAddOrEditEmployees")
    private boolean allowedToAddOrEditEmployees;

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getSpecId() {
        return specId;
    }

    public void setSpecId(int specId) {
        this.specId = specId;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public boolean isCompanyOwner() {
        return companyOwner;
    }

    public void setCompanyOwner(boolean companyOwner) {
        this.companyOwner = companyOwner;
    }

    public boolean isCompanyEmployee() {
        return companyEmployee;
    }

    public void setCompanyEmployee(boolean companyEmployee) {
        this.companyEmployee = companyEmployee;
    }

    public boolean isCanStartProcesses() {
        return canStartProcesses;
    }

    public void setCanStartProcesses(boolean canStartProcesses) {
        this.canStartProcesses = canStartProcesses;
    }

    public boolean isCanCreateReports() {
        return canCreateReports;
    }

    public void setCanCreateReports(boolean canCreateReports) {
        this.canCreateReports = canCreateReports;
    }

    public boolean isCanUseChat() {
        return canUseChat;
    }

    public void setCanUseChat(boolean canUseChat) {
        this.canUseChat = canUseChat;
    }

    public boolean isAllowedToAddOrEditEmployees() {
        return allowedToAddOrEditEmployees;
    }

    public void setAllowedToAddOrEditEmployees(boolean allowedToAddOrEditEmployees) {
        this.allowedToAddOrEditEmployees = allowedToAddOrEditEmployees;
    }
}
