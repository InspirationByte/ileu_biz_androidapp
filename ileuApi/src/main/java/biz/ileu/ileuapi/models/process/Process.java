package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Daniq on 23.02.2017.
 */
@Parcel
public class Process {

    @SerializedName("Id")
    private int id;
    @SerializedName("CompanyId")
    private int companyId;
    @SerializedName("Title")
    private String title;
    @SerializedName("DescriptionText")
    private String descriptionText;
    @SerializedName("Description")
    private String description;
    @SerializedName("IdProcessName")
    private String idProcessName;
    @SerializedName("Dins")
    private String dins;
    @SerializedName("LastUpdateTime")
    private String lastUpdateTime;
    @SerializedName("StartDateTime")
    private String startDateTime;
    @SerializedName("StartDate")
    private String startDate;
    @SerializedName("EndDateTime")
    private String endDateTime;
    @SerializedName("HasAdditionalTiming")
    private boolean hasAdditionalTiming;
    @SerializedName("IsRegistrational")
    private boolean registrational;
    @SerializedName("HasDocument")
    private boolean hasDocument;
    @SerializedName("DocSpecsToAgree")
    private List<EmployeeSpecViewModel> docSpecsToAgree;
    @SerializedName("Columns")
    private List<ProcessNameColumnViewModel> columns;
    @SerializedName("EventStartDate")
    private String eventStartDate;
    @SerializedName("EventEndDate")
    private String eventEndDate;
    @SerializedName("ProcessName")
    private String processName;
    @SerializedName("ProcessNameId")
    private int processNameId;
    @SerializedName("ProcessTypeId")
    private int processTypeId;
    @SerializedName("ProcessType")
    private String processType;
    @SerializedName("Status")
    private int status;
    @SerializedName("StatusName")
    private String statusName;
    @SerializedName("CompletionReasonText")
    private String completionReasonText;
    @SerializedName("ApprovedByQC")
    private boolean approvedByQC;
    @SerializedName("Creator")
    private ParticipantViewModel creator;
    @SerializedName("Executor")
    private ParticipantViewModel executor;
    @SerializedName("Responsible")
    private ParticipantViewModel responsible;
    @SerializedName("CommentsCount")
    private int commentsCount;
    @SerializedName("SolutionsCount")
    private int solutionsCount;
    @SerializedName("UnreadCommentsCount")
    private int unreadCommentsCount;
    @SerializedName("HasTable")
    private boolean hasTable;
    @SerializedName("TableRowsCount")
    private int tableRowsCount;
    @SerializedName("DocumentState")
    private int documentState;
    @SerializedName("DocumentStateText")
    private String documentStateText;
    @SerializedName("ParticipantsCount")
    private int participantsCount;
    @SerializedName("FilesCount")
    private int filesCount;
    @SerializedName("TargetProcessesCount")
    private int targetProcessesCount;
    @SerializedName("RelatedProcessesCount")
    private int relatedProcessesCount;
    @SerializedName("IsOverDue")
    private int overDue;
    @SerializedName("IsCurrent")
    private boolean current;
    @SerializedName("HasBookmark")
    private boolean hasBookmark;
    @SerializedName("Hidden")
    private boolean hidden;
    @SerializedName("NoteText")
    private String noteText;
    @SerializedName("Participants")
    private List<ParticipantViewModel> participants;
    @SerializedName("TargetProcessNames")
    private List<TargetProcessViewModel> targetProcessNames;
    @SerializedName("DynamicTargetProcessNames")
    private List<TargetProcessViewModel> dynamicTargetProcessNames;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescriptionText() {
        return descriptionText;
    }

    public void setDescriptionText(String descriptionText) {
        this.descriptionText = descriptionText;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdProcessName() {
        return idProcessName;
    }

    public void setIdProcessName(String idProcessName) {
        this.idProcessName = idProcessName;
    }

    public String getDins() {
        return dins;
    }

    public void setDins(String dins) {
        this.dins = dins;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public boolean isHasAdditionalTiming() {
        return hasAdditionalTiming;
    }

    public void setHasAdditionalTiming(boolean hasAdditionalTiming) {
        this.hasAdditionalTiming = hasAdditionalTiming;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public int getProcessNameId() {
        return processNameId;
    }

    public void setProcessNameId(int processNameId) {
        this.processNameId = processNameId;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public int getProcessTypeId() {
        return processTypeId;
    }

    public void setProcessTypeId(int processTypeId) {
        this.processTypeId = processTypeId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getCompletionReasonText() {
        return completionReasonText;
    }

    public void setCompletionReasonText(String completionReasonText) {
        this.completionReasonText = completionReasonText;
    }

    public boolean isApprovedByQC() {
        return approvedByQC;
    }

    public void setApprovedByQC(boolean approvedByQC) {
        this.approvedByQC = approvedByQC;
    }

    public ParticipantViewModel getCreator() {
        return creator;
    }

    public void setCreator(ParticipantViewModel creator) {
        this.creator = creator;
    }

    public ParticipantViewModel getExecutor() {
        return executor;
    }

    public void setExecutor(ParticipantViewModel executor) {
        this.executor = executor;
    }

    public ParticipantViewModel getResponsible() {
        return responsible;
    }

    public void setResponsible(ParticipantViewModel responsible) {
        this.responsible = responsible;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public int getSolutionsCount() {
        return solutionsCount;
    }

    public void setSolutionsCount(int solutionsCount) {
        this.solutionsCount = solutionsCount;
    }

    public int getUnreadCommentsCount() {
        return unreadCommentsCount;
    }

    public void setUnreadCommentsCount(int unreadCommentsCount) {
        this.unreadCommentsCount = unreadCommentsCount;
    }

    public boolean isHasTable() {
        return hasTable;
    }

    public void setHasTable(boolean hasTable) {
        this.hasTable = hasTable;
    }

    public int getTableRowsCount() {
        return tableRowsCount;
    }

    public void setTableRowsCount(int tableRowsCount) {
        this.tableRowsCount = tableRowsCount;
    }

    public int getDocumentState() {
        return documentState;
    }

    public void setDocumentState(int documentState) {
        this.documentState = documentState;
    }

    public String getDocumentStateText() {
        return documentStateText;
    }

    public void setDocumentStateText(String documentStateText) {
        this.documentStateText = documentStateText;
    }

    public int getParticipantsCount() {
        return participantsCount;
    }

    public int getAllParticipantsNumber() {
        int num = 1;

        if (creator.getId() != executor.getId())
            num++;

        if (responsible.getId() != creator.getId() && responsible.getId() != executor.getId())
            num++;

        num += participantsCount;

        return num;
    }

    public void setParticipantsCount(int participantsCount) {
        this.participantsCount = participantsCount;
    }

    public int getFilesCount() {
        return filesCount;
    }

    public void setFilesCount(int filesCount) {
        this.filesCount = filesCount;
    }

    public int getTargetProcessesCount() {
        return targetProcessesCount;
    }

    public void setTargetProcessesCount(int targetProcessesCount) {
        this.targetProcessesCount = targetProcessesCount;
    }

    public int getRelatedProcessesCount() {
        return relatedProcessesCount;
    }

    public void setRelatedProcessesCount(int relatedProcessesCount) {
        this.relatedProcessesCount = relatedProcessesCount;
    }

    public int getOverDue() {
        return overDue;
    }

    public void setOverDue(int overDue) {
        this.overDue = overDue;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

    public boolean isHasBookmark() {
        return hasBookmark;
    }

    public void setHasBookmark(boolean hasBookmark) {
        this.hasBookmark = hasBookmark;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    public List<ParticipantViewModel> getParticipants() {
        return participants;
    }

    public void setParticipants(List<ParticipantViewModel> participants) {
        this.participants = participants;
    }

    public boolean isRegistrational() {
        return registrational;
    }

    public void setRegistrational(boolean registrational) {
        this.registrational = registrational;
    }

    public boolean isHasDocument() {
        return hasDocument;
    }

    public void setHasDocument(boolean hasDocument) {
        this.hasDocument = hasDocument;
    }

    public List<EmployeeSpecViewModel> getDocSpecsToAgree() {
        return docSpecsToAgree;
    }

    public void setDocSpecsToAgree(List<EmployeeSpecViewModel> docSpecsToAgree) {
        this.docSpecsToAgree = docSpecsToAgree;
    }

    public List<ProcessNameColumnViewModel> getColumns() {
        return columns;
    }

    public void setColumns(List<ProcessNameColumnViewModel> columns) {
        this.columns = columns;
    }

    public List<TargetProcessViewModel> getTargetProcessNames() {
        return targetProcessNames;
    }

    public void setTargetProcessNames(List<TargetProcessViewModel> targetProcessNames) {
        this.targetProcessNames = targetProcessNames;
    }

    public List<TargetProcessViewModel> getDynamicTargetProcessNames() {
        return dynamicTargetProcessNames;
    }

    public void setDynamicTargetProcessNames(List<TargetProcessViewModel> dynamicTargetProcessNames) {
        this.dynamicTargetProcessNames = dynamicTargetProcessNames;
    }
}
