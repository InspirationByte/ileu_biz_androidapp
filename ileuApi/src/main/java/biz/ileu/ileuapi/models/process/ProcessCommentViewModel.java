package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class ProcessCommentViewModel {

    /**
     * Id : 1
     * Text : sample string 2
     * MsgTime : 2017-02-23T14:57:32.9686117+06:00
     * DisplayName : sample string 4
     * ProfilePicture : sample string 5
     * HasReplies : true
     * ReadStatus : 7
     * ReplyTo : 8
     * ReplyFor : sample string 9
     * EmployeeId : 10
     * Type : 0
     */

    @SerializedName("Id")
    private int id;
    @SerializedName("CompanyId")
    private int companyId;
    @SerializedName("Text")
    private String text;
    @SerializedName("ProcessId")
    private int processId;
    @SerializedName("MsgTime")
    private String msgTime;
    @SerializedName("DisplayName")
    private String displayName;
    @SerializedName("ProfilePicture")
    private String profilePicture;
    @SerializedName("HasReplies")
    private boolean hasReplies;
    @SerializedName("ReadStatus")
    private int readStatus;
    @SerializedName("ReplyTo")
    private int replyTo;
    @SerializedName("ReplyFor")
    private String replyFor;
    @SerializedName("EmployeeId")
    private int employeeId;
    @SerializedName("Type")
    private int type;

    @SerializedName("HasAudioMessage")
    private boolean hasAudioMessage;

    public boolean isHasAudioMessage() {
        return hasAudioMessage;
    }

    public int getProcessId() {
        return processId;
    }

    public void setProcessId(int processId) {
        this.processId = processId;
    }

    public void setHasAudioMessage(boolean hasAudioMessage) {
        this.hasAudioMessage = hasAudioMessage;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public boolean isHasReplies() {
        return hasReplies;
    }

    public void setHasReplies(boolean hasReplies) {
        this.hasReplies = hasReplies;
    }

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }

    public Integer getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(int replyTo) {
        this.replyTo = replyTo;
    }

    public String getReplyFor() {
        return replyFor;
    }

    public void setReplyFor(String replyFor) {
        this.replyFor = replyFor;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
