package biz.ileu.ileuapi.models.process;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 23.02.2017.
 */
public class UsersViewModel {
    @SerializedName("Id")
    private int id;

    @SerializedName("DisplayName")
    private String displayName;

    @SerializedName("PositionName")
    private String positionName;

    @SerializedName("Avatar")
    private String avatar;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public EmployeeViewModel getAsEmployeeViewModel() {
        EmployeeViewModel employeeViewModel = new EmployeeViewModel();
        employeeViewModel.setId(id);
        employeeViewModel.setNameSurname(displayName);
        employeeViewModel.setProfilePicture(avatar);
        employeeViewModel.setPosition(positionName);

        return employeeViewModel;
    }
}
