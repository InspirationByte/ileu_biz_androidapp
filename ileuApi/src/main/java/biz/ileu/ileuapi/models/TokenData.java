package biz.ileu.ileuapi.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniq on 22.02.2017.
 */
public class TokenData {


    /**
     * access_token : OUR_SERVER_GENERATED_TOKEN
     * token_type : bearer
     * expires_in : 1209599
     * userName : YOUR_EMAIL_HERE
     * .issued : Wed, 22 Feb 2017 05:17:39 GMT
     * .expires : Wed, 08 Mar 2017 05:17:39 GMT
     */

    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("token_type")
    private String tokenType;
    @SerializedName("expires_in")
    private int expiresIn;
    private String userName;
    @SerializedName(".issued")
    private String issued;
    @SerializedName(".expires")
    private String expires;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIssued() {
        return issued;
    }

    public void setIssued(String issued) {
        this.issued = issued;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TokenData{");
        sb.append("accessToken='").append(accessToken).append('\'');
        sb.append(", tokenType='").append(tokenType).append('\'');
        sb.append(", expiresIn=").append(expiresIn);
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", issued='").append(issued).append('\'');
        sb.append(", expires='").append(expires).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
